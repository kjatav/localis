package com.smart.weservice;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.smart.framework.Constants;
import com.smart.framework.SmartUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SmartWebManager implements Constants {

    private static final int TIMEOUT = 5000;

    public enum REQUEST_METHOD_PARAMS {CONTEXT, REQUEST_METHOD, URL, PARAMS, TAG, RESPONSE_LISTENER}

    private static SmartWebManager mInstance;
    private RequestQueue mRequestQueue;
    private Context mCtx;

    private SmartWebManager(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized SmartWebManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SmartWebManager(context);
        }
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }


    public <T> void addToRequestQueue(final HashMap<REQUEST_METHOD_PARAMS, Object> requestParams, final boolean isShowSnackbar) {
        Log.v("@@@@@WSRequest", ((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)));

        JSONObject parameters = null;

        final Context mContext = (Context) requestParams.get(REQUEST_METHOD_PARAMS.CONTEXT);
        int mRequestMethod = (int) requestParams.get(REQUEST_METHOD_PARAMS.REQUEST_METHOD);
        String url = (String) requestParams.get(REQUEST_METHOD_PARAMS.URL);
        Map<String, String> paramsMap = (Map<String, String>) requestParams.get(REQUEST_METHOD_PARAMS.PARAMS);


        if (mRequestMethod == Request.Method.GET) {
            if (paramsMap != null && paramsMap.size() > 0) {
                url = getUrlWithParams(url, paramsMap);
                Log.v("@@@@@URL", url);
            }
        } else if (mRequestMethod == Request.Method.POST) {
            parameters = new JSONObject(paramsMap);
            Log.v("@@@@@WSParameters", parameters.toString());
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(mRequestMethod, url.toString(), parameters,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.v("@@@@@WSResponse", response.toString());
                            if (SmartUtils.getResponseStatus(response)) {
                                SmartUtils.hideSoftKeyboard(mContext);

                                if (isShowSnackbar && !TextUtils.isEmpty(SmartUtils.validateResponse(response))) {
                             //       SmartUtils.showSnackBar(mContext, SmartUtils.validateResponse(response), Snackbar.LENGTH_LONG);
                                }

                                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(response);
                            } else {
                                SmartUtils.hideSoftKeyboard(mContext);

                                if (!TextUtils.isEmpty(SmartUtils.validateResponse(response))) {
                                    if (isShowSnackbar)
                                        SmartUtils.showSnackBar(mContext, SmartUtils.validateResponse(response), Snackbar.LENGTH_LONG);
                                } else {
                                    if (isShowSnackbar)
                                        SmartUtils.showSnackBar(mContext, "Error while receiving response.", Snackbar.LENGTH_LONG);
                                    Log.v("@@@@@WSResponse", "Response Status =  FAIL");
                                }

                                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError(response);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Log.v("@@@@@WSResponse", volleyError.toString());

                        SmartUtils.hideSoftKeyboard(mContext);

                        if (!TextUtils.isEmpty(VolleyErrorHelper.getMessage(volleyError, mContext))) {
                            if (isShowSnackbar)
                                SmartUtils.showSnackBar(mContext, VolleyErrorHelper.getMessage(volleyError, mContext), Snackbar.LENGTH_LONG);
                        } else {
                            if (isShowSnackbar)
                                SmartUtils.showSnackBar(mContext, "Volley Error while receiving response.", Snackbar.LENGTH_LONG);
                            Log.v("@@@@@WSResponse", "Volley Response Error is EMPTY");
                        }
                        try {
                            ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError(null);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        jsonObjectRequest.setTag(requestParams.get(REQUEST_METHOD_PARAMS.TAG));

        getRequestQueue().add(jsonObjectRequest);
        getRequestQueue().getCache().clear();

    }


    public <T> void addToRequestQueueMultipart(final HashMap<REQUEST_METHOD_PARAMS, Object> requestParams, String filePath, final boolean isShowSnackbar) {
        Log.v("@@@@@WSRequest", ((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)));
        final Context mContext = (Context) requestParams.get(REQUEST_METHOD_PARAMS.CONTEXT);


        MultiPartRequest smMultiPartRequest = new MultiPartRequest(((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)),
                (Map<String, String>) requestParams.get(REQUEST_METHOD_PARAMS.PARAMS), filePath,

                new Response.Listener() {

                    @Override
                    public void onResponse(Object object) {
                        JSONObject response = (JSONObject) object;
                        Log.v("@@@@@WSResponsekkk", response.toString());
                        try {
                            Log.v("@@@@@WSResponse", response.toString());
                            if (SmartUtils.getResponseStatus(response)) {
                                SmartUtils.hideSoftKeyboard(mContext);

                                if (isShowSnackbar && !TextUtils.isEmpty(SmartUtils.validateResponse(response))) {
                                 //   SmartUtils.showSnackBar(mContext, SmartUtils.validateResponse(response), Snackbar.LENGTH_LONG);
                                }

                                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(response);
                            } else {
                                SmartUtils.hideSoftKeyboard(mContext);

                                if (isShowSnackbar && !TextUtils.isEmpty(SmartUtils.validateResponse(response))) {
                                    SmartUtils.showSnackBar(mContext, SmartUtils.validateResponse(response), Snackbar.LENGTH_LONG);
                                } else {
                                    SmartUtils.showSnackBar(mContext, "Error while receiving response.", Snackbar.LENGTH_LONG);
                                    Log.v("@@@@@WSResponse", "Response Status =  FAIL");
                                }

                                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError(response);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Log.v("@@@@@WSResponse", volleyError.toString());

                        SmartUtils.hideSoftKeyboard(mContext);

                        if (isShowSnackbar && !TextUtils.isEmpty(VolleyErrorHelper.getMessage(volleyError, mContext))) {
                            SmartUtils.showSnackBar(mContext, VolleyErrorHelper.getMessage(volleyError, mContext), Snackbar.LENGTH_LONG);
                        } else {
                            SmartUtils.showSnackBar(mContext, "Volley Error while receiving response.", Snackbar.LENGTH_LONG);
                            Log.v("@@@@@WSResponse", "Volley Response Error is EMPTY");
                        }
                        try {
                            ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError(null);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }) {


        };

        smMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        smMultiPartRequest.setTag(requestParams.get(REQUEST_METHOD_PARAMS.TAG));
        getRequestQueue().add(smMultiPartRequest);
        getRequestQueue().getCache().clear();

    }


    public <T> void addToRequestQueueMultipart(final HashMap<REQUEST_METHOD_PARAMS, Object> requestParams, HashMap<String, String> filePath, final boolean isShowSnackbar) {
        Log.v("@@@@@WSRequest", ((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)));


        final Context mContext = (Context) requestParams.get(REQUEST_METHOD_PARAMS.CONTEXT);
        String url = (String) requestParams.get(REQUEST_METHOD_PARAMS.URL);
        Map<String, String> paramsMap = (Map<String, String>) requestParams.get(REQUEST_METHOD_PARAMS.PARAMS);


        MultiPartRequest smMultiPartRequest = new MultiPartRequest(url.toString(), paramsMap, filePath,
                new Response.Listener() {

                    @Override
                    public void onResponse(Object object) {
                        try {
                            JSONObject response = (JSONObject) object;
                           // JSONObject response = webData.getJSONObject("response");
                            Log.v("@@@@@WSResponse", response.toString());
                            if (SmartUtils.getResponseStatus(response)) {
                                SmartUtils.hideSoftKeyboard(mContext);

                                if (isShowSnackbar && !TextUtils.isEmpty(SmartUtils.validateResponse(response))) {
                           //         SmartUtils.showSnackBar(mContext, SmartUtils.validateResponse(response), Snackbar.LENGTH_LONG);
                                }

                                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(response);
                            } else {
                                SmartUtils.hideSoftKeyboard(mContext);

                                if (!TextUtils.isEmpty(SmartUtils.validateResponse(response))) {
                                    if (isShowSnackbar)
                                        SmartUtils.showSnackBar(mContext, SmartUtils.validateResponse(response), Snackbar.LENGTH_LONG);
                                } else {
                                    if (isShowSnackbar)
                                        SmartUtils.showSnackBar(mContext, "Error while receiving response.", Snackbar.LENGTH_LONG);
                                    Log.v("@@@@@WSResponse", "Response Status =  FAIL");
                                }

                                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError(response);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Log.v("@@@@@WSResponse", volleyError.toString());

                        SmartUtils.hideSoftKeyboard(mContext);

                        if (!TextUtils.isEmpty(VolleyErrorHelper.getMessage(volleyError, mContext))) {
                            if (isShowSnackbar)
                                SmartUtils.showSnackBar(mContext, VolleyErrorHelper.getMessage(volleyError, mContext), Snackbar.LENGTH_LONG);
                        } else {
                            if (isShowSnackbar)
                                SmartUtils.showSnackBar(mContext, "Volley Error while receiving response.", Snackbar.LENGTH_LONG);
                            Log.v("@@@@@WSResponse", "Volley Response Error is EMPTY");
                        }
                        try {
                            ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError(null);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        smMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        smMultiPartRequest.setTag(requestParams.get(REQUEST_METHOD_PARAMS.TAG));
        getRequestQueue().add(smMultiPartRequest);

    }




   /* public <T> void addToRequestQueueMultipart(final HashMap<REQUEST_METHOD_PARAMS, Object> requestParams, String filePath, final boolean isShowSnackbar) {
        Log.v("@@@@@WSRequest55", ((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)));
        final Context mContext = (Context) requestParams.get(REQUEST_METHOD_PARAMS.CONTEXT);

        MultiPartRequest smMultiPartRequest = new MultiPartRequest(((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)),
                (Map<String, String>) requestParams.get(REQUEST_METHOD_PARAMS.PARAMS), filePath, new Response.Listener() {

            @Override
            public void onResponse(Object object) {
                JSONObject webData = (JSONObject) object;
                Log.v("@@@@@WSResponsekkk", webData.toString());
                try {
                    //  JSONObject webData = (JSONObject) object;
                    //     JSONObject response = webData.getJSONObject("Results");
                    Log.v("@@@@@WSResponsekkk", webData.toString());


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v("@@@@@WSError99", error.toString());

                SmartUtils.hideSoftKeyboard(mContext);
                String errorMessage = VolleyErrorHelper.getMessage(error, mContext);
                SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError();
            }
        });
        smMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        smMultiPartRequest.setTag(requestParams.get(REQUEST_METHOD_PARAMS.TAG));
        getRequestQueue().add(smMultiPartRequest);
    }
*/
  /*

    public <T> void addToRequestQueueMultipart(final HashMap<REQUEST_METHOD_PARAMS, Object> requestParams, HashMap<String, String> filePath, final boolean isShowSnackbar) {
        Log.v("@@@@@WSRequest", ((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)));
        final Context mContext = (Context) requestParams.get(REQUEST_METHOD_PARAMS.CONTEXT);
        MultiPartRequest smMultiPartRequest = new MultiPartRequest(((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)),

                (Map<String, String>) requestParams.get(REQUEST_METHOD_PARAMS.PARAMS), filePath, new Response.Listener() {
            @Override
            public void onResponse(Object object) {

                JSONObject webData = (JSONObject) object;
                try {
                    //  JSONObject webData = (JSONObject) object;
                    //     JSONObject response = webData.getJSONObject("Results");
                    Log.v("@@@@@WSResponsekkk", webData.toString());

                    Log.v("@@@@@WSResponsekkk2", webData.toString());
                    if (SmartUtils.getResponseCode(webData) == STATUS_SUCCESS) {
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            // SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, true, 200);
                    } else if (SmartUtils.getResponseCode(webData) == STATUS_FAIL) {
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            //SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, true, 400);
                    } else {
                        int responseCode = SmartUtils.getResponseCode(webData);
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, false, responseCode);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.getMessage() != null) {
                    Log.v("@@@@@WSError", error.getMessage());
                } else {
                    Log.v("@@@@@WSError", error.toString());

                }
                SmartUtils.hideSoftKeyboard(mContext);
                String errorMessage = VolleyErrorHelper.getMessage(error, mContext);
                SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError();
            }
        });
        smMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        smMultiPartRequest.setTag(requestParams.get(REQUEST_METHOD_PARAMS.TAG));
        getRequestQueue().add(smMultiPartRequest);
    }


    public <T> void addToRequestQueueMultipart(final HashMap<REQUEST_METHOD_PARAMS, Object> requestParams, HashMap<String, String> filePath, String singleKey, final boolean isShowSnackbar) {
        Log.v("@@@@@WSRequest", ((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)));
        final Context mContext = (Context) requestParams.get(REQUEST_METHOD_PARAMS.CONTEXT);
        MultiPartRequest smMultiPartRequest = new MultiPartRequest(((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)),

                (Map<String, String>) requestParams.get(REQUEST_METHOD_PARAMS.PARAMS), filePath, singleKey, new Response.Listener() {
            @Override
            public void onResponse(Object object) {

                JSONObject webData = (JSONObject) object;
                try {
                    //  JSONObject webData = (JSONObject) object;
                    //     JSONObject response = webData.getJSONObject("Results");
                    Log.v("@@@@@WSResponsekkk", webData.toString());

                    Log.v("@@@@@WSResponsekkk2", webData.toString());
                    if (SmartUtils.getResponseCode(webData) == STATUS_SUCCESS) {
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            // SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, true, 200);
                    } else if (SmartUtils.getResponseCode(webData) == STATUS_FAIL) {
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            //SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, true, 400);
                    } else {
                        int responseCode = SmartUtils.getResponseCode(webData);
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, false, responseCode);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.getMessage() != null) {
                    Log.v("@@@@@WSError", error.getMessage());
                } else {
                    Log.v("@@@@@WSError", error.toString());

                }
                SmartUtils.hideSoftKeyboard(mContext);
                String errorMessage = VolleyErrorHelper.getMessage(error, mContext);
                SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError();
            }
        });
        smMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        smMultiPartRequest.setTag(requestParams.get(REQUEST_METHOD_PARAMS.TAG));
        getRequestQueue().add(smMultiPartRequest);
    }
*/


    public interface OnResponseReceivedListener {
        void onResponseReceived(JSONObject results) throws JSONException;

        void onResponseError(JSONObject results) throws JSONException;
    }


    private String getUrlWithParams(String url, Map<String, String> params) {
        StringBuilder urlBuilder = new StringBuilder(url);
        urlBuilder.append("?");
        Iterator it = params.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
            urlBuilder.append(pair.getKey()).append("=").append(pair.getValue()).append("&");
        }
        urlBuilder.deleteCharAt(urlBuilder.length() - 1);
        return urlBuilder.toString();
    }

}