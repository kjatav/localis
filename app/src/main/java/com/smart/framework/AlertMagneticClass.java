package com.smart.framework;

import android.content.DialogInterface;

public class AlertMagneticClass {

    public interface AlertMagneticList {

        void PositiveMethod(DialogInterface dialog, int id);

    }

    public interface AlertDialogListSelect {

        void onItemSelected(int selectedItemPosition);

    }


    public interface PopUpInterface {

        void onSetPopUp(int selectedOptionPosition);

    }


}

