package com.smart.framework;

public interface KeyboardStateListener {

    void onKeyboardOpen();

    void onKeyboardClose();
}
