package com.smart.framework;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Actvities.NotificationActivity;
import com.localis.AA_Actvities.RequestActivity;
import com.localis.Albums.Activites.AlbumListActivity;
import com.localis.Chat.Activities.ChatListActivity;
import com.localis.Complains.Activities.BrowseComplains;
import com.localis.Dashboard.Activities.DashboardActivity;
import com.localis.Groups.Activites.GroupListActivity;
import com.localis.Pages.Activites.PageListActivity;
import com.localis.Profile.Activities.AccountSettingActivity;
import com.localis.Profile.Activities.ProfileActivity;
import com.localis.R;
import com.localis.Videos.Activities.VideoListActivity;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartTextView;
import com.smart.customViews.SwipeableTextView;
import com.squareup.picasso.Picasso;

import org.apache.http.util.TextUtils;

import java.util.ArrayList;

import static com.smart.framework.SmartUtils.getUserEmail;
import static com.smart.framework.SmartUtils.getUserImage;
import static com.smart.framework.SmartUtils.getUserName;

public abstract class SmartActivity extends AppCompatActivity implements Constants, SmartActivityHandler {

    /*Parent Containers*/
    private FrameLayout childViewContainer;
    private FrameLayout drawerContainer;
    public Toolbar toolbar;
    private SwipeableTextView txtNetworkInfo;
    public DrawerLayout drawerLayout;
    private LayoutInflater layoutInflater;
    private ActionBarDrawerToggle mDrawerToggle;
    private OnDrawerStateListener drawerStateListener;
    private NetworkStateListener networkStateListener;
    private ArrayList<KeyboardStateListener> keyboardStateListeners = new ArrayList<>();
    private KillReceiver clearActivityStack;
    private FrameLayout lytChildViewContainer;

    private View bottomNav;
    private View semiTransparentView;


    public int orientation;

    public void setSnackbar(Snackbar snackbar) {

        this.snackbar = snackbar;
    }

    private Snackbar snackbar;

    private CoordinatorLayout snackBarContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setAnimations();
        }
        super.onCreate(savedInstanceState);

        preOnCreate();

        setContentView(R.layout.smart_activity);

        layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        drawerLayout = findViewById(R.id.drawerLayout);
        drawerContainer = findViewById(R.id.drawerContainer);
        childViewContainer = findViewById(R.id.lytChildViewContainer);


        clearActivityStack = new KillReceiver();

        registerReceiver(clearActivityStack, IntentFilter.create("clearStackActivity", "text/plain"));

        addChildViews();

        addSnackBarContainer();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    if (drawerStateListener != null) {
                        drawerStateListener.onDrawerOpen(drawerView);
                    }
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    if (drawerStateListener != null) {
                        drawerStateListener.onDrawerClose(drawerView);
                    }
                }

                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {
                    super.onDrawerSlide(drawerView, slideOffset);
                    childViewContainer.setTranslationX(slideOffset * drawerView.getWidth());
                    drawerLayout.bringChildToFront(drawerView);
                    drawerLayout.requestLayout();
                }
            };
            drawerLayout.addDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();

        }


        addDrawerLayout();

        initComponents();

        prepareViews();

        setActionListeners();

        if (toolbar != null) {
            manageAppBar(getSupportActionBar(), toolbar, mDrawerToggle);
        }

        postOnCreate();
    }

    protected void setupBottomNav(int position) {

        bottomNav = findViewById(R.id.bottom_nav);
        semiTransparentView = findViewById(R.id.semi_transparent_view);

        TabLayout bottomTabs = findViewById(R.id.bottom_tabs);
        LinearLayout roundedLl = findViewById(R.id.rounded_ll);
        SmartTextView nameBottomTv = findViewById(R.id.name_bottom_tv);
        SmartTextView emailBottomTv = findViewById(R.id.email_bottom_tv);
        LinearLayout profileBottomLl = findViewById(R.id.profile_bottom_ll);
        LinearLayout notificationsBottomLl = findViewById(R.id.notifications_bottom_ll);
        LinearLayout groupsBottomLl = findViewById(R.id.groups_bottom_ll);
        LinearLayout photosBottomLl = findViewById(R.id.photos_bottom_ll);
        LinearLayout videosBottomLl = findViewById(R.id.videos_bottom_ll);
        LinearLayout settingsBottomLl = findViewById(R.id.settings_bottom_ll);
        LinearLayout blogsBottomLl = findViewById(R.id.blogs_bottom_ll);
        LinearLayout pagesBottomLl = findViewById(R.id.pages_bottom_ll);
        LinearLayout complainsBottomLl = findViewById(R.id.complains_bottom_ll);
        SmartTextView localisTextTv = findViewById(R.id.localis_text_tv);
        RoundedImageView profileBottomIv = findViewById(R.id.profile_bottom_iv);

        ImageView mainBottomIv = findViewById(R.id.main_bottom_iv);


        bottomTabs.setVisibility(View.VISIBLE);
        mainBottomIv.setVisibility(View.VISIBLE);

        bottomTabs.getTabAt(position).select();

        mainBottomIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideRoundLayout();
            }
        });

        semiTransparentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideRoundLayout();
            }
        });

        nameBottomTv.setText(getUserName());
        emailBottomTv.setText(getUserEmail());
        if (!TextUtils.isEmpty(getUserImage())) {
            Picasso.with(SmartActivity.this).load(getUserImage()).placeholder(R.drawable.ic_detail_user).into(profileBottomIv);
        }


        nameBottomTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SmartActivity.this, ProfileActivity.class);
                startActivity(i);
            }
        });

        emailBottomTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SmartActivity.this, ProfileActivity.class);
                startActivity(i);
            }
        });

        profileBottomLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SmartActivity.this, ProfileActivity.class);
                startActivity(i);
            }
        });

        profileBottomIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SmartActivity.this, ProfileActivity.class);
                startActivity(i);
            }
        });

        notificationsBottomLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SmartActivity.this, NotificationActivity.class);
                startActivity(i);
            }
        });

        groupsBottomLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SmartActivity.this, GroupListActivity.class);
                startActivity(i);
            }
        });

        photosBottomLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SmartActivity.this, AlbumListActivity.class);
                startActivity(i);
            }
        });

        videosBottomLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SmartActivity.this, VideoListActivity.class);
                startActivity(i);
            }
        });

        settingsBottomLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SmartActivity.this, AccountSettingActivity.class);
                startActivity(i);
            }
        });

        blogsBottomLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            /*    Intent i = new Intent(SmartActivity.this, );
                startActivity(i);
            */
            }
        });

        pagesBottomLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SmartActivity.this, PageListActivity.class);
                startActivity(i);
            }
        });

        complainsBottomLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SmartActivity.this, BrowseComplains.class);
                startActivity(i);
            }
        });


        localisTextTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
        /*        Intent i = new Intent(SmartActivity.this, PageListActivity.class);
                startActivity(i);
        */
            }
        });

        bottomTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Intent i;
                switch (tab.getPosition()) {
                    case 0:
                        i = new Intent(SmartActivity.this, DashboardActivity.class);
                        startActivity(i);
                        bottomNav.setVisibility(View.GONE);
                        break;
                    case 1:
                        i = new Intent(SmartActivity.this, GroupListActivity.class);
                        startActivity(i);
                        bottomNav.setVisibility(View.GONE);
                        break;
                    case 2:

                        break;
                    case 3:
                        i = new Intent(SmartActivity.this, ChatListActivity.class);
                        startActivity(i);
                        bottomNav.setVisibility(View.GONE);
                        break;
                    case 4:
                        startActivity(new Intent(SmartActivity.this, RequestActivity.class));
                        bottomNav.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

/*
    public void hideRoundLayout() {
        if (bottomNav.getVisibility() == View.VISIBLE) {
            bottomNav.setVisibility(View.GONE);
        } else {
            bottomNav.setVisibility(View.VISIBLE);
        }
    }
*/

    public void hideRoundLayout() {
        if (bottomNav.getVisibility() != View.VISIBLE) {
            bottomNav.setVisibility(View.VISIBLE);
            semiTransparentView.setVisibility(View.VISIBLE);
            Animation slideOut = AnimationUtils.loadAnimation(SmartActivity.this, R.anim.slidein_animation);
            Animation fadeIn = AnimationUtils.loadAnimation(SmartActivity.this, R.anim.fadein_anmation);
            semiTransparentView.startAnimation(fadeIn);
            bottomNav.startAnimation(slideOut);
        } else {
            Animation slideIn = AnimationUtils.loadAnimation(SmartActivity.this, R.anim.slideout_animation);
            slideIn.setAnimationListener(animationListener);
            Animation fadeOut = AnimationUtils.loadAnimation(SmartActivity.this, R.anim.fadeout_anmation);
            fadeOut.setAnimationListener(animationListener2);
            semiTransparentView.startAnimation(fadeOut);
            bottomNav.startAnimation(slideIn);
        }
    }

    private Animation.AnimationListener animationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            if (bottomNav.getVisibility() == View.VISIBLE) {
                bottomNav.clearAnimation();
                bottomNav.setVisibility(View.GONE);
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    };

    private Animation.AnimationListener animationListener2 = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            if (semiTransparentView.getVisibility() == View.VISIBLE) {
                semiTransparentView.clearAnimation();
                semiTransparentView.setVisibility(View.GONE);
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    };


    private void addDrawerLayout() {
        if (getDrawerLayoutID() != 0) {
            layoutInflater.inflate(getDrawerLayoutID(), drawerContainer);
        } else {
            disableSideMenu();
        }
    }

    protected void addSnackBarContainer() {
        layoutInflater.inflate(R.layout.snackbar_container, childViewContainer);
        snackBarContainer = findViewById(R.id.snackBarPosition);
    }

    protected void closeDrawer() {
        drawerLayout.closeDrawer(drawerContainer);
    }

    private void setNetworkInfoProperties() {
        if (txtNetworkInfo != null) {
            txtNetworkInfo.setVisibility(SmartUtils.isNetworkAvailable() ? View.GONE : View.VISIBLE);
            txtNetworkInfo.setText(getString(R.string.network_not_available));
        }
    }

    protected void disableSideMenu() {
        if (drawerLayout != null && mDrawerToggle != null) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
        }
    }

    public void addChildViews() {
        if (getLayoutView() != null) {
            childViewContainer.addView(getLayoutView());
        } else {
            layoutInflater.inflate(getLayoutID(), childViewContainer);
        }

        txtNetworkInfo = (SwipeableTextView) findViewById(R.id.txtNetworkInfo);
        setNetworkInfoProperties();

        layoutInflater.inflate(R.layout.smart_transparent_frame, childViewContainer);
        childViewContainer.getViewTreeObserver().addOnGlobalLayoutListener(keyboardObserveListener);

    }

    public void setDrawerStateListener(OnDrawerStateListener drawerStateListener) {
        this.drawerStateListener = drawerStateListener;
    }

    protected View getScreenRootView() {
        return childViewContainer;
    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        int color = getResources().getColor(R.color.textSecondary);
        MenuColorizer.colorMenu(this, menu, color, 255);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (bottomNav != null && bottomNav.getVisibility() == View.VISIBLE) {
            bottomNav.setVisibility(View.GONE);
        } else {
            if (drawerLayout.isDrawerOpen(drawerContainer)) {
                closeDrawer();
            } else {
                super.onBackPressed();
            }
        }

    }

    private class NetworkStateListener extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            setNetworkInfoProperties();
        }
    }

    private final class KillReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }

    public int getDeviceWidth() {
        return getWindowManager().getDefaultDisplay().getWidth();
    }

    public int getDeviceHeight() {
        return getWindowManager().getDefaultDisplay().getHeight();
    }

    public View getSnackBarContainer() {
        return snackBarContainer;
    }

    public Snackbar getSnackbar() {
        return snackbar;
    }

    @Override
    protected void onResume() {
        super.onResume();
        networkStateListener = new NetworkStateListener();
        registerReceiver(networkStateListener, new IntentFilter("NetworkState"));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(networkStateListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(clearActivityStack);
    }

    public void setKeyboardStateListener(KeyboardStateListener keyboardStateListener) {
        keyboardStateListeners.add(keyboardStateListener);
    }

    ViewTreeObserver.OnGlobalLayoutListener keyboardObserveListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            Rect r = new Rect();
            childViewContainer.getRootView().getWindowVisibleDisplayFrame(r);
            int screenHeight = childViewContainer.getRootView().getRootView().getHeight();

            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            int keypadHeight = screenHeight - r.bottom;

            final FrameLayout transparentFrame = (FrameLayout) findViewById(R.id.lytTransparentFrame);

            if (keypadHeight > screenHeight * 0.15) {
                // keyboard is opened
                transparentFrame.setVisibility(View.VISIBLE);

                for (int i = 0; i < keyboardStateListeners.size(); i++) {

                    if (keyboardStateListeners.get(i) != null) {
                        keyboardStateListeners.get(i).onKeyboardOpen();
                    }
                }

                transparentFrame.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (shouldKeyboardHideOnOutsideTouch()) {
                            SmartUtils.hideSoftKeyboard(SmartActivity.this);
                            return true;
                        } else {
                            return false;
                        }

                    }
                });
            } else {
                // keyboard is closed
                for (int i = 0; i < keyboardStateListeners.size(); i++) {

                    if (keyboardStateListeners.get(i) != null) {
                        keyboardStateListeners.get(i).onKeyboardClose();
                    }
                }
                transparentFrame.setVisibility(View.GONE);
                transparentFrame.setOnTouchListener(null);
            }
        }
    };

    public interface OnDrawerStateListener {
        void onDrawerOpen(View drawerView);

        void onDrawerClose(View drawerView);
    }
}
