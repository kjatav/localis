package com.smart.framework;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.DrawableRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.localis.AA_POJOs.PopUpMenuObject;
import com.localis.Groups.POJOs.GroupItemObject;
import com.localis.R;
import com.smart.common.Iso2Phone;
import com.smart.common.Object_Image;
import com.smart.customViews.CustomClickListener;
import com.smart.customViews.SmartDatePicker;
import com.smart.customViews.SmartEditText;
import com.smart.customViews.SmartTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.jar.Pack200.Unpacker.TRUE;

public class SmartUtils implements Constants {

    private static final String TAG = "SmartUtil";
    private static boolean isNetworkAvailable;
    private static ProgressDialog progressDialog;
    private static Dialog loadingDialog;
    private static Geocoder geocoder;
    private static int count = 0;

    public static boolean isReloadRequired() {
        return isReloadRequired;
    }

    public static void setIsReloadRequired(boolean isReloadRequired) {
        SmartUtils.isReloadRequired = isReloadRequired;
    }

    private static boolean isReloadRequired = false;

    public static boolean isNetworkAvailable() {
        return isNetworkAvailable;
    }

    public static void setNetworkStateAvailability(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (NetworkInfo anInfo : info) {
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED) {

                        isNetworkAvailable = true;
                        return;
                    }
                }
            }
        }
        isNetworkAvailable = false;
    }

    // Validation

    /**
     * This method used to email validator.
     *
     * @param mailAddress represented email
     * @return represented {@link Boolean}
     */
    public static boolean emailValidator(final String mailAddress) {
        Pattern pattern;
        Matcher matcher;

        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(mailAddress);
        return matcher.matches();
    }

    public static boolean urlValidator(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }

    /**
     * This method used to birth date validator.
     *
     * @param birthDate represented birth date
     * @return represented {@link Boolean}
     */
    public static boolean birthdateValidator(String birthDate, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        try {
            Date date = dateFormat.parse(birthDate);
            Calendar bdate = Calendar.getInstance();
            bdate.setTime(date);
            Calendar today = Calendar.getInstance();

            if (bdate.compareTo(today) == 1) {
                return false;
            } else {
                return true;
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return false;
    }


    //Dates

    /**
     * This method used to get date from string.
     *
     * @param strDate represented date
     * @return represented {@link Date}
     */
    public static Calendar getDateFromString(String strDate, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        Calendar calnder = Calendar.getInstance();
        Date date;
        try {
            date = dateFormat.parse(strDate);
            calnder.setTime(date);
            return calnder;

        } catch (Throwable e) {
            Log.e("@@", e.toString());
            return Calendar.getInstance();
        }
    }

    /**
     * This method used to get date from string.
     *
     * @param strDate represented date
     * @return represented {@link Date}
     */
    public static Calendar getStringToCalender(String strDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar calnder = Calendar.getInstance();
        Date date;
        try {
            date = dateFormat.parse(strDate);
            calnder.setTime(date);
            return calnder;
        } catch (Throwable e) {
            return Calendar.getInstance();
        }
    }

    public static Calendar getStringToCalender(String strDate, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        Calendar calnder = Calendar.getInstance();
        Date date;
        try {
            date = dateFormat.parse(strDate);
            calnder.setTime(date);
            String s = dateFormat.format(calnder.getTime());
            Log.e("@@@@", dateFormat.format(calnder.getTime()));
            return calnder;
        } catch (Throwable e) {
            return Calendar.getInstance();
        }
    }

    public static Calendar getCalender(Calendar calendar, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        Date date;
        try {
            date = calendar.getTime();
            dateFormat.format(date);
            return calendar;
        } catch (Throwable e) {
            return Calendar.getInstance();
        }
    }

    /**
     * This method used to get time from string.
     *
     * @param strTime represented time
     * @return represented {@link Date}
     */
    public static Calendar getTimeFromString(String strTime, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date date;
        Calendar calnder = Calendar.getInstance();
        try {
            date = dateFormat.parse(strTime);
            calnder.setTime(date);
            return calnder;
        } catch (Throwable e) {
            return Calendar.getInstance();
        }
    }

    /**
     * This method used to get date dialog.
     *
     * @param strDate  represented date
     * @param restrict represented isRestrict
     */
    public static void getDateDialog(Context context, final String strDate, boolean restrict, boolean isMinDate, final CustomClickListener target, final String format) {
        Calendar date = getDateFromString(strDate, format);
        Calendar today = Calendar.getInstance();
        if (restrict && date.get(Calendar.YEAR) == today.get(Calendar.YEAR) && date.get(Calendar.MONTH) == today.get(Calendar.MONTH)
                && date.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH)) {
            date.add(Calendar.YEAR, -18);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            SmartDatePicker dateDlg = new SmartDatePicker(context, R.style.AppDatePicketStyle,
                    new DatePickerDialog.OnDateSetListener() {

                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            Time chosenDate = new Time();
                            chosenDate.set(dayOfMonth, monthOfYear, year);
                            long dt = chosenDate.toMillis(true);
                            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
                            CharSequence strDate = sdf.format(dt);
                            target.onClick(strDate.toString());
                        }
                    }, date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE), restrict);


            if (isMinDate) {
                Calendar calendar = Calendar.getInstance();
                dateDlg.getDatePicker().setMinDate(calendar.getTimeInMillis());
            }


            dateDlg.show();
        } else {
            SmartDatePicker dateDlg = new SmartDatePicker(context, new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Time chosenDate = new Time();
                    chosenDate.set(dayOfMonth, monthOfYear, year);
                    long dt = chosenDate.toMillis(true);
                    SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
                    CharSequence strDate = sdf.format(dt);
                    target.onClick(strDate.toString());
                }
            }, date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE), restrict);

            if (isMinDate) {
                Calendar calendar = Calendar.getInstance();
                dateDlg.getDatePicker().setMinDate(calendar.getTimeInMillis());
            }

            dateDlg.show();
        }
    }

    /**
     * This method used to get date-time dialog.
     *
     * @param strDate represented date-time
     * @param target  represented {@link CustomClickListener}
     */
    public static void getDateTimeDialog(final Context context, final String strDate, boolean isCurrent, final CustomClickListener target, final String format) {
        final Calendar date = getDateFromString(strDate, format);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            DatePickerDialog dateDialog = new DatePickerDialog(context, R.style.AppDatePicketStyle,
                    new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            final int y = year;
                            final int m = monthOfYear;
                            final int d = dayOfMonth;

                            new TimePickerDialog(context, R.style.AppDatePicketStyle,
                                    new TimePickerDialog.OnTimeSetListener() {

                                        @Override
                                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                                            Time chosenDate = new Time();
                                            chosenDate.set(0, minute, hourOfDay, d, m, y);
                                            long dt = chosenDate.toMillis(true);
                                            CharSequence strDate = DateFormat.format(format, dt);
                                            target.onClick(strDate.toString());

                                        }
                                    }, date.get(Calendar.HOUR), date.get(Calendar.MINUTE), true).show();

                        }
                    }, date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE));

            if (isCurrent) {
                Calendar calendar = Calendar.getInstance();
                dateDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            }
            dateDialog.show();
        } else {
            DatePickerDialog dateDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    final int y = year;
                    final int m = monthOfYear;
                    final int d = dayOfMonth;

                    if (count == 1) {

                        new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                                Time chosenDate = new Time();
                                chosenDate.set(0, minute, hourOfDay, d, m, y);
                                long dt = chosenDate.toMillis(true);
                                CharSequence strDate = DateFormat.format(format, dt);
                                target.onClick(strDate.toString());
                                count = 0;
                            }


                        }, date.get(Calendar.HOUR), date.get(Calendar.MINUTE), false).show();
                    }
                    count++;
                }
            }, date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE));
            if (isCurrent) {
                Calendar calendar = Calendar.getInstance();
                dateDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            }
            dateDialog.show();
        }
    }

    /**
     * This method used to get date-time dialog.
     *
     * @param strDate represented date-time
     * @param target  represented {@link CustomClickListener}
     */
    public static void getStartEndDateTimeDialog(final Context context, final String strDate, final CustomClickListener target, final String format) {
        final Calendar date = getDateFromString(strDate, format);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            DatePickerDialog dateDialog = new DatePickerDialog(context, R.style.AppDatePicketStyle,
                    new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            final int y = year;
                            final int m = monthOfYear;
                            final int d = dayOfMonth;

                            new TimePickerDialog(context, R.style.AppDatePicketStyle,
                                    new TimePickerDialog.OnTimeSetListener() {

                                        @Override
                                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                                            Time chosenDate = new Time();
                                            chosenDate.set(0, minute, hourOfDay, d, m, y);
                                            long dt = chosenDate.toMillis(true);
                                            CharSequence strDate = DateFormat.format(format, dt);
                                            target.onClick(strDate.toString());

                                        }
                                    }, date.get(Calendar.HOUR), date.get(Calendar.MINUTE), false).show();

                        }
                    }, date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE));
            dateDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            dateDialog.show();
        } else {
            DatePickerDialog dateDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    final int y = year;
                    final int m = monthOfYear;
                    final int d = dayOfMonth;

                    if (count == 1) {

                        new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                                Time chosenDate = new Time();
                                chosenDate.set(0, minute, hourOfDay, d, m, y);
                                long dt = chosenDate.toMillis(true);
                                CharSequence strDate = DateFormat.format(format, dt);
                                target.onClick(strDate.toString());
                                count = 0;
                            }


                        }, date.get(Calendar.HOUR), date.get(Calendar.MINUTE), false).show();
                    }
                    count++;
                }
            }, date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE));
            dateDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            dateDialog.show();
        }
    }

    /**
     * This method used to get time dialog.
     *
     * @param strTime represented time
     * @param target  represented {@link CustomClickListener}
     */
    public static void getTimeDialog(Context context, final String strTime, final CustomClickListener target, final String format) {
        Calendar date = getTimeFromString(strTime, format);
        TimePickerDialog timeDialog = new TimePickerDialog(context, R.style.AppDatePicketStyle,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Calendar date = Calendar.getInstance();
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                        @SuppressLint("SimpleDateFormat")
                        String dateString = new SimpleDateFormat(format, Locale.ENGLISH).format(date.getTime());
                        target.onClick(dateString);
                    }
                }, date.get(Calendar.HOUR), date.get(Calendar.MINUTE), true);

        timeDialog.show();
    }

    public static void showLoadingDialog(final Context context) {
        if (loadingDialog != null && loadingDialog.isShowing()) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
        loadingDialog = new Dialog(context);
        loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = View.inflate(context, R.layout.loading_dialog, null);
        loadingDialog.setContentView(view);
        loadingDialog.setCancelable(false);
        loadingDialog.setCanceledOnTouchOutside(false);
        Window window = loadingDialog.getWindow();
        assert window != null;
        window.setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        loadingDialog.show();
    }

    public static void hideLoadingDialog() {
        if (loadingDialog != null && loadingDialog.isShowing()) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    /**
     * This method will show the progress dialog with given message in the given
     * activity's context.<br>
     * The progress dialog can be set cancellable by passing appropriate flag in
     * parameter. User can dismiss the current progress dialog by pressing back
     * SmartButton if the flag is set to <b>true</b>; This method can also be
     * called from non UI threads.
     *
     * @param context = Context context will be current activity's context.
     *                <b>Note</b> : A new progress dialog will be generated on
     *                screen each time this method is called.
     */
    public static void showProgressDialog(final Context context, String msg, final boolean isCancellable) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        }
        progressDialog = ProgressDialog.show(context, "", "");

        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setCancelable(isCancellable);
        ((ProgressBar) progressDialog.findViewById(R.id.progressBar)).getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(context, R.color.primaryColor), PorterDuff.Mode.SRC_ATOP);
        ((SmartTextView) progressDialog.findViewById(R.id.txtMessage)).setText(msg == null || msg.trim().length() <= 0 ? context.getString(R.string.dialog_loading_please_wait) : msg);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    /**
     * This method will hide existing progress dialog.<br>
     * It will not throw any Exception if there is no progress dialog on the
     * screen and can also be called from non UI threads.
     */
    static public void hideProgressDialog() {
        try {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        } catch (Throwable e) {
            progressDialog = null;
        }
    }

    public static ProgressDialog getProgressDialog() {
        return progressDialog;
    }


    /**
     * This method will generate and show the Ok dialog with given message and
     * single message SmartButton.<br>
     *
     * @param title  = String title will be the title of OK dialog.
     * @param msg    = String msg will be the message in OK dialog.
     * @param target = String target is AlertNewtral callback for OK SmartButton
     *               click action.
     */
    static public void getConfirmDialog(Context context, String title, String msg, String positiveBtnCaption,
                                        String negativeBtnCaption, boolean isCancelable, final AlertMagnatic target) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title).setMessage(msg).setCancelable(false)
                .setPositiveButton(positiveBtnCaption, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        target.PositiveMethod(dialog, id);
                    }
                })
                .setNegativeButton(negativeBtnCaption, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        target.NegativeMethod(dialog, id);
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setCancelable(isCancelable);
        alert.show();
    }

    /**
     * This method will generate and show the Ok dialog with given message and
     * single message SmartButton.<br>
     *
     * @param msg    = String msg will be the message in OK dialog.
     * @param target = String target is AlertNewtral callback for OK SmartButton
     *               click action.
     */
    static public void getConfirmDialog(Context context, String msg, String positiveBtnCaption,
                                        String negativeBtnCaption, boolean isCancelable, final AlertMagnatic target) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setMessage(msg).setCancelable(false)
                .setPositiveButton(positiveBtnCaption, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        target.PositiveMethod(dialog, id);
                    }
                })
                .setNegativeButton(negativeBtnCaption, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        target.NegativeMethod(dialog, id);
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setCancelable(isCancelable);
        alert.show();
    }

    /**
     * This method will generate and show the Ok dialog with given message and
     * single message SmartButton.<br>
     *
     * @param msg           = String msg will be the message in OK dialog.
     * @param buttonCaption = String SmartButtonCaption will be the name of OK
     *                      SmartButton.
     * @param target        = String target is AlertNewtral callback for OK SmartButton
     *                      click action.
     */
    static public void getOKDialog(Context context, String msg, String buttonCaption, boolean isCancelable, final AlertNeutral target) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(buttonCaption, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        target.NeutralMethod(dialog, id);
                    }
                });

        AlertDialog alert = builder.create();
        alert.setCancelable(isCancelable);
        alert.show();
    }

    /**
     * This method will generate and show the Ok dialog with given message and
     * single message SmartButton.<br>
     *
     * @param title         = String title will be the title of OK dialog.
     * @param msg           = String msg will be the message in OK dialog.
     * @param buttonCaption = String SmartButtonCaption will be the name of OK
     *                      SmartButton.
     * @param target        = String target is AlertNewtral callback for OK SmartButton
     *                      click action.
     */
    static public void getOKDialog(Context context, String title, String msg, String buttonCaption,
                                   boolean isCancelable, final AlertNeutral target) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title).setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(buttonCaption, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        target.NeutralMethod(dialog, id);
                    }
                });

        AlertDialog alert = builder.create();
        alert.setCancelable(isCancelable);
        alert.show();
    }

    /**
     * This method will show short length Toast message with given string.
     *
     * @param msg = String msg to be shown in Toast message.
     */
    static public void ting(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * This method will show long length Toast message with given string.
     *
     * @param msg = String msg to be shown in Toast message.
     */
    static public void tong(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * This method will show Center with given String
     **/

    static public void toCenter(Context context, String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }


    // Audio, Image and Video

    /**
     * This method used to decode file from string path.
     *
     * @param path represented path
     * @return represented {@link Bitmap}
     */
    static public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 70;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }

    //General Methods

    /**
     * This method will return android device UDID.
     *
     * @return DeviceID = String DeviceId will be the Unique Id of android
     * device.
     */
    @SuppressLint({"MissingPermission", "HardwareIds"})
    static public String getDeviceUDID(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            return TextUtils.isEmpty(telephonyManager.getDeviceId()) ? "00000000000" : telephonyManager.getDeviceId();
        } catch (Exception e) {
            return "00000000000";
        }

    }


    public static String getB64Auth(String userName, String password) {
        String source = userName + ":" + password;
        return "Basic " + Base64.encodeToString(source.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
    }

    /**
     * This method used to hide soft keyboard.
     */
    static public void hideSoftKeyboard(Context context) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    /**
     * This method used to show soft keyboard.
     */
    static public void showSoftKeyboard(Context context) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        } catch (Exception e) {
        }
    }

    /**
     * This method used to convert json to map.
     *
     * @param object represented json object
     * @return represented {@link Map <String, String>}
     * @throws JSONException represented {@link JSONException}
     */
    public static Map<String, String> jsonToMap(JSONObject object) throws JSONException {
        Gson gson = new Gson();
        Map<String, String> map = new HashMap<>();
        map = (Map<String, String>) gson.fromJson(object.toString(), map.getClass());

//        Iterator keys = object.keys();
//        while (keys.hasNext()) {
//            String key = (String) keys.next();
//            map.put(key, fromJson(object.get(key)).toString());
//        }
        return map;
    }

    /**
     * This method used to convert json to Object.
     *
     * @param json represented json object
     * @return represented {@link Object}
     * @throws JSONException represented {@link JSONException}
     */
    static public Object fromJson(Object json) throws JSONException {
        if (json == JSONObject.NULL) {
            return null;
        } else if (json instanceof JSONObject) {
            return jsonToMap((JSONObject) json);
        } else if (json instanceof JSONArray) {
            return toList((JSONArray) json);
        } else {
            return json;
        }
    }

    public static String getFileNameFromUrl(String url) {
        return url.substring(url.lastIndexOf('/') + 1);
    }

    /**
     * This method used to convert json array to List.
     *
     * @param array represented json array
     * @return represented {@link List}
     * @throws JSONException represented {@link JSONException}
     */
    public static List toList(JSONArray array) throws JSONException {
        List list = new ArrayList();
        int size = array.length();
        for (int i = 0; i < size; i++) {
            list.add(fromJson(array.get(i)));
        }
        return list;
    }

    /**
     * This method used to string array from string with (,) separated.
     *
     * @param value represented value
     * @return represented {@link String} array
     */
    static public String[] getStringArray(final String value) {
        try {
            if (value.length() > 0) {
                final JSONArray temp = new JSONArray(value);
                int length = temp.length();
                if (length > 0) {
                    final String[] recipients = new String[length];
                    for (int i = 0; i < length; i++) {
                        recipients[i] = temp.getString(i).equalsIgnoreCase("null") ? "1" : temp.getString(i);
                    }
                    return recipients;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    /**
     * This method used to string array from arraylist.
     *
     * @param value represented value
     * @return represented {@link String} array
     */
    static public String[] getStringArray(final ArrayList<String> value) {
        try {
            String[] array = new String[value.size()];
            for (int i = 0; i < value.size(); i++) {
                array[i] = value.get(i);
            }
            return array;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONArray concatJsonArrays(JSONArray... arrs)
            throws JSONException {
        JSONArray result = new JSONArray();
        for (JSONArray arr : arrs) {
            for (int i = 0; i < arr.length(); i++) {
                result.put(arr.get(i));
            }
        }
        return result;
    }


    static public boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    static public String createExternalDirectory(String directoryName) {

        if (SmartUtils.isExternalStorageAvailable()) {

            File file = new File(Environment.getExternalStorageDirectory(), directoryName);
            if (!file.mkdirs()) {
                Log.e(TAG, "Directory may exist");
            }
            return file.getAbsolutePath();
        } else {

            Log.e(TAG, "External storage is not available");
        }
        return null;
    }

    static public void clearActivityStack(Activity currentActivity, Intent intent) {

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(currentActivity);
        Intent mainIntent = Intent.makeRestartActivityTask(intent.getComponent());
        ActivityCompat.startActivity(currentActivity, mainIntent, options.toBundle());
    }

    public static String getDayName(Calendar calendar) {
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);

        return dayFormat.format(calendar.getTime());
    }

    static public int convertSizeToDeviceDependent(Context context, int value) {
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        return ((dm.densityDpi * value) / 160);
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    static public void showSnackBarLong(Context context, String message) {
        Snackbar snackbar = Snackbar.make(((SmartActivity) context).getSnackBarContainer(), message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.snackBar_bg));
        TextView tv = snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextSize(15);
        tv.setTextColor(Color.WHITE);
        snackbar.show();
        ((SmartActivity) context).setSnackbar(snackbar);
    }


    static public void showSnackBarShort(Context context, String message) {
        Snackbar snackbar = Snackbar.make(((SmartActivity) context).getSnackBarContainer(), message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.snackBar_bg));
        TextView tv = snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextSize(15);
        tv.setTextColor(Color.WHITE);
        snackbar.show();
        ((SmartActivity) context).setSnackbar(snackbar);
    }


    static public void showSnackBar(Context context, String message, int length) {
        Snackbar snackbar = Snackbar.make(((SmartActivity) context).getSnackBarContainer(), message, length);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.snackBar_bg));
        TextView tv = snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextSize(15);
        tv.setTextColor(Color.WHITE);
        snackbar.show();
        ((SmartActivity) context).setSnackbar(snackbar);
    }

    static public void toastLong(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    static public void toastShort(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    static public void hideSnackBar(Context context) {
        if (((SmartActivity) context).getSnackbar() != null) {
            ((SmartActivity) context).getSnackbar().dismiss();
        }
    }

    public static boolean isOSPreLollipop() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP;
    }

    private static Date parseDate(String date, String hours24) {
        SimpleDateFormat inputParser = new SimpleDateFormat(hours24);
        try {
            return inputParser.parse(date);
        } catch (ParseException e) {
            return new Date(0);
        }
    }

    public static int getMonth(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yy");
        Date parse = null;
        try {
            parse = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(parse);
        return c.get(Calendar.MONTH);
    }

    public static int getYear(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yy");
        Date parse = null;
        try {
            parse = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(parse);
        return c.get(Calendar.YEAR);
    }

    public static String getTime(String dateString) {
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("h:mm a", Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat1.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormat2.format(convertedDate);
    }

    public static String getDate(String dateString) {
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat1.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormat2.format(convertedDate);
    }

    public static Date getStringDate(String dateString) {
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat1.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public static Date getStringDate(String dateString, String formate) {
        SimpleDateFormat dateFormat1 = new SimpleDateFormat(formate, Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat1.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public static String formatDate(String dateString) {
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy h:mm a", Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat1.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormat2.format(convertedDate);
    }

    public static String formatDateTime(String dateString) {
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMM,d yyyy (EEEE)", Locale.ENGLISH);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat1.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormat2.format(convertedDate);
    }

    public static boolean invalidActivityDateTimeEvent(String companyTime, String endUserTime) {
        boolean isValid = true;
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdfUserStartDate = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdfUserEndDate = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);

        //6:18 AM and 7:00AM
        try {
            Date userStart = sdfUserStartDate.parse(companyTime);
            Date userEnd = sdfUserEndDate.parse(endUserTime);
            long l = userStart.getTime();
            long l1 = userEnd.getTime();
            if (userEnd.after(userStart)) {
                isValid = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isValid;
    }


    public static String getApplicationName(Context context) {
        int stringId = context.getApplicationInfo().labelRes;
        return context.getString(stringId);
    }

    static public void removeCookie() {
        SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().edit().remove(Constants.SP_COOKIES);
        SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().edit().commit();
    }


    static public Map<String, Boolean> checkResponse(JSONObject responseJson) {
        Map<String, Boolean> response = new HashMap<>();
        response.put(STATUS, responseJson.has(STATUS));
        response.put(MESSAGE, responseJson.has(MESSAGE));
        response.put(DATA, responseJson.has(DATA));
        response.put(ERROR, responseJson.has(ERROR));


        return response;
    }


    static public String validateResponse(JSONObject response) {
        String errorMessage = null;
        if (response.has(STATUS)) {
            try {
                if (response.has(DATA)) {
                    errorMessage = response.getString(DATA);
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        } else {
            errorMessage = "Error while validating response.";
        }
        return errorMessage;
    }


    static public String validateResponse(JSONObject response, String errorMessage) {

        if (response.has("status")) {
            try {
                if (response.has("msg") && response.getString("msg").length() > 0) {
                    errorMessage = response.getString("msg");
                }
            } catch (Throwable e) {
                errorMessage = "There is some problem on server, Please contact to server guys!";
            }
        } else {
            errorMessage = "There is some problem on server, Please contact to server guys!";
        }

        return errorMessage;
    }

    static public int getResponseCode(JSONObject response) {

        if (response.has(CODE)) {
            try {
                return response.getInt(CODE);
            } catch (JSONException e) {
                return 108;
            }
        }

        return 108;
    }

    static public boolean getResponseStatus(JSONObject response) {
        if (response.has(STATUS)) {
            try {
                return response.getString(STATUS).equalsIgnoreCase(TRUE);
            } catch (JSONException e) {
                return false;
            }
        } else {
            Log.e("@@@RESPONSE ERROR====", "Response json structure invalid");
        }
        return false;
    }

    static public String getCountryPhoneCode(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm.getSimCountryIso() != null && tm.getSimCountryIso().length() > 0) {
            return Iso2Phone.getPhone(tm.getSimCountryIso());
        } else {
            return Iso2Phone.getPhone(tm.getNetworkCountryIso());
        }
    }

    public static boolean isPhoneValid(String input) {
        return input.length() < 8 ? false : android.util.Patterns.PHONE.matcher(input).matches();
    }

    /**
     * This method used to get address list from latitude-longitude
     *
     * @param lat represented latitude (0-for current latitude)
     * @param lng represented longitude (0-for current longitude)
     * @return represented {@link Address}
     */
    public static Address getAddressFromLatLong(Context context, double lat, double lng) {
        Log.v("@@@@PUT LAT", "" + lat);
        Log.v("@@@@PUT LONG", "" + lng);
        geocoder = new Geocoder(context, Locale.ENGLISH);
        try {
            List<Address> list = geocoder.getFromLocation(lat, lng, 1);
            Log.v("@@@@ADDRESS", list.toString());
            return list.get(0);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method used to get latitude-longitude from address.
     *
     * @param address represented address
     * @return represented {@link Address}
     */
    public static Address getLatLongFromAddress(Context context, String address) {
        if (address != null && address.length() > 0) {
            geocoder = new Geocoder(context);

            List<Address> list = null;
            try {
                list = geocoder.getFromLocationName(address, 1);
                Log.v("@@@@ADDRESS", list.toString());
                return list.get(0);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getLatitude() {
        return SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(SP_USER_LATITUDE, "23.0305");
    }

    public static void setLatitude(String lat) {
        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(SP_USER_LATITUDE, lat);
    }

    public static String getLongitude() {
        return SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(SP_USER_LONGITUDE, "72.5075");
    }

    public static void setLongitude(String lng) {
        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(SP_USER_LONGITUDE, lng);
    }

    public static Rect locateView(View v) {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe) {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }

    public static String getCommaSeparated(List<String> selectId) {
        if (selectId.size() > 0) {
            return TextUtils.join(",", selectId);
        } else
            return "";
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
    }

    public static Bitmap getCroppedBitmap(Bitmap bitmap, Context context) {
        if (bitmap == null || bitmap.isRecycled()) {
            return null;
        }
        Bitmap resizedBitmap = getResizedBitmap(bitmap, (int) SmartUtils.convertDpToPixel(35, context)
                , (int) SmartUtils.convertDpToPixel(35, context));
        Bitmap output = Bitmap.createBitmap(resizedBitmap.getWidth(),
                resizedBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(resizedBitmap.getWidth() / 2, resizedBitmap.getHeight() / 2,
                resizedBitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(resizedBitmap, rect, rect, paint);
        return output;
    }

    public static String getCurrentLanguage() {
        return Locale.getDefault().getLanguage().equals("en") ? "1" : "2";
    }

    @SuppressLint("ObsoleteSdkInt")
    public static void ForcefullyLocaleChange(Context context, String locale) {
        Locale.setDefault(new Locale(locale));
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            conf.setLocale(new Locale(locale));
        } else {
            conf.locale = new Locale(locale);
        }
        res.updateConfiguration(conf, dm);
        Log.e("@@@Locale", Locale.getDefault().getLanguage());
    }

    public static boolean checkPersonValidation(String personName) {
        Pattern regex = Pattern.compile("[$&+,:;=\\\\?@#|/'<>.^*()%!-]");
        if (regex.matcher(personName).find()) {
            Log.d(TAG, "SPECIAL CHARS FOUND");
            return true;
        } else {
            return false;
        }
    }


    public static String getCurrentDateTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.ENGLISH);
        return df.format(c.getTime());
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        return df.format(c.getTime());
    }

    public static String getTomorrowDate() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        return df.format(c.getTime());
    }

    public static ArrayList<String> getWeekDate(Calendar calendar) {
        ArrayList<String> days = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            if (i == 0) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                days.add(df.format(calendar.getTime()));
            } else {
                calendar.add(Calendar.DATE, 1);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                days.add(df.format(calendar.getTime()));
            }
        }
        return days;
    }

    public interface getAddressClickListener {
        void getAddress(HashMap<String, String> result);

        void addressFail(Exception e);
    }


    static String getFormattedDate(String strDate) {
        strDate = SmartUtils.getDate(strDate);

        Calendar cal = SmartUtils.getDateFromString(strDate, "d MMMM yyyy");
        //2nd of march 2015
        int day = cal.get(Calendar.DATE);
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = dateFormat.parse(strDate);
        } catch (Throwable e) {
            Log.e("@@@", "");
        }

        if (!((day > 10) && (day < 19)))
            switch (day % 10) {
                case 1:
                    return new SimpleDateFormat("d'st'  MMMM yyyy", Locale.ENGLISH).format(date);
                case 2:
                    return new SimpleDateFormat("d'nd'  MMMM yyyy", Locale.ENGLISH).format(date);
                case 3:
                    return new SimpleDateFormat("d'rd'  MMMM yyyy", Locale.ENGLISH).format(date);
                default:
                    return new SimpleDateFormat("d'th'  MMMM yyyy", Locale.ENGLISH).format(date);
            }
        return new SimpleDateFormat("d'th'  MMMM yyyy", Locale.ENGLISH).format(date);
    }

    public static boolean isValidNumberAndSpecialSymbols(String password) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("((?=.*\\d)(?=.*[@#$%]).{8,})");
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    @SuppressWarnings("unchecked")
    public static <T> T convertGsonModel(Class<T> subclass, String json) throws Exception {
        Gson gson = new Gson();
        TypeToken classTypeToken = TypeToken.get(subclass.getClass());
        Type type = classTypeToken.getType();
        return (T) gson.fromJson(json, type);
    }

    public static String ArrayListTojsonArr(List<Map<String, String>> mapList) {
        Gson gson = new Gson();
        return gson.toJson(mapList);
    }

    public static boolean returnBooleanBasedOnJsonObject(String jsonVal) {
        try {
            JSONObject object = new JSONObject(jsonVal);
            return true;
        } catch (JSONException e) {
            return false;
        }
    }

    public static boolean equalsOr(String key, String... value) {
        List<String> valueList = new ArrayList<>(Arrays.asList(value));
        return valueList.contains(key);
    }

    public static HashMap<String, List<String>> getCurrentTime(JSONObject response) throws JSONException {
        HashMap<String, List<String>> datetime = new HashMap<>();
        Calendar calendar = null;
        String stduration = "";
        String dateSelect = "";

        for (int i = 0; i < response.getJSONObject("data").getJSONArray("appointment").length(); i++) {
            List<String> time = new ArrayList<>();

            JSONObject object = response.getJSONObject("data").getJSONArray("appointment").getJSONObject(i);
            calendar = getStringToCalender(object.getString("boking_time").trim(), "hh:mm aa");
            stduration = object.getString("duration").trim();
            dateSelect = object.getString("date");


            int duration = Integer.parseInt(stduration.trim().split(":")[0]);
            int MINUTE = Integer.parseInt(stduration.trim().split(":")[1]);

            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
            String currentDateandTime = sdf.format(calendar.getTime());

            Date date = null;
            try {
                date = sdf.parse(currentDateandTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (!datetime.containsKey(dateSelect) && !time.contains(currentDateandTime))
                time.add(object.getString("boking_time").trim());

            for (int j = 1; j <= duration; j++) {

                calendar.setTime(date);
                calendar.add(Calendar.HOUR, j);

                if (!datetime.containsKey(dateSelect) && !time.contains(sdf.format(calendar.getTime()))) {
                    time.add(sdf.format(calendar.getTime()));
                    if (!datetime.containsKey(dateSelect) && !time.contains(sdf.format(calendar.getTime())))
                        time.add(sdf.format(calendar.getTime()));
                }

            }

            if (MINUTE > 0) {
                calendar.add(Calendar.MINUTE, MINUTE);

                String s = sdf.format(calendar.getTime());

                if (!datetime.containsKey(dateSelect) && !time.contains(sdf.format(calendar.getTime())))
                    time.add(sdf.format(calendar.getTime()));
            }
            if (!datetime.containsKey(dateSelect))
                datetime.put(dateSelect, time);

        }
        Log.e("@@@", datetime.toString());
        return datetime;
    }

    public static List<HashMap<Date, HashMap<String, Date>>> getCurrentTimePars(JSONObject response) throws JSONException {

        List<HashMap<Date, HashMap<String, Date>>> datetime = new ArrayList<>();

        for (int i = 0; i < response.getJSONObject("data").getJSONArray("appointment").length(); i++) {

            JSONObject object = response.getJSONObject("data").getJSONArray("appointment").getJSONObject(i);
            String boking_time = object.getString("boking_time").trim();
            String stduration = object.getString("duration").trim();
            String dateSelect = object.getString("date");

            Calendar calendar = getStringToCalender(boking_time, "hh:mm aa");

            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
            String currentDatedTime = sdf.format(calendar.getTime());

            Date date = null;
            try {
                date = sdf.parse(currentDatedTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            calendar.setTime(date);
            calendar.add(Calendar.HOUR, Integer.parseInt(stduration.split(":")[0]));
            int MINUTE = Integer.parseInt(stduration.trim().split(":")[1]);
            if (MINUTE > 0) {
                calendar.add(Calendar.MINUTE, Integer.parseInt(stduration.split(":")[1]));
            }

            HashMap<String, Date> dateHashMap = new HashMap<>();
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);

            String s = dateFormat2.format(getStringToCalender(boking_time, "hh:mm aa").getTime());

            dateHashMap.put("start_time", getStringToCalender(boking_time, "hh:mm aa").getTime());
            dateHashMap.put("end_time", calendar.getTime());
            HashMap<Date, HashMap<String, Date>> hashMap = new HashMap<>();
            hashMap.put(getStringDate(dateSelect), dateHashMap);
            datetime.add(hashMap);
        }
        return datetime;
    }


    public static boolean IsDateBetween(List<HashMap<String, Date>> hashMapList, Date selectDate) {
        boolean isBetween = false;
        for (HashMap<String, Date> dateHashMap : hashMapList) {
            Date startDate = dateHashMap.get("start_time");
            Date endDate = dateHashMap.get("end_time");

            SimpleDateFormat dateFormat2 = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
            String s = dateFormat2.format(startDate);
            String s1 = dateFormat2.format(endDate);
            String s2 = dateFormat2.format(selectDate);

            if (!(selectDate.before(startDate) || selectDate.after(endDate))) {
                System.out.println("Date is between...");
                isBetween = true;
                break;
            } else {
                System.out.println("Date is not between 1st april to 14th nov...");
                isBetween = false;
            }
        }
        return isBetween;
    }

    public static String streamToString(InputStream is) throws IOException {
        String str = "";

        if (is != null) {
            StringBuilder sb = new StringBuilder();
            String line;

            try {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is));

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                reader.close();
            } finally {
                is.close();
            }

            str = sb.toString();
        }

        return str;
    }

    public static Object_Image getImageFileUri(Context context) {

        File imagePath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "");
        if (!imagePath.exists()) {
            if (!imagePath.mkdirs()) {
                return null;
            } else {
                //create new folder
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageName = "MyProject_" + timeStamp + ".jpg";
        File image = new File(imagePath, imageName);

        if (!image.exists()) {
            try {
                image.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new Object_Image(FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", image), imageName);

    }

    public static String getCurrentTimeInHHmmss() {
        String delegate = "HH:mm:ss";
        return (String) DateFormat.format(delegate, Calendar.getInstance().getTime());
    }


    public static void focusEditTextRed(Context context, final SmartEditText smartEditText, Boolean showSnackBar, String errorMsg, final int drawable) {
        smartEditText.requestFocus();
        //  smartEditText.setBackgroundResource(R.drawable.bg_round_bordered_red);
        if (showSnackBar) {
            if (errorMsg != null) {
                SmartUtils.showSnackBar(context, errorMsg, Snackbar.LENGTH_LONG);
            } else {
                SmartUtils.showSnackBar(context, "Please fill the empty fields", Snackbar.LENGTH_LONG);
            }
        }
        // smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_1, 0);
        if (drawable != 0) {
            smartEditText.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, R.drawable.ic_error, 0);
        } else {
            smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0);
        }

        smartEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //   smartEditText.setBackgroundResource(R.drawable.bg_round_bordered_grey);
                if (drawable != 0) {
                    smartEditText.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
                } else {
                    smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    public static boolean checkAndfocusEditTextRed(Context context, final SmartEditText smartEditText, Boolean showSnackBar, String errorMsg, final int drawable) {
        if (TextUtils.isEmpty(smartEditText.getText().toString().trim())) {
            smartEditText.requestFocus();
            //  smartEditText.setBackgroundResource(R.drawable.bg_round_bordered_red);
            if (showSnackBar) {
                if (errorMsg != null) {
                    SmartUtils.showSnackBar(context, errorMsg, Snackbar.LENGTH_LONG);
                } else {
                    SmartUtils.showSnackBar(context, "Please fill the empty fields", Snackbar.LENGTH_LONG);
                }
            }
            // smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_1, 0);
            if (drawable != 0) {
                smartEditText.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, R.drawable.ic_error, 0);
            } else {
                smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0);
            }

            smartEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //   smartEditText.setBackgroundResource(R.drawable.bg_round_bordered_grey);
                    if (drawable != 0) {
                        smartEditText.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
                    } else {
                        smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            return false;
        } else {
            return true;
        }
    }

    public static void focusTextViewRed(Context context, final SmartTextView smartEditText, Boolean showSnackBar, String errorMsg) {
        smartEditText.requestFocus();
        //  smartEditText.setBackgroundResource(R.drawable.bg_round_bordered_red);
        if (showSnackBar) {
            if (errorMsg != null) {
                SmartUtils.showSnackBar(context, errorMsg, Snackbar.LENGTH_LONG);
            } else {
                SmartUtils.showSnackBar(context, "Please fill the empty fields", Snackbar.LENGTH_LONG);
            }
        }
        smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0);

        smartEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //   smartEditText.setBackgroundResource(R.drawable.bg_round_bordered_grey);
                smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    public static void focusEditTextRed2(Context context, final SmartEditText smartEditText, Boolean showSnackBar, String errorMsg) {
        smartEditText.requestFocus();

        if (showSnackBar) {
            if (errorMsg != null) {
                SmartUtils.showSnackBar(context, errorMsg, Snackbar.LENGTH_LONG);
            } else {
                SmartUtils.showSnackBar(context, "Please fill the empty fields", Snackbar.LENGTH_LONG);
            }
        }
        // smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_1, 0);

        smartEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static void focusEditTextRed3(Context context, final SmartEditText smartEditText, Boolean showSnackBar, String errorMsg) {
        smartEditText.requestFocus();
        //    smartEditText.setBackgroundResource(R.drawable.bg_round_bordered_red);
        if (showSnackBar) {
            if (errorMsg != null) {
                SmartUtils.showSnackBar(context, errorMsg, Snackbar.LENGTH_LONG);
            } else {
                SmartUtils.showSnackBar(context, "Please fill the empty fields", Snackbar.LENGTH_LONG);
            }
        }
        smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0);

        smartEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //    smartEditText.setBackgroundResource(R.drawable.bg_round_white);
                smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static void checkSetStoragePermission(Context context, Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED && context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_CODE);
        }
    }

    public static String decimalFormatter(String value) {
        value = value.replace(",", ".");
        return new DecimalFormat("0.00").format(Float.parseFloat(value));
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    public static String getFileExtensionFromUrlSmart(String url) {
        int dotPos = url.lastIndexOf('.');
        if (0 <= dotPos) {
            return (url.substring(dotPos + 1)).toLowerCase();
        }
        return "";
    }


    public static String getFileNameFromUrlSmart(String url) {
        int dotPos = url.lastIndexOf('/');
        if (0 <= dotPos) {
            return (url.substring(dotPos + 1));
        }
        return "";
    }

    public static void switchTabsColor(Context context, SmartTextView tabTextView, @DrawableRes int drawableId, boolean isSelected) {

        tabTextView.setCompoundDrawablesWithIntrinsicBounds(0, drawableId, 0, 0);
        if (isSelected) {
            tabTextView.setTextColor(ContextCompat.getColor(context, R.color.localisRedColor));
        } else {
            tabTextView.setTextColor(ContextCompat.getColor(context, R.color.grey));
        }
    }

    public static void showSortingPopup(final Context context, final View view, String[] popUpItems, final AlertMagneticClass.PopUpInterface popUpInterface) {

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_sort, null);
        LinearLayout viewGroup = layout.findViewById(R.id.popup);

        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setFocusable(true);
        int OFFSET_X = 0;
        int OFFSET_Y = 0;
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(view, OFFSET_X, OFFSET_Y);

        for (int i = 0; i < popUpItems.length; i++) {
            SmartTextView selectedTextTv = (SmartTextView) layoutInflater.inflate(R.layout.textview_group, null);
            selectedTextTv.setText(popUpItems[i]);
            viewGroup.addView(selectedTextTv);
            final int finalI = i;
            selectedTextTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popUpInterface.onSetPopUp(finalI);

                    popup.dismiss();
                }
            });
        }
    }


    public static void showSortingPopup(final Context context, final View view, ArrayList<PopUpMenuObject> popUpItems, final AlertMagneticClass.PopUpInterface popUpInterface) {

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_sort, null);
        LinearLayout viewGroup = layout.findViewById(R.id.popup);

        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setFocusable(true);
        int OFFSET_X = 0;
        int OFFSET_Y = 0;
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(view, OFFSET_X, OFFSET_Y);

        for (int i = 0; i < popUpItems.size(); i++) {
            SmartTextView selectedTextTv = (SmartTextView) layoutInflater.inflate(R.layout.textview_group, null);
            selectedTextTv.setText(popUpItems.get(i).getItemName());
            viewGroup.addView(selectedTextTv);
            final int finalI = i;
            selectedTextTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popUpInterface.onSetPopUp(finalI);

                    popup.dismiss();
                }
            });
        }
    }


    public static void showMenuPopup(final Context context, final View view, ArrayList<PopUpMenuObject> popUpItems, final AlertMagneticClass.PopUpInterface popUpInterface) {

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_sort, null);
        LinearLayout viewGroup = layout.findViewById(R.id.popup);

        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setFocusable(true);
        int OFFSET_X = 0;
        int OFFSET_Y = 0;
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(view, OFFSET_X, OFFSET_Y);

        for (int i = 0; i < popUpItems.size(); i++) {
            SmartTextView selectedTextTv = (SmartTextView) layoutInflater.inflate(R.layout.textview_group, null);
            selectedTextTv.setText(popUpItems.get(i).getItemName());
            viewGroup.addView(selectedTextTv);
            final int finalI = i;

            selectedTextTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popUpInterface.onSetPopUp(finalI);

                    popup.dismiss();
                }
            });
        }
    }

    public static void GetListDialog(Context context, final String[] dialogList, String s, boolean isCancelable, final AlertMagneticClass.AlertMagneticList alertMagneticList) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(s);
        builder.setItems(dialogList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                alertMagneticList.PositiveMethod(dialog, position);
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showFriendDialog(Context context, String s, String string, String ok, String cancel, boolean isCancelable, AlertMagnatic target) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(s).setView(R.drawable.bg_red_btn);
        builder.setView(R.layout.dialog_friend);

        AlertDialog alert = builder.create();
        alert.show();
    }


    public static void setAddButton(final Context context, final Class activity, FloatingActionButton floatingActionButton) {
        floatingActionButton.setVisibility(View.VISIBLE);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, activity);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
            }
        });
    }

    public static boolean checkEmptyEditTexts(Context context, final SmartEditText... smartEditText) {

        for (SmartEditText aSmartEditText : smartEditText) {
            if (TextUtils.isEmpty(aSmartEditText.getText().toString())) {
                errorEditTextRed(context, 0, false, null, aSmartEditText);
                return true;
            }
        }
        return false;
    }

    public static void errorEditTextRed(Context context, final int drawable, boolean showSnackBar, final String errorMsg, final SmartEditText smartEditText) {

        smartEditText.requestFocus();
        if (drawable != 0) {
            smartEditText.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, R.drawable.ic_error, 0);
        } else {
            smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0);
        }

        smartEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //   smartEditText.setBackgroundResource(R.drawable.bg_round_bordered_grey);
                if (drawable != 0) {
                    smartEditText.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
                } else {
                    smartEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (showSnackBar) {
            if (errorMsg != null) {
                SmartUtils.showSnackBar(context, errorMsg, Snackbar.LENGTH_LONG);
            } else {
                SmartUtils.showSnackBar(context, "Something went wrong", Snackbar.LENGTH_LONG);
            }
        }
    }


    public static boolean checkEmailEt(Context context, final SmartEditText editText) {
        if (!emailValidator(editText.getText().toString())) {
            editText.requestFocus();
            editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0);

            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            SmartUtils.showSnackBar(context, "Please enter a valid email address.", Snackbar.LENGTH_LONG);
            return false;
        } else {
            return true;
        }
    }


    public static boolean checkEmptyEt(Context context, final SmartEditText... focusedView) {
        boolean isEmpty = true;
        for (int i = 0; i < focusedView.length; i++) {
            if (TextUtils.isEmpty(focusedView[i].getText())) {
                isEmpty = false;
                focusedView[i].requestFocus();
                focusedView[i].setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0);

                final int finalI = i;
                focusedView[i].addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        focusedView[finalI].setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
            }
        }

        if (!isEmpty)
            SmartUtils.showSnackBar(context, "Please fill the empty fields.", Snackbar.LENGTH_LONG);

        return isEmpty;
    }

    public static ArrayList convertJsonToArray(JSONArray jsonArray, Class POJOclass) {
        ArrayList<Object> arrayToReturn = new ArrayList<>();
        Gson gson = new Gson();

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                arrayToReturn.add(gson.fromJson(jsonArray.getString(i), POJOclass));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrayToReturn;
    }

    public static String getUserId() {
//        return "47";
        return SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(USER_ID, null);
    }


    public static String getUserImage() {
//        return "47";
        try {
            return getUserData().getString("profile_image");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUserName() {
//        return "47";
        try {
            return getUserData().getString("first_name") + " " + getUserData().getString("last_name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getUserEmail() {
//        return "47";
        try {
            return getUserData().getString("email");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject getUserData() {
        try {
            return new JSONObject(SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(USER_DATA, null));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String booleanToInt(boolean isChecked) {
        if (isChecked)
            return "1";
        else
            return "0";
    }

}

