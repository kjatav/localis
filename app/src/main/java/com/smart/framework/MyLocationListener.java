package com.smart.framework;

import android.location.Location;

public interface MyLocationListener {

    void onReceived(Location mLastLocation);
}
