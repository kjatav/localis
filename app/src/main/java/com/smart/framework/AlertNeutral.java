package com.smart.framework;

import android.content.DialogInterface;

public interface AlertNeutral {

	public abstract void NeutralMethod(DialogInterface dialog, int id);
}
