package com.smart.framework;

public interface Constants {

    String TAG = "Universal Log TAG======";

    String IS_LOGGED_IN = "is_logged_in";
    String ENGLISH_STR = "en";
    String ENGLISH_CODE = "1";


    String SELECTED_LOCALE = "selected_local";


    int PERMISSIONS_REQUEST_CODE = 501;
    int SELECT_AREA = 101;
    int CAMERA_PROFILE_SOURCE = 234;
    int GALLERY_PROFILE_SOURCE = 235;
    int CAMERA_CAR_SOURCE = 236;
    int GALLERY_CAR_SOURCE = 237;

    String IS_SYNCED = "is_synced";

    String IS_REGISTER = "is_register";
    String HP_COMPLETED = "hpCompleted";


    /*-----------------SharedPreferences-----------*/
    String SP_COOKIES = "Set-Cookie";
    String SP_USER_LATITUDE = "userLatitude";
    String SP_USER_LONGITUDE = "userLongitude";
    String SP_USER_ID = "getUserId";
    String SP_USER_INFO = "getUserInfo";
    String SP_FIREBASE_REGID = "firebaseRegId";
    String NO_USER_ID = "no_user_id";
    String NO_DATA = "no_data";
    String CHAT_NOTIFICATION = "chat_notification";
    String CURRENT_OPPONENT_ID = "current_opponent_id";


    /* --------------Web Services-----------------*/

    String GET_PACKS = "get_packs";
    String UPDATE_PROFILE = "update_profile";
    String STATUS = "status";
    String CODE = "code";
    String ERROR = "error";
    String MESSAGE = "message";
    String RESULTS = "Results";
    String DATA = "data";



    /* --------------TAGS--------------------*/

    int STATUS_SUCCESS = 200;
    int STATUS_FAIL = 108;


    String USER_ID = "userId";

    String USER_DATA = "userData";

    String ANDROID = "Android";


    String IS_MEMBER = "isMember";

    String CONTENT = "content";
    String POST_TYPE = "post_type";
    String MAIN = "main";
    String COMPLAIN = "complain";
    String MAIN_MENU_POSITION = "main_menu_position";
    String SUB_MENU_POSITION = "sub_menu_position";

    String CONTENT_TYPE = "content_type";
    int STATUS_CODE = 0;
    int IMAGE_CODE = 1;
    int VIDEO_CODE = 2;

    int CHK_CODE = 0;
    int RB_CODE = 1;

    int TAG_FRIEND_CODE = 701;
    int SMILEY_CODE = 702;
    String TAGGED_FRIENDS_DATA = "tagged_friends_data";
    String SMILEY_DATA = "smiley_data";

    String FROM_BOTTOM_NAV = "from_bottom_nav";
    String PAST_EVENT_FLAG = "past_event_flag";
    String NEAR_BY_GROUP_FLAG = "near_by_group_flag";
    String GROUP_TYPE = "group_type";
    String GROUP_ID = "groupId";

    String ALL_GROUP_CODE = "1";
    String FEATURED_GROUP_CODE = "2";
    String MY_GROUP_CODE = "3";
    String NEARBY_GROUP_CODE = "4";

    String TABLE_STATES = "table_states";
    String TABLE_GROUP_CATEGORIES = "table_group_categories";


    String COUNTRY = "country";
    String ADDRESS_LINE_1 = "address_line_1";

    String WEB_PERFORM_STATES = "getStates";

    String DEVICE_TOKEN = "device_token";
    String DEVICE_TYPE = "device_type";

    String ACTION = "action";


    /* --------------REGISTRATION--------------------*/
    String USER_TYPE = "user_type";
    String FIRST_NAME = "firstname";
    String LAST_NAME = "lastname";
    String USER_NAME = "username";
    String EMAIL = "email";
    String PASSWORD = "password";
    String CITY = "city";
    String STATE = "state";
    String LATITUDE = "latitude";
    String LONGITUDE = "longitude";
    String ADDRESS = "address";
    String STREET = "street";
    String ASSOCIATE_GROUPS = "associate_groups";

    String PROFILE_TYPE_MEMBER_CODE = "1";
    String PROFILE_TYPE_INTITUTION_CODE = "3";


    /* --------------POP UP MENU ACTIONS--------------------*/
    int REPORT = 601;
    int EDIT = 602;
    int SET_AS_FEATURES = 603;
    int ADD_VIDEO = 604;
    int ADD_ALBUM = 605;
    int UNPUBLISH = 606;
    int DELETE = 607;
    int ADD = 608;

    /* --------------SORTING ACTIONS--------------------*/
    int BY_LATEST = 501;
    int BY_ALPHABETICALLY = 502;
    int BY_POPULARITY = 503;
    int BY_DISTANCE = 504;

}