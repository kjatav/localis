package com.localis.Complains.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.localis.R;

public class BrowseSubCHildAdapter extends RecyclerView.Adapter<BrowseSubCHildAdapter.MyViewHolder> {
    //    ArrayList<ImageObject> image_objects;
    Context context;

    public BrowseSubCHildAdapter(/*ArrayList<ImageObject> image_objects,*/ Context context) {
//        this.image_objects = image_objects;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_browse_category, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final BrowseSubCHildAdapter.MyViewHolder viewHolder = (BrowseSubCHildAdapter.MyViewHolder) holder;

        viewHolder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewHolder.rvChild.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RecyclerView rvChild;
        LinearLayout llParent;

        public MyViewHolder(View itemView) {
            super(itemView);

            rvChild = itemView.findViewById(R.id.rv_child);
            llParent = itemView.findViewById(R.id.llParent);
        }
    }
}