package com.localis.Complains.Activities;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Complains.Adapters.BadgesDetailAdapter;
import com.localis.R;
import com.smart.customViews.SmartTextView;


public class BadgesDetailsActivity extends MasterActivity {

    private RecyclerView badgedetailsRv;
    private Context context;
    private BadgesDetailAdapter badgesDetailAdapter;
    private SmartTextView headerToolBarTv;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_badges_details;
    }

    @Override
    public void initComponents() {
        super.initComponents();

        context = BadgesDetailsActivity.this;
        badgedetailsRv = findViewById(R.id.badgedetails_rv);
        headerToolBarTv = findViewById(R.id.header_toolbar_tv);
        headerToolBarTv.setAllCaps(false);
        headerToolBarTv.setTextSize(getResources().getDimension(R.dimen._16sdp));
        headerToolBarTv.setText("Badges Details");
        headerToolBarTv.setTextSize(getResources().getDimensionPixelSize(R.dimen._6sdp));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        badgedetailsRv.setLayoutManager(linearLayoutManager);

        badgesDetailAdapter = new BadgesDetailAdapter(context);
        badgedetailsRv.setAdapter(badgesDetailAdapter);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}