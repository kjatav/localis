package com.localis.Complains.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.CommentAdapter;
import com.localis.AA_POJOs.ImageObject;
import com.localis.Complains.Adapters.ComplainDetailAdapter;
import com.localis.R;

import java.util.ArrayList;

public class ComplainDetailActivity extends MasterActivity {

    private RecyclerView complainDetailRv1;
    private RecyclerView complainDetailRv2;
    ComplainDetailAdapter complainDetailAdapter;
    LinearLayoutManager linearLayoutManager, linearLayoutManager2;
    CommentAdapter pollCommentAdapter;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_complain_details;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.complain_details);

        complainDetailRv1 = findViewById(R.id.complain_detail_rv1);
        complainDetailRv2 = findViewById(R.id.complain_detail_rv2);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        complainDetailRv1.setLayoutManager(linearLayoutManager);
        complainDetailAdapter = new ComplainDetailAdapter(ComplainDetailActivity.this);
        complainDetailRv1.setAdapter(complainDetailAdapter);
        linearLayoutManager2 = new LinearLayoutManager(ComplainDetailActivity.this);
        complainDetailRv2.setLayoutManager(linearLayoutManager2);
        pollCommentAdapter = new CommentAdapter(new ArrayList<ImageObject>(), ComplainDetailActivity.this);
        complainDetailRv2.setAdapter(pollCommentAdapter);
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
