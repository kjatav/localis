package com.localis.Complains.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Complains.Adapters.BrowseTagAdapter;
import com.localis.R;

public class BrowseTagActivity extends MasterActivity {
    RecyclerView BrowseTagRv;
    BrowseTagAdapter browseTagAdapter;
    GridLayoutManager gridLayoutManager;

    @Override
    public int getLayoutID() {
        return R.layout.activity_browse_tags;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.browse_tags);

        BrowseTagRv = findViewById(R.id.browse_tag_rv);
        gridLayoutManager = new GridLayoutManager(getApplicationContext(), 7, LinearLayoutManager.VERTICAL, false);
        browseTagAdapter = new BrowseTagAdapter(BrowseTagActivity.this);
        BrowseTagRv.setAdapter(browseTagAdapter);
        BrowseTagRv.setLayoutManager(gridLayoutManager);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
    }
}
