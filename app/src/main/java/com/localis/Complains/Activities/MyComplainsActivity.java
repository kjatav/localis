package com.localis.Complains.Activities;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Complains.Adapters.MyComplainsAdapter;
import com.localis.R;
import com.smart.customViews.SmartTextView;


public class MyComplainsActivity extends MasterActivity {

    private Context context;
    private SmartTextView headerToolBarTv;
    MyComplainsAdapter myComplainsAdapter;
    private RecyclerView mydiscussionRv;

    @Override
    public int getLayoutID() {
        return R.layout.activity_my_complains;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        context = MyComplainsActivity.this;
        mydiscussionRv = findViewById(R.id.my_complains_rv);
        headerToolBarTv = findViewById(R.id.header_toolbar_tv);
        headerToolBarTv.setAllCaps(false);
        headerToolBarTv.setTextSize(getResources().getDimension(R.dimen._16sdp));
        headerToolBarTv.setText("My Complains");
        headerToolBarTv.setTextSize(getResources().getDimensionPixelSize(R.dimen._6sdp));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mydiscussionRv.setLayoutManager(linearLayoutManager);

        myComplainsAdapter = new MyComplainsAdapter(context);
        mydiscussionRv.setAdapter(myComplainsAdapter);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
    }
}