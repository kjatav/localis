package com.localis.Complains.Activities;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Complains.Adapters.BadgesAdapter;
import com.localis.R;
import com.smart.customViews.SmartTextView;


public class BrowseBadgesActivity extends MasterActivity {

    private RecyclerView badge_rv;
    private Context context;
    private BadgesAdapter badgesAdapter;
    private SmartTextView headerToolBarTv;

    @Override
    public int getLayoutID() {
        return R.layout.activity_browse_badges;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        context = BrowseBadgesActivity.this;
        badge_rv = findViewById(R.id.badge_rv);
        headerToolBarTv = findViewById(R.id.header_toolbar_tv);
        headerToolBarTv.setAllCaps(false);
        headerToolBarTv.setTextSize(getResources().getDimensionPixelSize(R.dimen._6sdp));
        headerToolBarTv.setText("Browse Badges");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        badge_rv.setLayoutManager(linearLayoutManager);

        badgesAdapter = new BadgesAdapter(context);
        badge_rv.setAdapter(badgesAdapter);

    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
    }
}