package com.localis.Discussions;

import com.localis.Discussions.POJOs.DiscussionsObject;

import java.util.ArrayList;
import java.util.Map;

public class DiscussionImp {

    public interface CreateDiscussionImpl {
        void onDiscussionCreated();

        void onDiscussionCreatedFailed();
    }

    public interface GetDiscussionsImpl {
        void getDiscussionSuccess(ArrayList<DiscussionsObject> discussions);

        void getDiscussionFailed();
    }


}
