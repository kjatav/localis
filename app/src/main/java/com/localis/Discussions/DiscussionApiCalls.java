package com.localis.Discussions;

import android.content.Context;

import com.android.volley.Request;
import com.google.gson.JsonObject;
import com.localis.Discussions.POJOs.DiscussionsObject;
import com.localis.R;
import com.smart.framework.SmartUtils;
import com.smart.weservice.SmartWebManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.smart.framework.Constants.DATA;
import static com.smart.framework.SmartUtils.convertJsonToArray;

public class DiscussionApiCalls {

    public static void createDiscussion(final Context context, Map<String, String> params, final DiscussionImp.CreateDiscussionImpl createAnnouncements) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_discussion) + "creatediscussion");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                //  SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences("user", results.toString());

                createAnnouncements.onDiscussionCreated();
            }

            @Override
            public void onResponseError(JSONObject results)  {
                SmartUtils.hideLoadingDialog();
                createAnnouncements.onDiscussionCreatedFailed();
            }

        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void getDiscussions(final Context context, Map<String, String> params, final DiscussionImp.GetDiscussionsImpl getDiscussions) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.GET);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_discussion) + "discussionlist");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    getDiscussions.getDiscussionSuccess((ArrayList<DiscussionsObject>) convertJsonToArray(results.getJSONArray(DATA), DiscussionsObject.class));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseError(JSONObject results)  {
                SmartUtils.hideLoadingDialog();
                getDiscussions.getDiscussionFailed();

            }


        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void getDiscussionDetails(final Context context, Map<String, String> params, final DiscussionImp.CreateDiscussionImpl createAnnouncements) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.GET);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_discussion) + "discussiondetail");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                //  SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences("user", results.toString());

                createAnnouncements.onDiscussionCreated();
            }

            @Override
            public void onResponseError(JSONObject results)  {
                SmartUtils.hideLoadingDialog();
                createAnnouncements.onDiscussionCreatedFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void likeDiscussion(final Context context, Map<String, String> params, final DiscussionImp.CreateDiscussionImpl createAnnouncements) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_discussion) + "likediscussion");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                //  SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences("user", results.toString());

                createAnnouncements.onDiscussionCreated();
            }

            @Override
            public void onResponseError(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                createAnnouncements.onDiscussionCreatedFailed();

            }

        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


}
