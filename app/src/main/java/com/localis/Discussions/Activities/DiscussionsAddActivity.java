package com.localis.Discussions.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Discussions.DiscussionApiCalls;
import com.localis.Discussions.DiscussionImp;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;

import java.util.HashMap;
import java.util.Map;

public class DiscussionsAddActivity extends MasterActivity {

    private SmartEditText tvTitleAnnounce;
    private SmartEditText tvDescAnnounce;
    private SmartButton btnSubmitAnnounce;
    private SmartButton btnCancelAnnounce;


    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_announcement_add;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.create_discussion);

        tvTitleAnnounce = findViewById(R.id.tv_title_announce);
        tvDescAnnounce = findViewById(R.id.tv_desc_announce);
        btnSubmitAnnounce = findViewById(R.id.btn_submit_announce);
        btnCancelAnnounce = findViewById(R.id.btn_cancel_announce);

    }


    @Override
    public void setActionListeners() {
        super.setActionListeners();

        btnSubmitAnnounce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> params = new HashMap<>();
                params.put("UserID", "47");
                params.put("Title", tvTitleAnnounce.getText().toString());
                params.put("Description", tvDescAnnounce.getText().toString());
                params.put("GroupID", "73");

                DiscussionApiCalls.createDiscussion(DiscussionsAddActivity.this, params, new DiscussionImp.CreateDiscussionImpl() {
                    @Override
                    public void onDiscussionCreated() {
                        Toast.makeText(DiscussionsAddActivity.this, "Done this", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onDiscussionCreatedFailed() {

                    }
                });
            }
        });

    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
