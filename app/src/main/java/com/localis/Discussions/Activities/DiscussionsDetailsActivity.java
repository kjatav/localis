package com.localis.Discussions.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.CommentAdapter;
import com.localis.AA_POJOs.ImageObject;
import com.localis.Announcements.Activities.AnnouncementDetailsActivity;
import com.localis.R;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartEditText;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;

public class DiscussionsDetailsActivity extends MasterActivity {

    private SmartTextView tvSubjectDiscussion;
    private ImageView ivMenuDiscussion;
    private SmartTextView tvDescDiscussion;
    private SmartTextView tvNameDiscussion;
    private SmartTextView tvDateDiscussion;
    private RecyclerView rvCommentsDiscussion;
    private RoundedImageView ivDpDiscussion;
    private SmartEditText etCommentDiscussion;
    private ImageView ivSendDiscussion;

    private CommentAdapter pollCommentAdapter;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }


    @Override
    public int getLayoutID() {
        return R.layout.activity_annoucement_details;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.discussion_details);

        tvSubjectDiscussion = findViewById(R.id.tv_subject_discussion);
        ivMenuDiscussion = findViewById(R.id.iv_menu_discussion);
        tvDescDiscussion = findViewById(R.id.tv_desc_discussion);
        tvNameDiscussion = findViewById(R.id.tv_name_discussion);
        tvDateDiscussion = findViewById(R.id.tv_date_discussion);
        rvCommentsDiscussion = findViewById(R.id.rv_comments_discussion);
        ivDpDiscussion = findViewById(R.id.iv_dp_discussion);
        etCommentDiscussion = findViewById(R.id.et_comment_discussion);
        ivSendDiscussion = findViewById(R.id.iv_send_discussion);


        rvCommentsDiscussion.setLayoutManager(new LinearLayoutManager(DiscussionsDetailsActivity.this));
        pollCommentAdapter = new CommentAdapter(new ArrayList<ImageObject>(), DiscussionsDetailsActivity.this);
        rvCommentsDiscussion.setAdapter(pollCommentAdapter);
    }

    private void getDiscussionDetails(){

    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
