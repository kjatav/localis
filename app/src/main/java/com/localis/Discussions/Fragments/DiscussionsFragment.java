package com.localis.Discussions.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.localis.Discussions.Adapters.DiscussionsAdapter;
import com.localis.Discussions.DiscussionApiCalls;
import com.localis.Discussions.DiscussionImp;
import com.localis.Discussions.POJOs.DiscussionsObject;
import com.localis.R;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DiscussionsFragment extends SmartFragment {

    RecyclerView rv_announcement;
    DiscussionsAdapter mDiscussionsAdapter;


    @Override
    public int setLayoutId() {
        return R.layout.fragment_group_announce;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        rv_announcement = currentView.findViewById(R.id.rv_announcement);

        rv_announcement.setLayoutManager(new LinearLayoutManager(getActivity()));

        setDiscussionData();

        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }


    @Override
    public void setActionListeners(View currentView) {

    }

    private void setDiscussionData() {

        Map<String, String> params = new HashMap<>();
        params.put("UserID", "47");
        params.put("GroupID", "73");

        DiscussionApiCalls.getDiscussions(getActivity(), params, new DiscussionImp.GetDiscussionsImpl() {
            @Override
            public void getDiscussionSuccess(ArrayList<DiscussionsObject> discussions) {
                mDiscussionsAdapter = new DiscussionsAdapter(getActivity(), discussions);
                rv_announcement.setAdapter(mDiscussionsAdapter);
            }

            @Override
            public void getDiscussionFailed() {

            }
        });
    }
}
