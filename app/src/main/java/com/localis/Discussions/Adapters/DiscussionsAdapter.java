package com.localis.Discussions.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localis.Discussions.Activities.DiscussionsDetailsActivity;
import com.localis.Discussions.POJOs.DiscussionsObject;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.List;

public class DiscussionsAdapter extends RecyclerView.Adapter<DiscussionsAdapter.MyViewHolder> {
    private List<DiscussionsObject> discussions;
    private Context context;


    public DiscussionsAdapter(Context context, List<DiscussionsObject> discussions) {
        this.discussions = discussions;
        this.context = context;
    }

    @NonNull
    @Override
    public DiscussionsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new DiscussionsAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_announcement, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DiscussionsAdapter.MyViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, DiscussionsDetailsActivity.class));
            }
        });

        holder.tvSubject.setText(discussions.get(position).getTitle());
        holder.tvDesc.setText(discussions.get(position).getContent());
        holder.tvName.setText(discussions.get(position).getName());
//        holder.tvDate.setText(discussions.get(position).get());

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return discussions.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private SmartTextView tvSubject;
        private ImageView ivMenu;
        private SmartTextView tvDesc;
        private SmartTextView tvName;
        private SmartTextView tvDate;


        public MyViewHolder(View itemView) {
            super(itemView);
            tvSubject = itemView.findViewById(R.id.tv_subject);
            ivMenu = itemView.findViewById(R.id.iv_menu);
            tvDesc = itemView.findViewById(R.id.tv_desc);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDate = itemView.findViewById(R.id.tv_date);


        }
    }
}
