package com.localis.AA_Adapter;

/**
 * Created by ebiztrait321 on 9/3/18.
 */

public class DataBindingTestObject {

    String firstName;

    public DataBindingTestObject() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
