package com.localis.AA_Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.R;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartTextView;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    Context context;


    public NotificationAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView profileNotificationItemIv;
        private SmartTextView titleNotificationItemTv;
        private SmartTextView timeNotificationItemTv;
        private SmartTextView descNotificationItemTv;


        public MyViewHolder(View itemView) {
            super(itemView);
            profileNotificationItemIv = itemView.findViewById(R.id.profile_notification_item_iv);
            titleNotificationItemTv = itemView.findViewById(R.id.title_notification_item_tv);
            timeNotificationItemTv = itemView.findViewById(R.id.time_notification_item_tv);
            descNotificationItemTv = itemView.findViewById(R.id.desc_notification_item_tv);

        }
    }
}
