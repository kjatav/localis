package com.localis.AA_Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.AA_POJOs.RequestObject;
import com.localis.R;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.MyViewHolder> {
    ArrayList<RequestObject> requestObjects;
    Context context;

    public RequestAdapter(ArrayList<RequestObject> requestObjects, Context context) {
        requestObjects = requestObjects;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_request_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView profileRequestItemIv;
        private SmartTextView nameRequestItemIv;
        private SmartTextView typeRequestItemIv;
        private SmartTextView descRequestItemIv;
        private SmartButton acceptRequestItemBtn;
        private SmartButton declineRequestItemBtn;


        public MyViewHolder(View itemView) {
            super(itemView);
            profileRequestItemIv = itemView.findViewById(R.id.profile_request_item_iv);
            nameRequestItemIv = itemView.findViewById(R.id.name_request_item_iv);
            typeRequestItemIv = itemView.findViewById(R.id.type_request_item_iv);
            descRequestItemIv = itemView.findViewById(R.id.desc_request_item_iv);
            acceptRequestItemBtn = itemView.findViewById(R.id.accept_request_item_btn);
            declineRequestItemBtn = itemView.findViewById(R.id.decline_request_item_btn);
        }
    }
}
