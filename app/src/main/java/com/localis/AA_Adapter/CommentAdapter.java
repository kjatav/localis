package com.localis.AA_Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localis.AA_POJOs.ImageObject;
import com.localis.R;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartUtils;

import java.util.ArrayList;


public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {
    private ArrayList<ImageObject> image_objects;
    private Context context;
    private String[] options = {"Report", "Edit", "Remove"};


    public CommentAdapter(ArrayList<ImageObject> image_objects, Context context) {
        this.image_objects = image_objects;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CommentAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.optionsCommentIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SmartUtils.showSortingPopup(context, holder.optionsCommentIv, options, new AlertMagneticClass.PopUpInterface() {
                    @Override
                    public void onSetPopUp(int selectedOptionPosition) {

                    }
                });
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView profileCommentIv;
        private SmartTextView nameCommentTv;
        private ImageView optionsCommentIv;
        private SmartTextView descCommentTv;
        private SmartTextView likeCommentTv;
        private SmartTextView reactionCommentTv;
        private SmartTextView timeCommentTv;


        public MyViewHolder(View itemView) {
            super(itemView);
            profileCommentIv = itemView.findViewById(R.id.profile_comment_iv);
            nameCommentTv = itemView.findViewById(R.id.name_comment_tv);
            timeCommentTv = itemView.findViewById(R.id.time_comment_tv);
            descCommentTv = itemView.findViewById(R.id.desc_comment_tv);
            likeCommentTv = itemView.findViewById(R.id.like_comment_tv);
            optionsCommentIv = itemView.findViewById(R.id.options_comment_iv);
            reactionCommentTv = itemView.findViewById(R.id.reaction_comment_tv);
        }
    }
}
