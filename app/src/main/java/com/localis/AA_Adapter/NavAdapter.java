package com.localis.AA_Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Authentication.Activities.LoginActivity;
import com.localis.AA_POJOs.NavItemObject;
import com.localis.Authentication.AuthImplementation;
import com.localis.Authentication.AuthenticationApiCalls;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;
import java.util.List;

import static com.smart.framework.Constants.COMPLAIN;
import static com.smart.framework.Constants.CONTENT;
import static com.smart.framework.Constants.MAIN;
import static com.smart.framework.Constants.MAIN_MENU_POSITION;
import static com.smart.framework.Constants.SUB_MENU_POSITION;


public class NavAdapter extends RecyclerView.Adapter<NavAdapter.MyViewHolder> {
    private Context context;
    private List<NavItemObject> navItemObjects;
    private int mainMenuPosition;
    private int subMenuPosition;
    private String content;
    private AuthImplementation.Logout logout;

    public NavAdapter(Context context, List<NavItemObject> navItemObjects, int mainMenuPosition, int subMenuPosition, String content, AuthImplementation.Logout logout) {
        this.context = context;
        this.navItemObjects = navItemObjects;
        this.mainMenuPosition = mainMenuPosition;
        this.subMenuPosition = subMenuPosition;
        this.content = content;
        this.logout = logout;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_nav_item, parent, false));
    }

    @Override
    public void onBindViewHolder(NavAdapter.MyViewHolder holder, final int position) {
        if (content.equalsIgnoreCase("main")) {
            if (position == mainMenuPosition) {
                holder.titleNavTv.setSelected(true);
            } else {
                holder.titleNavTv.setSelected(false);
            }
        } else {
            if (position == subMenuPosition) {
                holder.titleNavTv.setSelected(true);
            } else {
                holder.titleNavTv.setSelected(false);
            }
        }

        holder.titleNavTv.setText(navItemObjects.get(position).getTitleNav());

        Drawable endDrawable;
        if (navItemObjects.get(position).getEndDrawable() != 0) {
            endDrawable = context.getResources().getDrawable(navItemObjects.get(position).getEndDrawable());
        } else {
            endDrawable = null;
        }


        holder.titleNavTv.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(navItemObjects.get(position).getStartDrawable()),
                null,
                endDrawable,
                null);

        holder.titleNavTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (navItemObjects.get(position).getIntentClass() != null) {
                    Intent i = new Intent(context, navItemObjects.get(position).getIntentClass());
                    if (content.equalsIgnoreCase(MAIN)) {
                        i.putExtra(MAIN_MENU_POSITION, position);
                    } else if (content.equalsIgnoreCase(COMPLAIN)) {
                        i.putExtra(MAIN_MENU_POSITION, mainMenuPosition);
                        i.putExtra(SUB_MENU_POSITION, position);
                        i.putExtra(CONTENT, COMPLAIN);
                    }
                    context.startActivity(i);
                } else if (context.getString(navItemObjects.get(position).getTitleNav()).equalsIgnoreCase(context.getString(R.string.complains))) {
                    MasterActivity activity = (MasterActivity) context;
                    activity.content = COMPLAIN;
                    activity.mainMenuPosition = position;
                    activity.setMenuItems();
                } else if (context.getString(navItemObjects.get(position).getTitleNav()).equalsIgnoreCase("Logout")) {
                    AuthenticationApiCalls.logout(context, logout);
                }


            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return navItemObjects.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        SmartTextView titleNavTv;

        public MyViewHolder(View itemView) {
            super(itemView);
            titleNavTv = itemView.findViewById(R.id.title_nav_tv);

        }
    }
}