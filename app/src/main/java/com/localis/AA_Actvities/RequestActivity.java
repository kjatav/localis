package com.localis.AA_Actvities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.localis.AA_Adapter.RequestAdapter;
import com.localis.AA_POJOs.RequestObject;
import com.localis.R;

import java.util.ArrayList;

public class RequestActivity extends MasterActivity {
    private ArrayList<RequestObject> requestObjects;
    private RequestAdapter requestAdapter;
    private RecyclerView request_rv;


    @Override
    public int getLayoutID() {
        return R.layout.activity_request;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.request);
        setupBottomNav(4);

        request_rv = findViewById(R.id.request_rv);
        requestAdapter = new RequestAdapter(requestObjects, RequestActivity.this);
        request_rv.setAdapter(requestAdapter);
        request_rv.setLayoutManager(new LinearLayoutManager(RequestActivity.this));
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
    }
}
