package com.localis.AA_Actvities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.maps.MapFragment;
import com.localis.AA_Adapter.NavAdapter;
import com.localis.AA_POJOs.NavItemObject;
import com.localis.Authentication.Activities.LoginActivity;
import com.localis.Authentication.AuthImplementation;
import com.localis.Complains.Activities.BrowseBadgesActivity;
import com.localis.Complains.Activities.BrowseComplains;
import com.localis.Complains.Activities.BrowseTagActivity;
import com.localis.Complains.Activities.MyComplainsActivity;
import com.localis.Dashboard.Activities.DashboardActivity;
import com.localis.Events.Activities.EventListActivity;
import com.localis.Groups.Activites.GroupListActivity;
import com.localis.Pages.Activites.PageListActivity;
import com.localis.Polls.Activities.PollsListActivity;
import com.localis.Profile.Activities.AccountSettingActivity;
import com.localis.R;
import com.localis.Surveys.Activities.SurveyListActivity;
import com.localis.Videos.Activities.VideoListActivity;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartTextView;
import com.smart.framework.SmartSuperMaster;
import com.smart.framework.SmartUtils;
import com.squareup.picasso.Picasso;

import org.apache.http.util.TextUtils;

import java.util.ArrayList;
import java.util.List;

import static com.smart.framework.SmartUtils.getUserEmail;
import static com.smart.framework.SmartUtils.getUserImage;
import static com.smart.framework.SmartUtils.getUserName;


public abstract class MasterActivity extends SmartSuperMaster implements AuthImplementation.Logout {


    private SmartTextView headerToolbarTv;

    private LinearLayout navHeaderLl;

    private RoundedImageView profileNavIv;

    private SmartTextView nameNavTv;
    private SmartTextView emailNavTv;
    private SmartTextView mainNavItemTv;

    private RecyclerView itemNavRv;

    List<NavItemObject> navItemObjectArrayList;
    List<NavItemObject> complainItems;

    public String content;
    public int mainMenuPosition;
    public int subMenuPosition;

    private NavAdapter navAdapter;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void setAnimations() {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        getWindow().setAllowEnterTransitionOverlap(true);
        getWindow().setAllowReturnTransitionOverlap(true);
    }

    @Override
    public void preOnCreate() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA},
                    PERMISSIONS_REQUEST_CODE);
            return;
        }
    }


    @Override
    public void initComponents() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        headerToolbarTv = findViewById(R.id.header_toolbar_tv);

        if (getDrawerLayoutID() != 0) {

            navHeaderLl = findViewById(R.id.nav_header_ll);
            profileNavIv = findViewById(R.id.profile_nav_iv);
            nameNavTv = findViewById(R.id.name_nav_tv);
            emailNavTv = findViewById(R.id.email_nav_tv);
            mainNavItemTv = findViewById(R.id.main_nav_item_tv);
            itemNavRv = findViewById(R.id.item_nav_rv);

            nameNavTv.setText(getUserName());
            emailNavTv.setText(getUserEmail());
            if (!TextUtils.isEmpty(getUserImage())) {
                Picasso.with(MasterActivity.this).load(getUserImage()).placeholder(R.drawable.ic_detail_user).into(profileNavIv);
            }


            complainItems = new ArrayList<>();
            complainItems.add(new NavItemObject(R.drawable.ic_sidemenu_dot_60x60, R.string.edit_profile, 0, AccountSettingActivity.class));
            complainItems.add(new NavItemObject(R.drawable.ic_sidemenu_dot_60x60, R.string.view_my_subscription, 0, null));
            complainItems.add(new NavItemObject(R.drawable.ic_sidemenu_dot_60x60, R.string.my_complains, 0, MyComplainsActivity.class));
            complainItems.add(new NavItemObject(R.drawable.ic_sidemenu_dot_60x60, R.string.browse_complains, 0, BrowseComplains.class));
            complainItems.add(new NavItemObject(R.drawable.ic_sidemenu_dot_60x60, R.string.browse_categories, 0, null));
            complainItems.add(new NavItemObject(R.drawable.ic_sidemenu_dot_60x60, R.string.browse_tags, 0, BrowseTagActivity.class));
            complainItems.add(new NavItemObject(R.drawable.ic_sidemenu_dot_60x60, R.string.browse_badges, 0, BrowseBadgesActivity.class));


            navItemObjectArrayList = new ArrayList<>();
            navItemObjectArrayList.add(new NavItemObject(R.drawable.ic_sidemenu_dashboard, R.string.dashboard, 0, DashboardActivity.class));
            navItemObjectArrayList.add(new NavItemObject(R.drawable.ic_sidemenu_video, R.string.videos, 0, VideoListActivity.class));
            navItemObjectArrayList.add(new NavItemObject(R.drawable.ic_sidemenu_event, R.string.events, 0, EventListActivity.class));
            navItemObjectArrayList.add(new NavItemObject(R.drawable.ic_sidemenu_group, R.string.groups, 0, GroupListActivity.class));
            navItemObjectArrayList.add(new NavItemObject(R.drawable.ic_sidemenu_pages, R.string.pages, 0, PageListActivity.class));
            navItemObjectArrayList.add(new NavItemObject(R.drawable.ic_sidemenu_discussion, R.string.complains, R.drawable.ic_right_arrow, null));
            navItemObjectArrayList.add(new NavItemObject(R.drawable.ic_sidemenu_survey, R.string.survey, 0, SurveyListActivity.class));
            navItemObjectArrayList.add(new NavItemObject(R.drawable.ic_sidemenu_polls, R.string.polls, 0, PollsListActivity.class));
            navItemObjectArrayList.add(new NavItemObject(R.drawable.ic_sidemenu_logout, R.string.logout, 0, null));

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MasterActivity.this);
            itemNavRv.setLayoutManager(linearLayoutManager);

            if (getIntent().hasExtra(CONTENT)) {
                content = getIntent().getStringExtra(CONTENT);
            } else {
                content = MAIN;
            }
            mainMenuPosition = getIntent().getIntExtra(MAIN_MENU_POSITION, 0);
            subMenuPosition = getIntent().getIntExtra(SUB_MENU_POSITION, -1);

            setMenuItems();
        } else {
            drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        }
    }


    public void setMenuItems() {
        if (content.equalsIgnoreCase(MAIN)) {
            navAdapter = new NavAdapter(MasterActivity.this, navItemObjectArrayList, mainMenuPosition, subMenuPosition, MAIN, this);
        } else if (content.equalsIgnoreCase(COMPLAIN)) {
            setMainNavItem(getString(R.string.complains), R.drawable.ic_sidemenu_discussion);
            navAdapter = new NavAdapter(MasterActivity.this, complainItems, mainMenuPosition, subMenuPosition, COMPLAIN, null);
        }
        itemNavRv.setAdapter(navAdapter);
    }


    private void setMainNavItem(String title, int drawableId) {
        mainNavItemTv.setVisibility(View.VISIBLE);
        mainNavItemTv.setText(title);
        mainNavItemTv.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(drawableId),
                null,
                getResources().getDrawable(R.drawable.ic_left_arrow),
                null);
        mainNavItemTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content = MAIN;
                setMenuItems();
                mainNavItemTv.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void prepareViews() {
       /* if (getDrawerLayoutID() != 0) {
            JSONObject user = SmartUtils.getUser();
            if (user != null) {
                //   setNavDetails(SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(PROFILE_IMAGE,NO_DATA));
            }
        }*/
    }


    /*public void setNavDetails(String img_url) {
        if (!TextUtils.isEmpty(img_url))
            Picasso.with(this).load(img_url).placeholder(R.drawable.ic_profile_place_holder).into(userProfileNav);
        else {
            userProfileNav.setImageResource(R.drawable.ic_profile_place_holder);
        }
//        userNameNav.setText(first_name + " " + last_name);
    }*/


    @Override
    public void setActionListeners() {
        if (getDrawerLayoutID() != 0) {
          /*  navHeaderLl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MasterActivity.this, ProfileActivity.class));
                }
            });*/
        }
    }


    public void setHeaderToolbar(@StringRes int heading) {
        headerToolbarTv.setText(heading);
    }


    @Override
    public void postOnCreate() {
    }

    @Override
    public View getLayoutView() {
        return null;
    }

    @Override
    public View getHeaderLayoutView() {
        return null;
    }

    @Override
    public int getHeaderLayoutID() {
        return 0;
    }

    @Override
    public View getFooterLayoutView() {
        return null;
    }

    @Override
    public int getFooterLayoutID() {
        return 0;
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {

    }


    @Override
    public int getDrawerLayoutID() {
        return R.layout.drawer;
    }

    @Override
    public boolean shouldKeyboardHideOnOutsideTouch() {
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onLogoutSuccess(String responseMessage) {
        Intent intent = new Intent(MasterActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLogoutFail(String responseMessage) {

    }
}