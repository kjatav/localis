package com.localis.AA_Actvities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Adapter.NotificationAdapter;
import com.localis.R;

public class NotificationActivity extends MasterActivity {

    private RecyclerView notificationRv;
    private NotificationAdapter notificationAdapter;


    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_notification;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.notifications);

        notificationRv = findViewById(R.id.notification_rv);

        notificationRv.setLayoutManager(new LinearLayoutManager(NotificationActivity.this));
        notificationAdapter = new NotificationAdapter(NotificationActivity.this);
        notificationRv.setAdapter(notificationAdapter);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
