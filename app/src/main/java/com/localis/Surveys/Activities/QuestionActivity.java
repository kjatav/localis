package com.localis.Surveys.Activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.localis.Surveys.Adapters.SurveyQuestionAdapter;
import com.localis.Surveys.POJOS.SurveyAnsObject;
import com.localis.Surveys.POJOS.SurveyQueObject;

import java.util.ArrayList;

public class QuestionActivity extends MasterActivity {
    ImageView btn_add_question;
    RecyclerView rv_question;
    SurveyQuestionAdapter mSurveyQuestionAdapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<SurveyQueObject> survey_Question_objects;
    ArrayList<SurveyAnsObject> survey_Answer_objects;
    SurveyQueObject surveyQuestionObject;
    SurveyAnsObject surveyAnswerObject;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_question;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.question);
        btn_add_question = findViewById(R.id.btn_add_question);
        rv_question = findViewById(R.id.rv_question);
        survey_Question_objects = new ArrayList<>();


        for (int i = 0; i < 10; i++) {
            surveyQuestionObject = new SurveyQueObject();
            surveyQuestionObject.setQuestionId(String.valueOf(i));
            surveyQuestionObject.setQuestionContent(getString(R.string.lorem_ipsum_donor_lorem_ipsum_donor_lorem_ipsum_donor_lorem_ipsum_donor));
            surveyQuestionObject.setQuestionDesc("abcsdfgfgg");
            if (i % 2 == 0) {
                surveyQuestionObject.setQuestionType(RB_CODE);
            } else {
                surveyQuestionObject.setQuestionType(CHK_CODE);
            }
            survey_Answer_objects = new ArrayList<>();
            for (int j = 0; j < 5; j++) {

                surveyAnswerObject = new SurveyAnsObject();
                surveyAnswerObject.setAnsContent(getString(R.string.answer));
                surveyAnswerObject.setAnsId(String.valueOf(1));
                survey_Answer_objects.add(surveyAnswerObject);
            }

            surveyQuestionObject.setSurveyAnsObject(survey_Answer_objects);

            survey_Question_objects.add(surveyQuestionObject);
        }

        linearLayoutManager = new LinearLayoutManager(this);
        rv_question.setLayoutManager(linearLayoutManager);
        mSurveyQuestionAdapter = new SurveyQuestionAdapter(survey_Question_objects, this);
        rv_question.setAdapter(mSurveyQuestionAdapter);


        btn_add_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuestionActivity.this, QuestionAddActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
