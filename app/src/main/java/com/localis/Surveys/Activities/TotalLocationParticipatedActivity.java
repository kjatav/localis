package com.localis.Surveys.Activities;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.localis.Surveys.Adapters.TotalCompletedResponseAdapter;
import com.localis.Surveys.Adapters.TotalLocationParticipatedAdapter;

public class TotalLocationParticipatedActivity extends MasterActivity {
    Context mContext;
    RecyclerView location_participated_rv;
    LinearLayoutManager linearLayoutManager;
    TotalLocationParticipatedAdapter mParticipatedAdpter;

    @Override
    public int getLayoutID() {
        return R.layout.activity_total_location_participated;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        mContext = TotalLocationParticipatedActivity.this;

        setHeaderToolbar(R.string.total_locations_participated);
        location_participated_rv=findViewById(R.id.location_participated_rv);
        linearLayoutManager = new LinearLayoutManager(this);
        location_participated_rv.setLayoutManager(linearLayoutManager);

        mParticipatedAdpter = new TotalLocationParticipatedAdapter(this);
        location_participated_rv.setAdapter(mParticipatedAdpter);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
