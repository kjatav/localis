package com.localis.Surveys.Activities;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.localis.Surveys.Adapters.TotalDeviceUsedAdapter;
import com.localis.Surveys.Adapters.TotalResponseAdapter;

public class TotalResponceActivity extends MasterActivity {

    RecyclerView totalResponseRv;
    LinearLayoutManager linearLayoutManager;
    TotalResponseAdapter totalResponseAdapter;
    Context mContext;

    @Override
    public int getLayoutID() {
        return R.layout.activity_total_responce;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        mContext = TotalResponceActivity.this;
        setHeaderToolbar(R.string.total_responses);

        totalResponseRv = findViewById(R.id.total_response_rv);
        linearLayoutManager = new LinearLayoutManager(this);
        totalResponseRv.setLayoutManager(linearLayoutManager);

        totalResponseAdapter = new TotalResponseAdapter(this);
        totalResponseRv.setAdapter(totalResponseAdapter);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
