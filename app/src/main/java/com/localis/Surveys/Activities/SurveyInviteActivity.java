package com.localis.Surveys.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Groups.POJOs.GroupMemberObject;
import com.localis.R;
import com.localis.Surveys.Adapters.SurveyFriendItemAdapter;

import java.util.ArrayList;
import java.util.List;

public class SurveyInviteActivity extends MasterActivity {
    private RecyclerView rvFriend;
    SurveyFriendItemAdapter mSearchAdapter;
    LinearLayoutManager linearLayoutManager;
    private List<GroupMemberObject> FriendObjects = new ArrayList<>();

    @Override
    public int getLayoutID() {
        return R.layout.activity_survey_invite;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.friend);

        rvFriend = findViewById(R.id.rv_friend);
        linearLayoutManager = new LinearLayoutManager(this);
        rvFriend.setLayoutManager(linearLayoutManager);
        mSearchAdapter = new SurveyFriendItemAdapter(this, FriendObjects);
        rvFriend.setAdapter(mSearchAdapter);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.done_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

}
