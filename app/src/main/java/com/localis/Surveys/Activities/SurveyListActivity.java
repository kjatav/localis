package com.localis.Surveys.Activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.localis.Surveys.Fragments.SurveyListFragment;
import com.localis.Surveys.Fragments.MyResponceFragment;

import java.util.ArrayList;
import java.util.List;

public class SurveyListActivity extends MasterActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    FloatingActionButton add_surveys_btn;

    @Override
    public int getLayoutID() {
        return R.layout.activity_survey;
    }

    @Override
    public void initComponents() {
        super.initComponents();

        setHeaderToolbar(R.string.surveys);
        viewPager = findViewById(R.id.survey_viewpager);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.survey_sliding_tabs);
        add_surveys_btn = findViewById(R.id.add_survey_btn);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        add_surveys_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SurveyListActivity.this, SurveyAddActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SurveyListFragment(), "All Surveys");
        adapter.addFragment(new SurveyListFragment(), "My Surveys");
        adapter.addFragment(new SurveyListFragment(), "Popular Surveys");
        adapter.addFragment(new MyResponceFragment(), "My Responses");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
    }
}
