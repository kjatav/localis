package com.localis.Surveys.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.localis.Surveys.Adapters.TotalCompletedResponseAdapter;

public class SurveyCompletedResActivity extends MasterActivity {
    RecyclerView CompletedResponseRv;
    LinearLayoutManager linearLayoutManager;
    TotalCompletedResponseAdapter totalCompletedResponseAdapter;

    @Override
    public int getLayoutID() {
        return R.layout.activity_survey_completed_res;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.total_completed_responses);

        CompletedResponseRv=findViewById(R.id.completed_response_rv);
        linearLayoutManager = new LinearLayoutManager(this);
        CompletedResponseRv.setLayoutManager(linearLayoutManager);

        totalCompletedResponseAdapter = new TotalCompletedResponseAdapter(this);
        CompletedResponseRv.setAdapter(totalCompletedResponseAdapter);
    }
    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
