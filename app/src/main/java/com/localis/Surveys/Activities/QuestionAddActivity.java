package com.localis.Surveys.Activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.localis.Surveys.Fragments.AnswerQueFragment;
import com.localis.Surveys.Fragments.ContentQueFragment;
import com.smart.customViews.SmartTextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QuestionAddActivity extends MasterActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    SmartTextView btnsaveQuestion,btnCancelQuestion;
    ImageView btn_add_Ques;
    ContentQueFragment mContentQueFragment;
    AnswerQueFragment mAnswerQueFragment;
    @Override
    public int getLayoutID() {
        return R.layout.activity_question_add;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.enter_question);
        viewPager = findViewById(R.id.QuestionAdd_viewpager);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.QuestionAdd_sliding_tabs);
        btn_add_Ques = findViewById(R.id.add_survey_btn);
        tabLayout.setupWithViewPager(viewPager);
        btnsaveQuestion=findViewById(R.id.btnsaveQuestion);
        btnCancelQuestion=findViewById(R.id.btnCancelQuestion);
        btnsaveQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuestionAddActivity.this, QuestionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        btnCancelQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuestionAddActivity.this, QuestionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ContentQueFragment(), "Content");
        adapter.addFragment(new AnswerQueFragment(), "Answers");
        viewPager.setAdapter(adapter);
    }
    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
