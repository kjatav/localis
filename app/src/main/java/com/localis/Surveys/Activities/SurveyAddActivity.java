package com.localis.Surveys.Activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.localis.Surveys.Fragments.SurveyContentFragment;
import com.localis.Surveys.Fragments.SurveyOptionFragment;
import com.localis.Surveys.Fragments.SurveySetupFragment;
import com.localis.Surveys.Fragments.SurveysExtendedOptionFragment;
import com.localis.Surveys.Fragments.SurveysLanguagesFragment;
import com.localis.Surveys.Fragments.SurveysPublishingFragment;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;
import java.util.List;

public class SurveyAddActivity extends MasterActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    SmartTextView btnSaveNextSurvey, btnCancelSurvey;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_survey_add;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.create_new_survey);
        viewPager = findViewById(R.id.surveyAdd_viewpager);
        btnCancelSurvey=findViewById(R.id.btnCancelSurvey);
        btnSaveNextSurvey=findViewById(R.id.btnSaveNextSurvey);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.surveyAdd_sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        btnSaveNextSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SurveyAddActivity.this, SurveyListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        btnCancelSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SurveyAddActivity.this, SurveyListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SurveySetupFragment(), "Survey Setup");
        adapter.addFragment(new SurveyContentFragment(), "Survey Content");
        adapter.addFragment(new SurveyOptionFragment(), "Survey Options");
        adapter.addFragment(new SurveysExtendedOptionFragment(), "Extended Options");
        adapter.addFragment(new SurveysPublishingFragment(), "Publishing");
        adapter.addFragment(new SurveysLanguagesFragment(), "Languages");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
