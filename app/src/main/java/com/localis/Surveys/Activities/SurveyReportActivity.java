package com.localis.Surveys.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.smart.customViews.SmartTextView;

public class SurveyReportActivity extends MasterActivity {
    SmartTextView txtCompleteRes, txtViewRes, txtlocationParticipated, txtDeviceUsed;
    Intent intent;
    Context mContext;

    @Override
    public int getLayoutID() {
        return R.layout.activity_survey_report;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        mContext=SurveyReportActivity.this;
        setHeaderToolbar(R.string.survey_report);
        txtCompleteRes = findViewById(R.id.txtCompleteRes);
        txtViewRes = findViewById(R.id.txtViewRes);
        txtlocationParticipated = findViewById(R.id.txtlocationParticipated);
        txtDeviceUsed = findViewById(R.id.txtDeviceUsed);


    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        txtCompleteRes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(mContext, SurveyCompletedResActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        txtViewRes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(mContext, TotalResponceActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        txtlocationParticipated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(mContext, TotalLocationParticipatedActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        txtDeviceUsed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(mContext, TotalDeviceActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });


    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
