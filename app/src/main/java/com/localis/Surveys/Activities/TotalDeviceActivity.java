package com.localis.Surveys.Activities;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.localis.Surveys.Adapters.TotalDeviceUsedAdapter;
import com.localis.Surveys.Adapters.TotalLocationParticipatedAdapter;

public class TotalDeviceActivity extends MasterActivity {
    Context mContext;
    RecyclerView total_device_rv;
    LinearLayoutManager linearLayoutManager;
    TotalDeviceUsedAdapter totalDeviceUsedAdapter;

    @Override
    public int getLayoutID() {
        return R.layout.activity_total_device;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        mContext = TotalDeviceActivity.this;

        setHeaderToolbar(R.string.total_devices_used);

        total_device_rv=findViewById(R.id.total_device_rv);
        linearLayoutManager = new LinearLayoutManager(this);
        total_device_rv.setLayoutManager(linearLayoutManager);

        totalDeviceUsedAdapter = new TotalDeviceUsedAdapter(this);
        total_device_rv.setAdapter(totalDeviceUsedAdapter);
    }
    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
