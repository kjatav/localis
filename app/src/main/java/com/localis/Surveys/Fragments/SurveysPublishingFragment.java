package com.localis.Surveys.Fragments;

import android.content.DialogInterface;
import android.view.View;

import com.localis.R;
import com.smart.customViews.CustomClickListener;
import com.smart.customViews.SmartEditText;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartUtils;

import java.util.GregorianCalendar;

import static com.smart.framework.SmartUtils.GetListDialog;
import static com.smart.framework.SmartUtils.getDateTimeDialog;

/**
 * Created by pct1 on 6/3/18.
 */

public class SurveysPublishingFragment extends SmartFragment {
    private SmartEditText etStartPublishingDateSurvey;
    private SmartEditText etEndPublishingDateSurvey;
    private SmartEditText etAccessSurvey;
    private SmartEditText etDescriptionMetaSurvey;
    private SmartEditText etKeywordMetaSurvey;

    private String access[] = {"Published", "Unpublished", "Archived", "Deleted"};

    @Override
    public int setLayoutId() {
        return R.layout.fragment_surveys_publishing;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {

        etStartPublishingDateSurvey = currentView.findViewById(R.id.et_start_publishing_date_survey);
        etEndPublishingDateSurvey = currentView.findViewById(R.id.et_end_publishing_date_survey);
        etAccessSurvey = currentView.findViewById(R.id.et_access_survey);
        etDescriptionMetaSurvey = currentView.findViewById(R.id.et_description_meta_survey);
        etKeywordMetaSurvey = currentView.findViewById(R.id.et_keyword_meta_survey);

        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        /*etStartPublishingDateSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
                String currentMoth = String.valueOf(android.text.format.DateFormat.format(getContext().getString(R.string.date_format_2), gregorianCalendar));

                getDateTimeDialog(getContext(), currentMoth, true, getContext().getString(R.string.date_time_format_3), new CustomClickListener() {
                    @Override
                    public void onClick(String value) {
                        etStartPublishingDateSurvey.setText(value);
                    }
                });
            }
        });
        etEndPublishingDateSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
                String currentMoth = String.valueOf(android.text.format.DateFormat.format(getContext().getString(R.string.date_format_2), gregorianCalendar));

                getDateTimeDialog(getContext(), currentMoth, true, getContext().getString(R.string.date_time_format_3), new CustomClickListener() {
                    @Override
                    public void onClick(String value) {
                        etEndPublishingDateSurvey.setText(value);
                    }
                });
            }
        });*/
        etAccessSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), access, "Accession", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etAccessSurvey.setText(access[id]);
                    }
                });
            }
        });
    }
}
