package com.localis.Surveys.Fragments;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.localis.R;
import com.localis.Surveys.POJOS.SurveyAnsObject;
import com.smart.customViews.SmartEditText;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pct1 on 6/3/18.
 */

public class AnswerQueFragment extends SmartFragment {
    ImageView btn_add_answer;
    SmartEditText edtAnswer;
    LinearLayout root;
    ArrayList<SurveyAnsObject> surveyAnsObjects;
    SurveyAnsObject mSurveyAnsObject;
    List<EditText> allEdt;
    @Override
    public int setLayoutId() {
        return R.layout.fragment_quesadd_answer;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(final View currentView) {
        btn_add_answer=currentView.findViewById(R.id.btn_add_answer);
        root=currentView.findViewById(R.id.root);
        allEdt = new ArrayList<EditText>();
        btn_add_answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View child = getLayoutInflater().inflate(R.layout.child_edittext, null);//child.xml
                root.addView(child);
                edtAnswer=child.findViewById(R.id.edtAnswer);
                allEdt.add(edtAnswer);
                edtAnswer.requestFocus();
            }
        });
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }


}
