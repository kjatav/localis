package com.localis.Surveys.Fragments;

import android.content.DialogInterface;
import android.view.View;

import com.localis.R;
import com.smart.customViews.SmartEditText;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartFragment;

import static com.smart.framework.SmartUtils.GetListDialog;

/**
 * Created by pct1 on 6/3/18.
 */

public class SurveysLanguagesFragment extends SmartFragment {
    SmartEditText etAccessSurveyLanguage;
    private String accessLanguage[] = {"All", "English(UK)"};

    @Override
    public int setLayoutId() {
        return R.layout.fragment_survey_languages;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        etAccessSurveyLanguage = currentView.findViewById(R.id.et_access_surveyLanguage);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        etAccessSurveyLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), accessLanguage, "Languages", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etAccessSurveyLanguage.setText(accessLanguage[id]);
                    }
                });
            }
        });
    }
}
