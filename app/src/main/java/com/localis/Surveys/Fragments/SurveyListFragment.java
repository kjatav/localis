package com.localis.Surveys.Fragments;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.localis.AA_POJOs.ImageObject;
import com.localis.R;
import com.localis.Surveys.Activities.QuestionActivity;
import com.localis.Surveys.Activities.SurveyAddActivity;
import com.localis.Surveys.Activities.SurveyInviteActivity;
import com.localis.Surveys.Activities.SurveyReportActivity;
import com.localis.Surveys.Adapters.SurveyItemAdapter;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartRecyclerView;

import java.util.ArrayList;

/**
 * Created by pct1 on 5/3/18.
 */

public class SurveyListFragment extends SmartFragment implements SurveyItemAdapter.ItemClickListener {
    ArrayList<ImageObject> image_objects = new ArrayList<>();
    SmartRecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    SurveyItemAdapter surveyItemAdapter;
    Intent intent;
    @Override
    public int setLayoutId() {
        return R.layout.fragment_survey_list;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        recyclerView =  currentView.findViewById(R.id.rv_allSurvey);

        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));


        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        surveyItemAdapter = new SurveyItemAdapter(getActivity(),image_objects);
        recyclerView.setAdapter(surveyItemAdapter);
        recyclerView.setEmptyView(currentView.findViewById(R.id.layout_no_data));
        surveyItemAdapter.setClickListener(this);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }

    @Override
    public void onItemClick(View view, int position) {
        switch (view.getId()){
            case R.id.txtEditQue:
                intent = new Intent(getActivity(), QuestionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                getActivity().startActivity(intent);
                break;
            case R.id.txtReport:
                intent = new Intent(getActivity(), SurveyReportActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                getActivity().startActivity(intent);
                break;
            case R.id.txtInvite:
                intent = new Intent(getActivity(), SurveyInviteActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                getActivity().startActivity(intent);
                break;
            case R.id.txtEdit:
                intent = new Intent(getActivity(), SurveyAddActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                getActivity().startActivity(intent);
                break;
        }
    }
}
