package com.localis.Surveys.Fragments;

import android.content.DialogInterface;
import android.view.View;

import com.localis.R;
import com.smart.customViews.SmartEditText;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartFragment;

import static com.smart.framework.SmartUtils.GetListDialog;

/**
 * Created by pct1 on 6/3/18.
 */

public class SurveysExtendedOptionFragment extends SmartFragment {
    private SmartEditText etShowTitleSurvey;
    private SmartEditText etLinkTitleSurvey;
    private SmartEditText etShowTagsSurvey;
    private SmartEditText etPositionSurvey;
    private SmartEditText etSocialSharingBtn;
    private SmartEditText etShowCategorySurvey;
    private SmartEditText etLinkCategory;
    private SmartEditText etShowFather;
    private SmartEditText etParentLinkSurvey;
    private SmartEditText etShowAuthorSurvey;
    private SmartEditText etAuthorBindingSurvey;
    private SmartEditText etShowDateSurvey;
    private SmartEditText etShowDatechangeSurvey;
    private SmartEditText etShowDatePublicationSurvey;
    private SmartEditText etShowNavigation;
    private SmartEditText etShowHitsSurvey;
    private SmartEditText etShowUnauthorizedSurvey;
    private SmartEditText etLayoutAlternativeSurvey;

    private String ShowTitle[] = {"Use Global","To miss","Hide"};
    private String LinkTitle[] = {"Use Global","Yes","No"};

    @Override
    public int setLayoutId() {
        return R.layout.fragment_surveys_extended_option;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {

        etShowTitleSurvey = currentView.findViewById(R.id.et_show_title_survey);
        etLinkTitleSurvey = currentView.findViewById(R.id.et_link_title_survey);
        etShowTagsSurvey = currentView.findViewById(R.id.et_show_tags_survey);
        etPositionSurvey = currentView.findViewById(R.id.et_position_survey);
        etSocialSharingBtn = currentView.findViewById(R.id.et_social_sharing_btn);
        etShowCategorySurvey = currentView.findViewById(R.id.et_show_category_survey);
        etLinkCategory = currentView.findViewById(R.id.et_link_category);
        etShowFather = currentView.findViewById(R.id.et_show_father);
        etParentLinkSurvey = currentView.findViewById(R.id.et_parent_link_survey);
        etShowAuthorSurvey = currentView.findViewById(R.id.et_show_author_survey);
        etAuthorBindingSurvey = currentView.findViewById(R.id.et_author_binding_survey);
        etShowDateSurvey = currentView.findViewById(R.id.et_show_date_survey);
        etShowDatechangeSurvey = currentView.findViewById(R.id.et_show_datechange_survey);
        etShowDatePublicationSurvey = currentView.findViewById(R.id.et_show_datePublication_survey);
        etShowNavigation = currentView.findViewById(R.id.et_show_navigation);
        etShowHitsSurvey = currentView.findViewById(R.id.et_show_hits_survey);
        etShowUnauthorizedSurvey = currentView.findViewById(R.id.et_show_unauthorized_survey);
        etLayoutAlternativeSurvey = currentView.findViewById(R.id.et_layoutAlternative_survey);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        etShowTitleSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowTitle, "Show the title", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowTitleSurvey.setText(ShowTitle[id]);
                    }
                });
            }
        });
        etLinkTitleSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), LinkTitle, "Link titles", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etLinkTitleSurvey.setText(LinkTitle[id]);
                    }
                });
            }
        });
        etShowTagsSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowTitle, "Show Tags", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowTagsSurvey.setText(ShowTitle[id]);
                    }
                });
            }
        });
        etPositionSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowTitle, "Position of Survey Info", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etPositionSurvey.setText(ShowTitle[id]);
                    }
                });
            }
        });
        etSocialSharingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowTitle, "Social Sharing Buttons", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etSocialSharingBtn.setText(ShowTitle[id]);
                    }
                });
            }
        });
        etShowCategorySurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowTitle, "Show the category", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowCategorySurvey.setText(ShowTitle[id]);
                    }
                });
            }
        });
        etLinkCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), LinkTitle, "Link Category", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etLinkCategory.setText(LinkTitle[id]);
                    }
                });
            }
        });
        etShowFather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowTitle, "Show your father", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowFather.setText(LinkTitle[id]);
                    }
                });
            }
        });
        etParentLinkSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), LinkTitle, "Parent Link", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etParentLinkSurvey.setText(LinkTitle[id]);
                    }
                });
            }
        });
        etShowAuthorSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowTitle, "Show author", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowAuthorSurvey.setText(ShowTitle[id]);
                    }
                });
            }
        });
        etAuthorBindingSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), LinkTitle, "Author binding", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etAuthorBindingSurvey.setText(LinkTitle[id]);
                    }
                });
            }
        });
        etShowDateSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowTitle, "Shows date of creation", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowDateSurvey.setText(ShowTitle[id]);
                    }
                });
            }
        });
        etShowDatechangeSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), LinkTitle, "Shows the date of the change", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowDatechangeSurvey.setText(LinkTitle[id]);
                    }
                });
            }
        });
        etShowDatePublicationSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), LinkTitle, "Show date of publication", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowDatePublicationSurvey.setText(LinkTitle[id]);
                    }
                });
            }
        });
        etShowNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowTitle, "Show Navigation", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowNavigation.setText(ShowTitle[id]);
                    }
                });
            }
        });
        etShowHitsSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), LinkTitle, "Show hits", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowHitsSurvey.setText(LinkTitle[id]);
                    }
                });
            }
        });
        etShowUnauthorizedSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowTitle, "Shows unauthorized links", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowUnauthorizedSurvey.setText(ShowTitle[id]);
                    }
                });
            }
        });
        etLayoutAlternativeSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), LinkTitle, "Layout alternative", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etLayoutAlternativeSurvey.setText(LinkTitle[id]);
                    }
                });
            }
        });


    }
}
