package com.localis.Surveys.Fragments;

import android.view.View;

import com.localis.R;
import com.smart.framework.SmartFragment;

/**
 * Created by pct1 on 6/3/18.
 */

public class SurveyContentFragment extends SmartFragment {
    @Override
    public int setLayoutId() {
        return R.layout.fragment_survey_content;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        return null;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }
}
