package com.localis.Surveys.Fragments;

import android.content.DialogInterface;
import android.view.View;

import com.localis.R;
import com.smart.customViews.SmartEditText;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartUtils;

/**
 * Created by pct1 on 6/3/18.
 */

public class SurveySetupFragment extends SmartFragment {
    SmartEditText et_survey_age, et_survey_gender, et_survey_country, et_survey_city, et_survey_category, et_survey_private, et_survey_tag;
    private String State[] = {"Florida", "Georgia", "New Jersey", "California"};
    private String city[] = {"New York City", "Chicago, Illinois", "Las Vegas", "Washington"};
    private String age[] = {"20-25", "25-30", "30-35", "35-40", ">60"};
    private String gender[] = {"Male", "Female", "Other"};
    private String category[] = {"category test1"};
    private String privateSurvey[] = {"Yes", "Not"};
    private String tag[] = {"Music", "Social", "Singer", "Favorite", "Zootopia"};

    @Override
    public int setLayoutId() {
        return R.layout.fragment_surveys_setup;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        et_survey_age = currentView.findViewById(R.id.et_survey_age);
        et_survey_gender = currentView.findViewById(R.id.et_survey_gender);
        et_survey_country = currentView.findViewById(R.id.et_survey_country);
        et_survey_city = currentView.findViewById(R.id.et_survey_city);
        et_survey_category = currentView.findViewById(R.id.et_survey_category);
        et_survey_private = currentView.findViewById(R.id.et_survey_private);
        et_survey_tag = currentView.findViewById(R.id.et_survey_tag);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        et_survey_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(getContext(), age, getString(R.string.select_age), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        et_survey_age.setText(age[id]);
                    }
                });
            }
        });
        et_survey_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(getContext(), gender, getString(R.string.select_an_option), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        et_survey_gender.setText(gender[id]);
                    }
                });
            }
        });
        et_survey_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(getContext(), State, getString(R.string.select_country), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        et_survey_country.setText(State[id]);
                    }
                });
            }
        });
        et_survey_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(getContext(), city, getString(R.string.select_city), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        et_survey_city.setText(city[id]);
                    }
                });
            }
        });
        et_survey_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(getContext(), category, getString(R.string.select_an_option), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        et_survey_category.setText(category[id]);
                    }
                });
            }
        });
        et_survey_private.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(getContext(), privateSurvey, getString(R.string.select_private_survey), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        et_survey_private.setText(privateSurvey[id]);
                    }
                });
            }
        });
        et_survey_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(getContext(), tag, getString(R.string.select_tag), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        et_survey_tag.setText(tag[id]);
                    }
                });
            }
        });


    }
}
