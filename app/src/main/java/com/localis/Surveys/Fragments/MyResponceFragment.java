package com.localis.Surveys.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.localis.AA_POJOs.ImageObject;
import com.localis.R;
import com.localis.Surveys.Adapters.MyResponceAdapter;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;

/**
 * Created by pct1 on 7/3/18.
 */

public class MyResponceFragment extends SmartFragment {
    RecyclerView rv_myResponce;
    LinearLayoutManager linearLayoutManager;
    MyResponceAdapter groupsAdapter;
    ArrayList<ImageObject> image_objects = new ArrayList<>();
    @Override
    public int setLayoutId() {
        return R.layout.fragment_survey_my_responce;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        rv_myResponce=currentView.findViewById(R.id.rv_myResponce);

        linearLayoutManager = new LinearLayoutManager(getContext());
        rv_myResponce.setLayoutManager(linearLayoutManager);
        groupsAdapter = new MyResponceAdapter(image_objects, getActivity());
        rv_myResponce.setAdapter(groupsAdapter);

        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }
}
