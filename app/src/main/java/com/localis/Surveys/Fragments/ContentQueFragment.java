package com.localis.Surveys.Fragments;

import android.view.View;

import com.localis.R;
import com.localis.Surveys.POJOS.SurveyQueObject;
import com.smart.customViews.SmartEditText;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;

/**
 * Created by pct1 on 6/3/18.
 */

public class ContentQueFragment extends SmartFragment {
    SmartEditText edtQueTitle,edtQueType,edtQueDesc;
    ArrayList<SurveyQueObject> mSurveyQueObjects;
    SurveyQueObject surveyQueObject;
    String  temp;
    @Override
    public int setLayoutId() {
        return R.layout.fragment_quesadd_content;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        edtQueTitle=currentView.findViewById(R.id.edtQueTitle);
        edtQueType=currentView.findViewById(R.id.edtQueType);
        edtQueDesc=currentView.findViewById(R.id.edtQueDesc);
        mSurveyQueObjects=new ArrayList<>();

        temp=edtQueTitle.getText().toString();
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }


}
