package com.localis.Surveys.Fragments;

import android.content.DialogInterface;
import android.view.View;

import com.localis.R;
import com.smart.customViews.SmartEditText;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartFragment;

import static com.smart.framework.SmartUtils.GetListDialog;

/**
 * Created by pct1 on 6/3/18.
 */

public class SurveyOptionFragment extends SmartFragment {

    private SmartEditText etDefaultSurveyOption;
    private SmartEditText etCountryRestrictionSurvey;
    private SmartEditText etEmailNotificationSurvey;
    private SmartEditText etPdfReportSurvey;
    private SmartEditText etAnonymousSurvey;
    private SmartEditText etEnableCaptchSurvey;
    private SmartEditText etConfidentialityNoticeSurvey;
    private SmartEditText etShowIntroSurvey;
    private SmartEditText etShowTemplateSurvey;
    private SmartEditText etShowReportSurvey;
    private SmartEditText etShowConfidentNoticeSurvey;
    private SmartEditText etShowJumpToPageMenu;
    private SmartEditText etShowPagination;
    private SmartEditText etShowSaveBtn;
    private SmartEditText etShowPreBtn;
    private SmartEditText etShowCancelBtn;

    private String defaultLayout[] = {"Default Use", "bootstrap3"};
    private String CountryRestriction[] = {"Ascension Island", "andorra", "United Arb Emirates", "Afghanistan", "Antigua and barbuda", "Anguilla", "Albania", "Armenia", "Netherland Antilles"};
    private String EmailNotification[] = {"Yes", "No"};
    private String PdfReport[] = {"Yes", "No"};
    private String anonymous[] = {"Yes", "No"};
    private String EnbleCaptcha[] = {"Yes", "No"};
    private String ConfidentialityNotice[]={"Use Global","To miss","Hide"};
    private String ShowIntro[]={"Use Global","To miss","Hide"};
    private String ShowTemplate[]={"Use Global","To miss","Hide"};
    private String ShowReport[]={"Use Global","To miss","Hide"};
    private String ShowConfidentialityNotice[]={"Use Global","To miss","Hide"};
    private String ShowJumpToPageMenu[]={"Use Global","To miss","Hide"};
    private String ShowPagination[]={"Use Global","To miss","Hide"};
    private String ShowSaveButton[]={"Use Global","To miss","Hide"};
    private String ShowPreviousButton[]={"Use Global","To miss","Hide"};
    private String ShowCancelButton[]={"Use Global","To miss","Hide"};


    @Override
    public int setLayoutId() {
        return R.layout.fragment_surveys_option;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {

            etDefaultSurveyOption = currentView.findViewById( R.id.et_default_survey_option );
            etCountryRestrictionSurvey = currentView.findViewById( R.id.et_country_restriction_Survey );
            etEmailNotificationSurvey = currentView.findViewById( R.id.et_Email_notification_survey );
            etPdfReportSurvey = currentView.findViewById( R.id.et_pdf_report_survey );
            etAnonymousSurvey = currentView.findViewById( R.id.et_anonymous_survey );
            etEnableCaptchSurvey = currentView.findViewById( R.id.et_enable_captch_survey );
            etConfidentialityNoticeSurvey = currentView.findViewById( R.id.et_confidentiality_Notice_survey );
            etShowIntroSurvey = currentView.findViewById( R.id.et_show_intro_survey );
            etShowTemplateSurvey = currentView.findViewById( R.id.et_show_template_survey );
            etShowReportSurvey = currentView.findViewById( R.id.et_show_report_survey );
            etShowConfidentNoticeSurvey = currentView.findViewById( R.id.et_Show_Confident_Notice_survey );
            etShowJumpToPageMenu = currentView.findViewById( R.id.et_Show_JumpToPage_Menu );
            etShowPagination = currentView.findViewById( R.id.et_show_pagination );
            etShowSaveBtn = currentView.findViewById( R.id.et_show_save_btn );
            etShowPreBtn = currentView.findViewById( R.id.et_show_pre_btn );

            etShowCancelBtn = currentView.findViewById( R.id.et_show_cancel_btn );


        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        etDefaultSurveyOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), defaultLayout, "Default Layout", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etDefaultSurveyOption.setText(defaultLayout[id]);
                    }
                });
            }
        });
        etCountryRestrictionSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), CountryRestriction, "Country Restriction", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etCountryRestrictionSurvey.setText(CountryRestriction[id]);
                    }
                });
            }
        });
        etEmailNotificationSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), EmailNotification, "Send Email Notifications", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etEmailNotificationSurvey.setText(EmailNotification[id]);
                    }
                });
            }
        });
        etPdfReportSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), PdfReport, "Attach PDF Report", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etPdfReportSurvey.setText(PdfReport[id]);
                    }
                });
            }
        });
        etAnonymousSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), anonymous, "Anonymous", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etAnonymousSurvey.setText(anonymous[id]);
                    }
                });
            }
        });
        etEnableCaptchSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), EnbleCaptcha, "Enable Captcha", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etEnableCaptchSurvey.setText(EnbleCaptcha[id]);
                    }
                });
            }
        });
        etConfidentialityNoticeSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ConfidentialityNotice, "Confidentiality Notice", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etConfidentialityNoticeSurvey.setText(ConfidentialityNotice[id]);
                    }
                });
            }
        });
        etShowIntroSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowIntro, "Show Intro", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowIntroSurvey.setText(ShowIntro[id]);
                    }
                });
            }
        });
        etShowTemplateSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowTemplate, "Show Template", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowTemplateSurvey.setText(ShowTemplate[id]);
                    }
                });
            }
        });
        etShowReportSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowReport, "Show Report", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowReportSurvey.setText(ShowReport[id]);
                    }
                });
            }
        });
        etShowConfidentNoticeSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowConfidentialityNotice, "Show Confidentiality Notice", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowConfidentNoticeSurvey.setText(ShowConfidentialityNotice[id]);
                    }
                });
            }
        });
        etShowJumpToPageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowJumpToPageMenu, "Show JumpToPage Menue", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowJumpToPageMenu.setText(ShowJumpToPageMenu[id]);
                    }
                });
            }
        });
        etShowPagination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowPagination, "Show Pagination", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowPagination.setText(ShowPagination[id]);
                    }
                });
            }
        });
        etShowSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowSaveButton, "Show Save Button", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowSaveBtn.setText(ShowSaveButton[id]);
                    }
                });
            }
        });
        etShowPreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowPreviousButton, "Show Previous Button", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowPreBtn.setText(ShowPreviousButton[id]);
                    }
                });
            }
        });
        etShowCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(getContext(), ShowCancelButton, "Show Cancel Button", false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        etShowCancelBtn.setText(ShowCancelButton[id]);
                    }
                });
            }
        });
    }

}
