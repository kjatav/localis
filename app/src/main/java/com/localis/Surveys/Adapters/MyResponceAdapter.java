package com.localis.Surveys.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Groups.Activites.GroupDetailActivity;
import com.localis.R;

import java.util.ArrayList;


public class MyResponceAdapter extends  RecyclerView.Adapter<MyResponceAdapter.MyViewHolder> {
    ArrayList<ImageObject> image_objects;
    Context context;

    public MyResponceAdapter(ArrayList<ImageObject> image_objects, Context context) {
        this.image_objects = image_objects;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_responce, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivBackgroundGroup;

        public MyViewHolder(View itemView) {
            super(itemView);

        }
    }
}