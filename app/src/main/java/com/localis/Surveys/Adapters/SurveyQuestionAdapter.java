package com.localis.Surveys.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.Albums.Adapters.AddAlbumAdapter;
import com.localis.R;
import com.localis.Surveys.Activities.QuestionActivity;
import com.localis.Surveys.POJOS.SurveyAnsObject;
import com.localis.Surveys.POJOS.SurveyQueObject;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;


public class SurveyQuestionAdapter extends RecyclerView.Adapter<SurveyQuestionAdapter.MyViewHolder> {
    ArrayList<SurveyQueObject> question_objects;
    Context context;
    AnswerAdapter mAnswerAdapter;
    ArrayList<SurveyAnsObject> survey_answer_objects;

    public SurveyQuestionAdapter(ArrayList<SurveyQueObject> survey_question_objects, QuestionActivity questionActivity) {
        this.question_objects=survey_question_objects;
        this.context=questionActivity;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_survey_question, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.txtQuestion.setText(question_objects.get(position).getQuestionContent());

        holder.linearLayoutManager = new LinearLayoutManager(context);
        holder.rv_answer.setLayoutManager(holder.linearLayoutManager);

        survey_answer_objects=question_objects.get(position).getSurveyAnsObject();
        mAnswerAdapter = new AnswerAdapter(survey_answer_objects,context,question_objects.get(position).getQuestionType());
        holder.rv_answer.setAdapter(mAnswerAdapter);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return question_objects.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        SmartTextView txtQuestion;
        RecyclerView rv_answer;
        LinearLayoutManager linearLayoutManager;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtQuestion = itemView.findViewById(R.id.txtQuestion);
            rv_answer = itemView.findViewById(R.id.rv_answer);


        }
    }


}