package com.localis.Surveys.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.AA_POJOs.ImageObject;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.List;

public class SurveyItemAdapter extends RecyclerView.Adapter<SurveyItemAdapter.MyViewHolder> {
    private Context context;
    private List<ImageObject> memeberObjects;
    private ItemClickListener mClickListener;

    public SurveyItemAdapter(Context context, List<ImageObject> tagFriendsObjects) {
        this.context = context;
        this.memeberObjects = tagFriendsObjects;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_survey_item, parent, false));

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        //return memeberObjects.size();
        return  memeberObjects.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        SmartTextView txtEditQue,txtReport,txtInvite,txtEdit;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtEditQue = itemView.findViewById(R.id.txtEditQue);
            txtReport=itemView.findViewById(R.id.txtReport);
            txtInvite=itemView.findViewById(R.id.txtInvite);
            txtEdit=itemView.findViewById(R.id.txtEdit);

            txtEditQue.setOnClickListener(this);
            txtEdit.setOnClickListener(this);
            txtReport.setOnClickListener(this);
            txtInvite.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}