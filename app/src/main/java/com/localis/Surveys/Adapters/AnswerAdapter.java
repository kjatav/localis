package com.localis.Surveys.Adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.R;
import com.localis.Surveys.POJOS.SurveyAnsObject;
import com.localis.Surveys.POJOS.SurveyQueObject;
import com.smart.customViews.SmartRadioButton;

import java.util.ArrayList;

import static com.smart.framework.Constants.CHK_CODE;
import static com.smart.framework.Constants.RB_CODE;

public class AnswerAdapter extends RecyclerView.Adapter {
    private ArrayList<SurveyAnsObject> survey_answer_objects;
    private ArrayList<SurveyQueObject> survey_question_objects;
    private Context context;
    private  int VIEW_CHK = 0;
    private  int VIEW_RB = 1;

    int Type;

    public AnswerAdapter(ArrayList<SurveyAnsObject> survey_answer_objects, Context context, int questionType) {
        this.survey_answer_objects=survey_answer_objects;
        this.context=context;
        this.Type=questionType;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_CHK) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.answer_chk_item, parent, false);
            //  DashboardAdapter.CHKViewHolder vh = new DashboardAdapter.CHKViewHolder(v);
            return new CHKViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.answer_rb_item, parent, false);
            // DashboardAdapter.RADViewHolder vh = new DashboardAdapter.RADViewHolder(v);
            return new RBViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
       // survey_answer_objects=survey_question_objects.get(position).getSurveyAnsObject();

        if (viewHolder instanceof CHKViewHolder) {
            ((CHKViewHolder) viewHolder).chkAnswer.setText(survey_answer_objects.get(position).getAnsContent());
        } else if (viewHolder instanceof RBViewHolder) {
            ((RBViewHolder) viewHolder).rbAnswer.setText(survey_answer_objects.get(position).getAnsContent());
        }
    }

    @Override
    public int getItemViewType(int position) {
        int result;
        switch (Type) {
            case CHK_CODE:
                result = VIEW_CHK;
                break;
            case RB_CODE:
                result = VIEW_RB;
                break;
            default:
                result = VIEW_CHK;
        }
        return result;
    }

    @Override
    public int getItemCount() {
        if (survey_answer_objects!= null) {
            return survey_answer_objects.size();
        } else {
            return 0;
        }
        //return 3;
    }

    private class CHKViewHolder extends RecyclerView.ViewHolder {
        AppCompatCheckBox chkAnswer;

        public CHKViewHolder(View itemView) {
            super(itemView);
            chkAnswer = itemView.findViewById(R.id.chk_answer);
        }
    }

    private class RBViewHolder extends RecyclerView.ViewHolder {
        SmartRadioButton rbAnswer;

        public RBViewHolder(View itemView) {
            super(itemView);
            rbAnswer = itemView.findViewById(R.id.rb_answer);
        }
    }


}