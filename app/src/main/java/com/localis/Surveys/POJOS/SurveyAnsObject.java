package com.localis.Surveys.POJOS;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SurveyAnsObject {

    @SerializedName("ansId")
    @Expose
    private String ansId;
    @SerializedName("ansContent")
    @Expose
    private String ansContent;


    private int QuestionType;

    public String getAnsId() {
        return ansId;
    }

    public void setAnsId(String ansId) {
        this.ansId = ansId;
    }

    public String getAnsContent() {
        return ansContent;
    }

    public void setAnsContent(String ansContent) {
        this.ansContent = ansContent;
    }



}
