package com.localis.Surveys.POJOS;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SurveyQueObject {

    @SerializedName("questionId")
    @Expose
    private String questionId;
    @SerializedName("questionContent")
    @Expose
    private String questionContent;
    @SerializedName("questionType")
    @Expose
    private int questionType;
    @SerializedName("questionDesc")
    @Expose
    private String questionDesc;
    @SerializedName("Survey_Ans_Object")
    @Expose
    private ArrayList<SurveyAnsObject> surveyAnsObject = null;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public int getQuestionType() {
        return questionType;
    }

    public void setQuestionType(int questionType) {
        this.questionType = questionType;
    }

    public String getQuestionDesc() {
        return questionDesc;
    }

    public void setQuestionDesc(String questionDesc) {
        this.questionDesc = questionDesc;
    }

    public ArrayList<SurveyAnsObject> getSurveyAnsObject() {
        return surveyAnsObject;
    }

    public void setSurveyAnsObject(ArrayList<SurveyAnsObject> surveyAnsObject) {
        this.surveyAnsObject = surveyAnsObject;
    }
}