package com.localis.Surveys.POJOS;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SurveyReq {

@SerializedName("survey_Que_Object")
@Expose
private SurveyQueObject surveyQueObject;

public SurveyQueObject getSurveyQueObject() {
return surveyQueObject;
}

public void setSurveyQueObject(SurveyQueObject surveyQueObject) {
this.surveyQueObject = surveyQueObject;
}
}
