package com.localis.Dashboard.POJOs;


import android.support.annotation.StringRes;

import java.io.Serializable;

public class SmileyObject implements Serializable{
    private int image_id;
    private int image_name;
    private int drawable_id_150;
    private int drawable_id_120;

    public SmileyObject(int image_id, @StringRes int image_name, int drawable_id_150, int drawable_id_120) {
        this.image_id = image_id;
        this.image_name = image_name;
        this.drawable_id_150 = drawable_id_150;
        this.drawable_id_120 = drawable_id_120;
    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public int getImage_name() {
        return image_name;
    }

    public void setImage_name(int image_name) {
        this.image_name = image_name;
    }

    public int getDrawable_id_150() {
        return drawable_id_150;
    }

    public void setDrawable_id_150(int drawable_id_150) {
        this.drawable_id_150 = drawable_id_150;
    }

    public int getDrawable_id_120() {
        return drawable_id_120;
    }

    public void setDrawable_id_120(int drawable_id_120) {
        this.drawable_id_120 = drawable_id_120;
    }
}
