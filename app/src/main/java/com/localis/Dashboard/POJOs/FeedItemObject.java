package com.localis.Dashboard.POJOs;

/**
 * Created by pct1 on 14/2/18.
 */

public class FeedItemObject {
 int CONTENT_TYPE;

    public FeedItemObject(int CONTENT_TYPE) {
        this.CONTENT_TYPE = CONTENT_TYPE;
    }

    public int getCONTENT_TYPE() {
        return CONTENT_TYPE;
    }

    public void setCONTENT_TYPE(int CONTENT_TYPE) {
        this.CONTENT_TYPE = CONTENT_TYPE;
    }
}
