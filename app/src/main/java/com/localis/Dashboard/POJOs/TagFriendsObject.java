package com.localis.Dashboard.POJOs;

import java.io.Serializable;

public class TagFriendsObject implements Serializable {
    private int id;
    private String friendName;
    private boolean isChecked;

    public TagFriendsObject(int id, String friendName, boolean isChecked) {
        this.id = id;
        this.friendName = friendName;
        this.isChecked = isChecked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
