package com.localis.Dashboard.POJOs;

/**
 * Created by : Dhiren Parmar
 * Created date : January 24, 2018
 * Purpose of creation :
 * Modified by :
 * Modified date :
 * Purpose of modification :
 */

public class ImageObject {
int groupImages;

    public ImageObject(int groupImages) {
        this.groupImages = groupImages;
    }

    public int getGroupImages() {
        return groupImages;
    }

    public void setGroupImages(int groupImages) {
        this.groupImages = groupImages;
    }
}
