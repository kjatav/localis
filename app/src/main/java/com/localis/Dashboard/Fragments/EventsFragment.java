package com.localis.Dashboard.Fragments;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.localis.R;
import com.smart.customViews.SmartEditText;
import com.smart.framework.SmartFragment;


public class EventsFragment extends SmartFragment {

    private CardView cardView;
    private RelativeLayout rvSelectCategory;
    private LinearLayout dropdownList;
    private Spinner meetingSpinner;
    private SmartEditText edEventTitle;
    private SmartEditText edEventAllAbout;
    private SmartEditText dateCalender;
    private SmartEditText timeCalender;
    private Spinner utcSpinner;
    private ImageView ivProfile;

    private ImageView tagFooterDashboardIv;
    private ImageView locationFooterDashboardIv;
    private ImageView smileyFooterDashboardIv;
    private ImageView shareFooterDashboardIv;
    private ImageView visibilityFooterDashboardIv;

    private ImageView ivGroup, ivDownArrow;
    private RecyclerView dashRecyclerview;
    private boolean flag = true;
  /*  ArrayList<ImageObject> image_objects = new ArrayList<>();
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    EventAdapter eventAdapter;
*/
    @Override
    public int setLayoutId() {
        return R.layout.layout_events;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        setHasOptionsMenu(true);

        tagFooterDashboardIv = currentView.findViewById(R.id.tag_footer_dashboard_iv);
        locationFooterDashboardIv = currentView.findViewById(R.id.location_footer_dashboard_iv);
        smileyFooterDashboardIv = currentView.findViewById(R.id.smiley_footer_dashboard_iv);
        visibilityFooterDashboardIv = currentView.findViewById(R.id.visibility_footer_dashboard_iv);
/*
        cardView = (CardView) currentView.findViewById(R.id.card_view);
        rvSelectCategory = (RelativeLayout) currentView.findViewById(R.id.rv_select_category);
        dropdownList = (LinearLayout) currentView.findViewById(R.id.dropdown_list);
        meetingSpinner = (Spinner) currentView.findViewById(R.id.meeting_spinner);
        edEventTitle = (SmartEditText) currentView.findViewById(R.id.ed_event_title);
        edEventAllAbout = (SmartEditText) currentView.findViewById(R.id.ed_event_all_about);
        dateCalender = (SmartEditText) currentView.findViewById(R.id.date_calender);
        timeCalender = (SmartEditText) currentView.findViewById(R.id.time_calender);
        utcSpinner = (Spinner) currentView.findViewById(R.id.utc_spinner);
        ivProfile = (ImageView) currentView.findViewById(R.id.iv_profile);

        ivDownArrow = (ImageView) currentView.findViewById(R.id.iv_down_arrow);
       *//* recyclerView = (RecyclerView) currentView.findViewById(R.id.event_recyclerview);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        eventAdapter = new EventAdapter(image_objects, getActivity());
        recyclerView.setAdapter(eventAdapter);*//*
        dashRecyclerview = (RecyclerView) currentView.findViewById(R.id.dashboard_rv);
        rvSelectCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag) {
                    dropdownList.setVisibility(View.VISIBLE);
                    flag = false;
                } else {
                    dropdownList.setVisibility(View.GONE);
                    flag = true;
                }

            }
        });
      *//*  ivGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(getActivity());
            }
        });*/
        return currentView;
    }


    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {


    }


  /*  private void showPopup(final Activity context) {
        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_group, viewGroup);
       *//* WindowManager wm = (WindowManager)getActivity().getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams p = (WindowManager.LayoutParams)layout.getLayoutParams();
        p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        p.dimAmount = 0.3f;
        wm.updateViewLayout(layout, p);*//*
        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = -260;
        int OFFSET_Y = 5;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
//        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);

        popup.showAsDropDown(ivGroup, OFFSET_X, OFFSET_Y);

    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu); // set your file nam
    }
/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_clear_notifications) {
            showPopup(getActivity());
            //do something
            return true;
        }


        return super.onOptionsItemSelected(item);
    }*/
}