package com.localis.Dashboard.Fragments;

import android.view.View;
import android.widget.ImageView;

import com.localis.R;
import com.smart.framework.SmartFragment;


public class VideoFragment extends SmartFragment {
    private ImageView tagFooterDashboardIv;
    private ImageView locationFooterDashboardIv;
    private ImageView smileyFooterDashboardIv;
    private ImageView shareFooterDashboardIv;
    private ImageView visibilityFooterDashboardIv;

    @Override
    public int setLayoutId() {
        return R.layout.layout_video;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        tagFooterDashboardIv = currentView.findViewById(R.id.tag_footer_dashboard_iv);
        locationFooterDashboardIv = currentView.findViewById(R.id.location_footer_dashboard_iv);
        smileyFooterDashboardIv = currentView.findViewById(R.id.smiley_footer_dashboard_iv);
        visibilityFooterDashboardIv = currentView.findViewById(R.id.visibility_footer_dashboard_iv);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }
}