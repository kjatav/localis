package com.localis.Dashboard.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.localis.Dashboard.POJOs.TagFriendsObject;
import com.localis.Dashboard.Activities.SmileyActivity;
import com.localis.Dashboard.Activities.TagFriendActivity;
import com.localis.Dashboard.POJOs.SmileyObject;
import com.localis.R;
import com.smart.customViews.SmartTextView;
import com.smart.framework.SmartFragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class StatusFragment extends SmartFragment {

    private ImageView tagFooterDashboardIv;
    private ImageView locationFooterDashboardIv;
    private ImageView smileyFooterDashboardIv;
    private ImageView shareFooterDashboardIv;
    private ImageView visibilityFooterDashboardIv;
    private SmartTextView txtTagName;
    private SmartTextView smileyTxtTv;

    String strFirstTag = " ";
    String strSecTag;
    LinearLayout tagTextLl;
    List<TagFriendsObject> taggedFriendList = new ArrayList<>();
    SmileyObject smileyObject;

    public int viewHeight;

    @Override
    public int setLayoutId() {
        return R.layout.layout_status;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        txtTagName = currentView.findViewById(R.id.txtTagName);
        smileyTxtTv = currentView.findViewById(R.id.smiley_txt_tv);
        tagFooterDashboardIv = currentView.findViewById(R.id.tag_footer_dashboard_iv);
        locationFooterDashboardIv = currentView.findViewById(R.id.location_footer_dashboard_iv);
        smileyFooterDashboardIv = currentView.findViewById(R.id.smiley_footer_dashboard_iv);
        visibilityFooterDashboardIv = currentView.findViewById(R.id.visibility_footer_dashboard_iv);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        viewHeight =  display.getHeight();
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        tagFooterDashboardIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), TagFriendActivity.class);
                intent.putExtra(TAGGED_FRIENDS_DATA, (Serializable) taggedFriendList);
                getActivity().startActivityForResult(intent, TAG_FRIEND_CODE);

            }
        });
        tagTextLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), TagFriendActivity.class);
                intent.putExtra(TAGGED_FRIENDS_DATA, (Serializable) taggedFriendList);
                getActivity().startActivityForResult(intent, TAG_FRIEND_CODE);
            }
        });
        smileyTxtTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SmileyActivity.class);
                intent.putExtra(SMILEY_DATA, smileyObject);
                getActivity().startActivityForResult(intent, SMILEY_CODE);
            }
        });
        visibilityFooterDashboardIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(getContext(), visibilityFooterDashboardIv);
            }
        });
        smileyFooterDashboardIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SmileyActivity.class);
                intent.putExtra(SMILEY_DATA, smileyObject);
                getActivity().startActivityForResult(intent, SMILEY_CODE);
            }
        });
    }


    public void setFriendList(List<TagFriendsObject> list) {
        this.taggedFriendList = list;
        if (list.size() == 0) {
            tagTextLl.setVisibility(View.GONE);
        } else {
            tagTextLl.setVisibility(View.VISIBLE);
        }
        if (list.size() >= 1) {
            strFirstTag = list.get(0).getFriendName().toString();
            if (list.size() > 1) {
                strSecTag = String.valueOf(list.size() - 1) + "others";
                txtTagName.setText("- with" + " " + strFirstTag + " " + "and" + " " + strSecTag);
            } else {
                txtTagName.setText("- with" + " " + strFirstTag);
            }
        }
    }

    public void setSmiley(SmileyObject smileyObject) {
        this.smileyObject = smileyObject;
        if (smileyObject == null) {
            smileyTxtTv.setVisibility(View.GONE);
        } else {
            smileyTxtTv.setVisibility(View.VISIBLE);
        }
        smileyTxtTv.setText(smileyObject.getImage_name());
        smileyTxtTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, smileyObject.getDrawable_id_120(), 0);
    }

    private void showPopup(final Context context, View ivDownArrow) {
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_group, null);
        LinearLayout viewGroup = (LinearLayout) layout.findViewById(R.id.popup);
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setFocusable(true);
        int OFFSET_X = -300;
        int OFFSET_Y = 0;
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(ivDownArrow, OFFSET_X, OFFSET_Y);

    }
}


