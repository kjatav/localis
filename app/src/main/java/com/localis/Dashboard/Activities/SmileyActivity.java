package com.localis.Dashboard.Activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Dashboard.Adapter.SmileyAdapter;
import com.localis.Dashboard.POJOs.SmileyObject;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;

public class SmileyActivity extends MasterActivity {
    ArrayList<SmileyObject> smileyObjects = new ArrayList<>();
    GridLayoutManager gridLayoutManager;
    private SmartTextView selectedSmileyTv;
    private RecyclerView smiliesRv;
    private SmileyAdapter smileyAdapter;
    private SmileyObject smileyObject;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_smiley;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.select_a_feeling);

        selectedSmileyTv = findViewById(R.id.selected_smiley_tv);
        smiliesRv = findViewById(R.id.smilies_rv);
        smileyObjects.add(new SmileyObject(0, R.string.happy, R.drawable.ic_happy_150, R.drawable.ic_happy_120));
        smileyObjects.add(new SmileyObject(1, R.string.irritant, R.drawable.ic_upset_150, R.drawable.ic_upset_120));
        smileyObjects.add(new SmileyObject(2, R.string.pissed_off, R.drawable.ic_irritated_150, R.drawable.ic_irritated_120));
        smileyObjects.add(new SmileyObject(3, R.string.tired, R.drawable.ic_tired_150, R.drawable.ic_tired_120));
        smileyObjects.add(new SmileyObject(4, R.string.wonderful, R.drawable.ic_wonderfull_150, R.drawable.ic_wonderfull_120));
        smileyObjects.add(new SmileyObject(5, R.string.sweet, R.drawable.ic_sweet_150, R.drawable.ic_sweet_120));
        smileyObjects.add(new SmileyObject(6, R.string.nervous, R.drawable.ic_angry_150, R.drawable.ic_angry_120));
        smileyObjects.add(new SmileyObject(7, R.string.without_words, R.drawable.ic_without_word_150, R.drawable.ic_without_word_120));
        smileyObjects.add(new SmileyObject(8, R.string.sad, R.drawable.ic_sad_150, R.drawable.ic_sad_120));
        smileyObjects.add(new SmileyObject(9, R.string.cute, R.drawable.ic_sweetheart_150, R.drawable.ic_sweetheart_120));
        smileyObjects.add(new SmileyObject(10, R.string.sick, R.drawable.ic_sick_150, R.drawable.ic_sick_120));
        smileyObjects.add(new SmileyObject(11, R.string.blessed, R.drawable.ic_blessed_150, R.drawable.ic_blessed_120));
        smileyObjects.add(new SmileyObject(12, R.string.bored, R.drawable.ic_bored_150, R.drawable.ic_bored_120));
        smileyObjects.add(new SmileyObject(13, R.string.shocked, R.drawable.ic_shocked_150, R.drawable.ic_shocked_120));
        smileyObjects.add(new SmileyObject(14, R.string.special, R.drawable.ic_special_150, R.drawable.ic_special_120));
        smileyObjects.add(new SmileyObject(15, R.string.loved, R.drawable.ic_girlfriend_150, R.drawable.ic_girlfriend_120));

        if (getIntent().getSerializableExtra(SMILEY_DATA) != null) {
            setFeeling((SmileyObject) getIntent().getSerializableExtra(SMILEY_DATA));
        }

        gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2, LinearLayoutManager.VERTICAL, false);
        smiliesRv.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        smileyAdapter = new SmileyAdapter(smileyObjects, SmileyActivity.this);
        smiliesRv.setAdapter(smileyAdapter);
    }

    public void setFeeling(SmileyObject smileyObject) {
        this.smileyObject = smileyObject;
        selectedSmileyTv.setText(smileyObject.getImage_name());
        selectedSmileyTv.setCompoundDrawablesWithIntrinsicBounds(smileyObject.getDrawable_id_150(), 0, 0, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.done_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done_tagging_tv:
                Intent intent = new Intent();
                intent.putExtra(SMILEY_DATA, smileyObject);
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
