package com.localis.Dashboard.Activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.CommentAdapter;
import com.localis.AA_POJOs.ImageObject;
import com.localis.Albums.Activites.ImageDetailsActivity;
import com.localis.Albums.Adapters.AlbumDetailsAdapter;
import com.localis.R;

import java.util.ArrayList;
import java.util.Arrays;

public class DashboardFeedDetails extends MasterActivity {

    private int postType;

    private NestedScrollView videoDetailsScroll;
    private View statusFeedDetailsLayout;
    private ViewPager imageFeedDetailsVp;
    private View videoFeedDetailsLayout;
    private RecyclerView commentsFeedDetailsRv;

    private CommentAdapter commentAdapter;
    private ArrayList<ImageObject> image_objects = new ArrayList<>();

    private AlbumDetailsAdapter albumDetailsAdapter;
    private static final Integer[] slideImage = {R.drawable.bg_event, R.drawable.bg_event, R.drawable.bg_event, R.drawable.bg_event, R.drawable.bg_event};
    private ArrayList<Integer> slideArry = new ArrayList<>();


    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_dashboard_feed_details;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.feed_details);

        postType = getIntent().getIntExtra(POST_TYPE, 0);

        videoDetailsScroll = findViewById(R.id.video_details_scroll);
        statusFeedDetailsLayout = findViewById(R.id.status_feed_details_layout);
        imageFeedDetailsVp = findViewById(R.id.image_feed_details_vp);
        videoFeedDetailsLayout = findViewById(R.id.video_feed_details_layout);
        commentsFeedDetailsRv = findViewById(R.id.comments_feed_details_rv);

        slideArry.addAll(Arrays.asList(slideImage));
        //image sliding
        albumDetailsAdapter = new AlbumDetailsAdapter(DashboardFeedDetails.this, slideArry);
        imageFeedDetailsVp.setAdapter(albumDetailsAdapter);


        commentsFeedDetailsRv.setLayoutManager(new LinearLayoutManager(this));
        commentAdapter = new CommentAdapter(image_objects, this);
        commentsFeedDetailsRv.setAdapter(commentAdapter);

        switch (postType) {
            case STATUS_CODE:
                showView(statusFeedDetailsLayout);
                break;

            case IMAGE_CODE:
                showView(imageFeedDetailsVp);
                break;


            case VIDEO_CODE:
                showView(videoFeedDetailsLayout);
                break;
        }
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();
        albumDetailsAdapter.setClickListener(new AlbumDetailsAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(DashboardFeedDetails.this, ImageDetailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    private void showView(View view) {
        statusFeedDetailsLayout.setVisibility(View.GONE);
        videoFeedDetailsLayout.setVisibility(View.GONE);
        imageFeedDetailsVp.setVisibility(View.GONE);

        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
