package com.localis.Dashboard.Activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Dashboard.POJOs.TagFriendsObject;
import com.localis.Dashboard.Adapter.TagSearchAdapter;
import com.localis.R;
import com.smart.customViews.SmartEditText;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TagFriendActivity extends MasterActivity {
    private SmartEditText searchEt;
    private RecyclerView tagFriendsRv;
    private List<TagFriendsObject> friendsObjects = new ArrayList<>();
    private List<TagFriendsObject> checkedFriendsObjects = new ArrayList<>();
    private TagSearchAdapter tagSearchAdapter;


    @Override
    public int getDrawerLayoutID() {
        return 0;
    }


    @Override
    public int getLayoutID() {
        return R.layout.activity_tag_friend;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.tag_friends);
        searchEt = findViewById(R.id.search_et);
        tagFriendsRv = findViewById(R.id.tag_friends_rv);

        friendsObjects.add(new TagFriendsObject(0, "Salvador Zayas", false));
        friendsObjects.add(new TagFriendsObject(1, "Thurman Fimbres", false));
        friendsObjects.add(new TagFriendsObject(2, "Adolph Moctezuma", false));
        friendsObjects.add(new TagFriendsObject(3, "Leonor Ryals", false));
        friendsObjects.add(new TagFriendsObject(4, "Josefa Symonds", false));

        checkedFriendsObjects = (List<TagFriendsObject>) getIntent().getSerializableExtra(TAGGED_FRIENDS_DATA);
        if (checkedFriendsObjects != null) {
            for (int i = 0; i < checkedFriendsObjects.size(); i++) {
                for (int j = 0; j < friendsObjects.size(); j++) {
                    if (friendsObjects.get(j).getId() == checkedFriendsObjects.get(i).getId()) {
                        friendsObjects.get(j).setChecked(true);
                    }
                }
            }
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TagFriendActivity.this);
        tagFriendsRv.setLayoutManager(linearLayoutManager);
        tagSearchAdapter = new TagSearchAdapter(TagFriendActivity.this, friendsObjects);
        tagFriendsRv.setAdapter(tagSearchAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.done_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done_tagging_tv:
                Intent intent = new Intent();
                intent.putExtra(TAGGED_FRIENDS_DATA, (Serializable) tagSearchAdapter.getSelectedFriends());
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
