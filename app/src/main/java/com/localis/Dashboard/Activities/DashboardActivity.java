package com.localis.Dashboard.Activities;

import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.Tab;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_POJOs.ImageObject;
import com.localis.Dashboard.Adapter.DashboardAdapter;
import com.localis.Dashboard.POJOs.FeedItemObject;
import com.localis.Dashboard.POJOs.SmileyObject;
import com.localis.Dashboard.POJOs.TagFriendsObject;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.smart.framework.SmartUtils.switchTabsColor;

public class DashboardActivity extends MasterActivity {

    private NestedScrollView dashboardScroll;
    private TabLayout dashboardTabs;
    private SmartTextView statusTabTv;
    private SmartTextView photosTabTv;
    private SmartTextView videoTabTv;
    private SmartTextView eventsTabTv;
    private SmartTextView pollsTabTv;
    private ArrayList<ImageObject> image_objects = new ArrayList<>();
    private ArrayList<FeedItemObject> feedItem = new ArrayList<>();
    private RecyclerView dashboardRv;
    private DashboardAdapter dashboardAdapter;

    String strFirstTag = " ";
    String strSecTag;
    List<TagFriendsObject> taggedFriendList = new ArrayList<>();
    private SmileyObject smileyObject;

    private View statusLayout;
    private View photosLayout;
    private View videoLayout;
    private View eventsLayout;
    private View pollsLayout;

    private ImageView smileyFooterDashboardIv;
    private ImageView tagFooterDashboardIv;

    private SmartTextView txtTagName;
    private SmartTextView smileyTxtTv;


    @Override
    public int getLayoutID() {
        return R.layout.activity_dashboard;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.dashboard);
        setupBottomNav(0);

        dashboardScroll = findViewById(R.id.dashboard_scroll);

        statusLayout = findViewById(R.id.status_layout);
        photosLayout = findViewById(R.id.photos_layout);
        videoLayout = findViewById(R.id.video_layout);
        eventsLayout = findViewById(R.id.events_layout);
        pollsLayout = findViewById(R.id.polls_layout);

        dashboardTabs = findViewById(R.id.post_timeline_tabs);
        for (int i = 0; i < 5; i++) {
            dashboardTabs.addTab(dashboardTabs.newTab());
        }

        txtTagName = findViewById(R.id.txtTagName);
        smileyTxtTv = findViewById(R.id.smiley_txt_tv);

        smileyFooterDashboardIv = findViewById(R.id.smiley_footer_dashboard_iv);
        tagFooterDashboardIv = findViewById(R.id.tag_footer_dashboard_iv);

        dashboardRv = findViewById(R.id.dashboard_rv);
        dashboardRv.setLayoutManager(new LinearLayoutManager(DashboardActivity.this));
        dashboardAdapter = new DashboardAdapter(feedItem, image_objects, DashboardActivity.this);
        dashboardRv.setAdapter(dashboardAdapter);

        createPostTabIcons();

        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));
        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));
        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));
    }

    @Override
    public void prepareViews() {
        super.prepareViews();
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        tagFooterDashboardIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, TagFriendActivity.class);
                intent.putExtra(TAGGED_FRIENDS_DATA, (Serializable) taggedFriendList);
                startActivityForResult(intent, TAG_FRIEND_CODE);

            }
        });

        txtTagName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, TagFriendActivity.class);
                intent.putExtra(TAGGED_FRIENDS_DATA, (Serializable) taggedFriendList);
                startActivityForResult(intent, TAG_FRIEND_CODE);
            }
        });

        smileyFooterDashboardIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, SmileyActivity.class);
                intent.putExtra(SMILEY_DATA, smileyObject);
                startActivityForResult(intent, SMILEY_CODE);
            }
        });

        smileyTxtTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, SmileyActivity.class);
                intent.putExtra(SMILEY_DATA, smileyObject);
                startActivityForResult(intent, SMILEY_CODE);
            }
        });
    }

    private void setTab(SmartTextView textView, @StringRes int title, @DrawableRes int drawableId, int tabPosition) {
        textView.setText(title);
        textView.setCompoundDrawablesWithIntrinsicBounds(0, drawableId, 0, 0);
        dashboardTabs.getTabAt(tabPosition).setCustomView(textView);
    }

    private void createPostTabIcons() {
        statusTabTv = (SmartTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        photosTabTv = (SmartTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        videoTabTv = (SmartTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        eventsTabTv = (SmartTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        pollsTabTv = (SmartTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

        setTab(statusTabTv, R.string.status, R.drawable.ic_subheader_status, 0);
        setTab(photosTabTv, R.string.photos, R.drawable.ic_subheader_photo, 1);
        setTab(videoTabTv, R.string.video, R.drawable.ic_subheader_video, 2);
        setTab(eventsTabTv, R.string.events, R.drawable.ic_subheader_event, 3);
        setTab(pollsTabTv, R.string.polls, R.drawable.ic_subheader_poll, 4);

        dashboardTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        switchTabsColor(DashboardActivity.this, statusTabTv, R.drawable.ic_subheader_status_hover, true);
                        hideAndShow(statusLayout);
                        break;
                    case 1:
                        switchTabsColor(DashboardActivity.this, photosTabTv, R.drawable.ic_subheader_photo_hover, true);
                        hideAndShow(photosLayout);
                        break;
                    case 2:
                        switchTabsColor(DashboardActivity.this, videoTabTv, R.drawable.ic_subheader_video_hover, true);
                        hideAndShow(videoLayout);
                        break;
                    case 3:
                        switchTabsColor(DashboardActivity.this, eventsTabTv, R.drawable.ic_subheader_event_hover, true);
                        hideAndShow(eventsLayout);
                        break;
                    case 4:
                        switchTabsColor(DashboardActivity.this, pollsTabTv, R.drawable.ic_subheader_poll_hover, true);
                        hideAndShow(pollsLayout);
                        break;

                }
            }

            @Override
            public void onTabUnselected(Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        switchTabsColor(DashboardActivity.this, statusTabTv, R.drawable.ic_subheader_status, false);
                        break;
                    case 1:
                        switchTabsColor(DashboardActivity.this, photosTabTv, R.drawable.ic_subheader_photo, false);
                        break;
                    case 2:
                        switchTabsColor(DashboardActivity.this, videoTabTv, R.drawable.ic_subheader_video, false);
                        break;
                    case 3:
                        switchTabsColor(DashboardActivity.this, eventsTabTv, R.drawable.ic_subheader_event, false);
                        break;
                    case 4:
                        switchTabsColor(DashboardActivity.this, pollsTabTv, R.drawable.ic_subheader_poll, false);
                        break;
                }
            }

            @Override
            public void onTabReselected(Tab tab) {
            }
        });
        switchTabsColor(DashboardActivity.this, statusTabTv, R.drawable.ic_subheader_status_hover, true);
    }


    private void hideAndShow(View view) {
        statusLayout.setVisibility(View.GONE);
        photosLayout.setVisibility(View.GONE);
        videoLayout.setVisibility(View.GONE);
        eventsLayout.setVisibility(View.GONE);
        pollsLayout.setVisibility(View.GONE);

        view.setVisibility(View.VISIBLE);
    }

    private void setSmiley(SmileyObject smileyObject) {
        this.smileyObject = smileyObject;
        if (smileyObject == null) {
            smileyTxtTv.setVisibility(View.GONE);
        } else {
            smileyTxtTv.setVisibility(View.VISIBLE);
        }
        smileyTxtTv.setText(getString(R.string.is_feeling) + " " + getString(smileyObject.getImage_name()));
        smileyTxtTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, smileyObject.getDrawable_id_120(), 0);
    }

    public void setFriendList(List<TagFriendsObject> list) {
        this.taggedFriendList = list;
        if (list.size() == 0) {
            txtTagName.setVisibility(View.GONE);
        } else {
            txtTagName.setVisibility(View.VISIBLE);
        }
        if (list.size() >= 1) {
            strFirstTag = list.get(0).getFriendName().toString();
            if (list.size() > 2) {
                strSecTag = String.valueOf(list.size() - 1) + "others";
                txtTagName.setText(getString(R.string.dash_with) + " " + strFirstTag + " " + getString(R.string.and) + " " + strSecTag);
            } else if (list.size() == 2) {
                strSecTag = String.valueOf(list.size() - 1) + "other";
                txtTagName.setText(getString(R.string.dash_with) + " " + strFirstTag + " " + getString(R.string.and) + " " + strSecTag);
            } else {
                txtTagName.setText("- with" + " " + strFirstTag);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAG_FRIEND_CODE) {
            if (resultCode == RESULT_OK) {
                setFriendList((List<TagFriendsObject>) data.getSerializableExtra(TAGGED_FRIENDS_DATA));
            }
        } else if (requestCode == SMILEY_CODE) {
            if (resultCode == RESULT_OK) {
                smileyObject = (SmileyObject) data.getSerializableExtra(SMILEY_DATA);
                if (smileyObject != null) {
                    setSmiley(smileyObject);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.group_search, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setBackgroundColor(ContextCompat.getColor(DashboardActivity.this, R.color.localisRedColor));
        searchView.setQueryHint(getString(R.string.search_localis));

        SearchView.SearchAutoComplete searchAutoComplete = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHintTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.white));
        searchAutoComplete.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.white));

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       /* switch (item.getItemId()) {
            case R.id.search_group:
                break;
        }*/
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
    }
}
