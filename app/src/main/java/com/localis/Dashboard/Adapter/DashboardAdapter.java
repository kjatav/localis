package com.localis.Dashboard.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Dashboard.Activities.DashboardFeedDetails;
import com.localis.Dashboard.POJOs.FeedItemObject;
import com.localis.R;

import java.util.ArrayList;

import static com.smart.framework.Constants.IMAGE_CODE;
import static com.smart.framework.Constants.POST_TYPE;
import static com.smart.framework.Constants.STATUS_CODE;
import static com.smart.framework.Constants.VIDEO_CODE;

public class DashboardAdapter extends RecyclerView.Adapter {
    private ArrayList<ImageObject> image_objects;
    private ArrayList<FeedItemObject> feed_views_id;
    private Context context;
    private int VIEW_STATUS = 0;
    private int VIEW_IMAGE = 1;
    private int VIEW_VIDEO = 2;

    public DashboardAdapter(ArrayList<FeedItemObject> feedItem, ArrayList<ImageObject> image_objects, Context context) {
        this.image_objects = image_objects;
        this.feed_views_id = feedItem;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       /* View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_feed_item, parent, false);
        DashboardAdapter.MyViewHolder vh = new DashboardAdapter.MyViewHolder(v);*/

        if (viewType == VIEW_STATUS) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.status_feed_item, parent, false);
            //  DashboardAdapter.CHKViewHolder vh = new DashboardAdapter.CHKViewHolder(v);
            return new StatusViewHolder(v);
        } else if (viewType == VIEW_IMAGE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_feed_item, parent, false);
            // DashboardAdapter.RADViewHolder vh = new DashboardAdapter.RADViewHolder(v);
            return new ImageViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_video_feed_item, parent, false);
            //DashboardAdapter.VideoViewHolder vh = new DashboardAdapter.VideoViewHolder(v);
            return new VideoViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        if (viewHolder instanceof StatusViewHolder) {
            StatusViewHolder statusViewHolder = (StatusViewHolder) viewHolder;
            statusViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, DashboardFeedDetails.class);
                    i.putExtra(POST_TYPE, STATUS_CODE);
                    context.startActivity(i);
                }
            });
            statusViewHolder.ivDownArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopup(context, v);
                }
            });
        } else if (viewHolder instanceof ImageViewHolder) {
            ImageViewHolder imageViewHolder = (ImageViewHolder) viewHolder;
            imageViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, DashboardFeedDetails.class);
                    i.putExtra(POST_TYPE, IMAGE_CODE);
                    context.startActivity(i);
                }
            });
            imageViewHolder.ivDownArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopup(context, v);

                }


            });
        } else if (viewHolder instanceof VideoViewHolder) {
            VideoViewHolder videoViewHolder = (VideoViewHolder) viewHolder;
            videoViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, DashboardFeedDetails.class);
                    i.putExtra(POST_TYPE, VIDEO_CODE);
                    context.startActivity(i);
                }
            });
            videoViewHolder.ivDownArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopup(context, v);
                }
            });
        }

    }

    @Override
    public int getItemViewType(int position) {
        int result;

        switch (feed_views_id.get(position).getCONTENT_TYPE()) {
            case STATUS_CODE:
                result = VIEW_STATUS;
                break;
            case IMAGE_CODE:
                result = VIEW_IMAGE;
                break;
            case VIDEO_CODE:
                result = VIEW_VIDEO;
                break;
            default:
                result = VIEW_STATUS;
        }

        return result;
    }

    @Override
    public int getItemCount() {
        if (feed_views_id != null) {
            return feed_views_id.size();
        } else {
            return 0;
        }
        //return 3;
    }

    private class StatusViewHolder extends RecyclerView.ViewHolder {

        ImageView ivBackgroundDashboard, ivVedioPlay, ivDownArrow;

        public StatusViewHolder(View itemView) {
            super(itemView);
            ivDownArrow = itemView.findViewById(R.id.iv_down_arrow);
            /*ivBackgroundDashboard = itemView.findViewById(R.id.iv_back_dashboard);
            ivDownArrow = itemView.findViewById(R.id.iv_down_arrow);
            ivVedioPlay = itemView.findViewById(R.id.iv_vedio_play);*/
        }
    }

    private class ImageViewHolder extends RecyclerView.ViewHolder {

        ImageView ivBackgroundDashboard, ivVedioPlay, ivDownArrow;

        public ImageViewHolder(View itemView) {
            super(itemView);
            ivDownArrow = itemView.findViewById(R.id.iv_down_arrow);
            /*ivBackgroundDashboard = itemView.findViewById(R.id.iv_back_dashboard);
            ivDownArrow = itemView.findViewById(R.id.iv_down_arrow);
            ivVedioPlay = itemView.findViewById(R.id.iv_vedio_play);*/
        }
    }

    private class VideoViewHolder extends RecyclerView.ViewHolder {

        ImageView ivBackgroundDashboard, ivVedioPlay, ivDownArrow;

        public VideoViewHolder(View itemView) {
            super(itemView);
            ivDownArrow = itemView.findViewById(R.id.iv_down_arrow);
            /*ivBackgroundDashboard = itemView.findViewById(R.id.iv_back_dashboard);
            ivDownArrow = itemView.findViewById(R.id.iv_down_arrow);
            ivVedioPlay = itemView.findViewById(R.id.iv_vedio_play);*/
        }
    }

    private void showPopup(final Context context, View ivDownArrow) {

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_down_arrow, null);
        LinearLayout viewGroup = (LinearLayout) layout.findViewById(R.id.popup);
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setFocusable(true);
        int OFFSET_X = -300;
        int OFFSET_Y = 0;

        popup.setBackgroundDrawable(new BitmapDrawable());


        popup.showAsDropDown(ivDownArrow, OFFSET_X, OFFSET_Y);

    }

}