package com.localis.Dashboard.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.Dashboard.Activities.SmileyActivity;
import com.localis.Dashboard.POJOs.SmileyObject;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;

public class SmileyAdapter extends RecyclerView.Adapter<SmileyAdapter.MyViewHolder> {
    private ArrayList<SmileyObject> smileyObjects;
    private Context context;

    public SmileyAdapter(ArrayList<SmileyObject> smileyObjects, Context context) {
        this.smileyObjects = smileyObjects;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_smiley_item, parent, false));
    }

    @Override
    public void onBindViewHolder(SmileyAdapter.MyViewHolder holder, final int position) {
        holder.smileyTxt.setText(smileyObjects.get(position).getImage_name());
        holder.smileyTxt.setCompoundDrawablesWithIntrinsicBounds(smileyObjects.get(position).getDrawable_id_150(), 0, 0, 0);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmileyActivity smileyActivity = (SmileyActivity) context;
                smileyActivity.setFeeling(smileyObjects.get(position));
            }
        });

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return smileyObjects.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        SmartTextView smileyTxt;

        public MyViewHolder(View itemView) {
            super(itemView);
            smileyTxt = itemView.findViewById(R.id.smiley_txt);
        }
    }
}
