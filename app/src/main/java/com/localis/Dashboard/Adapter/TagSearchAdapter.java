package com.localis.Dashboard.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localis.Dashboard.POJOs.TagFriendsObject;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;
import java.util.List;


public class TagSearchAdapter extends RecyclerView.Adapter<TagSearchAdapter.MyViewHolder> {
    private Context context;
    private List<TagFriendsObject> tagFriendsObjects;

    public TagSearchAdapter(Context context, List<TagFriendsObject> tagFriendsObjects) {
        this.context = context;
        this.tagFriendsObjects = tagFriendsObjects;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tag_friend, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.nameTaggedFriendTv.setText(tagFriendsObjects.get(position).getFriendName());

        if (tagFriendsObjects.get(position).isChecked()) {
            holder.taggedFriendCb.setChecked(true);
        } else {
            holder.taggedFriendCb.setChecked(false);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (tagFriendsObjects.get(position).isChecked()) {
                    tagFriendsObjects.get(position).setChecked(false);
                } else {
                    tagFriendsObjects.get(position).setChecked(true);
                }
                notifyDataSetChanged();
            }
        });
    }

    public List<TagFriendsObject> getSelectedFriends() {
        List<TagFriendsObject> selectedFriends = new ArrayList<>();
        for (int i = 0; i < tagFriendsObjects.size(); i++) {
            if (tagFriendsObjects.get(i).isChecked()) {
                selectedFriends.add(tagFriendsObjects.get(i));
            }
        }
        return selectedFriends;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return tagFriendsObjects.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private AppCompatCheckBox taggedFriendCb;
        private ImageView photoTaggedFriendIv;
        private SmartTextView nameTaggedFriendTv;

        public MyViewHolder(View itemView) {
            super(itemView);
            taggedFriendCb = itemView.findViewById(R.id.taggedFriendCb);
            photoTaggedFriendIv = itemView.findViewById(R.id.photoTaggedFriendIv);
            nameTaggedFriendTv = itemView.findViewById(R.id.nameTaggedFriendTv);
        }
    }
}