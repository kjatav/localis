package com.localis.Chat.POJOs;

public class ChatObject {

    String senderName;
    String senderEmail;
    String senderProfilePic;
    String message;
    String dateTime;

    public ChatObject(String senderName, String senderEmail, String senderProfilePic, String message, String dateTime) {
        this.senderName = senderName;
        this.senderEmail = senderEmail;
        this.senderProfilePic = senderProfilePic;
        this.message = message;
        this.dateTime = dateTime;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getSenderProfilePic() {
        return senderProfilePic;
    }

    public void setSenderProfilePic(String senderProfilePic) {
        this.senderProfilePic = senderProfilePic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
