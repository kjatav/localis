package com.localis.Chat.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.Chat.POJOs.ChatObject;
import com.localis.R;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;


public class ChatAdapter extends RecyclerView.Adapter {

    private int VIEW_SEND = 0;
    private int VIEW_RECIVED = 1;

    private Context context;
    private ArrayList<ChatObject> chatObjects;

    public ChatAdapter(Context context, ArrayList<ChatObject> chatObjects) {
        this.context = context;
        this.chatObjects = chatObjects;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_SEND) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_msg_outgoing, parent, false);
            return new SendViewHolder(v);
        } else if (viewType == VIEW_RECIVED) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_msg_incoming, parent, false);
            return new RecievedViewHolder(v);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof SendViewHolder) {
            final SendViewHolder sendViewHolder = (SendViewHolder) holder;
            //Picasso.with(context).load(chatObjects.get(position).getSenderProfilePic()).placeholder(R.drawable.ic_user).into(((SendViewHolder) holder).imageChatIv);
            sendViewHolder.messageChatTv.setText(chatObjects.get(position).getMessage());
            sendViewHolder.timeChatTv.setText(chatObjects.get(position).getDateTime());

        } else if (holder instanceof RecievedViewHolder) {
            final RecievedViewHolder recievedViewHolder = (RecievedViewHolder) holder;
            //Picasso.with(context).load(chatObjects.get(position).getSenderProfilePic()).placeholder(R.drawable.ic_user).into(((RecievedViewHolder) holder).imageChatIv);
            recievedViewHolder.messageChatTv.setText(chatObjects.get(position).getMessage());
            recievedViewHolder.timeChatTv.setText(chatObjects.get(position).getDateTime());
        }
    }

    @Override
    public int getItemCount() {
        if (chatObjects != null) {
            return chatObjects.size();
        } else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        int result = 0;


        if (chatObjects.get(position).getSenderEmail().equals("amin.ebiztrait")) {
            result = VIEW_SEND;
        } else {
            result = VIEW_RECIVED;
        }
        return result;
    }


    private class SendViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView imageChatIv;
        private SmartTextView messageChatTv;
        private SmartTextView timeChatTv;


        private SendViewHolder(View itemView) {
            super(itemView);
            imageChatIv = itemView.findViewById(R.id.image_chat_iv);
            messageChatTv = itemView.findViewById(R.id.message_chat_tv);
            timeChatTv = itemView.findViewById(R.id.time_chat_tv);

        }
    }

    private class RecievedViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView imageChatIv;
        private SmartTextView messageChatTv;
        private SmartTextView timeChatTv;


        private RecievedViewHolder(View itemView) {
            super(itemView);
            imageChatIv = itemView.findViewById(R.id.image_chat_iv);
            messageChatTv = itemView.findViewById(R.id.message_chat_tv);
            timeChatTv = itemView.findViewById(R.id.time_chat_tv);

        }
    }
}