package com.localis.Chat.Activities;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_POJOs.ImageObject;
import com.localis.Chat.Adapters.ChatListAdapter;
import com.localis.R;

import java.util.ArrayList;

public class ChatListActivity extends MasterActivity {
    RecyclerView rv_message;
    Context context;
    LinearLayoutManager linearLayoutManager;
    ChatListAdapter chatListAdapter;
    ArrayList<ImageObject> image_objects = new ArrayList<>();

       @Override
    public int getLayoutID() {
        return R.layout.activity_message;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        context = ChatListActivity.this;
        setHeaderToolbar(R.string.messages);
        setupBottomNav(3);

        rv_message = findViewById(R.id.rv_message);

        linearLayoutManager = new LinearLayoutManager(this);
        rv_message.setLayoutManager(linearLayoutManager);
        chatListAdapter = new ChatListAdapter(image_objects, this);
        rv_message.setAdapter(chatListAdapter);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_menu_top));
    }
}
