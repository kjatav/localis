package com.localis.Chat.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Chat.Adapters.ChatAdapter;
import com.localis.Chat.POJOs.ChatObject;
import com.localis.R;

import java.util.ArrayList;

public class ChatActivity extends MasterActivity {
    RecyclerView messageRv;
    private ArrayList<ChatObject> chatObjects = new ArrayList<>();
    ChatObject chatObject;
    ChatAdapter chatAdapter;
    LinearLayoutManager linearLayoutManager;
    @Override
    public int getLayoutID() {
        return R.layout.activity_message_details;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.message_details);
        messageRv = findViewById(R.id.message_rv);

        chatObjects.add(new ChatObject("amin","amin.ebiztrait","","hello","6.35pm"));
        chatObjects.add(new ChatObject("amin123","amin.ebiztrait123","","hi","6.38pm"));
        chatObjects.add(new ChatObject("amin","amin.ebiztrait","","who are you","6.39pm"));
        chatObjects.add(new ChatObject("amin123","amin.ebiztrait123","","Superman","6.40pm"));
        chatObjects.add(new ChatObject("amin","amin.ebiztrait","",getString(R.string.lorem_ipsum_is_simply_dummy_text_of_the_printing_and_typesetting_industry),"6.39pm"));
        chatObjects.add(new ChatObject("amin123","amin.ebiztrait123","",getString(R.string.lorem_ipsum_is_simply_dummy_text_of_the_printing_and_typesetting_industry),"6.40pm"));


        linearLayoutManager = new LinearLayoutManager(this);
        messageRv.setLayoutManager(linearLayoutManager);
        chatAdapter = new ChatAdapter(this,chatObjects);
        messageRv.setAdapter(chatAdapter);

    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
