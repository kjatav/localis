package com.localis.Pages.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.Events.POJOs.AttendeeObject;
import com.localis.R;

import java.util.List;


public class PageFollowerItemAdapter extends RecyclerView.Adapter<PageFollowerItemAdapter.MyViewHolder> {
    private Context context;
    private List<AttendeeObject> memeberObjects;

    public PageFollowerItemAdapter(Context context, List<AttendeeObject> tagFriendsObjects) {
        this.context = context;
        this.memeberObjects = tagFriendsObjects;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_follower, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public MyViewHolder(View itemView) {
            super(itemView);

        }
    }
}