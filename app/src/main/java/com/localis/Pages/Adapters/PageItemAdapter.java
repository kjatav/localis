package com.localis.Pages.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Pages.Activites.PageDetailActivity;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;


public class PageItemAdapter extends RecyclerView.Adapter<PageItemAdapter.MyViewHolder> {
    private ArrayList<ImageObject> image_objects;
    private Context context;


    public PageItemAdapter(ArrayList<ImageObject> image_objects, Context context) {
        this.image_objects = image_objects;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PageItemAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_page_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PageDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return image_objects.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgPageItemIv;
        private ImageView profilePageItemIv;
        private SmartTextView namePageItemTv;
        private ImageView bookmarkPageItemIv;
        private SmartTextView categoryPageItemTv;
        private SmartTextView typePageItemTv;
        private SmartTextView descPageItemTv;
        private SmartTextView likesPageItemTv;
        private SmartTextView datePageItemTv;
        private SmartTextView likePageItemBtn;


        public MyViewHolder(View itemView) {
            super(itemView);
            imgPageItemIv = itemView.findViewById(R.id.img_page_item_iv);
            profilePageItemIv = itemView.findViewById(R.id.profile_page_item_iv);
            namePageItemTv = itemView.findViewById(R.id.name_page_item_tv);
            bookmarkPageItemIv = itemView.findViewById(R.id.bookmark_page_item_iv);
            categoryPageItemTv = itemView.findViewById(R.id.category_page_item_tv);
            typePageItemTv = itemView.findViewById(R.id.type_page_item_tv);
            descPageItemTv = itemView.findViewById(R.id.desc_page_item_tv);
            likesPageItemTv = itemView.findViewById(R.id.likes_page_item_tv);
            datePageItemTv = itemView.findViewById(R.id.date_page_item_tv);
            likePageItemBtn = itemView.findViewById(R.id.like_page_item_btn);
        }
    }
}
