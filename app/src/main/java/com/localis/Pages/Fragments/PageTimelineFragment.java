package com.localis.Pages.Fragments;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Dashboard.Adapter.DashboardAdapter;
import com.localis.Dashboard.Fragments.PhotosFragment;
import com.localis.Dashboard.Fragments.StatusFragment;
import com.localis.Dashboard.Fragments.VideoFragment;
import com.localis.Dashboard.POJOs.FeedItemObject;
import com.localis.Dashboard.POJOs.TagFriendsObject;
import com.localis.Events.Fragments.EventTimelineFragment;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartTextView;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;
import java.util.List;

import static com.smart.framework.SmartUtils.switchTabsColor;


public class PageTimelineFragment extends SmartFragment {
    private TabLayout dashboardTabs;

    private ImageView imgPageDetailIv;
    private ImageView optionsPageDetailIv;
    private ImageView profilePageDetailIv;
    private SmartTextView namePageDetailTv;
    private RatingBar ratingPageDetailRv;
    private SmartButton likePageTimelineBtn;
    private SmartTextView categoryPageTimelineTv;
    private SmartTextView namePageTimelineTv;
    private SmartTextView hitsPageTimelineTv;
    private SmartTextView openPageTimelineTv;
    private RecyclerView pagesTimelineRv;


    private ArrayList<ImageObject> image_objects = new ArrayList<>();
    private ArrayList<FeedItemObject> feedItem = new ArrayList<>();

    private DashboardAdapter dashboardAdapter;


    private SmartTextView statusTabTv;
    private SmartTextView photosTabTv;
    private SmartTextView videoTabTv;

    private View statusLayout;
    private View photosLayout;
    private View videoLayout;

    @Override
    public int setLayoutId() {
        return R.layout.fragment_pages_timeline;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {

        imgPageDetailIv = currentView.findViewById(R.id.img_page_detail_iv);
        optionsPageDetailIv = currentView.findViewById(R.id.options_page_detail_iv);
        profilePageDetailIv = currentView.findViewById(R.id.profile_page_detail_iv);
        namePageDetailTv = currentView.findViewById(R.id.name_page_detail_tv);
        ratingPageDetailRv = currentView.findViewById(R.id.rating_page_detail_rv);
        likePageTimelineBtn = currentView.findViewById(R.id.like_page_timeline_btn);
        categoryPageTimelineTv = currentView.findViewById(R.id.category_page_timeline_tv);
        namePageTimelineTv = currentView.findViewById(R.id.name_page_timeline_tv);
        hitsPageTimelineTv = currentView.findViewById(R.id.hits_page_timeline_tv);
        openPageTimelineTv = currentView.findViewById(R.id.open_page_timeline_tv);
        pagesTimelineRv = currentView.findViewById(R.id.pages_timeline_rv);


        dashboardTabs = currentView.findViewById(R.id.post_timeline_tabs);
        for (int i = 0; i < 3; i++) {
            dashboardTabs.addTab(dashboardTabs.newTab());
        }
        dashboardTabs.setTabMode(TabLayout.MODE_FIXED);

        statusLayout = currentView.findViewById(R.id.status_layout);
        photosLayout = currentView.findViewById(R.id.photos_layout);
        videoLayout = currentView.findViewById(R.id.video_layout);

        createPostTabIcons();


        pagesTimelineRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        dashboardAdapter = new DashboardAdapter(feedItem, image_objects, getContext());
        pagesTimelineRv.setAdapter(dashboardAdapter);

        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));
        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));
        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));


        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }

    private void createPostTabIcons() {
        statusTabTv = (SmartTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        statusTabTv.setText(R.string.status);
        statusTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_status, 0, 0);
        dashboardTabs.getTabAt(0).setCustomView(statusTabTv);

        photosTabTv = (SmartTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        photosTabTv.setText(R.string.photos);
        photosTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_photo, 0, 0);
        dashboardTabs.getTabAt(1).setCustomView(photosTabTv);

        videoTabTv = (SmartTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        videoTabTv.setText(R.string.video);
        videoTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_video, 0, 0);
        dashboardTabs.getTabAt(2).setCustomView(videoTabTv);


        dashboardTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        switchTabsColor(getActivity(), statusTabTv, R.drawable.ic_subheader_status_hover, true);
                        hideAndShow(statusLayout);
                        break;
                    case 1:
                        switchTabsColor(getActivity(), photosTabTv, R.drawable.ic_subheader_photo_hover, true);
                        hideAndShow(photosLayout);
                        break;
                    case 2:
                        switchTabsColor(getActivity(), videoTabTv, R.drawable.ic_subheader_video_hover, true);
                        hideAndShow(videoLayout);
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        switchTabsColor(getActivity(), statusTabTv, R.drawable.ic_subheader_status, false);
                        break;
                    case 1:
                        switchTabsColor(getActivity(), photosTabTv, R.drawable.ic_subheader_photo, false);
                        break;
                    case 2:
                        switchTabsColor(getActivity(), videoTabTv, R.drawable.ic_subheader_video, false);
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        switchTabsColor(getActivity(), statusTabTv, R.drawable.ic_subheader_status_hover, true);
    }

    private void hideAndShow(View view) {
        statusLayout.setVisibility(View.GONE);
        photosLayout.setVisibility(View.GONE);
        videoLayout.setVisibility(View.GONE);

        view.setVisibility(View.VISIBLE);
    }
}
