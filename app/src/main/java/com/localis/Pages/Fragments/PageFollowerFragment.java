package com.localis.Pages.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.localis.Events.POJOs.AttendeeObject;
import com.localis.Pages.Adapters.PageFollowerItemAdapter;
import com.localis.R;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pct1 on 5/3/18.
 */

public class PageFollowerFragment extends SmartFragment {

    private RecyclerView pageFollowerRv;
    private PageFollowerItemAdapter pageFollowerItemAdapter;
    private List<AttendeeObject> attendObjects = new ArrayList<>();

    @Override
    public int setLayoutId() {
        return R.layout.fragment_pages_follower;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        pageFollowerRv = currentView.findViewById(R.id.page_follower_rv);
        pageFollowerRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        pageFollowerItemAdapter = new PageFollowerItemAdapter(getActivity(), attendObjects);
        pageFollowerRv.setAdapter(pageFollowerItemAdapter);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }
}
