package com.localis.Pages.Fragments;


import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Pages.Adapters.PageItemAdapter;
import com.localis.R;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartRecyclerView;

import java.util.ArrayList;

import static com.smart.framework.SmartUtils.showSortingPopup;

/**
 * Created by pct1 on 28/2/18.
 */

public class PagesListFragment extends SmartFragment {

    private SmartTextView categoryPagesTv;
    private SmartTextView sortByDatePageTv;
    private SmartRecyclerView pageListRv;
    private View layoutNoData;


    LinearLayoutManager linearLayoutManager;
    PageItemAdapter pageItemAdapter;
    ArrayList<ImageObject> image_objects = new ArrayList<>();

    @Override
    public int setLayoutId() {
        return R.layout.fragment_page_list;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        categoryPagesTv = currentView.findViewById(R.id.category_pages_tv);
        sortByDatePageTv = currentView.findViewById(R.id.sort_by_date_page_tv);
        pageListRv = currentView.findViewById(R.id.page_list_rv);
        layoutNoData = currentView.findViewById(R.id.layout_no_data);


        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));

        pageListRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        pageItemAdapter = new PageItemAdapter(image_objects, getActivity());
        pageListRv.setAdapter(pageItemAdapter);
        pageListRv.setEmptyView(layoutNoData);

        setHasOptionsMenu(true);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        sortByDatePageTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSortingPopup(getActivity(), sortByDatePageTv, getResources().getStringArray(R.array.sort_by_date), new AlertMagneticClass.PopUpInterface() {
                    @Override
                    public void onSetPopUp(int selectedOptionPosition) {
                        sortByDatePageTv.setText(getResources().getStringArray(R.array.sort_by_date)[selectedOptionPosition]);
                    }
                });
            }
        });
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.pages_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        categoryPagesTv.setText(item.getTitle());
        return super.onOptionsItemSelected(item);
    }
}
