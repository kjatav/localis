package com.localis.Pages.Activites;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.ViewPagerAdapter;
import com.localis.Albums.Activites.AlbumAddActivity;
import com.localis.Albums.Fragments.AlbumFragment;
import com.localis.Announcements.Activities.AnnouncementAddActivity;
import com.localis.Announcements.Fragments.AnnouncementFragment;
import com.localis.Events.Activities.EventAddActivity;
import com.localis.Events.Fragments.EventListFragment;
import com.localis.Groups.Fragments.GroupAboutFragment;
import com.localis.Groups.Fragments.GroupAppsFragment;
import com.localis.Pages.Fragments.PageFollowerFragment;
import com.localis.Pages.Fragments.PageTimelineFragment;
import com.localis.R;
import com.localis.Videos.Activities.VideoAddActivity;
import com.localis.Videos.Fragments.VideoListFragment;

import static com.smart.framework.SmartUtils.setAddButton;

public class PageDetailActivity extends MasterActivity {

    private FloatingActionButton addPagesDetailsBtn;
    private ViewPager.OnPageChangeListener onPageChangeListener;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_aa_pager;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.pages_detail);

        ViewPager pageDetailsVp = findViewById(R.id.details_vp);
        TabLayout pagesTabLayout = findViewById(R.id.details_tab);
        addPagesDetailsBtn = findViewById(R.id.details_add_btn);

        setupViewPager(pageDetailsVp);
        pagesTabLayout.setupWithViewPager(pageDetailsVp);
        setupOnPageChangeListener();
        pageDetailsVp.addOnPageChangeListener(onPageChangeListener);
    }


    private void setupOnPageChangeListener() {
        onPageChangeListener = new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        addPagesDetailsBtn.setVisibility(View.GONE);
                        break;
                    case 1:
                        addPagesDetailsBtn.setVisibility(View.GONE);
                        break;
                    case 2:
                        addPagesDetailsBtn.setVisibility(View.GONE);
                        break;
                    case 3:
                        setAddButton(PageDetailActivity.this, AlbumAddActivity.class, addPagesDetailsBtn);
                        break;
                    case 4:
                        setAddButton(PageDetailActivity.this, VideoAddActivity.class, addPagesDetailsBtn);
                        break;
                    case 5:
                        setAddButton(PageDetailActivity.this, EventAddActivity.class, addPagesDetailsBtn);
                        break;
                    case 6:
                        setAddButton(PageDetailActivity.this, AnnouncementAddActivity.class, addPagesDetailsBtn);
                        break;
                    case 7:
                        addPagesDetailsBtn.setVisibility(View.GONE);
                        break;
                    case 8:
                        addPagesDetailsBtn.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }


    private void setupViewPager(final ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PageTimelineFragment(), "Timeline");
        adapter.addFragment(new GroupAboutFragment(), "About");
        adapter.addFragment(new PageFollowerFragment(), "Followers");
        adapter.addFragment(new AlbumFragment(), "Albums");
        adapter.addFragment(new VideoListFragment(), "Videos");
        adapter.addFragment(new EventListFragment(), "Event");
        adapter.addFragment(new AnnouncementFragment(), "Announcements");
        adapter.addFragment(new AnnouncementFragment(), "Complains");
        adapter.addFragment(new GroupAppsFragment(), "Apps");
        viewPager.setAdapter(adapter);
        viewPager.post(new Runnable() {
            @Override
            public void run() {
                onPageChangeListener.onPageSelected(viewPager.getCurrentItem());
            }
        });
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_menu_top));
    }
}
