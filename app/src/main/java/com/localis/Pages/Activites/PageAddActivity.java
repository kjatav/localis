package com.localis.Pages.Activites;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagneticClass;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import static com.smart.framework.SmartUtils.GetListDialog;

public class PageAddActivity extends MasterActivity {

    private SmartEditText titlePageAddEt;
    private SmartEditText categoryPageAddEt;
    private SmartEditText linkPageAddEt;
    private SmartEditText descPageAddEt;
    private SmartEditText typePageAddEt;
    private RoundedImageView profilePageAddIv;
    private SmartTextView profilePageAddTv;
    private ImageView coverPageAddIv;
    private SmartTextView coverPageAddTv;
    private SwitchCompat audioPageAddSw;
    private SwitchCompat eventsPageAddSw;
    private SwitchCompat feedsPageAddSw;
    private SwitchCompat filesPageAddSw;
    private SwitchCompat pollsPageAddSw;
    private SmartButton submitPageAddBtn;
    private SmartButton cancelPageAddBtn;


    private Context context;
    private String pageCategory[] = {"Local businesses", "Projects", "Tourism"};
    private String pageType[] = {"Public page", "private page", "Invite only page"};


    private boolean isProfile;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_page_add;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        context = PageAddActivity.this;
        setHeaderToolbar(R.string.add_new_pages);

        titlePageAddEt = findViewById(R.id.title_page_add_et);
        categoryPageAddEt = findViewById(R.id.category_page_add_et);
        linkPageAddEt = findViewById(R.id.link_page_add_et);
        descPageAddEt = findViewById(R.id.desc_page_add_et);
        typePageAddEt = findViewById(R.id.type_page_add_et);
        profilePageAddIv = findViewById(R.id.profile_page_add_iv);
        profilePageAddTv = findViewById(R.id.profile_page_add_tv);
        coverPageAddIv = findViewById(R.id.cover_page_add_iv);
        coverPageAddTv = findViewById(R.id.cover_page_add_tv);
        audioPageAddSw = findViewById(R.id.audio_page_add_sw);
        eventsPageAddSw = findViewById(R.id.events_page_add_sw);
        feedsPageAddSw = findViewById(R.id.feeds_page_add_sw);
        filesPageAddSw = findViewById(R.id.files_page_add_sw);
        pollsPageAddSw = findViewById(R.id.polls_page_add_sw);
        submitPageAddBtn = findViewById(R.id.submit_page_add_btn);
        cancelPageAddBtn = findViewById(R.id.cancel_page_add_btn);

    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        categoryPageAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(context, pageCategory, getString(R.string.choose_your_city), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        categoryPageAddEt.setText(pageCategory[id]);
                    }
                });
            }
        });

        typePageAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(context, pageType, getString(R.string.choose_your_city), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        typePageAddEt.setText(pageType[id]);
                    }
                });
            }
        });

        profilePageAddTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isProfile = true;
                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                        .start(PageAddActivity.this);
            }
        });

        coverPageAddTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isProfile = false;
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(PageAddActivity.this);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                if (isProfile) {
                    Picasso.with(PageAddActivity.this).load(resultUri).into(profilePageAddIv);
                } else {
                    Picasso.with(PageAddActivity.this).load(resultUri).into(coverPageAddIv);
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(PageAddActivity.this, String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
