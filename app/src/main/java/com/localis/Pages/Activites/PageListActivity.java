package com.localis.Pages.Activites;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.ViewPagerAdapter;
import com.localis.Pages.Fragments.PagesListFragment;
import com.localis.R;

public class PageListActivity extends MasterActivity {

    private FloatingActionButton addPagesBtn;

    @Override
    public int getLayoutID() {
        return R.layout.activity_aa_pager;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.pages);

        TabLayout tabLayout = findViewById(R.id.details_tab);
        ViewPager pageListVp = findViewById(R.id.details_vp);

        addPagesBtn = findViewById(R.id.details_add_btn);

        setupViewPager(pageListVp);
        tabLayout.setupWithViewPager(pageListVp);
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        addPagesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PageListActivity.this, PageAddActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PagesListFragment(), "All Pages");
        adapter.addFragment(new PagesListFragment(), "Featured Pages");
        adapter.addFragment(new PagesListFragment(), "My Pages");
        adapter.addFragment(new PagesListFragment(), "My Like Pages");

        viewPager.setAdapter(adapter);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_menu_top));
    }
}
