package com.localis.Authentication;

import android.content.Context;
import android.content.DialogInterface;

import com.android.volley.Request;
import com.localis.R;
import com.smart.framework.AlertMagnatic;
import com.smart.framework.SmartApplication;
import com.smart.framework.SmartUtils;
import com.smart.weservice.SmartWebManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.smart.framework.Constants.ANDROID;
import static com.smart.framework.Constants.CODE;
import static com.smart.framework.Constants.DATA;
import static com.smart.framework.Constants.DEVICE_TOKEN;
import static com.smart.framework.Constants.DEVICE_TYPE;
import static com.smart.framework.Constants.EMAIL;
import static com.smart.framework.Constants.PASSWORD;
import static com.smart.framework.Constants.USER_DATA;
import static com.smart.framework.Constants.USER_ID;

public class AuthenticationApiCalls {


    public static void login(final Context context, String email, String password, final AuthImplementation.Login login) {

        SmartUtils.showLoadingDialog(context);

        Map<String, String> params = new HashMap<>();
        params.put(EMAIL, email);
        params.put(PASSWORD, password);
        params.put(DEVICE_TYPE, ANDROID);
        params.put(DEVICE_TOKEN, "asdf");


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_user) + "login");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_DATA, results.getJSONObject(DATA).toString());
                    SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_ID, results.getJSONObject(DATA).getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                login.onLoginSuccess("Login Success");
            }

            @Override
            public void onResponseError(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    login.onLoginFail(results.getString(DATA), results.getString(CODE));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, false);
    }


    public static void signUp(final Context context, Map<String, String> signUpData, final AuthImplementation.SignUp signUp) {

        SmartUtils.showLoadingDialog(context);

        signUpData.put(DEVICE_TYPE, ANDROID);
        signUpData.put(DEVICE_TOKEN, "asdf");


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_user) + "signup");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, signUpData);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                signUp.onSignUpSuccess("SignUp Success");
            }

            @Override
            public void onResponseError(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                signUp.onSignUpFail("SignUp Failed");
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void activateAccount(final Context context, String email, String vCode, final AuthImplementation.ActivateAcc activateAcc) {

        SmartUtils.showLoadingDialog(context);

        Map<String, String> params = new HashMap<>();

        params.put("email", email);
        params.put("vcode", vCode);
        params.put(DEVICE_TYPE, ANDROID);
        params.put(DEVICE_TOKEN, "asdf");


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_user) + "verifyaccountwithvcode");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                activateAcc.onActivationSuccess("Activation Success");
            }

            @Override
            public void onResponseError(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                activateAcc.onActivationFail("Activation Failed");
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void resetPassword(final Context context, String email, final AuthImplementation.ResetPassword resetPassword) {

        SmartUtils.showLoadingDialog(context);

        Map<String, String> params = new HashMap<>();

        params.put("email", email);
        params.put(DEVICE_TYPE, ANDROID);
        params.put(DEVICE_TOKEN, "asdf");


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_user) + "forgotpassword");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                resetPassword.onPasswordResetSuccess("Password Reset Success");
            }

            @Override
            public void onResponseError(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                resetPassword.onPasswordResetFail("Password Reset Failed");
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }

    public static void resendActivationKey(final Context context, String email, final AuthImplementation.ResendActivationKey resendActivationKey) {

        SmartUtils.showLoadingDialog(context);

        Map<String, String> params = new HashMap<>();

        params.put("email", email);
        params.put(DEVICE_TYPE, ANDROID);
        params.put(DEVICE_TOKEN, "asdf");


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_user) + "resendActivationKey");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                resendActivationKey.onActivationKeySent("Check your email to get you activation key.");
            }

            @Override
            public void onResponseError(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                resendActivationKey.onActivationKeySentFail("Not able to send the email");
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void logout(final Context context, final AuthImplementation.Logout logout) {
        SmartUtils.getConfirmDialog(context, "Logout", "Are your sure your want to logout from Localis?", "Yes", "No", true, new AlertMagnatic() {
            @Override
            public void PositiveMethod(DialogInterface dialog, int id) {
                dialog.dismiss();
                SmartUtils.showLoadingDialog(context);

                Map<String, String> params = new HashMap<>();

                params.put("userid", SmartUtils.getUserId());
                params.put(DEVICE_TYPE, ANDROID);
                params.put(DEVICE_TOKEN, "asdf");


                HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

                requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.GET);
                requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_user) + "logout");
                requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
                requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
                requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

                    @Override
                    public void onResponseReceived(JSONObject results) {
                        SmartUtils.hideLoadingDialog();
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_DATA, null);
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_ID, null);
                        logout.onLogoutSuccess("Logout Success");
                    }

                    @Override
                    public void onResponseError(JSONObject results) {
                        SmartUtils.hideLoadingDialog();
                        logout.onLogoutFail("Logout Fail");
                    }
                });
                SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
            }

            @Override
            public void NegativeMethod(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

    }
}
