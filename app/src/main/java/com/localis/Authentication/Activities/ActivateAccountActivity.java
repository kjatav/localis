package com.localis.Authentication.Activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Authentication.AuthImplementation;
import com.localis.Authentication.AuthenticationApiCalls;
import com.localis.Dashboard.Activities.DashboardActivity;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.framework.SmartUtils;

public class ActivateAccountActivity extends MasterActivity implements AuthImplementation.ActivateAcc {
    private SmartEditText etActivationCode;
    private SmartButton btnActiveAccount;

    //1227

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_activate_account;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        etActivationCode = findViewById(R.id.et_activation_code);
        btnActiveAccount = findViewById(R.id.btn_active_account);

    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();
        btnActiveAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().getStringExtra(EMAIL) != null) {
                    if (!TextUtils.isEmpty(etActivationCode.getText().toString())) {
                        AuthenticationApiCalls.activateAccount(ActivateAccountActivity.this, getIntent().getStringExtra(EMAIL), etActivationCode.getText().toString(), ActivateAccountActivity.this);
                    } else {
                        SmartUtils.showSnackBarLong(ActivateAccountActivity.this, "Please enter a verification code.");
                    }
                } else {
                    SmartUtils.showSnackBarShort(ActivateAccountActivity.this, "Email not found");
                }


            }
        });
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);

    }

    @Override
    public void onActivationSuccess(String responseMessage) {
        SmartUtils.toastLong(ActivateAccountActivity.this, "Account activated, Login to continue.");
        Intent intent = new Intent(ActivateAccountActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onActivationFail(String responseMessage) {

    }
}
