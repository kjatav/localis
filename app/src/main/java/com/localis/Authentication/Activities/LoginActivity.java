package com.localis.Authentication.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Authentication.AuthImplementation;
import com.localis.Authentication.AuthenticationApiCalls;
import com.localis.Dashboard.Activities.DashboardActivity;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.customViews.SmartTextView;
import com.smart.framework.SmartApplication;
import com.smart.framework.SmartUtils;

import static com.smart.framework.SmartUtils.showSnackBarLong;
import static com.smart.framework.SmartUtils.toastLong;

public class LoginActivity extends MasterActivity implements AuthImplementation.Login {

    private SmartEditText emailLoginEt;
    private SmartEditText passwordLoginEt;

    private SmartTextView forgotPasswordLoginTv;


    private SmartButton goLoginBtn;
    private SmartButton registerLoginBtn;

    private Dialog dialogSendEmail;


    @Override
    public int getDrawerLayoutID() {
        return 0;
    }


    @Override
    public int getLayoutID() {
        return R.layout.activity_login;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void initComponents() {
        super.initComponents();

        emailLoginEt = findViewById(R.id.email_login_et);
        passwordLoginEt = findViewById(R.id.password_login_et);

        forgotPasswordLoginTv = findViewById(R.id.forgot_password_login_tv);

        goLoginBtn = findViewById(R.id.go_login_btn);

        registerLoginBtn = findViewById(R.id.register_login_btn);
    }


    @Override
    public void setActionListeners() {
        super.setActionListeners();

        forgotPasswordLoginTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ResetPasswordActivity.class);
                startActivity(i);
            }
        });

        goLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showSendEmailDialog();
                login();
            }
        });

        registerLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });
    }


    private void showSendEmailDialog() {

        dialogSendEmail = new Dialog(LoginActivity.this);
        dialogSendEmail.setContentView(R.layout.dialog_send_email);

        SmartButton getEmailBtn = dialogSendEmail.findViewById(R.id.get_email_btn);
        SmartButton cancelGetEmailBtn = dialogSendEmail.findViewById(R.id.cancel_get_email_btn);
        ImageView crossGetEmailIv = dialogSendEmail.findViewById(R.id.cross_get_email_iv);

        getEmailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthenticationApiCalls.resendActivationKey(LoginActivity.this, emailLoginEt.getText().toString(), new AuthImplementation.ResendActivationKey() {
                    @Override
                    public void onActivationKeySent(String responseMessage) {
                        toastLong(LoginActivity.this, responseMessage);
                        dialogSendEmail.dismiss();
                        Intent intent = new Intent(LoginActivity.this, ActivateAccountActivity.class);
                        intent.putExtra(EMAIL, emailLoginEt.getText().toString());
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }

                    @Override
                    public void onActivationKeySentFail(String responseMessage) {
                        toastLong(LoginActivity.this, responseMessage);
                    }
                });

            }
        });

        cancelGetEmailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSendEmail.dismiss();
            }
        });

        crossGetEmailIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSendEmail.dismiss();
            }
        });

        dialogSendEmail.show();
    }


    private void login() {

        if (TextUtils.isEmpty(passwordLoginEt.getText().toString())) {
            SmartUtils.showSnackBar(LoginActivity.this, "Please enter a valid Password", Snackbar.LENGTH_LONG);

        } else {
            AuthenticationApiCalls.login(LoginActivity.this, emailLoginEt.getText().toString(), passwordLoginEt.getText().toString(), this);
        }
    }


    public void enterUserName(View v) {
        emailLoginEt.setText("hardikb.ebiztrait@gmail.com");
        passwordLoginEt.setText("1234");
    }


    @Override
    public void onLoginSuccess(String responseMessage) {
        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onLoginFail(String responseMessage, String responseCode) {
        if (responseCode.equals("301")) {
            showSendEmailDialog();
        } else {
            showSnackBarLong(LoginActivity.this, responseMessage);
        }
    }
}
