package com.localis.Authentication.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Authentication.AuthImplementation;
import com.localis.Authentication.AuthenticationApiCalls;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.framework.SmartUtils;

import static com.smart.framework.SmartUtils.errorEditTextRed;

public class ResetPasswordActivity extends MasterActivity implements AuthImplementation.ResetPassword {
    private SmartEditText resetEmailEt;
    private SmartButton sendPasswordBtn;

    private Dialog dialogCheckEmail;


    @Override
    public int getDrawerLayoutID() {
        return 0;
    }


    @Override
    public int getLayoutID() {
        return R.layout.activity_reset_password;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        resetEmailEt = findViewById(R.id.reset_email_et);
        sendPasswordBtn = findViewById(R.id.send_password_btn);

        sendPasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(resetEmailEt.getText().toString())) {
                    errorEditTextRed(ResetPasswordActivity.this, 0, true, "Please enter an email address", resetEmailEt);
                } else if (!SmartUtils.emailValidator(resetEmailEt.getText().toString())) {
                    errorEditTextRed(ResetPasswordActivity.this, 0, true, "Please enter a valid email address", resetEmailEt);
                } else {
                    AuthenticationApiCalls.resetPassword(ResetPasswordActivity.this, resetEmailEt.getText().toString(), ResetPasswordActivity.this);
                }
            }
        });
    }


    private void showCheckEmailDialog() {

        dialogCheckEmail = new Dialog(ResetPasswordActivity.this);
        dialogCheckEmail.setContentView(R.layout.dialog_check_email);

        SmartButton okayCheckEmailBtn = dialogCheckEmail.findViewById(R.id.okay_check_email_btn);

        okayCheckEmailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCheckEmail.dismiss();
                Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        dialogCheckEmail.show();
    }


    @Override
    public void onPasswordResetSuccess(String responseMessage) {
        SmartUtils.toastLong(ResetPasswordActivity.this, "Check your email for new password.");
        Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onPasswordResetFail(String responseMessage) {

    }
}
