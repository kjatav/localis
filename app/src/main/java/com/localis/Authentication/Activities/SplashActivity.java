package com.localis.Authentication.Activities;

import android.content.Intent;

import com.localis.AA_Actvities.CoreMaster;
import com.localis.Dashboard.Activities.DashboardActivity;
import com.localis.Groups.Activites.GroupListActivity;
import com.localis.Groups.GroupApis;
import com.localis.Groups.GroupImpl;
import com.localis.Groups.POJOs.GroupCategoriesObject;
import com.localis.R;
import com.localis.ZZ_Package.LocationAPIs;
import com.localis.ZZ_Package.LocationImpl;
import com.smart.caching.SmartCaching;
import com.smart.framework.SmartUtils;

import org.json.JSONArray;

import java.util.ArrayList;

import static com.smart.framework.SmartUtils.getUserData;
import static com.smart.framework.SmartUtils.getUserId;

public class SplashActivity extends CoreMaster implements GroupImpl.GetGroupCategories, LocationImpl.GetStates {

    private SmartCaching smartCaching;


    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_splash;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        smartCaching = new SmartCaching(this);

        GroupApis.getGroupCategoriesAPI(SplashActivity.this, this);

    }

    @Override
    public void prepareViews() {
    }

    @Override
    public void setActionListeners() {
    }


    @Override
    public void onStatesReceived(JSONArray states) {
        smartCaching.cacheResponse(states, TABLE_STATES, false);
        if (getUserId() != null && getUserData() != null) {
            Intent intent = new Intent(SplashActivity.this, DashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onGetStateFailed() {

    }


    @Override
    public void onGroupCategoriesReceived(JSONArray groupCategories) {
        smartCaching.cacheResponse(groupCategories, TABLE_GROUP_CATEGORIES, false);
        LocationAPIs.getStates(SplashActivity.this, this);
    }

    @Override
    public void onGroupCategoriesFailed() {

    }

}
