package com.localis.Authentication.Activities;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Authentication.AuthImplementation;
import com.localis.Authentication.AuthenticationApiCalls;
import com.localis.R;
import com.localis.ZZ_Package.LocationAPIs;
import com.localis.ZZ_Package.LocationImpl;
import com.smart.caching.SmartCaching;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.AlertNeutral;
import com.smart.framework.SmartUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.localis.ZZ_Package.LocationAPIs.setCityList;
import static com.localis.ZZ_Package.LocationAPIs.setStateList;
import static com.localis.ZZ_Package.LocationAPIs.setStreetList;
import static com.smart.framework.SmartUtils.GetListDialog;
import static com.smart.framework.SmartUtils.checkEmptyEditTexts;
import static com.smart.framework.SmartUtils.getLatitude;
import static com.smart.framework.SmartUtils.getLongitude;
import static com.smart.framework.SmartUtils.getOKDialog;
import static com.smart.framework.SmartUtils.toastShort;
import static com.smart.framework.SmartUtils.urlValidator;

public class RegisterActivity extends MasterActivity implements AuthImplementation.SignUp, LocationImpl.StateSelecting, LocationImpl.CitySelecting, LocationImpl.StreetSelecting {

    private SmartEditText userProfileTypeRegEt;
    private SmartEditText firstNameRegEt;
    private SmartEditText lastNameRegEt;
    private TextInputLayout usernameWrapper;
    private SmartEditText userNameRegEt;
    private SmartEditText passwordRegEt;
    private SmartEditText confirmPasswordRegEt;
    private ImageView passwordRegIv;
    private ImageView confirmPasswordRegIv;
    private SmartEditText emailRegEt;
    private SmartEditText stateRegEt;
    private SmartEditText cityRegEt;
    private TextInputLayout streetRegTil;
    private SmartEditText streetRegEt;
    private TextInputLayout associationRegTil;
    private SmartEditText associationRegEt;
    private TextInputLayout addressRegTil;
    private SmartEditText addressRegEt;
    private TextInputLayout mobileRegTil;
    private SmartEditText mobileRegEt;
    private TextInputLayout websiteRegTil;
    private SmartEditText websiteRegEt;
    private SmartButton createAccBtn;

    private String profileTypeCode;


    private String[] stateNames;

    private String stateId;
    private String cityId;
    private String streetId;


    private String profileTypes[];

    private AlertDialog.Builder stateDialogBuilder;
    private AlertDialog.Builder cityDialogBuilder;
    private AlertDialog.Builder streetDialogBuilder;

    private boolean citySelected = false;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }


    @Override
    public int getLayoutID() {
        return R.layout.activity_register;
    }

    @Override
    public void initComponents() {
        super.initComponents();

        userProfileTypeRegEt = findViewById(R.id.user_profileType_reg_et);
        firstNameRegEt = findViewById(R.id.first_name_reg_et);
        lastNameRegEt = findViewById(R.id.last_name_reg_et);
        usernameWrapper = findViewById(R.id.username_wrapper);
        userNameRegEt = findViewById(R.id.user_name_reg_et);
        passwordRegEt = findViewById(R.id.password_reg_et);
        confirmPasswordRegEt = findViewById(R.id.confirm_password_reg_et);
        passwordRegIv = findViewById(R.id.password_reg_iv);
        confirmPasswordRegIv = findViewById(R.id.confirm_password_reg_iv);
        emailRegEt = findViewById(R.id.email_reg_et);
        stateRegEt = findViewById(R.id.state_reg_et);
        cityRegEt = findViewById(R.id.city_reg_et);
        streetRegTil = findViewById(R.id.street_reg_til);
        streetRegEt = findViewById(R.id.street_reg_et);
        associationRegTil = findViewById(R.id.association_reg_til);
        associationRegEt = findViewById(R.id.association_reg_et);
        addressRegTil = findViewById(R.id.address_reg_til);
        addressRegEt = findViewById(R.id.address_reg_et);
        mobileRegTil = findViewById(R.id.mobile_reg_til);
        mobileRegEt = findViewById(R.id.mobile_reg_et);
        websiteRegTil = findViewById(R.id.website_reg_til);
        websiteRegEt = findViewById(R.id.website_reg_et);
        createAccBtn = findViewById(R.id.create_acc_btn);


        profileTypes = new String[]{getString(R.string.member_tenant), getString(R.string.public_institution)};

        List<ContentValues> states = new SmartCaching(RegisterActivity.this).getDataFromCache(TABLE_STATES);

        if (states != null) {
            Log.d("@@@", states.get(0).getAsString("name"));

            stateNames = new String[states.size()];
            for (int i = 0; i < states.size(); i++) {
                stateNames[i] = states.get(i).getAsString("name");
            }
        }

        stateDialogBuilder = setStateList(RegisterActivity.this, this);
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();
        createAccBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();

            }
        });

        userProfileTypeRegEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(RegisterActivity.this, profileTypes, getString(R.string.choose_your_Profiletype), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int position) {
                        userProfileTypeRegEt.setText(profileTypes[position]);
                        setProfileTypeFields(position);
                    }
                });
            }
        });


        stateRegEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alert = stateDialogBuilder.create();
                alert.show();
            }
        });

        cityRegEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cityDialogBuilder != null) {
                    AlertDialog alert = cityDialogBuilder.create();
                    alert.show();
                } else {
                    SmartUtils.showSnackBarLong(RegisterActivity.this, "Please select a state first.");
                }
            }
        });

        streetRegEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cityDialogBuilder == null) {
                    SmartUtils.showSnackBarLong(RegisterActivity.this, "Please select a state first.");
                } else if (streetDialogBuilder == null) {
                    if (!citySelected) {
                        SmartUtils.showSnackBarLong(RegisterActivity.this, "Please select a city first.");
                    } else {
                        SmartUtils.showSnackBarLong(RegisterActivity.this, "No Streets Available");
                    }
                } else {
                    AlertDialog alert = streetDialogBuilder.create();
                    alert.show();
                }

            }
        });


        associationRegEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(RegisterActivity.this, getResources().getStringArray(R.array.street_array), getString(R.string.choose_your_street), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        associationRegEt.setText(getResources().getStringArray(R.array.street_array)[id]);
                    }
                });
            }
        });

        passwordRegIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (passwordRegEt.getInputType() == 129) {
                    passwordRegEt.setInputType(InputType.TYPE_CLASS_TEXT);
                    passwordRegEt.setSelection(passwordRegEt.getText().length());
                } else {
                    passwordRegEt.setInputType(129);
                    passwordRegEt.setSelection(passwordRegEt.getText().length());
                }
            }
        });

        confirmPasswordRegIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (confirmPasswordRegEt.getInputType() == 129) {
                    confirmPasswordRegEt.setInputType(InputType.TYPE_CLASS_TEXT);
                    confirmPasswordRegEt.setSelection(confirmPasswordRegEt.getText().length());

                } else {
                    confirmPasswordRegEt.setInputType(129);
                    confirmPasswordRegEt.setSelection(confirmPasswordRegEt.getText().length());
                }
            }
        });
    }


    private void register() {

        if (profileTypeCode == null) {
            SmartUtils.showSnackBar(RegisterActivity.this, "Please select a User Type", Snackbar.LENGTH_LONG);
        } else if (checkEmptyEditTexts(RegisterActivity.this, firstNameRegEt, lastNameRegEt, userNameRegEt, passwordRegEt, confirmPasswordRegEt, emailRegEt)) {
            SmartUtils.showSnackBar(RegisterActivity.this, "Please fill empty fields", Snackbar.LENGTH_LONG);
        } else if (passwordRegEt.getText().toString().length() < 8) {
            SmartUtils.showSnackBar(RegisterActivity.this, "Password should have more than 8 characters", Snackbar.LENGTH_LONG);
        } else if (!passwordRegEt.getText().toString().equals(confirmPasswordRegEt.getText().toString())) {
            SmartUtils.showSnackBar(RegisterActivity.this, "Password doesn't match with Confirmed Password field", Snackbar.LENGTH_LONG);
        } else if (!SmartUtils.emailValidator(emailRegEt.getText().toString())) {
            SmartUtils.showSnackBar(RegisterActivity.this, "Please enter a valid email", Snackbar.LENGTH_LONG);
        } else if (profileTypeCode.equals(PROFILE_TYPE_INTITUTION_CODE) && !TextUtils.isEmpty(websiteRegEt.getText().toString()) && !urlValidator(websiteRegEt.getText().toString())) {
            SmartUtils.showSnackBar(RegisterActivity.this, "Please enter a valid url", Snackbar.LENGTH_LONG);
        } else {

            Map<String, String> registerData = new HashMap<>();
            registerData.put("longitude", getLatitude());
            registerData.put("latitude", getLongitude());


            registerData.put("firstname", firstNameRegEt.getText().toString());
            registerData.put("lastname", lastNameRegEt.getText().toString());
            registerData.put("username", userNameRegEt.getText().toString());
            registerData.put("password", passwordRegEt.getText().toString());
            registerData.put("email", emailRegEt.getText().toString());
            registerData.put("state", stateId);
            registerData.put("city", cityId);
            registerData.put("street", streetId);
            registerData.put("associate_groups", associationRegEt.getText().toString());
            registerData.put("user_type", profileTypeCode);


            if (profileTypeCode.equals(PROFILE_TYPE_INTITUTION_CODE)) {
                registerData.put("address", addressRegEt.getText().toString());
                registerData.put("phone", mobileRegEt.getText().toString());
                registerData.put("website", websiteRegEt.getText().toString());

            }

            AuthenticationApiCalls.signUp(RegisterActivity.this, registerData, this);
        }

    }


    private void setProfileTypeFields(int position) {
        switch (position) {
            case 0:
                profileTypeCode = PROFILE_TYPE_MEMBER_CODE;

                streetRegTil.setVisibility(View.VISIBLE);
                associationRegTil.setVisibility(View.VISIBLE);

                addressRegTil.setVisibility(View.GONE);
                mobileRegTil.setVisibility(View.GONE);
                websiteRegTil.setVisibility(View.GONE);
                break;
            case 1:
                profileTypeCode = PROFILE_TYPE_INTITUTION_CODE;

                streetRegTil.setVisibility(View.GONE);
                associationRegTil.setVisibility(View.GONE);

                addressRegTil.setVisibility(View.VISIBLE);
                mobileRegTil.setVisibility(View.VISIBLE);
                websiteRegTil.setVisibility(View.VISIBLE);
                break;
        }
    }

//==========================================================================================================================================================

    @Override
    public void onSignUpSuccess(String responseMessage) {
        toastShort(RegisterActivity.this, "Account registered");
        getOKDialog(RegisterActivity.this, "Please check your registered email to verify your account", "Okay", false, new AlertNeutral() {
            @Override
            public void NeutralMethod(DialogInterface dialog, int id) {
                dialog.dismiss();
                Intent intent = new Intent(RegisterActivity.this, ActivateAccountActivity.class);
                intent.putExtra(EMAIL,emailRegEt.getText().toString());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onSignUpFail(String responseMessage) {

    }


//==========================================================================================================================================================


    @Override
    public void onStateSelected(ContentValues contentValues) {
        citySelected = false;
        stateRegEt.setText(contentValues.getAsString("name"));
        stateId = contentValues.getAsString("id");

        cityRegEt.setText("Select City");
        cityId = null;

        streetRegEt.setText("Select Street");
        streetId = null;
        streetDialogBuilder = null;

        LocationAPIs.getCities(RegisterActivity.this, contentValues.getAsString("id"), new LocationImpl.GetCities() {
            @Override
            public void onCitiesReceived(JSONArray cities, String stateId) {
                cityDialogBuilder = setCityList(RegisterActivity.this, cities, stateId, RegisterActivity.this);
            }

            @Override
            public void onGetCitiesFailed() {

            }
        });
    }

//==========================================================================================================================================================


    @Override
    public void onCitySelected(JSONObject jsonObject, String stateId) {
        try {
            citySelected = true;
            cityRegEt.setText(jsonObject.getString("name"));
            cityId = jsonObject.getString("id");

            streetRegEt.setText("Select Street");
            streetId = null;

            stateId = jsonObject.getString("id");

            LocationAPIs.getStreets(RegisterActivity.this, stateId, jsonObject.getString("id"), new LocationImpl.GetStreets() {
                @Override
                public void onStreetsReceived(JSONArray cities) {
                    cityDialogBuilder = setStreetList(RegisterActivity.this, cities, RegisterActivity.this);
                }

                @Override
                public void onGetStreetsFailed() {
                    streetRegEt.setText("No Streets Available");
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//==========================================================================================================================================================

    @Override
    public void onStreetSelected(JSONObject jsonObject) {
        try {
            streetRegEt.setText(jsonObject.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
