package com.localis.Authentication;

import org.json.JSONObject;

public class AuthImplementation {

    public interface Login {
        void onLoginSuccess(String responseMessage);

        void onLoginFail(String responseMessage, String responseCode);

    }

    public interface SignUp {
        void onSignUpSuccess(String responseMessage);

        void onSignUpFail(String responseMessage);

    }

    public interface ActivateAcc {
        void onActivationSuccess(String responseMessage);

        void onActivationFail(String responseMessage);

    }

    public interface ResetPassword {
        void onPasswordResetSuccess(String responseMessage);

        void onPasswordResetFail(String responseMessage);

    }

    public interface ResendActivationKey {
        void onActivationKeySent(String responseMessage);

        void onActivationKeySentFail(String responseMessage);

    }

    public interface Logout {
        void onLogoutSuccess(String responseMessage);

        void onLogoutFail(String responseMessage);

    }




}
