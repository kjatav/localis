package com.localis.Groups.Activites;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.ViewPagerAdapter;
import com.localis.Albums.Activites.AlbumAddActivity;
import com.localis.Albums.Fragments.AlbumFragment;
import com.localis.Announcements.Activities.AnnouncementAddActivity;
import com.localis.Announcements.Fragments.AnnouncementFragment;
import com.localis.Discussions.Activities.DiscussionsAddActivity;
import com.localis.Discussions.Fragments.DiscussionsFragment;
import com.localis.Events.Activities.EventAddActivity;
import com.localis.Events.Fragments.EventListFragment;
import com.localis.Groups.Fragments.GroupAboutFragment;
import com.localis.Groups.Fragments.GroupAppsFragment;
import com.localis.Groups.Fragments.GroupMembersFragment;
import com.localis.Groups.Fragments.GroupTimelineFragment;
import com.localis.Groups.GroupApis;
import com.localis.Groups.GroupImpl;
import com.localis.Groups.POJOs.GroupDetailsObject;
import com.localis.R;
import com.localis.Videos.Activities.VideoAddActivity;
import com.localis.Videos.Fragments.VideoListFragment;

import java.util.HashMap;
import java.util.Map;

import static com.smart.framework.SmartUtils.getDeviceUDID;
import static com.smart.framework.SmartUtils.getUserId;
import static com.smart.framework.SmartUtils.setAddButton;
import static com.smart.framework.SmartUtils.toastLong;

public class GroupDetailActivity extends MasterActivity {
    private FloatingActionButton addGroupDetailsBtn;
    private ViewPager.OnPageChangeListener onPageChangeListener;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_aa_pager;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.group_detail);

        ViewPager groupDetailVp = findViewById(R.id.details_vp);
        TabLayout groupTabLayout = findViewById(R.id.details_tab);
        addGroupDetailsBtn = findViewById(R.id.details_add_btn);


        setupViewPager(groupDetailVp);
        groupTabLayout.setupWithViewPager(groupDetailVp);
        setupOnPageChangeListener();
        groupDetailVp.addOnPageChangeListener(onPageChangeListener);


    }


    private void setupOnPageChangeListener() {
        onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        addGroupDetailsBtn.setVisibility(View.GONE);
                        break;
                    case 1:
                        addGroupDetailsBtn.setVisibility(View.GONE);
                        break;
                    case 2:
                        addGroupDetailsBtn.setVisibility(View.GONE);
                        break;
                    case 3:
                        setAddButton(GroupDetailActivity.this, AlbumAddActivity.class, addGroupDetailsBtn);
                        break;
                    case 4:
                        setAddButton(GroupDetailActivity.this, VideoAddActivity.class, addGroupDetailsBtn);
                        break;
                    case 5:
                        setAddButton(GroupDetailActivity.this, EventAddActivity.class, addGroupDetailsBtn);
                        break;
                    case 6:
                        setAddButton(GroupDetailActivity.this, AnnouncementAddActivity.class, addGroupDetailsBtn);
                        break;
                    case 7:
                        setAddButton(GroupDetailActivity.this, DiscussionsAddActivity.class, addGroupDetailsBtn);
                        break;
                    case 8:
                        addGroupDetailsBtn.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        };
    }


    private void setupViewPager(final ViewPager viewPager) {
        GroupAboutFragment groupAboutFragment = new GroupAboutFragment();
        Bundle b = new Bundle();
        b.putString("groupId", getIntent().getStringExtra("groupId"));
        groupAboutFragment.setArguments(b);

        GroupTimelineFragment groupTimelineFragment = new GroupTimelineFragment();
        groupTimelineFragment.setArguments(b);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(groupTimelineFragment, "Timeline");
        adapter.addFragment(groupAboutFragment, "About");
        adapter.addFragment(new GroupMembersFragment(), "Members");
        adapter.addFragment(new AlbumFragment(), "Albums");
        adapter.addFragment(new VideoListFragment(), "Videos");
        adapter.addFragment(new EventListFragment(), "Event");
        adapter.addFragment(new AnnouncementFragment(), "Announcements");
        adapter.addFragment(new DiscussionsFragment(), "Discussions");
        adapter.addFragment(new GroupAppsFragment(), "Apps");
        viewPager.setAdapter(adapter);
        viewPager.post(new Runnable() {
            @Override
            public void run() {
                onPageChangeListener.onPageSelected(viewPager.getCurrentItem());
            }
        });
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_menu_top));
    }


}
