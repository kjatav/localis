package com.localis.Groups.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.ViewPagerAdapter;
import com.localis.Groups.Fragments.GroupListFragment;
import com.localis.R;

public class GroupListActivity extends MasterActivity {

    private FloatingActionButton addGroupBtn;

    @Override
    public int getLayoutID() {
        return R.layout.activity_aa_pager;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.groups);
        setupBottomNav(1);

        TabLayout tabLayout = findViewById(R.id.details_tab);
        ViewPager groupListVp = findViewById(R.id.details_vp);
        addGroupBtn = findViewById(R.id.details_add_btn);

        setupViewPager(groupListVp);
        tabLayout.setupWithViewPager(groupListVp);
        groupListVp.setOffscreenPageLimit(4);
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();
        addGroupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GroupListActivity.this, GroupAddEditActivity.class);
                intent.putExtra(ACTION, ADD);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        GroupListFragment allGroups = new GroupListFragment();

        Bundle bundle1 = new Bundle();
        bundle1.putString(GROUP_TYPE, ALL_GROUP_CODE);
        allGroups.setArguments(bundle1);

        GroupListFragment featuredGroups = new GroupListFragment();

        Bundle bundle2 = new Bundle();
        bundle2.putString(GROUP_TYPE, FEATURED_GROUP_CODE);
        featuredGroups.setArguments(bundle2);

        GroupListFragment myGroups = new GroupListFragment();

        Bundle bundle3 = new Bundle();
        bundle3.putString(GROUP_TYPE, MY_GROUP_CODE);
        myGroups.setArguments(bundle3);

        GroupListFragment nearByGroups = new GroupListFragment();

        Bundle bundle4 = new Bundle();
        bundle4.putString(GROUP_TYPE, NEARBY_GROUP_CODE);
//        bundle4.putString(GROUP_TYPE, MY_GROUP_CODE);
        bundle4.putInt(NEAR_BY_GROUP_FLAG, 1);
        nearByGroups.setArguments(bundle4);


        adapter.addFragment(allGroups, getString(R.string.all_groups));
        adapter.addFragment(featuredGroups, getString(R.string.featured_groups));
        adapter.addFragment(myGroups, getString(R.string.my_groups));
        adapter.addFragment(nearByGroups, getString(R.string.nearby_groups));
        viewPager.setAdapter(adapter);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_menu_top));
    }
}