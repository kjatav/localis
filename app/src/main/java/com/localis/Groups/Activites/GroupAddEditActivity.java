package com.localis.Groups.Activites;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Authentication.Activities.RegisterActivity;
import com.localis.Groups.GroupApis;
import com.localis.Groups.GroupImpl;
import com.localis.Groups.POJOs.GroupDetailsObject;
import com.localis.R;
import com.localis.ZZ_Package.LocationAPIs;
import com.localis.ZZ_Package.LocationImpl;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartUtils;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.localis.Groups.GroupApis.setGroupCategoryList;
import static com.localis.ZZ_Package.LocationAPIs.setCityList;
import static com.localis.ZZ_Package.LocationAPIs.setStateList;
import static com.smart.framework.SmartUtils.GetListDialog;
import static com.smart.framework.SmartUtils.booleanToInt;
import static com.smart.framework.SmartUtils.checkEmptyEditTexts;
import static com.smart.framework.SmartUtils.getUserId;
import static com.smart.framework.SmartUtils.toastLong;

public class GroupAddEditActivity extends MasterActivity implements GroupImpl.GroupEdition, GroupImpl.GroupAddition, GroupImpl.GroupCategorySelecting, LocationImpl.StateSelecting, LocationImpl.CitySelecting {

    private SmartEditText titleGroupAddEt;
    private SmartEditText descGroupAddEt;
    private SmartEditText stateGroupAddEt;
    private SmartEditText cityGroupAddEt;
    private SmartEditText categoryGroupAddEt;
    private SmartEditText typeGroupAddEt;
    private SmartEditText notificationTypeGroupAddEt;


    private SwitchCompat photoGroupAddSw;
    private SwitchCompat videoGroupAddSw;
    private SwitchCompat announcementGroupAddSw;
    private SwitchCompat complainsGroupAddSw;

    private RoundedImageView profileGroupAddIv;

    private SmartTextView profileGroupAddTv;

    private ImageView coverGroupAddIv;

    private SmartTextView coverGroupAddTv;

    private SwitchCompat audioGroupAddSw;
    private SwitchCompat eventsGroupAddSw;
    private SwitchCompat feedsGroupAddSw;
    private SwitchCompat filesGroupAddSw;
    private SwitchCompat pollsGroupAddSw;
    private SwitchCompat tasksGroupAddSw;

    private SmartButton submitGroupAddBtn;
    private SmartButton cancelGroupAddBtn;

    private boolean isProfile;

    private AlertDialog.Builder stateDialogBuilder;
    private AlertDialog.Builder cityDialogBuilder;
    private AlertDialog.Builder groupCategoryDialogBuilder;
    private int action;

    private GroupDetailsObject groupDetails;

    private String stateId = null;
    private String cityId = null;
    private String categoryId = null;
    private String typeId = null;
    private String notificationId = null;

    private HashMap<String, String> imageFilePaths = new HashMap<>();

    private String groupProfileImagePath = "";
    private String groupCoverImagePath = "";

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_group_add;
    }

    @Override
    public void initComponents() {
        super.initComponents();

        action = getIntent().getIntExtra(ACTION, ADD);

        titleGroupAddEt = findViewById(R.id.title_group_add_et);
        descGroupAddEt = findViewById(R.id.desc_group_add_et);
        stateGroupAddEt = findViewById(R.id.state_group_add_et);
        cityGroupAddEt = findViewById(R.id.city_group_add_et);
        categoryGroupAddEt = findViewById(R.id.category_group_add_et);
        typeGroupAddEt = findViewById(R.id.type_group_add_et);
        notificationTypeGroupAddEt = findViewById(R.id.notification_type_group_add_et);

        photoGroupAddSw = findViewById(R.id.photo_group_add_sw);
        videoGroupAddSw = findViewById(R.id.video_group_add_sw);
        announcementGroupAddSw = findViewById(R.id.announcement_group_add_sw);
        complainsGroupAddSw = findViewById(R.id.complains_group_add_sw);

        profileGroupAddIv = findViewById(R.id.profile_group_add_iv);
        profileGroupAddTv = findViewById(R.id.profile_group_add_tv);
        coverGroupAddIv = findViewById(R.id.cover_group_add_iv);
        coverGroupAddTv = findViewById(R.id.cover_group_add_tv);

        audioGroupAddSw = findViewById(R.id.audio_group_add_sw);
        eventsGroupAddSw = findViewById(R.id.events_group_add_sw);
        feedsGroupAddSw = findViewById(R.id.feeds_group_add_sw);
        filesGroupAddSw = findViewById(R.id.files_group_add_sw);
        pollsGroupAddSw = findViewById(R.id.polls_group_add_sw);
        tasksGroupAddSw = findViewById(R.id.tasks_group_add_sw);

        submitGroupAddBtn = findViewById(R.id.submit_group_add_btn);
        cancelGroupAddBtn = findViewById(R.id.cancel_group_add_btn);


        setHeaderToolbar(R.string.add_new_group);

        stateDialogBuilder = setStateList(GroupAddEditActivity.this, this);
        groupCategoryDialogBuilder = setGroupCategoryList(GroupAddEditActivity.this, this);

        if (action == EDIT) {
            setHeaderToolbar(R.string.edit_group);

            groupDetails = (GroupDetailsObject) getIntent().getSerializableExtra("groupDetails");
            if (groupDetails != null) {
                setDataFields();
            }
        }
    }


    @Override
    public void setActionListeners() {
        super.setActionListeners();

        stateGroupAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alert = stateDialogBuilder.create();
                alert.show();

            }
        });

        cityGroupAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cityDialogBuilder != null) {
                    AlertDialog alert = cityDialogBuilder.create();
                    alert.show();
                } else {
                    SmartUtils.showSnackBarLong(GroupAddEditActivity.this, "Please select a state first.");
                }
            }
        });


        categoryGroupAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alert = groupCategoryDialogBuilder.create();
                alert.show();

            }
        });


        typeGroupAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(GroupAddEditActivity.this, getResources().getStringArray(R.array.type_group_array), getString(R.string.choose_your_city), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        typeGroupAddEt.setText(getResources().getStringArray(R.array.type_group_array)[id]);
                        typeId = String.valueOf(id + 1);

                    }
                });
            }
        });

        notificationTypeGroupAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(GroupAddEditActivity.this);
                builder.setTitle("Notification Type");
                builder.setItems(getResources().getStringArray(R.array.notification_type_group_array), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        notificationTypeGroupAddEt.setText(getResources().getStringArray(R.array.notification_type_group_array)[position]);
                        notificationId = String.valueOf(position + 1);

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        profileGroupAddTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isProfile = true;
                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .start(GroupAddEditActivity.this);
            }
        });

        coverGroupAddTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isProfile = false;
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(GroupAddEditActivity.this);
            }
        });


        submitGroupAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAndCreateEditGroup();
            }
        });
    }

//===============================================================================================================================================================================================


    private void checkAndCreateEditGroup() {
        if (checkEmptyEditTexts(GroupAddEditActivity.this, titleGroupAddEt, descGroupAddEt)) {
            SmartUtils.showSnackBar(GroupAddEditActivity.this, "Please fill empty fields", Snackbar.LENGTH_LONG);
        } else if (stateId == null) {
            SmartUtils.showSnackBar(GroupAddEditActivity.this, "Please select a state", Snackbar.LENGTH_LONG);
        } else if (cityId == null) {
            SmartUtils.showSnackBar(GroupAddEditActivity.this, "Please select a city", Snackbar.LENGTH_LONG);
        } else if (categoryId == null) {
            SmartUtils.showSnackBar(GroupAddEditActivity.this, "Please select a Group Category", Snackbar.LENGTH_LONG);
        } else if (typeId == null) {
            SmartUtils.showSnackBar(GroupAddEditActivity.this, "Please select a Group Type", Snackbar.LENGTH_LONG);
        } else if (notificationId == null) {
            SmartUtils.showSnackBar(GroupAddEditActivity.this, "Please select a Notification Type", Snackbar.LENGTH_LONG);
        } else {
            Map<String, String> params = new HashMap<>();

            params.put("UserId", getUserId());
            params.put("title", titleGroupAddEt.getText().toString());
            params.put("description", descGroupAddEt.getText().toString());
            params.put("stateID", stateId);
            params.put("cityID", cityId);
            params.put("categoryId", categoryId);
            params.put("grouptypeId", typeId);
            params.put("NotificationType", notificationId);

            params.put("PhotoAlbums", booleanToInt(photoGroupAddSw.isChecked()));
            params.put("Videos", booleanToInt(videoGroupAddSw.isChecked()));
            params.put("Announcement", booleanToInt(announcementGroupAddSw.isChecked()));
            params.put("Discussions", booleanToInt(complainsGroupAddSw.isChecked()));
            params.put("Audios", booleanToInt(audioGroupAddSw.isChecked()));
            params.put("Events", booleanToInt(eventsGroupAddSw.isChecked()));
            params.put("Feeds", booleanToInt(feedsGroupAddSw.isChecked()));
            params.put("Files", booleanToInt(filesGroupAddSw.isChecked()));
            params.put("Polls", booleanToInt(pollsGroupAddSw.isChecked()));
            params.put("Tasks", booleanToInt(tasksGroupAddSw.isChecked()));

            imageFilePaths.put("gcover", groupCoverImagePath);
            imageFilePaths.put("gprofile", groupProfileImagePath);

            if (action == ADD) {
                GroupApis.addGroupAPI(GroupAddEditActivity.this, params, imageFilePaths, GroupAddEditActivity.this);
            } else if (action == EDIT) {
                params.put("groupId", groupDetails.getGroupId());
                GroupApis.editGroupAPI(GroupAddEditActivity.this, params, imageFilePaths, GroupAddEditActivity.this);
            }

        }
    }


    private void setDataFields() {
        titleGroupAddEt.setText(groupDetails.getGroupName());
        descGroupAddEt.setText(groupDetails.getGroupDescription());

        stateGroupAddEt.setText(groupDetails.getState());
        stateId = groupDetails.getStateid();
        cityGroupAddEt.setText(groupDetails.getCity());
        cityId = groupDetails.getCityid();
        categoryId = groupDetails.getGroupCat();
        categoryGroupAddEt.setText(groupDetails.getGroupCatName());

        notificationId = groupDetails.getGroupNotificationType();
        typeId = groupDetails.getGroupType();

        notificationTypeGroupAddEt.setText(GroupApis.getGroupNotificationType(GroupAddEditActivity.this,notificationId));
        typeGroupAddEt.setText(GroupApis.getGroupType(GroupAddEditActivity.this,typeId));
        photoGroupAddSw.setChecked(groupDetails.getIsPhotoalbum());
        videoGroupAddSw.setChecked(groupDetails.getIsVideos());
        announcementGroupAddSw.setChecked(groupDetails.getIsAnnouncement());
        complainsGroupAddSw.setChecked(groupDetails.getIsDiscussions());
        audioGroupAddSw.setChecked(groupDetails.getIsAudios());
        eventsGroupAddSw.setChecked(groupDetails.getIsEvents());
        feedsGroupAddSw.setChecked(groupDetails.getIsFeeds());
        filesGroupAddSw.setChecked(groupDetails.getIsFiles());
        pollsGroupAddSw.setChecked(groupDetails.getIsPolls());
        tasksGroupAddSw.setChecked(groupDetails.getIsTasks());

        if (TextUtils.isEmpty(groupDetails.getProfileImage())) {
            Picasso.with(GroupAddEditActivity.this).load(groupDetails.getProfileImage()).placeholder(R.drawable.ic_group_user).into(profileGroupAddIv);
        }
        if (TextUtils.isEmpty(groupDetails.getCoverImage())) {
            Picasso.with(GroupAddEditActivity.this).load(groupDetails.getCoverImage()).placeholder(R.drawable.bg_group_cover).into(coverGroupAddIv);
        }
    }


//===============================================================================================================================================================================================

    @Override
    public void onGroupCategorySelected(ContentValues contentValues) {
        categoryGroupAddEt.setText(contentValues.getAsString("title"));
        categoryId = contentValues.getAsString("id");
    }


    @Override
    public void onStateSelected(ContentValues contentValues) {
        stateGroupAddEt.setText(contentValues.getAsString("name"));
        stateId = contentValues.getAsString("id");

        cityGroupAddEt.setText("Select City");
        cityId = null;

        LocationAPIs.getCities(GroupAddEditActivity.this, contentValues.getAsString("id"), new LocationImpl.GetCities() {
            @Override
            public void onCitiesReceived(JSONArray cities, String stateID) {
                cityDialogBuilder = setCityList(GroupAddEditActivity.this, cities, stateID, GroupAddEditActivity.this);
            }

            @Override
            public void onGetCitiesFailed() {

            }
        });
    }


    @Override
    public void onCitySelected(JSONObject jsonObject, String stateID) {

        try {
            cityGroupAddEt.setText(jsonObject.getString("name"));
            cityId = jsonObject.getString("id");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


//===============================================================================================================================================================================================

    @Override
    public void onGroupEdited(String message) {
        toastLong(GroupAddEditActivity.this, "Group Details Edited");
        Intent intent = new Intent(GroupAddEditActivity.this, GroupListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onGroupEditionFailed() {
        toastLong(GroupAddEditActivity.this, "Something went wrong");
    }

//===============================================================================================================================================================================================

    @Override
    public void onGroupAdded(String message) {
        toastLong(GroupAddEditActivity.this, "Group Created");
        Intent intent = new Intent(GroupAddEditActivity.this, GroupListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onGroupAdditionFailed() {
        toastLong(GroupAddEditActivity.this, "Something went wrong");
    }

//===============================================================================================================================================================================================

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                if (isProfile) {
                    groupProfileImagePath = resultUri.getPath();
                    Picasso.with(GroupAddEditActivity.this).load(resultUri).into(profileGroupAddIv);
                } else {
                    groupCoverImagePath = resultUri.getPath();
                    Picasso.with(GroupAddEditActivity.this).load(resultUri).into(coverGroupAddIv);
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(GroupAddEditActivity.this, String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}

