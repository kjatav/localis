package com.localis.Groups;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.localis.Groups.POJOs.GroupAlbumObject;
import com.localis.Groups.POJOs.GroupDetailsObject;
import com.localis.Groups.POJOs.GroupItemObject;
import com.localis.Groups.POJOs.GroupVideoObject;
import com.localis.R;
import com.smart.caching.SmartCaching;
import com.smart.framework.SmartUtils;
import com.smart.weservice.SmartWebManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.smart.framework.Constants.DATA;
import static com.smart.framework.Constants.MESSAGE;
import static com.smart.framework.Constants.TABLE_GROUP_CATEGORIES;
import static com.smart.framework.SmartUtils.convertJsonToArray;
import static com.smart.framework.SmartUtils.getStringArray;

public class GroupApis {


    public static void addGroupAPI(final Context context, Map<String, String> parameters, HashMap<String, String> imagePaths, final GroupImpl.GroupAddition groupAddition) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "creategroup");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, parameters);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    groupAddition.onGroupAdded(results.getString(MESSAGE));
                    //SmartUtils.showSnackBarLong(context, results.getString(MESSAGE));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                groupAddition.onGroupAdditionFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueueMultipart(requestParams, imagePaths, true);
    }


    public static void editGroupAPI(final Context context, Map<String, String> parameters, HashMap<String, String> imagePaths, final GroupImpl.GroupEdition groupEdition) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "updategroup");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, parameters);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    groupEdition.onGroupEdited(results.getString(DATA));

                    // SmartUtils.showSnackBarLong(context, results.getString(MESSAGE));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                groupEdition.onGroupEditionFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueueMultipart(requestParams, imagePaths, true);
    }


    public static void getGroupCategoriesAPI(final Context context, final GroupImpl.GetGroupCategories getGroupCategories) {


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.GET);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "getgroupcategories");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                try {
                    getGroupCategories.onGroupCategoriesReceived(results.getJSONArray(DATA));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(JSONObject response) {
                getGroupCategories.onGroupCategoriesFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void getGroupListAPI(final Context context, Map<String, String> parameters, final GroupImpl.GetGroupList groupListReceived) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.GET);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "getgrouplist");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, parameters);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    groupListReceived.onGroupListReceived((ArrayList<GroupItemObject>) convertJsonToArray(results.getJSONArray(DATA), GroupItemObject.class), null);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                groupListReceived.onGroupListFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void getGroupDetailsAPI(final Context context, Map<String, String> parameters, final GroupImpl.GetGroupDetails getGroupDetails) {

        SmartUtils.showLoadingDialog(context);


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "getgroupdetails");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, parameters);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    getGroupDetails.onGroupDetailReceived(new Gson().fromJson(results.getString(DATA), GroupDetailsObject.class));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                getGroupDetails.onGroupDetailsFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void setGroupAsFeatureAPI(final Context context, Map<String, String> parameters, final GroupImpl.SetGroupAsFeature setGroupAsFeature) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "setgroupasfeatured");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, parameters);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    setGroupAsFeature.onFeatureSet(results.getString(MESSAGE));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                setGroupAsFeature.onFeatureSettingFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void deleteGroupAPI(final Context context, Map<String, String> parameters, final GroupImpl.GroupDeletion groupDeletion, final int itemPosition) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "deletegroup");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, parameters);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    groupDeletion.onGroupDeleted(results.getString(MESSAGE), itemPosition);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                groupDeletion.onGroupDeletionFailed();

            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void unpublishGroupAPI(final Context context, Map<String, String> parameters, final GroupImpl.GroupUnpublish groupUnpublish) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "unpublishgroup");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, parameters);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    groupUnpublish.onGroupUnpublished(results.getString(MESSAGE));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                groupUnpublish.onGroupUnpublishFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void joinGroupAPI(final Context context, String userId, String groupId, final GroupImpl.GroupJoining groupJoining) {

        SmartUtils.showLoadingDialog(context);

        Map<String, String> params = new HashMap<>();
        params.put("UserID", userId);
        params.put("GroupID", groupId);


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();


        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "joingroup");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    groupJoining.onGroupJoined(results.getString(MESSAGE));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                groupJoining.onGroupJoiningFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void reportGroupAPI(final Context context, Map<String, String> parameters, final GroupImpl.GroupReporting groupReporting) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "reportgroup");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, parameters);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    groupReporting.onGroupReported(results.getString(MESSAGE));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                groupReporting.onGroupReportingFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void getGroupAlbumsAPI(final Context context, Map<String, String> parameters, final GroupImpl.GetGroupAlbums getGroupAlbums) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.GET);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "getgroupalbums");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, parameters);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    getGroupAlbums.onGroupAlbumsReceived((ArrayList<GroupAlbumObject>) convertJsonToArray(results.getJSONArray(DATA), GroupAlbumObject.class));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                getGroupAlbums.onGetGroupAlbumFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void getGroupVideosAPI(final Context context, Map<String, String> parameters, final GroupImpl.GetGroupVideos getGroupVideos) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.GET);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "getgroupvideos");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, parameters);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    getGroupVideos.onGroupVideosReceived((ArrayList<GroupVideoObject>) convertJsonToArray(results.getJSONArray(DATA), GroupVideoObject.class));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                getGroupVideos.onGetGroupVidoesFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void getGroupAboutUsAPI(final Context context, Map<String, String> parameters, final GroupImpl.GetGroupAboutUs getGroupAboutUs) {

        SmartUtils.showLoadingDialog(context);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.GET);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_group) + "getgroupabout");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, parameters);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    getGroupAboutUs.onGroupAboutUsReceived(results.getJSONObject(DATA));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseError(JSONObject response) {
                SmartUtils.hideLoadingDialog();
                getGroupAboutUs.onGetGroupAboutUsFailed();
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static AlertDialog.Builder setGroupCategoryList(final Context context, final GroupImpl.GroupCategorySelecting groupCategorySelecting) {

        SmartCaching smartCaching = new SmartCaching(context);
        final List<ContentValues> categoryContent = smartCaching.getDataFromCache(TABLE_GROUP_CATEGORIES);
        Log.d("@@@", categoryContent.get(0).getAsString("title"));

        final String[] stateNames = new String[categoryContent.size()];
        for (int i = 0; i < categoryContent.size(); i++) {
            stateNames[i] = categoryContent.get(i).getAsString("title");
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Group Category");
        builder.setItems(stateNames, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                groupCategorySelecting.onGroupCategorySelected(categoryContent.get(position));
                //SmartUtils.toastShort(context, stateNames[position]);
            }
        });

        return builder;

    }

    public static String getGroupType(Context context, String groupTypeCode) {
        switch (groupTypeCode) {
            case "1":
                return context.getResources().getStringArray(R.array.type_group_array)[0];
            case "2":
                return context.getResources().getStringArray(R.array.type_group_array)[1];
            case "3":
                return context.getResources().getStringArray(R.array.type_group_array)[2];
            default:
                return context.getResources().getStringArray(R.array.type_group_array)[0];
        }

    }

    public static String getGroupNotificationType(Context context, String notificationTypeCode) {
        switch (notificationTypeCode) {
            case "1":
                return context.getResources().getStringArray(R.array.notification_type_group_array)[0];
            case "2":
                return context.getResources().getStringArray(R.array.notification_type_group_array)[1];
            case "3":
                return context.getResources().getStringArray(R.array.notification_type_group_array)[2];
            case "4":
                return context.getResources().getStringArray(R.array.notification_type_group_array)[4];
            default:
                return context.getResources().getStringArray(R.array.notification_type_group_array)[0];
        }

    }

}
