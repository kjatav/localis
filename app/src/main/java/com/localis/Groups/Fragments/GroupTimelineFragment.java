package com.localis.Groups.Fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_POJOs.ImageObject;
import com.localis.AA_POJOs.PopUpMenuObject;
import com.localis.Dashboard.Adapter.DashboardAdapter;
import com.localis.Dashboard.POJOs.FeedItemObject;
import com.localis.Groups.Activites.GroupAddEditActivity;
import com.localis.Groups.Activites.GroupDetailActivity;
import com.localis.Groups.Activites.GroupListActivity;
import com.localis.Groups.GroupApis;
import com.localis.Groups.GroupImpl;
import com.localis.Groups.POJOs.GroupDetailsObject;
import com.localis.Groups.POJOs.GroupItemObject;
import com.localis.R;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagnatic;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartUtils;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.smart.framework.SmartUtils.getUserId;
import static com.smart.framework.SmartUtils.showMenuPopup;
import static com.smart.framework.SmartUtils.switchTabsColor;
import static com.smart.framework.SmartUtils.toastLong;
import static com.smart.framework.SmartUtils.toastShort;

/**
 * Created by pct1 on 22/2/18.
 */

public class GroupTimelineFragment extends SmartFragment implements AlertMagneticClass.PopUpInterface, GroupImpl.GetGroupDetails {

    private TabLayout dashboardTabs;

    private ImageView imgGroupTimelineIv;
    private ImageView optionsGroupTimelineIv;
    private RoundedImageView profileGroupTimelineIv;
    private SmartTextView nameGroupTimelineTv;
    private SmartButton joinGroupTimelineBtn;
    private SmartTextView hitsGroupTimelineTv;
    private SmartTextView categoryGroupTimelineTv;
    private SmartTextView typeGroupTimelineTv;
    private RecyclerView groupTimelineRv;


    private ArrayList<ImageObject> image_objects = new ArrayList<>();
    private ArrayList<FeedItemObject> feedItem = new ArrayList<>();

    private DashboardAdapter dashboardAdapter;

    private SmartTextView statusTabTv;
    private SmartTextView photosTabTv;
    private SmartTextView videoTabTv;

    private View statusLayout;
    private View photosLayout;
    private View videoLayout;

    private Dialog reportDialog;

    private ArrayList<PopUpMenuObject> popUpMenuObjects;
    private GroupDetailsObject groupDetailsObject;

    @Override
    public int setLayoutId() {
        return R.layout.fragment_group_timeline;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {

        imgGroupTimelineIv = currentView.findViewById(R.id.img_group_timeline_iv);
        optionsGroupTimelineIv = currentView.findViewById(R.id.options_group_timeline_iv);
        profileGroupTimelineIv = currentView.findViewById(R.id.profile_group_timeline_iv);
        nameGroupTimelineTv = currentView.findViewById(R.id.name_group_timeline_tv);
        joinGroupTimelineBtn = currentView.findViewById(R.id.join_group_timeline_btn);
        hitsGroupTimelineTv = currentView.findViewById(R.id.hits_group_timeline_tv);
        categoryGroupTimelineTv = currentView.findViewById(R.id.category_group_timeline_tv);
        typeGroupTimelineTv = currentView.findViewById(R.id.type_group_timeline_tv);
        groupTimelineRv = currentView.findViewById(R.id.group_timeline_rv);

        joinGroupTimelineBtn.setVisibility(View.GONE);

        dashboardTabs = currentView.findViewById(R.id.post_timeline_tabs);
        for (int i = 0; i < 3; i++) {
            dashboardTabs.addTab(dashboardTabs.newTab());
        }

        statusLayout = currentView.findViewById(R.id.status_layout);
        photosLayout = currentView.findViewById(R.id.photos_layout);
        videoLayout = currentView.findViewById(R.id.video_layout);


        createPostTabIcons();

        groupTimelineRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        dashboardAdapter = new DashboardAdapter(feedItem, image_objects, getContext());
        groupTimelineRv.setAdapter(dashboardAdapter);

        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));
        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));
        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));

        Map<String, String> params = new HashMap<>();
        /*params.put("groupID", getIntent().getStringExtra(GROUP_ID));
        params.put("userID", getUserId());*/

        if (getArguments() != null) {
            params.put("groupID", getArguments().getString("groupId"));
            params.put("userID", getUserId());
            GroupApis.getGroupDetailsAPI(getActivity(), params, this);
        } else {
            toastShort(getActivity(), "No groupIdFound");
        }
        return currentView;
    }


    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        optionsGroupTimelineIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpMenuObjects = new ArrayList<>();
                popUpMenuObjects.add(new PopUpMenuObject("Report", REPORT));
                popUpMenuObjects.add(new PopUpMenuObject("Edit Group", EDIT));
                popUpMenuObjects.add(new PopUpMenuObject("Set as Featured", SET_AS_FEATURES));
                popUpMenuObjects.add(new PopUpMenuObject("Add New Video", ADD_VIDEO));
                popUpMenuObjects.add(new PopUpMenuObject("Add New Album", ADD_ALBUM));
                popUpMenuObjects.add(new PopUpMenuObject("Unpublish Group", UNPUBLISH));
                popUpMenuObjects.add(new PopUpMenuObject("Delete Group", DELETE));

                showMenuPopup(getActivity(), v, popUpMenuObjects, GroupTimelineFragment.this);
            }
        });
    }

    private void createPostTabIcons() {
        statusTabTv = (SmartTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        statusTabTv.setText(R.string.status);
        statusTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_status, 0, 0);
        dashboardTabs.getTabAt(0).setCustomView(statusTabTv);

        photosTabTv = (SmartTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        photosTabTv.setText(R.string.photos);
        photosTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_photo, 0, 0);
        dashboardTabs.getTabAt(1).setCustomView(photosTabTv);

        videoTabTv = (SmartTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        videoTabTv.setText(R.string.video);
        videoTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_video, 0, 0);
        dashboardTabs.getTabAt(2).setCustomView(videoTabTv);


        dashboardTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        switchTabsColor(getActivity(), statusTabTv, R.drawable.ic_subheader_status_hover, true);
                        hideAndShow(statusLayout);
                        break;
                    case 1:
                        switchTabsColor(getActivity(), photosTabTv, R.drawable.ic_subheader_photo_hover, true);
                        hideAndShow(photosLayout);
                        break;
                    case 2:
                        switchTabsColor(getActivity(), videoTabTv, R.drawable.ic_subheader_video_hover, true);
                        hideAndShow(videoLayout);
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        switchTabsColor(getActivity(), statusTabTv, R.drawable.ic_subheader_status, false);
                        break;
                    case 1:
                        switchTabsColor(getActivity(), photosTabTv, R.drawable.ic_subheader_photo, false);
                        break;
                    case 2:
                        switchTabsColor(getActivity(), videoTabTv, R.drawable.ic_subheader_video, false);
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        switchTabsColor(getActivity(), statusTabTv, R.drawable.ic_subheader_status_hover, true);
    }


    private void hideAndShow(View view) {
        statusLayout.setVisibility(View.GONE);
        photosLayout.setVisibility(View.GONE);
        videoLayout.setVisibility(View.GONE);

        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSetPopUp(int selectedOptionPosition) {


        final Map<String, String> params = new HashMap<>();
        params.put("UserID", getUserId());
        params.put("GroupID", "105");

        switch (popUpMenuObjects.get(selectedOptionPosition).getItemAction()) {
            case DELETE:
                SmartUtils.getConfirmDialog(getActivity(), "Delete Group", "Are you sure you want to delete this group?",
                        "Yes", "No", true, new AlertMagnatic() {

                            @Override
                            public void PositiveMethod(DialogInterface dialog, int id) {
                                //   GroupApis.deleteGroupAPI(getActivity(), params, groupDeletion, groupItemPosition);
                            }

                            @Override
                            public void NegativeMethod(DialogInterface dialog, int id) {

                            }
                        });
                break;
            case REPORT:
                showReportGroupDialog(params);
                break;
            case EDIT:
                Intent intent = new Intent(getActivity(), GroupAddEditActivity.class);
                intent.putExtra(ACTION, EDIT);
                intent.putExtra("groupDetails", (Serializable) groupDetailsObject);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case 3:
                //  GroupApis.deleteGroupAPI(getActivity(), params, groupDeletion, groupItemPosition);
                break;
        }
    }


    private void showReportGroupDialog(final Map<String, String> params) {

        reportDialog = new Dialog(getActivity());
        reportDialog.setContentView(R.layout.dialog_report_group);

        ImageView ivCloseReportGroup = reportDialog.findViewById(R.id.iv_close_report_group);
        final SmartEditText etReportGroup = reportDialog.findViewById(R.id.et_report_group);
        SmartButton btnReportGroup = reportDialog.findViewById(R.id.btn_report_group);


        btnReportGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                params.put("text", etReportGroup.getText().toString());

                GroupApis.reportGroupAPI(getActivity(), params, new GroupImpl.GroupReporting() {
                    @Override
                    public void onGroupReported(String message) {
                        toastShort(getActivity(), message);
                        reportDialog.dismiss();
                    }

                    @Override
                    public void onGroupReportingFailed() {

                    }
                });

            }
        });

        ivCloseReportGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportDialog.dismiss();
            }
        });

        reportDialog.show();
    }

    private void setData() {
        if (!TextUtils.isEmpty(groupDetailsObject.getProfileImage())) {
            Picasso.with(getActivity()).load(groupDetailsObject.getProfileImage()).placeholder(R.drawable.ic_group_user).into(profileGroupTimelineIv);
        }
        if (!TextUtils.isEmpty(groupDetailsObject.getCoverImage())) {
            Picasso.with(getActivity()).load(groupDetailsObject.getCoverImage()).placeholder(R.drawable.bg_group_cover).into(imgGroupTimelineIv);
        }
        nameGroupTimelineTv.setText(groupDetailsObject.getGroupName());

        hitsGroupTimelineTv.setText(groupDetailsObject.getGroupHits());
        categoryGroupTimelineTv.setText(groupDetailsObject.getGroupCatName());
//        typeGroupTimelineTv.setText(getResources().getStringArray(R.array.type_group_array)[Integer.valueOf(groupDetailsObject.getGroupType()) - 1]);

        if (groupDetailsObject.getGroupStatus() != null) {
            switch (groupDetailsObject.getGroupStatus()) {
                case 0:
                    joinGroupTimelineBtn.setVisibility(View.VISIBLE);
                    joinGroupTimelineBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            GroupApis.joinGroupAPI(getActivity(), getUserId(), groupDetailsObject.getGroupId(), new GroupImpl.GroupJoining() {
                                @Override
                                public void onGroupJoined(String message) {

                                }

                                @Override
                                public void onGroupJoiningFailed() {

                                }
                            });
                        }
                    });
                    break;
                case 1:
                    joinGroupTimelineBtn.setVisibility(View.GONE);
                    break;
                case 2:
                    joinGroupTimelineBtn.setVisibility(View.VISIBLE);
                    joinGroupTimelineBtn.setText("Waiting for approval");
                    break;
            }
        }
    }

    @Override
    public void onGroupDetailReceived(GroupDetailsObject groupDetails) {
        groupDetailsObject = groupDetails;
        setData();
    }

    @Override
    public void onGroupDetailsFailed() {

    }
}
