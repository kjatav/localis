package com.localis.Groups.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.localis.Groups.Adapters.GroupMemberItemAdapter;
import com.localis.Groups.POJOs.GroupMemberObject;
import com.localis.R;
import com.smart.customViews.SmartEditText;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartRecyclerView;

import java.util.ArrayList;
import java.util.List;

public class GroupMembersFragment extends SmartFragment {

    private SmartEditText searchMemberGroupEt;
    private SmartRecyclerView memberGroupRv;
    private View layoutNoData;


    private GroupMemberItemAdapter mSearchAdapter;

    private List<GroupMemberObject> memberObjects = new ArrayList<>();

    @Override
    public int setLayoutId() {
        return R.layout.fragment_member;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        searchMemberGroupEt = currentView.findViewById(R.id.search_member_group_et);
        memberGroupRv = currentView.findViewById(R.id.member_group_rv);
        layoutNoData = currentView.findViewById(R.id.layout_no_data);

        memberGroupRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        mSearchAdapter = new GroupMemberItemAdapter(getActivity(), memberObjects);
        memberGroupRv.setAdapter(mSearchAdapter);
        memberGroupRv.setEmptyView(layoutNoData);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }
}
