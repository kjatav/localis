package com.localis.Groups.Fragments;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.localis.AA_POJOs.ImageObject;
import com.localis.AA_POJOs.PopUpMenuObject;
import com.localis.Groups.Adapters.GroupItemAdapter;
import com.localis.Groups.GroupApis;
import com.localis.Groups.GroupImpl;
import com.localis.Groups.POJOs.GroupItemObject;
import com.localis.R;
import com.localis.ZZ_Package.PaginationScrollListener;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartRecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.smart.framework.SmartUtils.getUserId;
import static com.smart.framework.SmartUtils.showSortingPopup;
import static com.smart.framework.SmartUtils.toastShort;


public class GroupListFragment extends SmartFragment implements GroupImpl.GroupReporting, GroupImpl.SetGroupAsFeature, GroupImpl.GroupJoining, GroupImpl.GroupUnpublish, GroupImpl.GroupDeletion {

    private ArrayList<GroupItemObject> groupItems = new ArrayList<>();
    private GroupItemAdapter groupItemAdapter;

    private SmartTextView categoryGroupTv;
    private SmartTextView sortTv;
    private SmartTextView sortByDistanceTv;
    private SmartRecyclerView groupListRv;

    private static final int PAGE_START = 0;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.

    private String TOTAL_PAGES = "0";

    private int currentPage = PAGE_START;
    private int pageSize = 2;

    private LinearLayoutManager linearLayoutManager;

    Map<String, String> groupRequestData = new HashMap<>();

    private PaginationScrollListener paginationScrollListener;

    @Override
    public int setLayoutId() {
        return R.layout.fragment_group_list;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {

        categoryGroupTv = currentView.findViewById(R.id.category_group_tv);
        sortTv = currentView.findViewById(R.id.sort_tv);
        sortByDistanceTv = currentView.findViewById(R.id.sort_by_distance_tv);
        groupListRv = currentView.findViewById(R.id.group_list_rv);


        groupItemAdapter = new GroupItemAdapter(groupItems, getActivity(), this, this, this, this, this);

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        groupListRv.setLayoutManager(linearLayoutManager);
        groupListRv.setAdapter(groupItemAdapter);
        groupListRv.setEmptyView(currentView.findViewById(R.id.layout_no_data));
        groupListRv.setItemAnimator(new DefaultItemAnimator());


        if (getArguments() != null) {
            if (getArguments().getInt(NEAR_BY_GROUP_FLAG) == 1) {
                sortByDistanceTv.setVisibility(View.VISIBLE);
            }
        }


        setHasOptionsMenu(true);

        loadGroupList("1");


        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        sortTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ArrayList<PopUpMenuObject> popUpMenuObjects = new ArrayList<>();
                popUpMenuObjects.add(new PopUpMenuObject("Sort by Latest Items", BY_LATEST));
                popUpMenuObjects.add(new PopUpMenuObject("Sort Alphabetically", BY_ALPHABETICALLY));
                popUpMenuObjects.add(new PopUpMenuObject("Sort by Popularity", BY_POPULARITY));

                showSortingPopup(getActivity(), sortTv, popUpMenuObjects, new AlertMagneticClass.PopUpInterface() {
                    @Override
                    public void onSetPopUp(int selectedOptionPosition) {
                        sortTv.setText(popUpMenuObjects.get(selectedOptionPosition).getItemName());
                        switch (popUpMenuObjects.get(selectedOptionPosition).getItemAction()) {
                            case BY_LATEST:
                                loadGroupList("1");
                                break;
                            case BY_ALPHABETICALLY:
                                loadGroupList("2");
                                break;
                            case BY_POPULARITY:
                                loadGroupList("3");
                                break;
                        }
                    }
                });
            }
        });

        sortByDistanceTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ArrayList<PopUpMenuObject> popUpMenuObjects = new ArrayList<>();
                popUpMenuObjects.add(new PopUpMenuObject("15 Km", BY_DISTANCE));
                popUpMenuObjects.add(new PopUpMenuObject("25 Km", BY_DISTANCE));
                popUpMenuObjects.add(new PopUpMenuObject("35 Km", BY_DISTANCE));
                popUpMenuObjects.add(new PopUpMenuObject("45 Km", BY_DISTANCE));
                popUpMenuObjects.add(new PopUpMenuObject("55 Km", BY_DISTANCE));

                showSortingPopup(getActivity(), sortByDistanceTv, popUpMenuObjects, new AlertMagneticClass.PopUpInterface() {
                    @Override
                    public void onSetPopUp(int selectedOptionPosition) {
                        sortByDistanceTv.setText(popUpMenuObjects.get(selectedOptionPosition).getItemName());
                    }
                });
            }
        });

    }


    private void loadGroupList(String sortBy) {
        groupRequestData.put("userID", getUserId());
        groupRequestData.put("sortBy", sortBy);
        groupRequestData.put("cateID", getArguments().getString(GROUP_TYPE));

        GroupApis.getGroupListAPI(getActivity(), groupRequestData, new GroupImpl.GetGroupList() {
            @Override
            public void onGroupListReceived(ArrayList<GroupItemObject> groupItems, String totalPages) {

                if (groupItems.size() > 0) {

                    Log.d("@@@TEST", groupItems.get(0).getTitle());
                    GroupListFragment.this.groupItems.clear();
                    GroupListFragment.this.groupItems.addAll(groupItems);
                    groupItemAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onGroupListFailed() {

            }
        });
    }

    //====================================================================================================================================================================================================================

    @Override
    public void onFeatureSet(String message) {

    }

    @Override
    public void onFeatureSettingFailed() {

    }

    //====================================================================================================================================================================================================================

    @Override
    public void onGroupDeleted(String message, int itemPosition) {
        groupItems.remove(itemPosition);
        groupItemAdapter.notifyDataSetChanged();
    }

    @Override
    public void onGroupDeletionFailed() {

    }

    //====================================================================================================================================================================================================================

    @Override
    public void onGroupUnpublished(String message) {
    }

    @Override
    public void onGroupUnpublishFailed() {

    }

    //====================================================================================================================================================================================================================

    @Override
    public void onGroupJoined(String message) {

    }

    @Override
    public void onGroupJoiningFailed() {

    }

    //====================================================================================================================================================================================================================

    @Override
    public void onGroupReported(String message) {
    }

    @Override
    public void onGroupReportingFailed() {

    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.group_menu, menu);
    }

    //====================================================================================================================================================================================================================

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        categoryGroupTv.setText(item.getTitle());
        return super.onOptionsItemSelected(item);
    }
}
