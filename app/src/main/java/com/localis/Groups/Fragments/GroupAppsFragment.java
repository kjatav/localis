package com.localis.Groups.Fragments;

import android.view.View;

import com.localis.R;
import com.smart.framework.SmartFragment;

public class GroupAppsFragment extends SmartFragment {
    @Override
    public int setLayoutId() {
        return R.layout.fragment_group_apps;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        return null;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }
}