package com.localis.Groups.Fragments;

import android.view.View;

import com.localis.Groups.GroupApis;
import com.localis.Groups.GroupImpl;
import com.localis.R;
import com.smart.customViews.SmartTextView;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.smart.framework.SmartUtils.getUserId;

public class GroupAboutFragment extends SmartFragment implements GroupImpl.GetGroupAboutUs {

    private SmartTextView nameGroupAboutTv;
    private SmartTextView descGroupAboutTv;
    private SmartTextView stateGroupAboutTv;
    private SmartTextView cityGroupAboutTv;


    @Override
    public int setLayoutId() {
        return R.layout.fragment_group_about;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        nameGroupAboutTv = currentView.findViewById(R.id.name_group_about_tv);
        descGroupAboutTv = currentView.findViewById(R.id.desc_group_about_tv);
        stateGroupAboutTv = currentView.findViewById(R.id.state_group_about_tv);
        cityGroupAboutTv = currentView.findViewById(R.id.city_group_about_tv);

        if (getArguments() != null) {
            Map<String, String> requestParams = new HashMap<>();
            requestParams.put("UserID", getUserId());
            requestParams.put("GroupID", getArguments().getString("groupId"));
            GroupApis.getGroupAboutUsAPI(getActivity(), requestParams, this);
        }

        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }


    @Override
    public void onGroupAboutUsReceived(JSONObject data) {

        try {
            nameGroupAboutTv.setText(data.getString("title"));
            descGroupAboutTv.setText(data.getString("description"));
            stateGroupAboutTv.setText(data.getString("state"));
            cityGroupAboutTv.setText(data.getString("city"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onGetGroupAboutUsFailed() {

    }
}