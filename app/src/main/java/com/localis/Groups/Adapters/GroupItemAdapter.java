package com.localis.Groups.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.localis.Authentication.Activities.ActivateAccountActivity;
import com.localis.Authentication.Activities.LoginActivity;
import com.localis.Groups.Activites.GroupDetailActivity;
import com.localis.Groups.GroupApis;
import com.localis.Groups.GroupImpl;
import com.localis.Groups.POJOs.GroupItemObject;
import com.localis.R;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagnatic;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.widget.Toast.LENGTH_LONG;
import static com.smart.framework.SmartUtils.getUserId;
import static com.smart.framework.SmartUtils.toastShort;


public class GroupItemAdapter extends RecyclerView.Adapter<GroupItemAdapter.MyViewHolder> {
    private ArrayList<GroupItemObject> groupItems;
    private Context context;

    private GroupImpl.SetGroupAsFeature setGroupAsFeature;
    private GroupImpl.GroupJoining groupJoining;
    private GroupImpl.GroupUnpublish groupUnpublish;
    private GroupImpl.GroupDeletion groupDeletion;
    private GroupImpl.GroupReporting groupReporting;

    private Dialog reportDialog;

    public GroupItemAdapter(ArrayList<GroupItemObject> groupItems, Context context,
                            GroupImpl.SetGroupAsFeature setGroupAsFeature, GroupImpl.GroupJoining groupJoining,
                            GroupImpl.GroupUnpublish groupUnpublish, GroupImpl.GroupDeletion groupDeletion,
                            GroupImpl.GroupReporting groupReporting) {
        this.groupItems = groupItems;
        this.context = context;
        this.setGroupAsFeature = setGroupAsFeature;
        this.groupJoining = groupJoining;
        this.groupUnpublish = groupUnpublish;
        this.groupDeletion = groupDeletion;
        this.groupReporting = groupReporting;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_group_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {


        holder.nameGroupItemTv.setText(groupItems.get(position).getTitle());
        holder.membersGroupItemTv.setText(groupItems.get(position).getMembersCount() + "" + (groupItems.get(position).getMembersCount() > 1 ? context.getString(R.string.members) : context.getString(R.string.member)));
        holder.typeGroupItemTv.setText(groupItems.get(position).getGroupType());
        holder.categoryGroupItemTv.setText(groupItems.get(position).getGroupCat());
        holder.descGroupItemTv.setText(groupItems.get(position).getDescription());
        if (!TextUtils.isEmpty(groupItems.get(position).getAvatar())) {
            Picasso.with(context).load(groupItems.get(position).getAvatar()).placeholder(R.drawable.ic_complain_user).into(holder.profileGroupItemIv);
        }
        if (!TextUtils.isEmpty(groupItems.get(position).getCover())) {
            Picasso.with(context).load(groupItems.get(position).getCover()).placeholder(R.drawable.bg_group_cover).into(holder.imgGroupItemIv);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GroupDetailActivity.class);
                intent.putExtra("groupId", groupItems.get(position).getGid());
                context.startActivity(intent);
            }
        });

        final String[] popupmenu = {"Delete", "Report", "Unpublish"};

        holder.optionsGroupItemIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGroupItemMenu(context, v, popupmenu, new AlertMagneticClass.PopUpInterface() {
                    @Override
                    public void onSetPopUp(int selectedOptionPosition) {
                        doAction(context, selectedOptionPosition, position);
                    }
                });
            }
        });

        if (groupItems.get(position).getGroupStatus() != null) {
            switch (groupItems.get(position).getGroupStatus()) {
                case 0:
                    holder.joinGroupItemBtn.setVisibility(View.VISIBLE);
                    holder.joinGroupItemBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            GroupApis.joinGroupAPI(context, getUserId(), groupItems.get(position).getGid(), new GroupImpl.GroupJoining() {
                                @Override
                                public void onGroupJoined(String message) {

                                }

                                @Override
                                public void onGroupJoiningFailed() {

                                }
                            });
                        }
                    });
                    break;
                case 1:
                    holder.joinGroupItemBtn.setVisibility(View.GONE);
                    break;
                case 2:
                    holder.joinGroupItemBtn.setVisibility(View.VISIBLE);
                    holder.joinGroupItemBtn.setText("Waiting for approval");
                    break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    @Override
    public int getItemCount() {
        return groupItems.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgGroupItemIv;
        private ImageView optionsGroupItemIv;
        private RoundedImageView profileGroupItemIv;
        private SmartTextView nameGroupItemTv;
        private SmartTextView membersGroupItemTv;
        private SmartButton joinGroupItemBtn;
        private SmartTextView typeGroupItemTv;
        private SmartTextView categoryGroupItemTv;
        private SmartTextView descGroupItemTv;


        public MyViewHolder(View itemView) {
            super(itemView);
            imgGroupItemIv = itemView.findViewById(R.id.img_group_item_iv);
            optionsGroupItemIv = itemView.findViewById(R.id.options_group_item_iv);
            profileGroupItemIv = itemView.findViewById(R.id.profile_group_item_iv);
            nameGroupItemTv = itemView.findViewById(R.id.name_group_item_tv);
            membersGroupItemTv = itemView.findViewById(R.id.members_group_item_tv);
            joinGroupItemBtn = itemView.findViewById(R.id.join_group_item_btn);
            typeGroupItemTv = itemView.findViewById(R.id.type_group_item_tv);
            categoryGroupItemTv = itemView.findViewById(R.id.category_group_item_tv);
            descGroupItemTv = itemView.findViewById(R.id.desc_group_item_tv);
        }
    }


    private static void showGroupItemMenu(final Context context, final View view, String[] popUpItems, final AlertMagneticClass.PopUpInterface popUpInterface) {

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_sort, null);
        LinearLayout viewGroup = layout.findViewById(R.id.popup);

        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setFocusable(true);
        int OFFSET_X = 0;
        int OFFSET_Y = 0;
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(view, OFFSET_X, OFFSET_Y);

        for (int i = 0; i < popUpItems.length; i++) {
            SmartTextView selectedTextTv = (SmartTextView) layoutInflater.inflate(R.layout.textview_group, null);
            selectedTextTv.setText(popUpItems[i]);
            viewGroup.addView(selectedTextTv);
            final int finalI = i;

            selectedTextTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popUpInterface.onSetPopUp(finalI);

                    popup.dismiss();
                }
            });
        }
    }


    private void doAction(final Context context, int menuItemPosition, final int groupItemPosition) {

        final Map<String, String> params = new HashMap<>();
        params.put("UserID", getUserId());
        params.put("GroupID", groupItems.get(groupItemPosition).getGid());

        switch (menuItemPosition) {
            case 0:
                SmartUtils.getConfirmDialog(context, "Delete Group", "Are you sure you want to delete this group?",
                        "Yes", "No", true, new AlertMagnatic() {

                            @Override
                            public void PositiveMethod(DialogInterface dialog, int id) {
                                GroupApis.deleteGroupAPI(context, params, groupDeletion, groupItemPosition);
                            }

                            @Override
                            public void NegativeMethod(DialogInterface dialog, int id) {

                            }
                        });
                break;
            case 1:
                showReportGroupDialog(params);
                break;
            case 2:
                GroupApis.deleteGroupAPI(context, params, groupDeletion, groupItemPosition);
                break;
            case 3:
                GroupApis.deleteGroupAPI(context, params, groupDeletion, groupItemPosition);
                break;
        }

    }


    private void showReportGroupDialog(final Map<String, String> params) {

        reportDialog = new Dialog(context);
        reportDialog.setContentView(R.layout.dialog_report_group);

        ImageView ivCloseReportGroup = reportDialog.findViewById(R.id.iv_close_report_group);
        final SmartEditText etReportGroup = reportDialog.findViewById(R.id.et_report_group);
        SmartButton btnReportGroup = reportDialog.findViewById(R.id.btn_report_group);


        btnReportGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                params.put("text", etReportGroup.getText().toString());

                GroupApis.reportGroupAPI(context, params, new GroupImpl.GroupReporting() {
                    @Override
                    public void onGroupReported(String message) {
                        toastShort(context, message);
                        reportDialog.dismiss();
                    }

                    @Override
                    public void onGroupReportingFailed() {

                    }
                });

            }
        });

        ivCloseReportGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportDialog.dismiss();
            }
        });

        reportDialog.show();
    }

}