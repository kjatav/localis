package com.localis.Groups.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localis.Groups.POJOs.GroupMemberObject;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.List;


public class GroupMemberItemAdapter extends RecyclerView.Adapter<GroupMemberItemAdapter.MyViewHolder> {
    private Context context;
    private List<GroupMemberObject> groupMemberObjects;


    public GroupMemberItemAdapter(Context context, List<GroupMemberObject> tagFriendsObjects) {
        this.context = context;
        this.groupMemberObjects = tagFriendsObjects;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_group_member_item, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    @Override
    public int getItemCount() {
        return 10;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView profileGroupMemberIv;
        private SmartTextView nameGroupMemberTv;
        private SmartTextView messageGroupMemberTv;
        private ImageView optionsGroupMemberTv;


        public MyViewHolder(View itemView) {
            super(itemView);
            profileGroupMemberIv = itemView.findViewById(R.id.profile_group_member_iv);
            nameGroupMemberTv = itemView.findViewById(R.id.name_group_member_tv);
            messageGroupMemberTv = itemView.findViewById(R.id.message_group_member_tv);
            optionsGroupMemberTv = itemView.findViewById(R.id.options_group_member_tv);
        }
    }
}