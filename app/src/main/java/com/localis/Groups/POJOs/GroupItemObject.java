package com.localis.Groups.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupItemObject {

    @SerializedName("gid")
    @Expose
    private String gid;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("group_hits")
    @Expose
    private String groupHits;
    @SerializedName("group_type")
    @Expose
    private String groupType;
    @SerializedName("group_cat")
    @Expose
    private String groupCat;
    @SerializedName("cover_id")
    @Expose
    private String coverId;
    @SerializedName("avatar_id")
    @Expose
    private String avatarId;
    @SerializedName("group_cat_name")
    @Expose
    private String groupCatName;
    @SerializedName("members_count")
    @Expose
    private Integer membersCount;
    @SerializedName("group_status")
    @Expose
    private Integer groupStatus;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("cover")
    @Expose
    private String cover;

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroupHits() {
        return groupHits;
    }

    public void setGroupHits(String groupHits) {
        this.groupHits = groupHits;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getGroupCat() {
        return groupCat;
    }

    public void setGroupCat(String groupCat) {
        this.groupCat = groupCat;
    }

    public String getCoverId() {
        return coverId;
    }

    public void setCoverId(String coverId) {
        this.coverId = coverId;
    }

    public String getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(String avatarId) {
        this.avatarId = avatarId;
    }

    public String getGroupCatName() {
        return groupCatName;
    }

    public void setGroupCatName(String groupCatName) {
        this.groupCatName = groupCatName;
    }

    public Integer getMembersCount() {
        return membersCount;
    }

    public void setMembersCount(Integer membersCount) {
        this.membersCount = membersCount;
    }

    public Integer getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(Integer groupStatus) {
        this.groupStatus = groupStatus;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

}
