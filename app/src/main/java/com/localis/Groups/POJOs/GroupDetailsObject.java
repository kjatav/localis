package com.localis.Groups.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GroupDetailsObject implements Serializable{

    @SerializedName("group_id")
    @Expose
    private String groupId;
    @SerializedName("group_name")
    @Expose
    private String groupName;
    @SerializedName("group_description")
    @Expose
    private String groupDescription;
    @SerializedName("group_member_count")
    @Expose
    private Integer groupMemberCount;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("cover_image")
    @Expose
    private String coverImage;
    @SerializedName("creator_name")
    @Expose
    private String creatorName;
    @SerializedName("group_type")
    @Expose
    private String groupType;
    @SerializedName("group_status")
    @Expose
    private Integer groupStatus;
    @SerializedName("group_hits")
    @Expose
    private String groupHits;
    @SerializedName("group_notification_type")
    @Expose
    private String groupNotificationType;
    @SerializedName("group_cat")
    @Expose
    private String groupCat;
    @SerializedName("group_cat_name")
    @Expose
    private String groupCatName;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("stateid")
    @Expose
    private String stateid;
    @SerializedName("cityid")
    @Expose
    private String cityid;
    @SerializedName("is_photoalbum")
    @Expose
    private Boolean isPhotoalbum;
    @SerializedName("is_videos")
    @Expose
    private Boolean isVideos;
    @SerializedName("is_announcement")
    @Expose
    private Boolean isAnnouncement;
    @SerializedName("is_discussions")
    @Expose
    private Boolean isDiscussions;
    @SerializedName("is_audios")
    @Expose
    private Boolean isAudios;
    @SerializedName("is_events")
    @Expose
    private Boolean isEvents;
    @SerializedName("is_feeds")
    @Expose
    private Boolean isFeeds;
    @SerializedName("is_files")
    @Expose
    private Boolean isFiles;
    @SerializedName("is_polls")
    @Expose
    private Boolean isPolls;
    @SerializedName("is_tasks")
    @Expose
    private Boolean isTasks;
    @SerializedName("is_user_able_to_delete")
    @Expose
    private String isUserAbleToDelete;
    @SerializedName("is_user_able_to_cancel")
    @Expose
    private String isUserAbleToCancel;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public Integer getGroupMemberCount() {
        return groupMemberCount;
    }

    public void setGroupMemberCount(Integer groupMemberCount) {
        this.groupMemberCount = groupMemberCount;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public Integer getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(Integer groupStatus) {
        this.groupStatus = groupStatus;
    }

    public String getGroupHits() {
        return groupHits;
    }

    public void setGroupHits(String groupHits) {
        this.groupHits = groupHits;
    }

    public String getGroupNotificationType() {
        return groupNotificationType;
    }

    public void setGroupNotificationType(String groupNotificationType) {
        this.groupNotificationType = groupNotificationType;
    }

    public String getGroupCat() {
        return groupCat;
    }

    public void setGroupCat(String groupCat) {
        this.groupCat = groupCat;
    }

    public String getGroupCatName() {
        return groupCatName;
    }

    public void setGroupCatName(String groupCatName) {
        this.groupCatName = groupCatName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateid() {
        return stateid;
    }

    public void setStateid(String stateid) {
        this.stateid = stateid;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public Boolean getIsPhotoalbum() {
        return isPhotoalbum;
    }

    public void setIsPhotoalbum(Boolean isPhotoalbum) {
        this.isPhotoalbum = isPhotoalbum;
    }

    public Boolean getIsVideos() {
        return isVideos;
    }

    public void setIsVideos(Boolean isVideos) {
        this.isVideos = isVideos;
    }

    public Boolean getIsAnnouncement() {
        return isAnnouncement;
    }

    public void setIsAnnouncement(Boolean isAnnouncement) {
        this.isAnnouncement = isAnnouncement;
    }

    public Boolean getIsDiscussions() {
        return isDiscussions;
    }

    public void setIsDiscussions(Boolean isDiscussions) {
        this.isDiscussions = isDiscussions;
    }

    public Boolean getIsAudios() {
        return isAudios;
    }

    public void setIsAudios(Boolean isAudios) {
        this.isAudios = isAudios;
    }

    public Boolean getIsEvents() {
        return isEvents;
    }

    public void setIsEvents(Boolean isEvents) {
        this.isEvents = isEvents;
    }

    public Boolean getIsFeeds() {
        return isFeeds;
    }

    public void setIsFeeds(Boolean isFeeds) {
        this.isFeeds = isFeeds;
    }

    public Boolean getIsFiles() {
        return isFiles;
    }

    public void setIsFiles(Boolean isFiles) {
        this.isFiles = isFiles;
    }

    public Boolean getIsPolls() {
        return isPolls;
    }

    public void setIsPolls(Boolean isPolls) {
        this.isPolls = isPolls;
    }

    public Boolean getIsTasks() {
        return isTasks;
    }

    public void setIsTasks(Boolean isTasks) {
        this.isTasks = isTasks;
    }

    public String getIsUserAbleToDelete() {
        return isUserAbleToDelete;
    }

    public void setIsUserAbleToDelete(String isUserAbleToDelete) {
        this.isUserAbleToDelete = isUserAbleToDelete;
    }

    public String getIsUserAbleToCancel() {
        return isUserAbleToCancel;
    }

    public void setIsUserAbleToCancel(String isUserAbleToCancel) {
        this.isUserAbleToCancel = isUserAbleToCancel;
    }

}
