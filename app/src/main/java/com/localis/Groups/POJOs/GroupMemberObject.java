package com.localis.Groups.POJOs;

import java.io.Serializable;

/**
 * Created by pct1 on 14/2/18.
 */

public class GroupMemberObject implements Serializable {
    private int id;
    private String memberName;


    public GroupMemberObject(int id, String friendName) {
        this.id = id;
        this.memberName = friendName;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }


}
