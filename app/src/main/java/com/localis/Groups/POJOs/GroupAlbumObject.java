package com.localis.Groups.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupAlbumObject {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("cover_id")
    @Expose
    private String coverId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("album_created_date")
    @Expose
    private String albumCreatedDate;
    @SerializedName("hits")
    @Expose
    private String hits;
    @SerializedName("album_comment_count")
    @Expose
    private String albumCommentCount;
    @SerializedName("albumCreatorName")
    @Expose
    private String albumCreatorName;
    @SerializedName("albumCreatorId")
    @Expose
    private String albumCreatorId;
    @SerializedName("images")
    @Expose
    private Integer images;
    @SerializedName("album_like_count")
    @Expose
    private Integer albumLikeCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCoverId() {
        return coverId;
    }

    public void setCoverId(String coverId) {
        this.coverId = coverId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlbumCreatedDate() {
        return albumCreatedDate;
    }

    public void setAlbumCreatedDate(String albumCreatedDate) {
        this.albumCreatedDate = albumCreatedDate;
    }

    public String getHits() {
        return hits;
    }

    public void setHits(String hits) {
        this.hits = hits;
    }

    public String getAlbumCommentCount() {
        return albumCommentCount;
    }

    public void setAlbumCommentCount(String albumCommentCount) {
        this.albumCommentCount = albumCommentCount;
    }

    public String getAlbumCreatorName() {
        return albumCreatorName;
    }

    public void setAlbumCreatorName(String albumCreatorName) {
        this.albumCreatorName = albumCreatorName;
    }

    public String getAlbumCreatorId() {
        return albumCreatorId;
    }

    public void setAlbumCreatorId(String albumCreatorId) {
        this.albumCreatorId = albumCreatorId;
    }

    public Integer getImages() {
        return images;
    }

    public void setImages(Integer images) {
        this.images = images;
    }

    public Integer getAlbumLikeCount() {
        return albumLikeCount;
    }

    public void setAlbumLikeCount(Integer albumLikeCount) {
        this.albumLikeCount = albumLikeCount;
    }

}
