package com.localis.Groups;

import android.content.ContentValues;

import com.google.gson.JsonArray;
import com.localis.Groups.POJOs.GroupAlbumObject;
import com.localis.Groups.POJOs.GroupCategoriesObject;
import com.localis.Groups.POJOs.GroupDetailsObject;
import com.localis.Groups.POJOs.GroupItemObject;
import com.localis.Groups.POJOs.GroupVideoObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GroupImpl {

    public interface GroupAddition {
        void onGroupAdded(String message);

        void onGroupAdditionFailed();
    }

    public interface GroupEdition {
        void onGroupEdited(String message);

        void onGroupEditionFailed();
    }

    public interface GetGroupCategories {
        void onGroupCategoriesReceived(JSONArray groupCategories);

        void onGroupCategoriesFailed();
    }

    public interface GroupCategorySelecting {
        void onGroupCategorySelected(ContentValues contentValues);
    }


    public interface GetGroupList {
        void onGroupListReceived(ArrayList<GroupItemObject> groupItems, String totalPages);

        void onGroupListFailed();
    }


    public interface GetGroupDetails {
        void onGroupDetailReceived(GroupDetailsObject groupDetails);

        void onGroupDetailsFailed();
    }


    public interface SetGroupAsFeature {
        void onFeatureSet(String message);

        void onFeatureSettingFailed();
    }


    public interface GroupDeletion {
        void onGroupDeleted(String message, int ItemPosition);

        void onGroupDeletionFailed();
    }

    public interface GroupUnpublish {
        void onGroupUnpublished(String message);

        void onGroupUnpublishFailed();
    }

    public interface GroupJoining {
        void onGroupJoined(String message);

        void onGroupJoiningFailed();
    }

    public interface GroupReporting {
        void onGroupReported(String message);

        void onGroupReportingFailed();
    }

    public interface GetGroupAlbums {
        void onGroupAlbumsReceived(ArrayList<GroupAlbumObject> groupAlbums);

        void onGetGroupAlbumFailed();
    }

    public interface GetGroupVideos {
        void onGroupVideosReceived(ArrayList<GroupVideoObject> groupVideo);

        void onGetGroupVidoesFailed();
    }

    public interface GetGroupAboutUs {
        void onGroupAboutUsReceived(JSONObject data);

        void onGetGroupAboutUsFailed();
    }


}
