package com.localis.AA_POJOs;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

/**
 * Created by ebiztrait321 on 14/2/18.
 */

public class NavItemObject {
    private int startDrawable;
    private int titleNav;
    private int endDrawable;
    private Class intentClass;

    public NavItemObject(@DrawableRes int startDrawable, @StringRes int titleNav, @DrawableRes int endDrawable, Class<?> intentClass) {
        this.startDrawable = startDrawable;
        this.titleNav = titleNav;
        this.endDrawable = endDrawable;
        this.intentClass = intentClass;

    }

    public int getStartDrawable() {
        return startDrawable;
    }

    public void setStartDrawable(int startDrawable) {
        this.startDrawable = startDrawable;
    }

    public int getTitleNav() {
        return titleNav;
    }

    public void setTitleNav(int titleNav) {
        this.titleNav = titleNav;
    }

    public int getEndDrawable() {
        return endDrawable;
    }

    public void setEndDrawable(int endDrawable) {
        this.endDrawable = endDrawable;
    }

    public Class getIntentClass() {
        return intentClass;
    }

    public void setIntentClass(Class intentClass) {
        this.intentClass = intentClass;
    }

}
