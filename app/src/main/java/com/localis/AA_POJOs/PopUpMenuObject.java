package com.localis.AA_POJOs;

public class PopUpMenuObject {

    String itemName;
    int itemAction;


    public PopUpMenuObject(String itemName, int itemAction) {
        this.itemName = itemName;
        this.itemAction = itemAction;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemAction() {
        return itemAction;
    }

    public void setItemAction(int itemAction) {
        this.itemAction = itemAction;
    }
}
