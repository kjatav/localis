package com.localis.AA_POJOs;

/**
 * Created by : Dhiren Parmar
 * Created date : March 09, 2018
 * Purpose of creation :
 * Modified by :
 * Modified date :
 * Purpose of modification :
 */

public class RequestObject {
    String image;
    String name;

    public RequestObject(String image, String name) {
        this.image = image;
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
