package com.localis.AA_POJOs;

import android.graphics.Bitmap;

public class ImageObject {
String groupImages;

    public ImageObject(String groupImages) {
        this.groupImages = groupImages;
    }

    public String getGroupImages() {
        return groupImages;
    }

    public void setGroupImages(String groupImages) {
        this.groupImages = groupImages;
    }
}
