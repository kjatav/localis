package com.localis.Events.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Events.Activities.EventDetailActivity;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;


public class EventItemAdapter extends RecyclerView.Adapter<EventItemAdapter.MyViewHolder> {
    private ArrayList<ImageObject> imageObjects;
    private Context context;


    public EventItemAdapter(ArrayList<ImageObject> imageObjects, Context context) {
        this.imageObjects = imageObjects;
        this.context = context;
    }

    @NonNull
    @Override
    public EventItemAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_event_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull EventItemAdapter.MyViewHolder holder, final int position) {
        holder.eventItemLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EventDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return imageObjects.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout eventItemLl;
        private ImageView eventItemIv;
        private ImageView optionsEventItemIv;
        private SmartTextView monthEventItemTv;
        private SmartTextView dateEventItemTv;
        private SmartTextView titleEventItemTv;
        private SmartTextView durationEventItemTv;
        private ImageView pastEventItemIv;
        private SmartTextView categoryEventItemTv;
        private SmartTextView typeEventItemTv;
        private SmartTextView descEventItemTv;
        private SmartTextView attendeeEventItemTv;
        private SmartTextView locationEventItemTv;
        private SmartButton requestEventItemBtn;


        public MyViewHolder(View itemView) {
            super(itemView);
            eventItemLl = itemView.findViewById(R.id.event_item_ll);
            eventItemIv = itemView.findViewById(R.id.event_item_iv);
            optionsEventItemIv = itemView.findViewById(R.id.options_event_item_iv);
            monthEventItemTv = itemView.findViewById(R.id.month_event_item_tv);
            dateEventItemTv = itemView.findViewById(R.id.date_event_item_tv);
            titleEventItemTv = itemView.findViewById(R.id.title_event_item_tv);
            durationEventItemTv = itemView.findViewById(R.id.duration_event_item_tv);
            pastEventItemIv = itemView.findViewById(R.id.past_event_item_iv);
            categoryEventItemTv = itemView.findViewById(R.id.category_event_item_tv);
            typeEventItemTv = itemView.findViewById(R.id.type_event_item_tv);
            descEventItemTv = itemView.findViewById(R.id.desc_event_item_tv);
            attendeeEventItemTv = itemView.findViewById(R.id.attendee_event_item_tv);
            locationEventItemTv = itemView.findViewById(R.id.location_event_item_tv);
            requestEventItemBtn = itemView.findViewById(R.id.request_event_item_btn);
        }
    }
}
