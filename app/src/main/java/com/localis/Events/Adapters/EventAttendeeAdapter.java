package com.localis.Events.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localis.Events.POJOs.AttendeeObject;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartTextView;

import java.util.List;

/**
 * Created by : Dhiren Parmar
 * Created date : January 24, 2018
 * Purpose of creation :
 * Modified by :
 * Modified date :
 * Purpose of modification :
 */

public class EventAttendeeAdapter extends RecyclerView.Adapter<EventAttendeeAdapter.MyViewHolder> {
    private Context context;
    private List<AttendeeObject> memeberObjects;

    public EventAttendeeAdapter(Context context, List<AttendeeObject> tagFriendsObjects) {
        this.context = context;
        this.memeberObjects = tagFriendsObjects;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_event_attendee, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgEventAttendeeIv;
        private SmartTextView nameEventAttendeeIv;
        private SmartButton ownerEventAttendeeBtn;
        private SmartButton goingEventAttendeeBtn;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgEventAttendeeIv = itemView.findViewById(R.id.img_event_attendee_iv);
            nameEventAttendeeIv = itemView.findViewById(R.id.name_event_attendee_iv);
            ownerEventAttendeeBtn = itemView.findViewById(R.id.owner_event_attendee_btn);
            goingEventAttendeeBtn = itemView.findViewById(R.id.going_event_attendee_btn);
        }
    }
}