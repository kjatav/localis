package com.localis.Events.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_POJOs.ImageObject;
import com.localis.Albums.Activites.AlbumAddActivity;
import com.localis.R;
import com.smart.customViews.CustomClickListener;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartUtils;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.GregorianCalendar;

import static com.smart.framework.SmartUtils.getDateTimeDialog;

public class EventAddActivity extends MasterActivity {
    private SmartEditText categoryEventAddEt;
    private SmartEditText titleEventAddEt;
    private SmartEditText permalinkEventAddEt;
    private SmartEditText startDateEventAddEt;
    private SmartEditText endDateEventAddEt;
    private SwitchCompat allDayEventAddSw;
    private SmartEditText repeatEventAddEt;
    private SmartEditText descEventAddEt;
    private SmartEditText typeEventAddEt;
    private RoundedImageView imgEventAddIv;
    private SmartTextView imgEventAddTv;
    private ImageView coverEventAddIv;
    private SmartTextView coverEventAddTv;
    private SwitchCompat audioEventAddSw;
    private SwitchCompat filesEventAddSw;
    private SwitchCompat pollsEventAddSw;
    private SmartTextView tasksEventAddSw;
    private SwitchCompat maybeEventAddSw;
    private SwitchCompat goingEventAddSw;
    private SmartEditText attendeeEventAddEt;
    private SmartButton submitEventAddBtn;
    private SmartButton cancelEventAddBtn;


    String categoryTypeEvent[] = {"Meeting", "Social Events"};
    String repentEvent[] = {"No one", "Daily", "Monthly", "Annual"};
    String eventType[] = {"Open event", "Event closed", "Only invite participants from the event"};
    private boolean isProfile;
    Context context;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_event_add;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.event);

        context = EventAddActivity.this;

        categoryEventAddEt = findViewById(R.id.category_event_add_et);
        titleEventAddEt = findViewById(R.id.title_event_add_et);
        permalinkEventAddEt = findViewById(R.id.permalink_event_add_et);
        startDateEventAddEt = findViewById(R.id.start_date_event_add_et);
        endDateEventAddEt = findViewById(R.id.end_date_event_add_et);
        allDayEventAddSw = findViewById(R.id.all_day_event_add_sw);
        repeatEventAddEt = findViewById(R.id.repeat_event_add_et);
        descEventAddEt = findViewById(R.id.desc_event_add_et);
        typeEventAddEt = findViewById(R.id.type_event_add_et);
        imgEventAddIv = findViewById(R.id.img_event_add_iv);
        imgEventAddTv = findViewById(R.id.img_event_add_tv);
        coverEventAddIv=findViewById(R.id.cover_event_add_iv);
        coverEventAddTv = findViewById(R.id.cover_event_add_tv);
        audioEventAddSw = findViewById(R.id.audio_event_add_sw);
        filesEventAddSw = findViewById(R.id.files_event_add_sw);
        pollsEventAddSw = findViewById(R.id.polls_event_add_sw);
        tasksEventAddSw = findViewById(R.id.tasks_event_add_sw);
        maybeEventAddSw = findViewById(R.id.maybe_event_add_sw);
        goingEventAddSw = findViewById(R.id.going_event_add_sw);
        attendeeEventAddEt = findViewById(R.id.attendee_event_add_et);
        submitEventAddBtn = findViewById(R.id.submit_event_add_btn);
        cancelEventAddBtn = findViewById(R.id.cancel_event_add_btn);
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();
        categoryEventAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(context, categoryTypeEvent, getString(R.string.select_a_category), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        categoryEventAddEt.setText(categoryTypeEvent[id]);
                    }
                });
            }
        });
        repeatEventAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(context, repentEvent, getString(R.string.select_a_category), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        repeatEventAddEt.setText(repentEvent[id]);
                    }
                });
            }
        });
        typeEventAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(context, eventType, getString(R.string.select_a_category), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        typeEventAddEt.setText(eventType[id]);
                    }
                });
            }
        });
      /*  startDateEventAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
                String currentMoth = String.valueOf(android.text.format.DateFormat.format(context.getString(R.string.date_format_2), gregorianCalendar));
                getDateTimeDialog(context, currentMoth, true,context.getString(R.string.date_time_format_3), new CustomClickListener() {
                    @Override
                    public void onClick(String value) {
                        startDateEventAddEt.setText(value);
                    }
                });
            }
        });
        endDateEventAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
                String currentMoth = String.valueOf(android.text.format.DateFormat.format(context.getString(R.string.date_format_2), gregorianCalendar));
                getDateTimeDialog(context, currentMoth, true,context.getString(R.string.date_time_format_3), new CustomClickListener() {
                    @Override
                    public void onClick(String value) {
                        endDateEventAddEt.setText(value);
                    }
                });
            }
        });*/
        imgEventAddTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isProfile=true;
                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                        .start(EventAddActivity.this);
            }
        });
        coverEventAddTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isProfile=false;
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(EventAddActivity.this);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Bitmap bitmap = null;
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                if (isProfile) {
                    Picasso.with(context).load(resultUri).into(imgEventAddIv);
                }else {
                    Picasso.with(context).load(resultUri).into(coverEventAddIv);
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(context, String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}
