package com.localis.Events.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.ViewPagerAdapter;
import com.localis.Events.Fragments.EventListFragment;
import com.localis.R;

public class EventListActivity extends MasterActivity {

    private FloatingActionButton addEventBtn;

    @Override
    public int getLayoutID() {
        return R.layout.activity_aa_pager;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.events);

        TabLayout tabLayout = findViewById(R.id.details_tab);
        ViewPager eventListVp = findViewById(R.id.details_vp);
        addEventBtn = findViewById(R.id.details_add_btn);

        setupViewPager(eventListVp);
        tabLayout.setupWithViewPager(eventListVp);
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();
        addEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EventListActivity.this, EventAddActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        Bundle bundle = new Bundle();
        bundle.putInt(PAST_EVENT_FLAG, 1);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        EventListFragment myEventListFragment = new EventListFragment();
        myEventListFragment.setArguments(bundle);

        EventListFragment nearByEventListFragment = new EventListFragment();
        nearByEventListFragment.setArguments(bundle);

        adapter.addFragment(new EventListFragment(), "All Events");
        adapter.addFragment(new EventListFragment(), "Featured Events");
        adapter.addFragment(myEventListFragment, "My Events");
        adapter.addFragment(nearByEventListFragment, "Nearby Events");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_menu_top));
    }
}