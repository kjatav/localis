package com.localis.Events.POJOs;

import java.io.Serializable;

/**
 * Created by pct1 on 14/2/18.
 */

public class AttendeeObject implements Serializable {
    private int id;
    private String attendName;


    public AttendeeObject(int id, String friendName) {
        this.id = id;
        this.attendName = friendName;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAttendName() {
        return attendName;
    }

    public void setAttendName(String attendName) {
        this.attendName = attendName;
    }


}
