package com.localis.Events.Fragments;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartTextView;
import com.smart.framework.SmartFragment;

/**
 * Created by pct1 on 1/3/18.
 */

public class EventAboutFragment extends SmartFragment {

    private ImageView imgEventDetailIv;
    private ImageView optionsEventDetailIv;
    private ImageView profileEventDetailIv;
    private SmartTextView nameEventDetailIv;
    private SmartTextView dateEventDetailIv;
    private RatingBar ratingEventDetailIv;

    private SmartButton rsvpEventAboutBtn;
    private SmartTextView titleEventAboutBtn;
    private SmartTextView dateEventAboutBtn;
    private SmartTextView startEventAboutBtn;
    private SmartTextView endEventAboutBtn;

    private SmartTextView nameDetailsThumbsTv;
    private SmartTextView membersDetailsThumbsTv;
    private SmartTextView viewDetailsThumbsTv;


    @Override
    public int setLayoutId() {
        return R.layout.fragment_event_about;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        imgEventDetailIv = currentView.findViewById(R.id.img_event_detail_iv);
        optionsEventDetailIv = currentView.findViewById(R.id.options_event_detail_iv);
        profileEventDetailIv = currentView.findViewById(R.id.profile_event_detail_iv);
        nameEventDetailIv = currentView.findViewById(R.id.name_event_detail_iv);
        dateEventDetailIv = currentView.findViewById(R.id.date_event_detail_iv);
        ratingEventDetailIv = currentView.findViewById(R.id.rating_event_detail_iv);

        rsvpEventAboutBtn = currentView.findViewById(R.id.rsvp_event_about_btn);

        nameDetailsThumbsTv = currentView.findViewById(R.id.name_details_thumbs_tv);
        membersDetailsThumbsTv = currentView.findViewById(R.id.members_details_thumbs_tv);
        viewDetailsThumbsTv = currentView.findViewById(R.id.view_details_thumbs_tv);

        titleEventAboutBtn = currentView.findViewById(R.id.title_event_about_btn);
        dateEventAboutBtn = currentView.findViewById(R.id.date_event_about_btn);
        startEventAboutBtn = currentView.findViewById(R.id.start_event_about_btn);
        endEventAboutBtn = currentView.findViewById(R.id.end_event_about_btn);


        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }
}
