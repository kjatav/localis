package com.localis.Events.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.localis.Events.Adapters.EventAttendeeAdapter;
import com.localis.Events.POJOs.AttendeeObject;
import com.localis.R;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pct1 on 1/3/18.
 */

public class EventAttendeesFragment extends SmartFragment {

    private RecyclerView eventAttendeesRv;
    private EventAttendeeAdapter EventAttendeeAdapter;
    private List<AttendeeObject> attendObjects = new ArrayList<>();

    @Override
    public int setLayoutId() {
        return R.layout.fragment_event_attendes;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        eventAttendeesRv = currentView.findViewById(R.id.event_attendees_rv);
        eventAttendeesRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        EventAttendeeAdapter = new EventAttendeeAdapter(getActivity(), attendObjects);
        eventAttendeesRv.setAdapter(EventAttendeeAdapter);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }
}
