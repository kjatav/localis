package com.localis.Events.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Events.Adapters.EventItemAdapter;
import com.localis.R;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartRecyclerView;

import java.util.ArrayList;

import static com.smart.framework.SmartUtils.showSortingPopup;

public class EventListFragment extends SmartFragment {
    private ArrayList<ImageObject> image_objects = new ArrayList<>();

    private EventItemAdapter eventItemAdapter;

    private SmartTextView categoryEventTv;
    private SmartTextView sortByDateTv;
    private CheckBox pastEventsCb;
    private SmartRecyclerView eventListRv;
    private View layoutNoData;


    @Override
    public int setLayoutId() {
        return R.layout.fragment_event_list;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        categoryEventTv = currentView.findViewById(R.id.category_event_tv);
        sortByDateTv = currentView.findViewById(R.id.sort_by_date_tv);
        pastEventsCb = currentView.findViewById(R.id.past_events_cb);
        eventListRv = currentView.findViewById(R.id.event_list_rv);
        layoutNoData = currentView.findViewById(R.id.layout_no_data);

        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));
        image_objects.add(new ImageObject(""));

        eventItemAdapter = new EventItemAdapter(image_objects, getActivity());

        eventListRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        eventListRv.setAdapter(eventItemAdapter);
        eventListRv.setEmptyView(layoutNoData);


        if (getArguments() != null) {
            if (getArguments().getInt(PAST_EVENT_FLAG) == 1) {
                pastEventsCb.setVisibility(View.VISIBLE);
            }
        }


        setHasOptionsMenu(true);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        sortByDateTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showSortingPopup(getActivity(), sortByDateTv, getResources().getStringArray(R.array.sortingItems_event), new AlertMagneticClass.PopUpInterface() {
                    @Override
                    public void onSetPopUp(int selectedOptionPosition) {
                        sortByDateTv.setText(getResources().getStringArray(R.array.sortingItems_event)[selectedOptionPosition]);
                    }
                });
            }
        });
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.event_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        categoryEventTv.setText(item.getTitle());
        return super.onOptionsItemSelected(item);
    }
}