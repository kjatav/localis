package com.localis.Events.Fragments;

import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Dashboard.Adapter.DashboardAdapter;
import com.localis.Dashboard.POJOs.FeedItemObject;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartTextView;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;

import static com.smart.framework.SmartUtils.switchTabsColor;

/**
 * Created by pct1 on 1/3/18.
 */

public class EventTimelineFragment extends SmartFragment {
    private ArrayList<ImageObject> image_objects = new ArrayList<>();
    private ArrayList<FeedItemObject> feedItem = new ArrayList<>();
    private DashboardAdapter dashboardAdapter;
    private LinearLayoutManager linearLayoutManager;

    private ImageView imgEventDetailIv;
    private ImageView optionsEventDetailIv;
    private ImageView profileEventDetailIv;
    private SmartTextView nameEventDetailIv;
    private SmartTextView dateEventDetailIv;
    private RatingBar ratingEventDetailIv;

    private SmartButton rsvpEventTimelineBtn;
    private SmartTextView categoryEventTimelineTv;
    private SmartTextView nameEventTimelineTv;
    private SmartTextView hitsEventTimelineTv;
    private SmartTextView openEventTimelineTv;

    private TabLayout eventTimelineTabs;


    private SmartTextView statusTabTv;
    private SmartTextView photosTabTv;
    private SmartTextView videoTabTv;
    private SmartTextView eventsTabTv;
    private SmartTextView pollsTabTv;

    private View statusLayout;
    private View photosLayout;
    private View videoLayout;
    private View eventsLayout;
    private View pollsLayout;

    private RecyclerView eventTimelineRv;


    @Override
    public int setLayoutId() {
        return R.layout.fragment_event_timeline;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {

        imgEventDetailIv = currentView.findViewById(R.id.img_event_detail_iv);
        optionsEventDetailIv = currentView.findViewById(R.id.options_event_detail_iv);
        profileEventDetailIv = currentView.findViewById(R.id.profile_event_detail_iv);
        nameEventDetailIv = currentView.findViewById(R.id.name_event_detail_iv);
        dateEventDetailIv = currentView.findViewById(R.id.date_event_detail_iv);
        ratingEventDetailIv = currentView.findViewById(R.id.rating_event_detail_iv);


        rsvpEventTimelineBtn = currentView.findViewById(R.id.rsvp_event_timeline_btn);
        categoryEventTimelineTv = currentView.findViewById(R.id.category_event_timeline_tv);
        categoryEventTimelineTv = currentView.findViewById(R.id.category_event_timeline_tv);
        nameEventTimelineTv = currentView.findViewById(R.id.name_event_timeline_tv);
        hitsEventTimelineTv = currentView.findViewById(R.id.hits_event_timeline_tv);
        openEventTimelineTv = currentView.findViewById(R.id.open_event_timeline_tv);
        eventTimelineTabs = currentView.findViewById(R.id.post_timeline_tabs);
        eventTimelineRv = currentView.findViewById(R.id.event_timeline_rv);

        for (int i = 0; i < 5; i++) {
            eventTimelineTabs.addTab(eventTimelineTabs.newTab());
        }

        statusLayout = currentView.findViewById(R.id.status_layout);
        photosLayout = currentView.findViewById(R.id.photos_layout);
        videoLayout = currentView.findViewById(R.id.video_layout);
        eventsLayout = currentView.findViewById(R.id.events_layout);
        pollsLayout = currentView.findViewById(R.id.polls_layout);

        createPostTabIcons();


        eventTimelineRv = currentView.findViewById(R.id.event_timeline_rv);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        eventTimelineRv.setLayoutManager(linearLayoutManager);

        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));
        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));
        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));

        dashboardAdapter = new DashboardAdapter(feedItem, image_objects, getContext());
        eventTimelineRv.setAdapter(dashboardAdapter);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }


    private void createPostTabIcons() {
        statusTabTv = (SmartTextView)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        statusTabTv.setText(R.string.status);
        statusTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_status, 0, 0);
        eventTimelineTabs.getTabAt(0).setCustomView(statusTabTv);

        photosTabTv = (SmartTextView)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        photosTabTv.setText(R.string.photos);
        photosTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_photo, 0, 0);
        eventTimelineTabs.getTabAt(1).setCustomView(photosTabTv);

        videoTabTv = (SmartTextView)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        videoTabTv.setText(R.string.video);
        videoTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_video, 0, 0);
        eventTimelineTabs.getTabAt(2).setCustomView(videoTabTv);

        eventsTabTv = (SmartTextView)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        eventsTabTv.setText(R.string.events);
        eventsTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_event, 0, 0);
        eventTimelineTabs.getTabAt(3).setCustomView(eventsTabTv);

        pollsTabTv = (SmartTextView)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        pollsTabTv.setText(R.string.polls);
        pollsTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_poll, 0, 0);
        eventTimelineTabs.getTabAt(4).setCustomView(pollsTabTv);


        eventTimelineTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        switchTabsColor(getActivity(), statusTabTv, R.drawable.ic_subheader_status_hover, true);
                        hideAndShow(statusLayout);
                        break;
                    case 1:
                        switchTabsColor(getActivity(), photosTabTv, R.drawable.ic_subheader_photo_hover, true);
                        hideAndShow(photosLayout);
                        break;
                    case 2:
                        switchTabsColor(getActivity(), videoTabTv, R.drawable.ic_subheader_video_hover, true);
                        hideAndShow(videoLayout);
                        break;
                    case 3:
                        switchTabsColor(getActivity(), eventsTabTv, R.drawable.ic_subheader_event_hover, true);
                        hideAndShow(eventsLayout);
                        break;
                    case 4:
                        switchTabsColor(getActivity(), pollsTabTv, R.drawable.ic_subheader_poll_hover, true);
                        hideAndShow(pollsLayout);
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        switchTabsColor(getActivity(), statusTabTv, R.drawable.ic_subheader_status, false);
                        break;
                    case 1:
                        switchTabsColor(getActivity(), photosTabTv, R.drawable.ic_subheader_photo, false);
                        break;
                    case 2:
                        switchTabsColor(getActivity(), videoTabTv, R.drawable.ic_subheader_video, false);
                        break;
                    case 3:
                        switchTabsColor(getActivity(), eventsTabTv, R.drawable.ic_subheader_event, false);
                        break;
                    case 4:
                        switchTabsColor(getActivity(), pollsTabTv, R.drawable.ic_subheader_poll, false);
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        switchTabsColor(getActivity(), statusTabTv, R.drawable.ic_subheader_status_hover, true);
    }


    private void hideAndShow(View view) {
        statusLayout.setVisibility(View.GONE);
        photosLayout.setVisibility(View.GONE);
        videoLayout.setVisibility(View.GONE);
        eventsLayout.setVisibility(View.GONE);
        pollsLayout.setVisibility(View.GONE);

        view.setVisibility(View.VISIBLE);
    }
}
