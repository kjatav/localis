package com.localis.ZZ_Package;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.android.volley.Request;
import com.localis.R;
import com.smart.caching.SmartCaching;
import com.smart.framework.SmartUtils;
import com.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.smart.framework.Constants.DATA;
import static com.smart.framework.Constants.TABLE_STATES;

public class LocationAPIs {

    public static void getStates(final Context context, final LocationImpl.GetStates getStates) {


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.GET);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_utility) + "getstate");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                try {
                    getStates.onStatesReceived(results.getJSONArray(DATA));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseError(JSONObject results) {
                getStates.onGetStateFailed();

            }
            });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void getCities(final Context context, final String stateId, final LocationImpl.GetCities getCities) {

        SmartUtils.showLoadingDialog(context);

        Map<String, String> params = new HashMap<>();
        params.put("id", stateId);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_utility) + "getcity");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    getCities.onCitiesReceived(results.getJSONArray(DATA), stateId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseError(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                getCities.onGetCitiesFailed();

            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, true);
    }


    public static void getStreets(final Context context, String stateId, String cityId, final LocationImpl.GetStreets getStreets) {

        SmartUtils.showLoadingDialog(context);

        Map<String, String> params = new HashMap<>();
        params.put("sid", stateId);
        params.put("cid", cityId);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_utility) + "getstreet");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    getStreets.onStreetsReceived(results.getJSONArray(DATA));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseError(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                getStreets.onGetStreetsFailed();

            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, false);
    }


    public static AlertDialog.Builder setStateList(final Context context, final LocationImpl.StateSelecting stateSelecting) {

        SmartCaching smartCaching = new SmartCaching(context);
        final List<ContentValues> statesContent = smartCaching.getDataFromCache(TABLE_STATES);
        Log.d("@@@", statesContent.get(0).getAsString("name"));

        final String[] stateNames = new String[statesContent.size()];
        for (int i = 0; i < statesContent.size(); i++) {
            stateNames[i] = statesContent.get(i).getAsString("name");
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("States");
        builder.setItems(stateNames, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                stateSelecting.onStateSelected(statesContent.get(position));
                //SmartUtils.toastShort(context, stateNames[position]);
            }
        });

        return builder;

    }

    public static AlertDialog.Builder setCityList(final Context context, final JSONArray citiesData, final String stateId, final LocationImpl.CitySelecting citySelecting) {

        final String[] cityNames = new String[citiesData.length()];
        for (int i = 0; i < citiesData.length(); i++) {
            try {
                cityNames[i] = citiesData.getJSONObject(i).getString("name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Cities");
        builder.setItems(cityNames, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                try {
                    citySelecting.onCitySelected(citiesData.getJSONObject(position), stateId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        return builder;
    }


    public static AlertDialog.Builder setStreetList(final Context context, final JSONArray streetData, final LocationImpl.StreetSelecting streetSelecting) {

        final String[] cityNames = new String[streetData.length()];
        for (int i = 0; i < streetData.length(); i++) {
            try {
                cityNames[i] = streetData.getJSONObject(i).getString("name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Cities");
        builder.setItems(cityNames, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                try {
                    streetSelecting.onStreetSelected(streetData.getJSONObject(position));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        return builder;
    }


}
