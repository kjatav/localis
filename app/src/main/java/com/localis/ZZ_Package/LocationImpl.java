package com.localis.ZZ_Package;

import android.content.ContentValues;

import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONObject;

public class LocationImpl {

    public interface GetStates {

        void onStatesReceived(JSONArray states);

        void onGetStateFailed();

    }


    public interface GetCities {

        void onCitiesReceived(JSONArray cities, String stateId);

        void onGetCitiesFailed();

    }

    public interface GetStreets {

        void onStreetsReceived(JSONArray cities);

        void onGetStreetsFailed();

    }

    public interface StateSelecting {
        void onStateSelected(ContentValues contentValues);
    }


    public interface CitySelecting {
        void onCitySelected(JSONObject jsonObject,String stateId);
    }

    public interface StreetSelecting {
        void onStreetSelected(JSONObject jsonObject);
    }


}
