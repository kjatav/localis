package com.localis.Polls.Fragments;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.localis.R;
import com.smart.framework.SmartFragment;


public class PollReportGraphFragment extends SmartFragment {

    private ImageView imgDescPollGraphIv;
    private RelativeLayout videoDescPollGraphRl;
    private ImageView videoDescPollGraphIv;


    @Override
    public int setLayoutId() {
        return R.layout.fragment_graph;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {

        imgDescPollGraphIv = currentView.findViewById(R.id.img_desc_poll_graph_iv);
        videoDescPollGraphRl = currentView.findViewById(R.id.video_desc_poll_graph_rl);
        videoDescPollGraphIv = currentView.findViewById(R.id.video_desc_poll_graph_iv);

        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }
}

