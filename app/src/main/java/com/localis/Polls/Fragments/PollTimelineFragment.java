package com.localis.Polls.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.localis.AA_Adapter.CommentAdapter;
import com.localis.AA_POJOs.ImageObject;
import com.localis.Polls.Adapters.PollTimelineAdapter;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartTextView;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;


public class PollTimelineFragment extends SmartFragment {

    private ImageView imgPollDetailIv;
    private ImageView optionsPollDetailIv;
    private ImageView profilePollDetailIv;

    private SmartTextView namePollDetailTv;
    private SmartTextView typePollDetailTv;

    private SmartButton rsvpPollTimelineBtn;

    private ImageView imgDesPollDetailIv;

    private RelativeLayout videoDesPollDetailRl;

    private ImageView videoDesPollDetailIv;

    private RecyclerView itemPollDetailRv;
    private RecyclerView commentsPollDetailRv;


    private PollTimelineAdapter pollTimelineAdapter;
    private CommentAdapter pollCommentAdapter;


    @Override
    public int setLayoutId() {
        return R.layout.fragment_poll_timeline;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {

        imgPollDetailIv = currentView.findViewById(R.id.img_poll_detail_iv);
        optionsPollDetailIv = currentView.findViewById(R.id.options_poll_detail_iv);
        profilePollDetailIv = currentView.findViewById(R.id.profile_poll_detail_iv);
        namePollDetailTv = currentView.findViewById(R.id.name_poll_detail_tv);
        typePollDetailTv = currentView.findViewById(R.id.type_poll_detail_tv);
        rsvpPollTimelineBtn = currentView.findViewById(R.id.rsvp_poll_timeline_btn);
        imgDesPollDetailIv = currentView.findViewById(R.id.img_des_poll_detail_iv);
        videoDesPollDetailRl = currentView.findViewById(R.id.video_des_poll_detail_rl);
        videoDesPollDetailIv = currentView.findViewById(R.id.video_des_poll_detail_iv);
        itemPollDetailRv = currentView.findViewById(R.id.item_poll_detail_rv);
        commentsPollDetailRv = currentView.findViewById(R.id.comments_poll_detail_rv);

        itemPollDetailRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        pollTimelineAdapter = new PollTimelineAdapter(getActivity());
        itemPollDetailRv.setAdapter(pollTimelineAdapter);

        commentsPollDetailRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        pollCommentAdapter = new CommentAdapter(new ArrayList<ImageObject>(), getActivity());
        commentsPollDetailRv.setAdapter(pollCommentAdapter);

        return currentView;
    }


    @Override
    public void prepareViews(View currentView) {

    }


    @Override
    public void setActionListeners(View currentView) {

    }
}
