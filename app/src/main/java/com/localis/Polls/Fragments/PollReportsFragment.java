package com.localis.Polls.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.localis.Polls.Adapters.PollReportItemAdapter;
import com.localis.R;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartRecyclerView;


public class PollReportsFragment extends SmartFragment {
    private SmartRecyclerView pollsReportsRv;
    private PollReportItemAdapter pollReportItemAdapter;

    @Override
    public int setLayoutId() {
        return R.layout.fragment_poll_report;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        pollsReportsRv = currentView.findViewById(R.id.polls_reports_rv);

        pollsReportsRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        pollReportItemAdapter = new PollReportItemAdapter(getActivity());
        pollsReportsRv.setAdapter(pollReportItemAdapter);
        pollsReportsRv.setEmptyView(currentView.findViewById(R.id.layout_no_data));

        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {
    }

    @Override
    public void setActionListeners(View currentView) {
    }
}


