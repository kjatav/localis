package com.localis.Polls.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Polls.Adapters.PollItemAdapter;
import com.localis.R;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartRecyclerView;

import java.util.ArrayList;


public class PollListFragment extends SmartFragment {
    private SmartRecyclerView pollsRv;
    private ArrayList<ImageObject> image_objects = new ArrayList<>();

    @Override
    public int setLayoutId() {
        return R.layout.fragment_poll_list;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        pollsRv = currentView.findViewById(R.id.polls_rv);

        pollsRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        PollItemAdapter adapter = new PollItemAdapter(image_objects, getActivity());
        pollsRv.setAdapter(adapter);
        pollsRv.setEmptyView(currentView.findViewById(R.id.layout_no_data));

        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {
    }

    @Override
    public void setActionListeners(View currentView) {
    }
}