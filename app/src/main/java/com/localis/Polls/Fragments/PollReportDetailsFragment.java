package com.localis.Polls.Fragments;

import android.view.View;

import com.localis.R;
import com.smart.customViews.SmartTextView;
import com.smart.framework.SmartFragment;


public class PollReportDetailsFragment extends SmartFragment {

    private SmartTextView agePollReportDetailTv;
    private SmartTextView genderPollReportDetailTv;
    private SmartTextView countryPollReportDetailTv;
    private SmartTextView cityPollReportDetailTv;
    private SmartTextView streetPollReportDetailTv;
    private SmartTextView detailVoteTxt;
    private SmartTextView votesPollReportDetailTv;
    private SmartTextView locationPollReportDetailTv;
    private SmartTextView useridPollReportDetailTv;
    private SmartTextView usernamePollReportDetailTv;
    private SmartTextView optionPollReportDetailTv;


    @Override
    public int setLayoutId() {
        return R.layout.fragment_poll_report_details;
    }


    @Override
    public View setLayoutView() {
        return null;
    }


    @Override
    public View initComponents(View currentView) {
        agePollReportDetailTv = currentView.findViewById(R.id.age_poll_report_detail_tv);
        genderPollReportDetailTv = currentView.findViewById(R.id.gender_poll_report_detail_tv);
        countryPollReportDetailTv = currentView.findViewById(R.id.country_poll_report_detail_tv);
        cityPollReportDetailTv = currentView.findViewById(R.id.city_poll_report_detail_tv);
        streetPollReportDetailTv = currentView.findViewById(R.id.street_poll_report_detail_tv);
        detailVoteTxt = currentView.findViewById(R.id.detail_vote_txt);
        votesPollReportDetailTv = currentView.findViewById(R.id.votes_poll_report_detail_tv);
        locationPollReportDetailTv = currentView.findViewById(R.id.location_poll_report_detail_tv);
        useridPollReportDetailTv = currentView.findViewById(R.id.userid_poll_report_detail_tv);
        usernamePollReportDetailTv = currentView.findViewById(R.id.username_poll_report_detail_tv);
        optionPollReportDetailTv = currentView.findViewById(R.id.option_poll_report_detail_tv);
        return currentView;
    }


    @Override
    public void prepareViews(View currentView) {

    }


    @Override
    public void setActionListeners(View currentView) {

    }
}

