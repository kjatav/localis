package com.localis.Polls.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.smart.customViews.CustomClickListener;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartUtils;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.GregorianCalendar;


public class PollAddActivity extends MasterActivity {
    private String age[] = {"20-25", "25-30", "30-35", "35-40", ">60"};

    private SmartEditText titlePollAddEt;
    private ImageView imgPollAddIv;
    private SmartButton imgAddPollAddBtn;
    private SmartButton videoAddPollAddBtn;
    private SmartEditText urlAddPollAddEt;
    private SmartEditText agePollAddEt;
    private SmartEditText genderPollAddEt;
    private SmartEditText countryPollAddEt;
    private SmartEditText cityPollAddEt;
    private SmartEditText itemPollAddEt;
    private SmartButton itemAddPollAddBtn;
    private SmartEditText expiryPollAddEt;
    private CheckBox multiplePollAddCb;
    private SmartButton submitPollAddBtn;
    private SmartButton cancelPollAddBtn;


    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_poll_add;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.add_new_poll);

        titlePollAddEt = findViewById(R.id.title_poll_add_et);
        imgPollAddIv = findViewById(R.id.img_poll_add_iv);
        imgAddPollAddBtn = findViewById(R.id.img_add_poll_add_btn);
        videoAddPollAddBtn = findViewById(R.id.video_add_poll_add_btn);
        urlAddPollAddEt = findViewById(R.id.url_add_poll_add_et);
        agePollAddEt = findViewById(R.id.age_poll_add_et);
        genderPollAddEt = findViewById(R.id.gender_poll_add_et);
        countryPollAddEt = findViewById(R.id.country_poll_add_et);
        cityPollAddEt = findViewById(R.id.city_poll_add_et);
        itemPollAddEt = findViewById(R.id.item_poll_add_et);
        itemAddPollAddBtn = findViewById(R.id.item_add_poll_add_btn);
        expiryPollAddEt = findViewById(R.id.expiry_poll_add_et);
        multiplePollAddCb = findViewById(R.id.multiple_poll_add_cb);
        submitPollAddBtn = findViewById(R.id.submit_poll_add_btn);
        cancelPollAddBtn = findViewById(R.id.cancel_poll_add_btn);
    }


    @Override
    public void setActionListeners() {
        super.setActionListeners();
        agePollAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(PollAddActivity.this, age, getString(R.string.select_age), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        agePollAddEt.setText(age[id]);
                    }
                });
            }
        });

        genderPollAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(PollAddActivity.this, getResources().getStringArray(R.array.gender_array), getString(R.string.select_an_option), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        genderPollAddEt.setText(getResources().getStringArray(R.array.gender_array)[id]);
                    }
                });
            }
        });

        countryPollAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(PollAddActivity.this, getResources().getStringArray(R.array.country_array), getString(R.string.select_country), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        countryPollAddEt.setText(getResources().getStringArray(R.array.country_array)[id]);
                    }
                });
            }
        });

        cityPollAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(PollAddActivity.this, getResources().getStringArray(R.array.city_array), getString(R.string.select_city), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        cityPollAddEt.setText(getResources().getStringArray(R.array.city_array)[id]);
                    }
                });
            }
        });

        imgAddPollAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(PollAddActivity.this);
            }
        });

       /*  expiryPollAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
               String currentMoth = String.valueOf(android.text.format.DateFormat.format(PollAddActivity.this.getString(R.string.date_format_2), gregorianCalendar));
                SmartUtils.getDateDialog(PollAddActivity.this, currentMoth, false, new CustomClickListener() {
                    @Override
                    public void onClick(String value) {
                        expiryPollAddEt.setText(value);
                    }
                }, PollAddActivity.this.getString(R.string.date_format_2), false);
            }
        });*/
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Bitmap bitmap = null;
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Picasso.with(PollAddActivity.this).load(resultUri).into(imgPollAddIv);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(PollAddActivity.this, String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
