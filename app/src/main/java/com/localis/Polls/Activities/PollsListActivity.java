package com.localis.Polls.Activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.ViewPagerAdapter;
import com.localis.Polls.Fragments.PollListFragment;
import com.localis.Polls.Fragments.PollReportsFragment;
import com.localis.R;

public class PollsListActivity extends MasterActivity {

    private FloatingActionButton addPollBtn;

    @Override
    public int getLayoutID() {
        return R.layout.activity_aa_pager;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.polls);

        ViewPager pollsViewpager = findViewById(R.id.details_vp);
        TabLayout pollsTabs = findViewById(R.id.details_tab);
        addPollBtn = findViewById(R.id.details_add_btn);

        pollsTabs.setTabMode(TabLayout.MODE_FIXED);

        setupViewPager(pollsViewpager);
        pollsTabs.setupWithViewPager(pollsViewpager);

    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();
        addPollBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PollsListActivity.this, PollAddActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PollListFragment(), "All Polls");
        adapter.addFragment(new PollListFragment(), "My Polls");
        adapter.addFragment(new PollReportsFragment(), "Poll Reports");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
    }
}
