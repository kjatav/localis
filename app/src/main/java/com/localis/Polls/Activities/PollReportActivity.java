package com.localis.Polls.Activities;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.Polls.Fragments.PollReportDetailsFragment;
import com.localis.Polls.Fragments.PollReportGraphFragment;
import com.localis.R;

import java.util.ArrayList;
import java.util.List;

public class PollReportActivity extends MasterActivity {
    private ViewPager pollReportViewpager;
    private FloatingActionButton addDetailBtn;


    @Override
    public int getDrawerLayoutID() {
        return 0;
    }


    @Override
    public int getLayoutID() {
        return R.layout.activity_aa_pager;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.poll_report);

        TabLayout pollReportTab = findViewById(R.id.details_tab);
        pollReportViewpager = findViewById(R.id.details_vp);
        addDetailBtn = findViewById(R.id.details_add_btn);

        pollReportTab.setTabMode(TabLayout.MODE_FIXED);
        addDetailBtn.setVisibility(View.GONE);

        setupViewPager(pollReportViewpager);
        pollReportTab.setupWithViewPager(pollReportViewpager);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PollReportDetailsFragment(), "Details");
        adapter.addFragment(new PollReportGraphFragment(), "Graph");
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        private void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);

        }
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}
