package com.localis.Polls.Activities;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.ViewPagerAdapter;
import com.localis.Albums.Activites.AlbumAddActivity;
import com.localis.Albums.Fragments.AlbumFragment;
import com.localis.Events.Activities.EventAddActivity;
import com.localis.Events.Fragments.EventListFragment;
import com.localis.Groups.Activites.GroupAddEditActivity;
import com.localis.Groups.Fragments.GroupAboutFragment;
import com.localis.Groups.Fragments.GroupListFragment;
import com.localis.Groups.Fragments.GroupMembersFragment;
import com.localis.Pages.Activites.PageAddActivity;
import com.localis.Pages.Fragments.PagesListFragment;
import com.localis.Polls.Fragments.PollTimelineFragment;
import com.localis.R;
import com.localis.Videos.Activities.VideoAddActivity;
import com.localis.Videos.Fragments.VideoListFragment;

import static com.smart.framework.SmartUtils.setAddButton;

public class PollDetailActivity extends MasterActivity {

    private FloatingActionButton addPollDetailBtn;
    private ViewPager.OnPageChangeListener onPageChangeListener;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }


    @Override
    public int getLayoutID() {
        return R.layout.activity_aa_pager;
    }


    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.poll_details);

        TabLayout pollReportTab = findViewById(R.id.details_tab);

        ViewPager pollReportViewpager = findViewById(R.id.details_vp);
        addPollDetailBtn = findViewById(R.id.details_add_btn);

        setupViewPager(pollReportViewpager);
        pollReportTab.setupWithViewPager(pollReportViewpager);
        setupOnPageChangeListener();
        pollReportViewpager.addOnPageChangeListener(onPageChangeListener);
    }


    private void setupOnPageChangeListener() {
        onPageChangeListener = new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        addPollDetailBtn.setVisibility(View.GONE);
                        break;
                    case 1:
                        addPollDetailBtn.setVisibility(View.GONE);
                        break;
                    case 2:
                        addPollDetailBtn.setVisibility(View.GONE);
                        break;
                    case 3:
                        setAddButton(PollDetailActivity.this, AlbumAddActivity.class, addPollDetailBtn);
                        break;
                    case 4:
                        setAddButton(PollDetailActivity.this, VideoAddActivity.class, addPollDetailBtn);
                        break;
                    case 5:
                        setAddButton(PollDetailActivity.this, GroupAddEditActivity.class, addPollDetailBtn);
                        break;
                    case 6:
                        setAddButton(PollDetailActivity.this, EventAddActivity.class, addPollDetailBtn);
                        break;
                    case 7:
                        setAddButton(PollDetailActivity.this, PageAddActivity.class, addPollDetailBtn);
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }


    private void setupViewPager(final ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PollTimelineFragment(), "Timeline");
        adapter.addFragment(new GroupAboutFragment(), "About");
        adapter.addFragment(new GroupMembersFragment(), "Friends");
        adapter.addFragment(new AlbumFragment(), "Albums");
        adapter.addFragment(new VideoListFragment(), "Videos");
        adapter.addFragment(new GroupListFragment(), "Groups");
        adapter.addFragment(new EventListFragment(), "Events");
        adapter.addFragment(new PagesListFragment(), "Pages");

        viewPager.setAdapter(adapter);
        viewPager.post(new Runnable() {
            @Override
            public void run() {
                onPageChangeListener.onPageSelected(viewPager.getCurrentItem());
            }
        });
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_menu_top));
    }
}
