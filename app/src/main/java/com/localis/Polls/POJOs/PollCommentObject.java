package com.localis.Polls.POJOs;

/**
 * Created by : Dhiren Parmar
 * Created date : February 27, 2018
 * Purpose of creation :
 * Modified by :
 * Modified date :
 * Purpose of modification :
 */

public class PollCommentObject {
    String comment;

    public PollCommentObject(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
