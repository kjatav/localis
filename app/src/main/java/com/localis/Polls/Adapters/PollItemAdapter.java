package com.localis.Polls.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Polls.Activities.PollDetailActivity;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;

public class PollItemAdapter extends RecyclerView.Adapter<PollItemAdapter.MyViewHolder> {
    private ArrayList<ImageObject> imageObjects;
    private Context context;

    public PollItemAdapter(ArrayList<ImageObject> image_objects, Context context) {
        this.imageObjects = image_objects;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_poll_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PollItemAdapter.MyViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PollDetailActivity.class);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private SmartTextView titlePollItemTv;
        private SmartTextView creatorPollItemTv;
        private SmartTextView votesPollItemTv;
        private SmartTextView typePollItemTv;
        private SmartTextView statusPollItemTv;


        public MyViewHolder(View itemView) {
            super(itemView);
            titlePollItemTv = itemView.findViewById( R.id.title_poll_item_tv );
            creatorPollItemTv = itemView.findViewById( R.id.creator_poll_item_tv );
            votesPollItemTv = itemView.findViewById( R.id.votes_poll_item_tv );
            typePollItemTv = itemView.findViewById( R.id.type_poll_item_tv );
            statusPollItemTv = itemView.findViewById( R.id.status_poll_item_tv );

        }
    }
}
