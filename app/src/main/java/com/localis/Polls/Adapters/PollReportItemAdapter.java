package com.localis.Polls.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.Polls.Activities.PollReportActivity;
import com.localis.R;
import com.smart.customViews.SmartTextView;

public class PollReportItemAdapter extends RecyclerView.Adapter<PollReportItemAdapter.MyViewHolder> {
    Context context;

    public PollReportItemAdapter(Context context) {
        this.context = context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.poll_report_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PollReportActivity.class);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return 4;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private SmartTextView titlePollReportTv;
        private SmartTextView agePollReportTv;
        private SmartTextView genderPollReportTv;
        private SmartTextView locationPollReportTv;
        private SmartTextView votesPollReportTv;
        private SmartTextView createdPollReportTv;
        private SmartTextView idPollReportTv;

        public MyViewHolder(View itemView) {
            super(itemView);
            titlePollReportTv = itemView.findViewById(R.id.title_poll_report_tv);
            agePollReportTv = itemView.findViewById(R.id.age_poll_report_tv);
            genderPollReportTv = itemView.findViewById(R.id.gender_poll_report_tv);
            locationPollReportTv = itemView.findViewById(R.id.location_poll_report_tv);
            votesPollReportTv = itemView.findViewById(R.id.votes_poll_report_tv);
            createdPollReportTv = itemView.findViewById(R.id.created_poll_report_tv);
            idPollReportTv = itemView.findViewById(R.id.id_poll_report_tv);
        }
    }
}
