package com.localis.Polls.Adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;

import com.localis.R;
import com.smart.customViews.SmartTextView;


public class PollTimelineAdapter extends RecyclerView.Adapter<PollTimelineAdapter.MyViewHolder> {

    private Context context;

    public PollTimelineAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_poll_timeline, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable wrapDrawable = DrawableCompat.wrap(holder.votesPollItemPb.getIndeterminateDrawable());
            DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(context, android.R.color.holo_blue_dark));
            holder.votesPollItemPb.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable));
        } else {
            holder.votesPollItemPb.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(context, android.R.color.holo_blue_dark), PorterDuff.Mode.SRC_IN);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    @Override
    public int getItemCount() {
        return 5;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CheckBox namePollItemCb;
        private ProgressBar votesPollItemPb;
        private SmartTextView votesPollItemTv;


        public MyViewHolder(View itemView) {
            super(itemView);
            namePollItemCb = itemView.findViewById(R.id.name_poll_item_cb);
            votesPollItemPb = itemView.findViewById(R.id.votes_poll_item_pb);
            votesPollItemTv = itemView.findViewById(R.id.votes_poll_item_tv);
        }
    }
}
