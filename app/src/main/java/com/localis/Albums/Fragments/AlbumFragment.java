package com.localis.Albums.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Albums.Adapters.AlbumItemAdapter;
import com.localis.R;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;

public class AlbumFragment extends SmartFragment {
    RecyclerView albumRv;
    AlbumItemAdapter albumItemAdapter;
    ArrayList<ImageObject> image_objects = new ArrayList<>();

    @Override
    public int setLayoutId() {
        return R.layout.fragment_album_list;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        albumRv = currentView.findViewById(R.id.album_rv);

        albumRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        albumItemAdapter = new AlbumItemAdapter(image_objects, getActivity());
        albumRv.setAdapter(albumItemAdapter);

        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }

}
