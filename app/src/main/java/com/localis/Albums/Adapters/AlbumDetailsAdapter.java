package com.localis.Albums.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;

public class AlbumDetailsAdapter extends PagerAdapter {

    private ArrayList<Integer> images;
    private LayoutInflater inflater;
    private Context context;
    private ItemClickListener mClickListener;

    public AlbumDetailsAdapter(Context context, ArrayList<Integer> images) {
        this.context = context;
        this.images = images;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, final int position) {
        View myImageLayout = inflater.inflate(R.layout.album_detail_slide, view, false);

        ImageView imgAlbumSlideIv = myImageLayout.findViewById(R.id.img_album_slide_iv);
        SmartTextView detailsAlbumSlideTv = myImageLayout.findViewById(R.id.details_album_slide_tv);
        SmartTextView likesAlbumSlideTv = myImageLayout.findViewById(R.id.likes_album_slide_tv);
        SmartTextView commentsAlbumSlideTv = myImageLayout.findViewById(R.id.comments_album_slide_tv);

        imgAlbumSlideIv.setImageResource(images.get(position));
        myImageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) mClickListener.onItemClick(view, position);
            }
        });
        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }


    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}