package com.localis.Albums.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Albums.Activites.AlbumDetailActivity;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;

public class AlbumItemAdapter extends RecyclerView.Adapter<AlbumItemAdapter.MyViewHolder> {
    private ArrayList<ImageObject> image_objects;
    private Context context;


    public AlbumItemAdapter(ArrayList<ImageObject> image_objects, Context context) {
        this.image_objects = image_objects;
        this.context = context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_album, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumItemAdapter.MyViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AlbumDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    @Override
    public int getItemCount() {
        return 3;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private SmartTextView descAlbumItemTv;
        private ImageView imgAlbumItemIv;
        private SmartTextView titleAlbumItemIv;
        private SmartTextView nameAlbumItemIv;
        private SmartTextView likesAlbumItemIv;
        private SmartTextView commentsAlbumItemIv;


        public MyViewHolder(View itemView) {
            super(itemView);
            descAlbumItemTv = itemView.findViewById(R.id.desc_album_item_tv);
            imgAlbumItemIv = itemView.findViewById(R.id.img_album_item_iv);
            titleAlbumItemIv = itemView.findViewById(R.id.title_album_item_iv);
            nameAlbumItemIv = itemView.findViewById(R.id.name_album_item_iv);
            likesAlbumItemIv = itemView.findViewById(R.id.likes_album_item_iv);
            commentsAlbumItemIv = itemView.findViewById(R.id.comments_album_item_iv);
        }
    }
}
