package com.localis.Albums.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localis.AA_POJOs.ImageObject;
import com.localis.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AddAlbumAdapter extends RecyclerView.Adapter<AddAlbumAdapter.MyViewHolder> {
    private ArrayList<ImageObject> imageArrayList;
    private Context context;
    private ItemClickListener mClickListener;

    public AddAlbumAdapter(ArrayList<ImageObject> imageArrayList, Context context) {
        this.imageArrayList = imageArrayList;
        this.context = context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_add_album, parent, false)); // pass the view to View Holder
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        if (imageArrayList.size() == 0) {
            Picasso.with(context).load(R.drawable.btn_plus_red).into(holder.addImageIv);
        } else {
            if (position == 0) {
                Picasso.with(context).load(R.drawable.btn_plus_red).into(holder.addImageIv);
                holder.closeAddImageIv.setVisibility(View.GONE);
            } else {
                holder.closeAddImageIv.setVisibility(View.VISIBLE);
                Picasso.with(context).load(String.valueOf(imageArrayList.get(position).getGroupImages())).into(holder.addImageIv);
            }
        }
    }


    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView addImageIv;
        private ImageView closeAddImageIv;


        public MyViewHolder(View itemView) {
            super(itemView);
            addImageIv = itemView.findViewById(R.id.add_image_iv);
            closeAddImageIv = itemView.findViewById(R.id.close_add_image_iv);

            itemView.setOnClickListener(this);
            closeAddImageIv.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }


    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

