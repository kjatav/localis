package com.localis.Albums.Activites;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.CommentAdapter;
import com.localis.AA_POJOs.ImageObject;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;

public class ImageDetailsActivity extends MasterActivity {
    private SmartTextView descImageDetailsTv;
    private ImageView imgImageDetailsIv;
    private SmartTextView likesImageDetailsTv;
    private SmartTextView commentsImageDetailsTv;
    private RecyclerView commentsImageDetailsRv;


    private CommentAdapter commentAdapter;
    private ArrayList<ImageObject> image_objects = new ArrayList<>();

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_image_details;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.image_details);

        descImageDetailsTv = findViewById(R.id.desc_image_details_tv);
        imgImageDetailsIv = findViewById(R.id.img_image_details_iv);
        likesImageDetailsTv = findViewById(R.id.likes_image_details_tv);
        commentsImageDetailsTv = findViewById(R.id.comments_image_details_tv);
        commentsImageDetailsRv = findViewById(R.id.comments_image_details_rv);


        commentsImageDetailsRv.setLayoutManager(new LinearLayoutManager(this));
        commentAdapter = new CommentAdapter(image_objects, this);
        commentsImageDetailsRv.setAdapter(commentAdapter);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}
