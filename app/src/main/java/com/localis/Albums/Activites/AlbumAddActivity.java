package com.localis.Albums.Activites;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_POJOs.ImageObject;
import com.localis.Albums.Adapters.AddAlbumAdapter;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;

public class AlbumAddActivity extends MasterActivity implements AddAlbumAdapter.ItemClickListener {


    private SmartEditText titleAlbumAddEt;
    private ImageView visibilityAlbumAddIv;
    private SmartEditText descAlbumAddEt;
    private SmartEditText locationAlbumAddEt;
    private RecyclerView imgsAlbumAddRv;
    private SmartButton saveAddAlbumBtn;
    private SmartButton cancelAddAlbumBtn;


    AddAlbumAdapter addAlbumAdapter;

    ArrayList<ImageObject> imageObjects = new ArrayList<>();

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_album_add;
    }


    @Override
    public void initComponents() {
        super.initComponents();

        setHeaderToolbar(R.string.add_alubm);

        titleAlbumAddEt = findViewById(R.id.title_album_add_et);
        visibilityAlbumAddIv = findViewById(R.id.visibility_album_add_iv);
        descAlbumAddEt = findViewById(R.id.desc_album_add_et);
        locationAlbumAddEt = findViewById(R.id.location_album_add_et);
        imgsAlbumAddRv = findViewById(R.id.imgs_album_add_rv);
        saveAddAlbumBtn = findViewById(R.id.save_add_album_btn);
        cancelAddAlbumBtn = findViewById(R.id.cancel_add_album_btn);


        String imageUrl = getURLForResource(R.drawable.btn_plus_red);
        imageObjects.add(new ImageObject(imageUrl));

        addAlbumAdapter = new AddAlbumAdapter(imageObjects, AlbumAddActivity.this);
        imgsAlbumAddRv.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3, LinearLayoutManager.VERTICAL, false));
        imgsAlbumAddRv.setAdapter(addAlbumAdapter);
        addAlbumAdapter.setClickListener(this);

    }

    public String getURLForResource(int resourceId) {
        return Uri.parse("android.resource://" + R.class.getPackage().getName() + "/" + resourceId).toString();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                imageObjects.add(new ImageObject(String.valueOf(resultUri)));
                addAlbumAdapter.notifyDataSetChanged();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Log.e("CROP_IMAGE_ERROR", result.getError().toString());
            }
        }
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        if (position == 0) {
            CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                    .start(AlbumAddActivity.this);
        } else {
            switch (view.getId()) {
                case R.id.close_add_image_iv:
                    imageObjects.remove(position);
                    addAlbumAdapter.notifyDataSetChanged();
                    break;
            }

        }
    }
}