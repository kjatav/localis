package com.localis.Albums.Activites;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.CommentAdapter;
import com.localis.AA_POJOs.ImageObject;
import com.localis.Albums.Adapters.AlbumDetailsAdapter;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;
import java.util.Arrays;

public class AlbumDetailActivity extends MasterActivity {

    private NestedScrollView albumDetailScroll;
    private SmartTextView descAlbumDetailsTv;
    private ViewPager albumDetailsVp;
    private SmartTextView nameAlbumDetailsTv;
    private SmartTextView likesAlbumDetailsTv;
    private SmartTextView commentsAlbumDetailsTv;
    private RecyclerView commentsAlbumDetailsRv;


    private CommentAdapter commentAdapter;

    private AlbumDetailsAdapter albumDetailsAdapter;

    ArrayList<ImageObject> image_objects = new ArrayList<>();
    private static final Integer[] slideImage = {R.drawable.bg_event, R.drawable.bg_event, R.drawable.bg_event, R.drawable.bg_event, R.drawable.bg_event};
    private ArrayList<Integer> slideArry = new ArrayList<Integer>();


    @Override
    public int getDrawerLayoutID() {
        return 0;
    }


    @Override
    public int getLayoutID() {
        return R.layout.activity_album_details;
    }


    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.album_details);

        albumDetailScroll = findViewById(R.id.album_detail_scroll);
        descAlbumDetailsTv = findViewById(R.id.desc_album_details_tv);
        albumDetailsVp = findViewById(R.id.album_details_vp);
        nameAlbumDetailsTv = findViewById(R.id.name_album_details_tv);
        likesAlbumDetailsTv = findViewById(R.id.likes_album_details_tv);
        commentsAlbumDetailsTv = findViewById(R.id.comments_album_details_tv);
        commentsAlbumDetailsRv = findViewById(R.id.comments_album_details_rv);


        slideArry.addAll(Arrays.asList(slideImage));

        albumDetailsAdapter = new AlbumDetailsAdapter(this, slideArry);
        albumDetailsVp.setAdapter(albumDetailsAdapter);

        commentsAlbumDetailsRv.setLayoutManager(new LinearLayoutManager(this));
        commentAdapter = new CommentAdapter(image_objects, this);
        commentsAlbumDetailsRv.setAdapter(commentAdapter);
    }


    @Override
    public void setActionListeners() {
        super.setActionListeners();
        albumDetailsAdapter.setClickListener(new AlbumDetailsAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(AlbumDetailActivity.this, ImageDetailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}

