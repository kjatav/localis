package com.localis.Profile.Fragments;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Dashboard.Adapter.DashboardAdapter;
import com.localis.Dashboard.POJOs.FeedItemObject;
import com.localis.Dashboard.POJOs.SmileyObject;
import com.localis.Dashboard.POJOs.TagFriendsObject;
import com.localis.Profile.Activities.AccountSettingActivity;
import com.localis.Profile.ProfileApiCalls;
import com.localis.Profile.ProfileImpl;
import com.localis.R;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartTextView;
import com.smart.framework.SmartFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.smart.framework.SmartUtils.getUserId;
import static com.smart.framework.SmartUtils.switchTabsColor;


public class ProfileDetailsFragment extends SmartFragment implements ProfileImpl.GetBasicUserInfo {
    private TabLayout profileTabs;


    private ImageView imgProfileDetailIv;
    private ImageView optionsProfileDetailIv;
    private RoundedImageView profileProfileDetailIv;
    private SmartTextView nameProfileDetailTv;
    private SmartTextView typeProfileDetailTv;
    private SmartTextView genderProfileDetailTv;
    private SmartTextView pointsProfileDetailTv;
    private SmartTextView infoProfileDetailTv;
    private RecyclerView profileDetailsRv;


    private ArrayList<ImageObject> image_objects = new ArrayList<>();
    private ArrayList<FeedItemObject> feedItem = new ArrayList<>();

    private DashboardAdapter dashboardAdapter;

    private List<TagFriendsObject> taglist = new ArrayList<>();
    private SmileyObject smileyObject;

    private SmartTextView statusTabTv;
    private SmartTextView photosTabTv;
    private SmartTextView videoTabTv;
    private SmartTextView eventsTabTv;
    private SmartTextView pollsTabTv;

    private View statusLayout;
    private View photosLayout;
    private View videoLayout;
    private View eventsLayout;
    private View pollsLayout;


    @Override
    public int setLayoutId() {
        return R.layout.fragment_profile_details;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {

        imgProfileDetailIv = currentView.findViewById(R.id.img_profile_detail_iv);
        optionsProfileDetailIv = currentView.findViewById(R.id.options_profile_detail_iv);
        profileProfileDetailIv = currentView.findViewById(R.id.profile_profile_detail_iv);
        nameProfileDetailTv = currentView.findViewById(R.id.name_profile_detail_tv);
        typeProfileDetailTv = currentView.findViewById(R.id.type_profile_detail_tv);
        genderProfileDetailTv = currentView.findViewById(R.id.gender_profile_detail_tv);
        pointsProfileDetailTv = currentView.findViewById(R.id.points_profile_detail_tv);
        infoProfileDetailTv = currentView.findViewById(R.id.info_profile_detail_tv);
        profileDetailsRv = currentView.findViewById(R.id.profile_details_rv);


        profileTabs = currentView.findViewById(R.id.post_timeline_tabs);
        for (int i = 0; i < 5; i++) {
            profileTabs.addTab(profileTabs.newTab());
        }
        profileTabs.setTabMode(TabLayout.MODE_FIXED);

        statusLayout = currentView.findViewById(R.id.status_layout);
        photosLayout = currentView.findViewById(R.id.photos_layout);
        videoLayout = currentView.findViewById(R.id.video_layout);
        eventsLayout = currentView.findViewById(R.id.events_layout);
        pollsLayout = currentView.findViewById(R.id.polls_layout);

        profileDetailsRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        dashboardAdapter = new DashboardAdapter(feedItem, image_objects, getContext());
        profileDetailsRv.setAdapter(dashboardAdapter);

        createPostTabIcons();

        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));
        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));
        feedItem.add(new FeedItemObject(STATUS_CODE));
        feedItem.add(new FeedItemObject(IMAGE_CODE));
        feedItem.add(new FeedItemObject(VIDEO_CODE));

        ProfileApiCalls.getBasicUserInfo(getActivity(), getUserId(), this);

        return currentView;

    }

    private void createPostTabIcons() {
        statusTabTv = (SmartTextView) LayoutInflater.from(getContext()).inflate(R.layout.custom_tab, null);
        statusTabTv.setText(R.string.status);
        statusTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_status, 0, 0);
        profileTabs.getTabAt(0).setCustomView(statusTabTv);

        photosTabTv = (SmartTextView) LayoutInflater.from(getContext()).inflate(R.layout.custom_tab, null);
        photosTabTv.setText(R.string.photos);
        photosTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_photo, 0, 0);
        profileTabs.getTabAt(1).setCustomView(photosTabTv);

        videoTabTv = (SmartTextView) LayoutInflater.from(getContext()).inflate(R.layout.custom_tab, null);
        videoTabTv.setText(R.string.video);
        videoTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_video, 0, 0);
        profileTabs.getTabAt(2).setCustomView(videoTabTv);

        eventsTabTv = (SmartTextView) LayoutInflater.from(getContext()).inflate(R.layout.custom_tab, null);
        eventsTabTv.setText(R.string.events);
        eventsTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_event, 0, 0);
        profileTabs.getTabAt(3).setCustomView(eventsTabTv);

        pollsTabTv = (SmartTextView) LayoutInflater.from(getContext()).inflate(R.layout.custom_tab, null);
        pollsTabTv.setText(R.string.polls);
        pollsTabTv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subheader_poll, 0, 0);
        profileTabs.getTabAt(4).setCustomView(pollsTabTv);


        profileTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        switchTabsColor(getContext(), statusTabTv, R.drawable.ic_subheader_status_hover, true);
                        hideAndShow(statusLayout);
                        break;
                    case 1:
                        switchTabsColor(getContext(), photosTabTv, R.drawable.ic_subheader_photo_hover, true);
                        hideAndShow(photosLayout);
                        break;
                    case 2:
                        switchTabsColor(getContext(), videoTabTv, R.drawable.ic_subheader_video_hover, true);
                        hideAndShow(videoLayout);
                        break;
                    case 3:
                        switchTabsColor(getContext(), eventsTabTv, R.drawable.ic_subheader_event_hover, true);
                        hideAndShow(eventsLayout);
                        break;
                    case 4:
                        switchTabsColor(getContext(), pollsTabTv, R.drawable.ic_subheader_poll_hover, true);
                        hideAndShow(pollsLayout);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        switchTabsColor(getContext(), statusTabTv, R.drawable.ic_subheader_status, false);
                        break;
                    case 1:
                        switchTabsColor(getContext(), photosTabTv, R.drawable.ic_subheader_photo, false);
                        break;
                    case 2:
                        switchTabsColor(getContext(), videoTabTv, R.drawable.ic_subheader_video, false);
                        break;
                    case 3:
                        switchTabsColor(getContext(), eventsTabTv, R.drawable.ic_subheader_event, false);
                        break;
                    case 4:
                        switchTabsColor(getContext(), pollsTabTv, R.drawable.ic_subheader_poll, false);
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        switchTabsColor(getContext(), statusTabTv, R.drawable.ic_subheader_status_hover, true);

    }

    private void setDetails(JSONObject data) {
        try {
            nameProfileDetailTv.setText(data.getString("first_name") + "" + data.getString("last_name"));
            if (!TextUtils.isEmpty(data.getString("profile_image"))) {
                Picasso.with(getActivity()).load(data.getString("profile_image")).placeholder(R.drawable.ic_user).into(profileProfileDetailIv);
            }
            if (!TextUtils.isEmpty(data.getString("cover_image"))) {
                Picasso.with(getActivity()).load(data.getString("cover_image")).placeholder(R.drawable.bg_profile).into(imgProfileDetailIv);
            }
            typeProfileDetailTv.setText(data.getString("user_type"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void hideAndShow(View view) {
        statusLayout.setVisibility(View.GONE);
        photosLayout.setVisibility(View.GONE);
        videoLayout.setVisibility(View.GONE);
        eventsLayout.setVisibility(View.GONE);
        pollsLayout.setVisibility(View.GONE);

        view.setVisibility(View.VISIBLE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
     /*   if (requestCode == TAG_FRIEND_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                taglist = (List<TagFriendsObject>) data.getSerializableExtra(TAGGED_FRIENDS_DATA);
                statusFragment.setFriendList(taglist);
            }
        } else if (requestCode == SMILEY_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                smileyObject = (SmileyObject) data.getSerializableExtra(SMILEY_DATA);
                if (smileyObject != null) {
                    statusFragment.setSmiley(smileyObject);
                }
            }
        }*/
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        optionsProfileDetailIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AccountSettingActivity.class));
            }
        });
    }

    @Override
    public void onBasicUserInfoReceived(JSONObject response) {
        setDetails(response);
    }

    @Override
    public void onBasicUserInfoFailed(String responseMessage) {

    }
}