package com.localis.Profile.Fragments;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import com.localis.Albums.Adapters.AddAlbumAdapter;
import com.localis.Chat.Activities.ChatActivity;
import com.localis.Profile.Activities.ProfileActivity;
import com.localis.Profile.Activities.ProfileFriendAddActivity;
import com.localis.Profile.Adapters.FriendItemAdapter;
import com.localis.Profile.Adapters.FriendItemListAdapter;
import com.localis.Profile.POJOs.FriendObject;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartFragment;
import com.smart.framework.SmartRecyclerView;

import java.util.ArrayList;
import java.util.List;

import static com.smart.framework.SmartUtils.showSortingPopup;

/**
 * Created by pct1 on 15/3/18.
 */

public class ProfileFriendFragment extends SmartFragment implements AddAlbumAdapter.ItemClickListener {
    private SmartRecyclerView rvFriend;
    FriendItemAdapter friendItemAdapter;
    FriendItemListAdapter friendItemListAdapter;
    private LinearLayoutManager linearLayoutManager;
    private List<FriendObject> friendObjects = new ArrayList<>();
    String strTypeFriend = "All friends";
    Intent intent;

    Dialog blockUser;

    @Override
    public int setLayoutId() {
        return R.layout.fragment_profile_friend;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        rvFriend = currentView.findViewById(R.id.rv_friend);

        linearLayoutManager = new LinearLayoutManager(getContext());
        rvFriend.setLayoutManager(linearLayoutManager);

        for (int i = 0; i < 10; i++) {
            friendObjects.add(new FriendObject(i, "john Doe + " + i));
        }

        friendItemAdapter = new FriendItemAdapter(getActivity(), friendObjects);
        rvFriend.setAdapter(friendItemAdapter);

        rvFriend.setEmptyView(currentView.findViewById(R.id.layout_no_data));
        friendItemAdapter.setClickListener(this);

        setHasOptionsMenu(true);
        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

    }

    @Override
    public void onItemClick(View view, int position) {
        ImageView imageView = view.findViewById(R.id.iv_red_dot_friend);

        switch (view.getId()) {
            case R.id.ll_main:
                intent = new Intent(getContext(), ProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;

            case R.id.iv_red_dot_friend:
                if (strTypeFriend.equalsIgnoreCase(getString(R.string.all_friends))) {
                    showSortingPopup(getActivity(), imageView, getResources().getStringArray(R.array.all_friend), new AlertMagneticClass.PopUpInterface() {
                        @Override
                        public void onSetPopUp(int selectedOptionPosition) {
                            switch (selectedOptionPosition) {
                                case 0:
                                    showSendEmailDialog(getString(R.string.block_user_title), getString(R.string.blocking_msg), getString(R.string.et_hint_blockUSer));
                                    break;
                                case 1:
                                    showSendEmailDialog(getString(R.string.report_user_title), getString(R.string.report_msg), getString(R.string.et_hint_reportUSer));
                                    break;
                                case 2:
                                    showSendEmailDialog(getString(R.string.ban_user_title), getString(R.string.ban_msg), getString(R.string.et_hint_banUSer));
                                    break;
                                case 3:
                                    showSendEmailDialog(getString(R.string.unfriend), getString(R.string.unfriend_msg), "");
                                    break;
                            }
                        }
                    });
                } else if (strTypeFriend.equalsIgnoreCase(getString(R.string.suggested_friends))) {
                    showSortingPopup(getActivity(), imageView, getResources().getStringArray(R.array.all_friend), new AlertMagneticClass.PopUpInterface() {
                        @Override
                        public void onSetPopUp(int selectedOptionPosition) {
                            switch (selectedOptionPosition) {
                                case 0:
                                    showSendEmailDialog(getString(R.string.block_user_title), getString(R.string.blocking_msg), getString(R.string.et_hint_blockUSer));
                                    break;
                                case 1:
                                    showSendEmailDialog(getString(R.string.report_user_title), getString(R.string.report_msg), getString(R.string.et_hint_reportUSer));
                                    break;
                                case 2:
                                    showSendEmailDialog(getString(R.string.ban_user_title), getString(R.string.ban_msg), getString(R.string.et_hint_banUSer));
                                    break;
                                case 3:
                                    showSendEmailDialog(getString(R.string.unfriend), getString(R.string.unfriend_msg), "");
                                    break;
                            }
                        }
                    });
                } else if (strTypeFriend.equalsIgnoreCase(getString(R.string.wait_for_approval))) {
                    showSortingPopup(getActivity(), imageView, getResources().getStringArray(R.array.wait_approval), new AlertMagneticClass.PopUpInterface() {
                        @Override
                        public void onSetPopUp(int selectedOptionPosition) {
                            switch (selectedOptionPosition) {
                                case 0:

                                    break;
                            }
                        }
                    });
                } else if (strTypeFriend.equalsIgnoreCase(getString(R.string.your_requests))) {
                    showSortingPopup(getActivity(), imageView, getResources().getStringArray(R.array.your_request), new AlertMagneticClass.PopUpInterface() {
                        @Override
                        public void onSetPopUp(int selectedOptionPosition) {
                            switch (selectedOptionPosition) {
                                case 2:
                                    showSendEmailDialog(getString(R.string.block_user_title), getString(R.string.blocking_msg), getString(R.string.et_hint_blockUSer));
                                    break;
                                case 3:
                                    showSendEmailDialog(getString(R.string.report_user_title), getString(R.string.report_msg), getString(R.string.et_hint_reportUSer));
                                    break;
                                case 4:
                                    showSendEmailDialog(getString(R.string.ban_user_title), getString(R.string.ban_msg), getString(R.string.et_hint_banUSer));
                                    break;
                            }
                        }
                    });
                } else if (strTypeFriend.equalsIgnoreCase(getString(R.string.your_list))) {
                    showSortingPopup(getActivity(), imageView, getResources().getStringArray(R.array.Your_list), new AlertMagneticClass.PopUpInterface() {
                        @Override
                        public void onSetPopUp(int selectedOptionPosition) {
                            switch (selectedOptionPosition) {
                                case 0:
                                    intent = new Intent(getContext(), ProfileFriendAddActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    break;
                                case 1:
                                    intent = new Intent(getContext(), ProfileFriendAddActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    break;
                                case 2:
                                    showSendEmailDialog(getString(R.string.delete_list), getString(R.string.delete_list_msg), "");
                                    break;
                            }
                        }
                    });
                }
                break;
            case R.id.txt_friend_msg:
                intent = new Intent(getContext(), ChatActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case R.id.txt_member_msg:
//                intent = new Intent(getContext(), .class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
                break;

        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.friend_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        strTypeFriend = " ";
        //categoryVideoTv.setText(item.getTitle());
        switch (item.getItemId()) {
            case R.id.all_friends:
                strTypeFriend = item.getTitle().toString();
                friendObjects.clear();
                friendItemAdapter = new FriendItemAdapter(getActivity(), friendObjects);
                rvFriend.setAdapter(friendItemAdapter);
                for (int i = 0; i < 10; i++) {
                    friendObjects.add(new FriendObject(i, "john Doe + " + i));
                }
                friendItemAdapter.notifyDataSetChanged();
                friendItemAdapter.setClickListener(this);
                break;
            case R.id.suggested_friends:
                strTypeFriend = item.getTitle().toString();
                friendObjects.clear();
                friendItemAdapter = new FriendItemAdapter(getActivity(), friendObjects);
                rvFriend.setAdapter(friendItemAdapter);


                for (int i = 0; i < 10; i++) {
                    friendObjects.add(new FriendObject(i, "john Doe"));
                }
                friendItemAdapter.notifyDataSetChanged();
                friendItemAdapter.setClickListener(this);
                break;
            case R.id.wait_for_approval:
                strTypeFriend = item.getTitle().toString();
                friendObjects.clear();
                friendItemAdapter = new FriendItemAdapter(getActivity(), friendObjects);
                rvFriend.setAdapter(friendItemAdapter);


                for (int i = 0; i < 10; i++) {
                    friendObjects.add(new FriendObject(i, "john Doe"));
                }
                friendItemAdapter.notifyDataSetChanged();
                friendItemAdapter.setClickListener(this);
                break;
            case R.id.your_requests:
                strTypeFriend = item.getTitle().toString();

                friendItemAdapter = new FriendItemAdapter(getActivity(), friendObjects);
                rvFriend.setAdapter(friendItemAdapter);


                for (int i = 0; i < 10; i++) {
                    friendObjects.add(new FriendObject(i, "john Doe"));
                }
                friendItemAdapter.notifyDataSetChanged();
                friendItemAdapter.setClickListener(this);
                break;
            case R.id.your_list:
                strTypeFriend = item.getTitle().toString();

                friendItemListAdapter = new FriendItemListAdapter(getActivity(), friendObjects);
                rvFriend.setAdapter(friendItemListAdapter);
                friendItemListAdapter.setClickListener(this);

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSendEmailDialog(String title, String msg, String hint) {

        blockUser = new Dialog(getContext());
        blockUser.setContentView(R.layout.dialog_friend);

        SmartTextView txtTitle = blockUser.findViewById(R.id.dialog_title);
        SmartTextView txtMsg = blockUser.findViewById(R.id.dialog_msg);
        SmartEditText et_description_dialog = blockUser.findViewById(R.id.et_description_dialog);

        txtTitle.setText(title);
        txtMsg.setText(msg);
        if (hint.equalsIgnoreCase("")) {
            et_description_dialog.setVisibility(View.GONE);
        } else {
            et_description_dialog.setVisibility(View.VISIBLE);
            et_description_dialog.setHint(hint);
        }

        SmartButton getEmailBtn = blockUser.findViewById(R.id.btn_positive_dialog);
        SmartButton cancelGetEmailBtn = blockUser.findViewById(R.id.btn_negative_dialog);
        ImageView crossGetEmailIv = blockUser.findViewById(R.id.iv_cross_dialog);

        getEmailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blockUser.dismiss();

            }
        });

        cancelGetEmailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blockUser.dismiss();
            }
        });

        crossGetEmailIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blockUser.dismiss();
            }
        });

        blockUser.show();
    }
}
