package com.localis.Profile.POJOs;

import java.io.Serializable;

public class FriendObject implements Serializable {
    private int id;
    private String friendName;


    public FriendObject(int id, String friendName) {
        this.id = id;
        this.friendName = friendName;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String memberName) {
        this.friendName = memberName;
    }


}