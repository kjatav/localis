package com.localis.Profile.Activities;

import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.smart.customViews.CustomClickListener;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartUtils;

import java.util.GregorianCalendar;

import static com.smart.framework.SmartUtils.GetListDialog;

public class AccountSettingActivity extends MasterActivity {


    private RoundedImageView profileAccIv;
    private SmartTextView editAccTv;
    private SmartEditText firstNameAccEt;
    private SmartEditText lastNameAccEt;
    private SmartEditText userNameAccEt;
    private SmartEditText passwordAccEt;
    private SmartEditText confirmPasswordAccEt;
    private SmartEditText cnpAccEt;
    private RadioGroup radiogroupAcc;
    private RadioButton maleAccRb;
    private RadioButton femaleAccRb;
    private SmartEditText birthdayAccEt;
    private SmartEditText stateAccEt;
    private ImageView stateDropdownIv;
    private SmartEditText cityAccEt;
    private ImageView cityDropdownIv;
    private SmartEditText streetAccEt;
    private ImageView streetDropdownIv;
    private SmartButton updateAccBtn;
    private SmartButton cancelAccBtn;


    @Override
    public int getLayoutID() {
        return R.layout.activity_account_setting;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.account_settings);


        profileAccIv = findViewById(R.id.profile_acc_iv);
        editAccTv = findViewById(R.id.edit_acc_tv);
        firstNameAccEt = findViewById(R.id.first_name_acc_et);
        lastNameAccEt = findViewById(R.id.last_name_acc_et);
        userNameAccEt = findViewById(R.id.user_name_acc_et);
        passwordAccEt = findViewById(R.id.password_acc_et);
        confirmPasswordAccEt = findViewById(R.id.confirm_password_acc_et);
        cnpAccEt = findViewById(R.id.cnp_acc_et);
        radiogroupAcc = findViewById(R.id.radiogroup_acc);
        maleAccRb = findViewById(R.id.male_acc_rb);
        femaleAccRb = findViewById(R.id.female_acc_rb);
        birthdayAccEt = findViewById(R.id.birthday_acc_et);
        stateAccEt = findViewById(R.id.state_acc_et);
        stateDropdownIv = findViewById(R.id.state_dropdown_iv);
        cityAccEt = findViewById(R.id.city_acc_et);
        cityDropdownIv = findViewById(R.id.city_dropdown_iv);
        streetAccEt = findViewById(R.id.street_acc_et);
        streetDropdownIv = findViewById(R.id.street_dropdown_iv);
        updateAccBtn = findViewById(R.id.update_acc_btn);
        cancelAccBtn = findViewById(R.id.cancel_acc_btn);
    }


    @Override
    public void setActionListeners() {
        super.setActionListeners();
        stateAccEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(AccountSettingActivity.this, getResources().getStringArray(R.array.state_array), getString(R.string.choose_your_state), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        stateAccEt.setText(getResources().getStringArray(R.array.state_array)[id]);
                    }
                });
            }
        });

        cityAccEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(AccountSettingActivity.this, getResources().getStringArray(R.array.city_array), getString(R.string.choose_your_city), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        cityAccEt.setText(getResources().getStringArray(R.array.city_array)[id]);

                    }
                });
            }
        });

        streetAccEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetListDialog(AccountSettingActivity.this, getResources().getStringArray(R.array.street_array), getString(R.string.choose_your_street), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        streetAccEt.setText(getResources().getStringArray(R.array.street_array)[id]);
                    }
                });
            }
        });

        birthdayAccEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
                String currentMoth = String.valueOf(android.text.format.DateFormat.format(getString(R.string.date_format_2), gregorianCalendar));
                SmartUtils.getDateDialog(AccountSettingActivity.this, currentMoth, false, new CustomClickListener() {
                    @Override
                    public void onClick(String value) {
                        birthdayAccEt.setText(value);
                    }
                }, getString(R.string.date_format_2), false);*/
            }
        });
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
