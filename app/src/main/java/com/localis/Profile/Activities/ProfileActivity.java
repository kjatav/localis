package com.localis.Profile.Activities;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.ViewPagerAdapter;
import com.localis.Albums.Activites.AlbumAddActivity;
import com.localis.Albums.Fragments.AlbumFragment;
import com.localis.Events.Activities.EventAddActivity;
import com.localis.Events.Fragments.EventListFragment;
import com.localis.Groups.Activites.GroupAddEditActivity;
import com.localis.Groups.Fragments.GroupListFragment;
import com.localis.Pages.Activites.PageAddActivity;
import com.localis.Pages.Fragments.PagesListFragment;
import com.localis.Profile.Fragments.ProfileDetailsFragment;
import com.localis.Profile.Fragments.ProfileFriendFragment;
import com.localis.R;
import com.localis.Videos.Activities.VideoAddActivity;
import com.localis.Videos.Fragments.VideoListFragment;

import static com.smart.framework.SmartUtils.setAddButton;

public class ProfileActivity extends MasterActivity {

    private FloatingActionButton addProfileBtn;
    private ViewPager.OnPageChangeListener onPageChangeListener;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }


    @Override
    public int getLayoutID() {
        return R.layout.activity_profile;
    }


    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.profile);

        TabLayout profileTabs = findViewById(R.id.profile_tabs);

        ViewPager profileVp = findViewById(R.id.profile_viewpager);

        addProfileBtn = findViewById(R.id.add_profile_btn);

        setupViewPager(profileVp);
        profileTabs.setupWithViewPager(profileVp);
        setupOnPageChangeListener();
        profileVp.addOnPageChangeListener(onPageChangeListener);
    }

    private void setupOnPageChangeListener() {
        onPageChangeListener = new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        addProfileBtn.setVisibility(View.GONE);
                        break;
                    case 1:
                        setAddButton(ProfileActivity.this, AlbumAddActivity.class, addProfileBtn);
                        break;
                    case 2:
                        setAddButton(ProfileActivity.this, VideoAddActivity.class, addProfileBtn);
                        break;
                    case 3:
                        addProfileBtn.setVisibility(View.GONE);
                        break;
                    case 4:
                        setAddButton(ProfileActivity.this, GroupAddEditActivity.class, addProfileBtn);
                        break;
                    case 5:
                        setAddButton(ProfileActivity.this, EventAddActivity.class, addProfileBtn);
                        break;
                    case 6:
                        setAddButton(ProfileActivity.this, PageAddActivity.class, addProfileBtn);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ProfileDetailsFragment(), "Details");
        adapter.addFragment(new AlbumFragment(), "Albums");
        adapter.addFragment(new VideoListFragment(), "Videos");
        adapter.addFragment(new ProfileFriendFragment(), "Friends");
        adapter.addFragment(new GroupListFragment(), "Groups");
        adapter.addFragment(new EventListFragment(), "Events");
        adapter.addFragment(new PagesListFragment(), "Pages");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_menu_top));
    }
}
