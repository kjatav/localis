package com.localis.Profile;

import android.content.Context;

import com.android.volley.Request;
import com.localis.R;
import com.smart.framework.SmartUtils;
import com.smart.weservice.SmartWebManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.smart.framework.Constants.DATA;

public class ProfileApiCalls {


    public static void getBasicUserInfo(final Context context, String userId, final ProfileImpl.GetBasicUserInfo getBasicUserInfo) {

        SmartUtils.showLoadingDialog(context);

        Map<String, String> params = new HashMap<>();
        params.put("userid", userId);

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();

        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.REQUEST_METHOD, Request.Method.POST);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, context.getString(R.string.domain_user) + "getbasicuserinfo");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, context);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    getBasicUserInfo.onBasicUserInfoReceived(results.getJSONObject(DATA));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseError(JSONObject results) {
                SmartUtils.hideLoadingDialog();
                try {
                    getBasicUserInfo.onBasicUserInfoFailed(results.getString(DATA));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        SmartWebManager.getInstance(context).addToRequestQueue(requestParams, false);
    }

}
