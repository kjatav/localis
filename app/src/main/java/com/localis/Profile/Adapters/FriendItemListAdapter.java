package com.localis.Profile.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.localis.Albums.Adapters.AddAlbumAdapter;
import com.localis.Groups.POJOs.GroupMemberObject;
import com.localis.Profile.POJOs.FriendObject;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.List;

public class FriendItemListAdapter extends RecyclerView.Adapter<FriendItemListAdapter.MyViewHolder> {
    private Context context;
    private List<FriendObject> groupMemberObjects;
    private AddAlbumAdapter.ItemClickListener mClickListener;

    public FriendItemListAdapter(Context context, List<FriendObject> tagFriendsObjects) {
        this.context = context;
        this.groupMemberObjects = tagFriendsObjects;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_friend_item_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        //return groupMemberObjects.size();
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout llMain;
        ImageView ivRedDot;
        SmartTextView txtMemberMsg;

        public MyViewHolder(View itemView) {
            super(itemView);
            llMain = itemView.findViewById(R.id.ll_main);
            ivRedDot = itemView.findViewById(R.id.iv_red_dot_friend);
            txtMemberMsg=itemView.findViewById(R.id.txt_member_msg);
            ivRedDot.setOnClickListener(this);
            llMain.setOnClickListener(this);
            txtMemberMsg.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    // allows clicks events to be caught
    public void setClickListener(AddAlbumAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}