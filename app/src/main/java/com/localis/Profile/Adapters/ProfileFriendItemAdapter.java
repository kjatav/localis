package com.localis.Profile.Adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.Groups.POJOs.GroupMemberObject;
import com.localis.R;

import java.util.List;

public class ProfileFriendItemAdapter extends RecyclerView.Adapter<ProfileFriendItemAdapter.MyViewHolder> {
    private Context context;
    private List<GroupMemberObject> groupMemberObjects;

    public ProfileFriendItemAdapter(Context context, List<GroupMemberObject> tagFriendsObjects) {
        this.context = context;
        this.groupMemberObjects = tagFriendsObjects;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_friend_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.chk_friend_row.isChecked()) {
                    holder.chk_friend_row.setChecked(false);
                } else {
                    holder.chk_friend_row.setChecked(true);
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        //return groupMemberObjects.size();
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        AppCompatCheckBox chk_friend_row;

        public MyViewHolder(View itemView) {
            super(itemView);
            chk_friend_row = itemView.findViewById(R.id.chk_friend_row);
        }
    }
}