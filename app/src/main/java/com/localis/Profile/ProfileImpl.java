package com.localis.Profile;

import org.json.JSONObject;

public class ProfileImpl {

    public interface GetBasicUserInfo {
        void onBasicUserInfoReceived(JSONObject response);

        void onBasicUserInfoFailed(String responseMessage);
    }

}
