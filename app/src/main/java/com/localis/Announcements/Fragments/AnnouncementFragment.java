package com.localis.Announcements.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.localis.Announcements.Adapters.AnnouncementAdapter;
import com.localis.Announcements.POJOs.AnnounceObject;
import com.localis.R;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;
import java.util.List;

public class AnnouncementFragment extends SmartFragment {
    RecyclerView rv_announcement;
    AnnouncementAdapter mAnnouncementAdapter;
    LinearLayoutManager linearLayoutManager;
    private List<AnnounceObject> announceObjects = new ArrayList<>();

    @Override
    public int setLayoutId() {
        return R.layout.fragment_group_announce;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public View initComponents(View currentView) {
        rv_announcement = currentView.findViewById(R.id.rv_announcement);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        rv_announcement.setLayoutManager(linearLayoutManager);
        mAnnouncementAdapter = new AnnouncementAdapter(getActivity(), announceObjects);
        rv_announcement.setAdapter(mAnnouncementAdapter);

        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }


    @Override
    public void setActionListeners(View currentView) {

    }
}
