package com.localis.Announcements.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.CommentAdapter;
import com.localis.AA_POJOs.ImageObject;
import com.localis.R;

import java.util.ArrayList;

public class AnnouncementDetailsActivity extends MasterActivity {

    CommentAdapter pollCommentAdapter;
    RecyclerView timeline_poll_rv;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }


    @Override
    public int getLayoutID() {
        return R.layout.activity_annoucement_details;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.announcement_details);
        timeline_poll_rv = findViewById(R.id.rv_comments_discussion);
        timeline_poll_rv.setLayoutManager(new LinearLayoutManager(AnnouncementDetailsActivity.this));
        pollCommentAdapter = new CommentAdapter(new ArrayList<ImageObject>(),AnnouncementDetailsActivity.this);
        timeline_poll_rv.setAdapter(pollCommentAdapter);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
