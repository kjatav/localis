package com.localis.Announcements.POJOs;

import java.io.Serializable;

public class AnnounceObject implements Serializable {
    private int id;
    private String subjectName;


    public AnnounceObject(int id, String friendName) {
        this.id = id;
        this.subjectName = friendName;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }


}