package com.localis.Announcements.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localis.Announcements.Activities.AnnouncementDetailsActivity;
import com.localis.Announcements.POJOs.AnnounceObject;
import com.localis.R;

import java.util.List;

public class AnnouncementAdapter extends RecyclerView.Adapter<AnnouncementAdapter.MyViewHolder> {
    private List<AnnounceObject> announce_objects;
    private Context context;


    public AnnouncementAdapter(Context context, List<AnnounceObject> announceObjects) {
        this.announce_objects = announceObjects;
        this.context = context;
    }

    @Override
    public AnnouncementAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_announcement, parent, false);
        AnnouncementAdapter.MyViewHolder vh = new AnnouncementAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(AnnouncementAdapter.MyViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, AnnouncementDetailsActivity.class));
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public MyViewHolder(View itemView) {
            super(itemView);

        }
    }


}
