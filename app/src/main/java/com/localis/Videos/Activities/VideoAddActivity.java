package com.localis.Videos.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.R;
import com.smart.customViews.SmartButton;
import com.smart.customViews.SmartEditText;

import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class VideoAddActivity extends MasterActivity {

    private SmartEditText categoryVideoAddEt;
    private SmartEditText titleVideoAddEt;
    private SmartEditText descVideoAddEt;
    private SmartEditText linkVideoAddEt;
    private SmartEditText peopleVideoAddEt;
    private SmartEditText tagsVideoAddEt;
    private SmartEditText locationVideoAddEt;
    private SmartButton saveVideoAddEt;
    private SmartButton cancelVideoAddEt;

    private static int LOCATION_CODE = 901;

    private Double locationLatitude;
    private Double locationLongitude;

    String VideoCat[] = {"General", "Music", "Sports", "News", "Gaming", "Movies", "Documentary", "Fashion", "Travel", "Technology"};


    @Override
    public int getLayoutID() {
        return R.layout.activity_video_add;
    }


    @Override
    public int getDrawerLayoutID() {
        return 0;
    }


    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.add_new_video);

        categoryVideoAddEt = findViewById(R.id.category_video_add_et);
        titleVideoAddEt = findViewById(R.id.title_video_add_et);
        descVideoAddEt = findViewById(R.id.desc_video_add_et);
        linkVideoAddEt = findViewById(R.id.link_video_add_et);
        peopleVideoAddEt = findViewById(R.id.people_video_add_et);
        tagsVideoAddEt = findViewById(R.id.tags_video_add_et);
        locationVideoAddEt = findViewById(R.id.location_video_add_et);
        saveVideoAddEt = findViewById(R.id.save_video_add_et);
        cancelVideoAddEt = findViewById(R.id.cancel_video_add_et);
    }


    @Override
    public void prepareViews() {
        super.prepareViews();
        ArrayList<String> categories = new ArrayList<String>();
        categories.add("Select category");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Travel");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    }


    @Override
    public void setActionListeners() {
        super.setActionListeners();
        categoryVideoAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmartUtils.GetListDialog(VideoAddActivity.this, VideoCat, getString(R.string.select_categroy_video), false, new AlertMagneticClass.AlertMagneticList() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        categoryVideoAddEt.setText(VideoCat[id]);
                    }
                });
            }
        });
        locationVideoAddEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(VideoAddActivity.this, SelectAddress.class);
                if (locationLatitude != null && locationLongitude != null) {
                    i.putExtra(LATITUDE, String.valueOf(locationLatitude));
                    i.putExtra(LONGITUDE, String.valueOf(locationLongitude));
                }
                startActivityForResult(i, LOCATION_CODE);
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            ArrayList<HashMap<String, String>> selected_Address;
            if (requestCode == LOCATION_CODE) {
                selected_Address = (ArrayList<HashMap<String, String>>) data.getSerializableExtra(ADDRESS);
                HashMap<String, String> hashMapAddress = selected_Address.get(0);
                locationLatitude = Double.parseDouble(hashMapAddress.get(LATITUDE));
                locationLongitude = Double.parseDouble(hashMapAddress.get(LONGITUDE));
                Log.d("@@Address_in_video", hashMapAddress.toString());
                locationVideoAddEt.setText(hashMapAddress.get(ADDRESS));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
