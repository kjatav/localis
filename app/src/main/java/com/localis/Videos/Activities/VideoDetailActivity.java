package com.localis.Videos.Activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.CommentAdapter;
import com.localis.AA_POJOs.ImageObject;
import com.localis.R;
import com.smart.customViews.RoundedImageView;
import com.smart.customViews.SmartEditText;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;

public class VideoDetailActivity extends MasterActivity {
    private SmartTextView descVideoDetailTv;
    private ImageView bannerVideoDetailIv;
    private SmartTextView timeVideoDetailTv;
    private ImageView playVideoDetailIv;
    private SmartTextView nameVideoDetailsTv;
    private SmartTextView categoryVideoDetailsTv;
    private SmartTextView viewsVideoDetailsTv;
    private SmartTextView likesVideoDetailsTv;
    private SmartTextView commentsVideoDetailsTv;
    private RecyclerView commentsVideoDetailsRv;

    private RoundedImageView userCommentIv;
    private SmartEditText commentEt;
    private ImageView sendCommentIv;


    private CommentAdapter commentAdapter;
    private ArrayList<ImageObject> image_objects = new ArrayList<>();

    @Override
    public int getLayoutID() {
        return R.layout.activity_video_detail;
    }

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.video_details);

        descVideoDetailTv = findViewById(R.id.desc_video_detail_tv);
        bannerVideoDetailIv = findViewById(R.id.banner_video_detail_iv);
        timeVideoDetailTv = findViewById(R.id.time_video_detail_tv);
        playVideoDetailIv = findViewById(R.id.play_video_detail_iv);
        nameVideoDetailsTv = findViewById(R.id.name_video_details_tv);
        categoryVideoDetailsTv = findViewById(R.id.category_video_details_tv);
        viewsVideoDetailsTv = findViewById(R.id.views_video_details_tv);
        likesVideoDetailsTv = findViewById(R.id.likes_video_details_tv);
        commentsVideoDetailsTv = findViewById(R.id.comments_video_details_tv);
        commentsVideoDetailsRv = findViewById(R.id.comments_video_details_rv);

        userCommentIv = findViewById(R.id.user_comment_iv);
        commentEt = findViewById(R.id.comment_et);
        sendCommentIv = findViewById(R.id.send_comment_iv);


        commentsVideoDetailsRv.setLayoutManager(new LinearLayoutManager(this));
        commentAdapter = new CommentAdapter(image_objects, this);
        commentsVideoDetailsRv.setAdapter(commentAdapter);

    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
