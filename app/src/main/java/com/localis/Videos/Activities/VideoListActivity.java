package com.localis.Videos.Activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.localis.AA_Actvities.MasterActivity;
import com.localis.AA_Adapter.ViewPagerAdapter;
import com.localis.R;
import com.localis.Videos.Fragments.VideoListFragment;

public class VideoListActivity extends MasterActivity {

    private FloatingActionButton addVideoBtn;

    @Override
    public int getLayoutID() {
        return R.layout.activity_aa_pager;
    }


    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar(R.string.videos);

        TabLayout videoTabs = findViewById(R.id.details_tab);
        ViewPager videoVp = findViewById(R.id.details_vp);

        addVideoBtn = findViewById(R.id.details_add_btn);
        videoTabs.setTabMode(TabLayout.MODE_FIXED);

        setupViewPager(videoVp);
        videoTabs.setupWithViewPager(videoVp);
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        addVideoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VideoListActivity.this, VideoAddActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new VideoListFragment(), "All Videos");
        viewPagerAdapter.addFragment(new VideoListFragment(), "Featured Videos");
        viewPagerAdapter.addFragment(new VideoListFragment(), "My Videos");
        viewPager.setAdapter(viewPagerAdapter);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_menu_top));
    }
}
