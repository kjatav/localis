package com.localis.Videos.Fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.localis.AA_POJOs.ImageObject;
import com.localis.R;
import com.localis.Videos.Adapters.VideoItemAdapter;
import com.smart.customViews.SmartTextView;
import com.smart.framework.AlertMagneticClass;
import com.smart.framework.SmartRecyclerView;
import com.smart.framework.SmartFragment;

import java.util.ArrayList;

import static com.smart.framework.SmartUtils.showSortingPopup;

/**
 * Created by pct1 on 19/2/18.
 */

public class VideoListFragment extends SmartFragment {

    private VideoItemAdapter videoItemAdapter;

    private ArrayList<ImageObject> imageObjects = new ArrayList<>();

    private SmartTextView categoryVideoTv;
    private SmartTextView sortingTv;
    private SmartRecyclerView videoListRv;


    @Override
    public int setLayoutId() {
        return R.layout.fragment_video_list;
    }


    @Override
    public View setLayoutView() {
        return null;
    }


    @Override
    public View initComponents(View currentView) {
        categoryVideoTv = currentView.findViewById(R.id.category_video_tv);
        sortingTv = currentView.findViewById(R.id.sorting_tv);
        videoListRv = currentView.findViewById(R.id.video_list_rv);

        imageObjects.add(new ImageObject(""));
        imageObjects.add(new ImageObject(""));
        imageObjects.add(new ImageObject(""));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        videoListRv.setLayoutManager(linearLayoutManager);
        videoItemAdapter = new VideoItemAdapter(getActivity(), imageObjects);
        videoListRv.setAdapter(videoItemAdapter);
        videoListRv.setEmptyView(currentView.findViewById(R.id.layout_no_data));
        setHasOptionsMenu(true);

        return currentView;
    }

    @Override
    public void prepareViews(View currentView) {

    }


    @Override
    public void setActionListeners(View currentView) {
        sortingTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSortingPopup(getActivity(), sortingTv, getResources().getStringArray(R.array.sort_by_date), new AlertMagneticClass.PopUpInterface() {
                    @Override
                    public void onSetPopUp(int selectedOptionPosition) {
                        sortingTv.setText(getResources().getStringArray(R.array.sort_by_date)[selectedOptionPosition]);
                    }
                });
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        categoryVideoTv.setText(item.getTitle());
        return super.onOptionsItemSelected(item);
    }


    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.video_menu, menu);
    }
}
