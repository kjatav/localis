package com.localis.Videos.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localis.AA_POJOs.ImageObject;
import com.localis.Videos.Activities.VideoDetailActivity;
import com.localis.R;
import com.smart.customViews.SmartTextView;

import java.util.ArrayList;


public class VideoItemAdapter extends RecyclerView.Adapter<VideoItemAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ImageObject> imageObjects;


    public VideoItemAdapter(Context context, ArrayList<ImageObject> imageObjects) {
        this.context = context;
        this.imageObjects = imageObjects;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_video_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, VideoDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return imageObjects.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private SmartTextView descVideoTv;

        private ImageView bannerVideoIv;

        private SmartTextView timeVideoTv;

        private ImageView playVideoIv;

        private SmartTextView nameVideoTv;
        private SmartTextView categoryVideoTv;
        private SmartTextView viewsVideoTv;
        private SmartTextView likesVideoTv;
        private SmartTextView commentsVideoTv;


        public ViewHolder(View itemView) {
            super(itemView);
            descVideoTv = itemView.findViewById(R.id.desc_video_tv);
            bannerVideoIv = itemView.findViewById(R.id.banner_video_iv);
            timeVideoTv = itemView.findViewById(R.id.time_video_tv);
            playVideoIv = itemView.findViewById(R.id.play_video_iv);
            nameVideoTv = itemView.findViewById(R.id.name_video_tv);
            categoryVideoTv = itemView.findViewById(R.id.category_video_tv);
            viewsVideoTv = itemView.findViewById(R.id.views_video_tv);
            likesVideoTv = itemView.findViewById(R.id.likes_video_tv);
            commentsVideoTv = itemView.findViewById(R.id.comments_video_tv);
        }
    }
}
